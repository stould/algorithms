#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 103
#define ll long long int
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define MOD 1000000007LL
  
using namespace std;

vector<pair<int, int> > p(2, pair<int, int>());

int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	cin >> p[0].first >> p[0].second >> p[1].first >> p[1].second;
	sort(p.begin(), p.end());
	int ans = 0;
	if(p[1].first < p[0].second){
		ans = p[1].second - p[0].first;
		ans = max(p[0].second - p[0].first, ans);
		ans = max(p[1].second - p[1].first, ans);
	}else{
		ans = (p[0].second - p[0].first) + (p[1].second - p[1].first);
	}
	cout << ans << endl;
    return 0;
}
