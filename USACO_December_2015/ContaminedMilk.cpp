#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 103
#define ll long long int
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define MOD 1000000007LL
  
using namespace std;

int N,M,D,S;

int person[MAXN], milk[MAXN], tempo[MAXN];

struct Drink{
	int p,t;
	Drink(){}
	Drink(int A, int B): p(A), t(B){}
	bool operator < (const Drink &o) const{
		if(t == o.t){
			return p < o.p;
		}
		return t < o.t;
	}
};

int sickP[MAXN], sickTime[MAXN];
bool isSick[MAXN], vis[MAXN];
vector<vector<Drink> > group(MAXN);
vector<Drink> sickness;
int last[MAXN];

int main(){
	//freopen("badmilk.in","r",stdin);
	//	freopen("badmilk.out","w",stdout);
	memset(last, 63,sizeof(last));
	cin >> N >> M >> D >> S;
	for(int i = 0; i < D; i++){
		cin >> person[i] >> milk[i] >> tempo[i];
		group[milk[i]].push_back(Drink(person[i],  tempo[i]));
	}
	for(int i = 1; i <= 50; i++){
		sort(group[i].begin(), group[i].end());
	}
	for(int i = 0; i < S; i++){
		cin >> sickP[i] >> sickTime[i];
		sickness.push_back(Drink(sickP[i], sickTime[i]));
		isSick[sickP[i]] = 1;
	}
	sort(sickness.begin(), sickness.end());
	int ans = 0;
	for(int i = 1; i <= 50; i++){
		int idx = 0;
		bool can = 1;
		int cnt = 0;
		memset(vis,0,sizeof(vis));
		if(group[i].size()){
			cout << "Group " << i << ":" << endl;

			memset(vis,0,sizeof(vis));
			for(int j = 0; idx < sickness.size() && j < group[i].size(); j++){
				if(vis[group[i][j].p] && group[i][j].t > last[group[i][j].p]) can = 0;
				if(vis[group[i][j].p] || !isSick[group[i][j].p]) continue;
				
				//cout << idx << " " << now << endl;
				//pra pessoa i:
				int pos = 0;
				int T = INF;
				//cout << "---------" << endl;
				while(1){
					if(pos >= group[i].size()){
						break;
					}
					cout << sickness[idx].p << " " << group[i][pos].p << endl;
					if(sickness[idx].p == group[i][pos].p && !vis[group[i][pos].p]){
						
						if(group[i][pos].t < sickness[idx].t){
							last[group[i][pos].p] = min(sickness[idx].t, last[group[i][pos].p]);
							vis[group[i][pos].p] = 1;
							T = 0;

						}
					}
					pos++;
				}
				//se deu certo:
				if(T != INF){
					idx++;
				}else{
					can = 0;
					break;
				}
			}
			//cout << idx << " " << now << endl;
			if(can){
				memset(vis,0,sizeof(vis));
				int S = 0;
				for(int j = 0; j < group[i].size(); j++){
					if(!vis[group[i][j].p]){
						if(isSick[group[i][j].p]) cnt++;
						vis[group[i][j].p] = 1;
						S++;
					}
				}
				if(cnt <= sickness.size()){
					cout << " group is ok" << endl;
					ans = max(ans, S);
				}else{
					cout << "X group is not ok" << endl;
				}
			}else{
				cout << "Y group is not ok" << endl;
			}
		}
	}
	cout << ans << endl;
    return 0;
}
