#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 103
#define ll long long int
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define MOD 1000000007LL
  
using namespace std;


int n,m, len, speed;

int go[MAXN], maxVel[MAXN];

int main(){
	freopen("speeding.in","r",stdin);
	freopen("speeding.out","w",stdout);
	int ans = 0;
	cin >> n >> m;
	int total = 0;
	for(int i = 0; i < n; i++){
		cin >> len >> speed;
		for(int j = total; j <= total + len; j++){
			maxVel[j] = speed;
		}
		total += len;
	}
	total = 0;
	for(int i = 0; i < m; i++){
		cin >> len >> speed;
		for(int j = total; j <= total + len; j++){
			go[j] = speed;
		}
		total += len;
	}
	for(int i = 0; i <= 100; i++){
		if(go[i] > maxVel[i]){
			ans = max(go[i] - maxVel[i], ans);
		}
	}
	cout << ans << endl;
    return 0;
}
