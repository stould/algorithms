#include <bits/stdc++.h>
#define MAXN 2000001
using namespace std;

int fail[MAXN];

void failFunc(string &s){
    int n = s.size();
    fail[0] = -1;
    for(int i = 0; i < n; i++){
        int node = fail[i];
        while(node >= 0 && s[node] != s[i]){
            node = fail[node];
        }
        fail[i+1] = node+1;
    }

}

int match(string &t, string &p){
    int i = 0, j = 0, cnt = 0;
    while(i < (int) t.size()){
        while(j >= 0 && p[j] != t[i]){
            j = fail[j];
        }
        i++; j++;
        if(j == (int) p.size()){
            cnt++;
        }
    }
    return cnt;
}

int n;
string p,t;

int main(){
    bool can = 0;
    while(cin >> n){
        if(can) cout << endl;
        can = 1;
        cin >> p >> t;
        failFunc(p);
        cout << "number of matchings = " << match(t,p) << endl;
    }
    return 0;
}
