#include <bits/stdc++.h>
#define MAXN 200
#define MAXQ 200
#define ALPHABET 2
#define INF 0x3f3f3f3f
#define ll long long

using namespace std;
 
int INDEX, trie[MAXN*MAXQ][ALPHABET], fail[MAXN*MAXQ];
int  term[MAXN*MAXQ];

void add(string &s){
    int root = 0;
    for(int i = 0; i < (int) s.size(); i++){
        int child = s[i] - '0';
        if(trie[root][child] == -1){
            trie[root][child] = INDEX++;
        }
        root = trie[root][child];
    }
    term[root] = 1;
}

void ahoCorasick(int root = 0){
    queue<int> q;
    for(int child = 0; child < ALPHABET; child++){
        if(trie[root][child] == -1){
            trie[root][child] = 0;
        }else{
            q.push(trie[root][child]);
            fail[trie[root][child]] = root;
        }
    }
    while(!q.empty()){
        int parent = q.front(); q.pop();
        for(int child = 0; child < ALPHABET; child++){
            if(trie[parent][child] != -1){
                int node = fail[parent];
                while(trie[node][child] == -1){
                    node = fail[node];
                }
                fail[trie[parent][child]] = trie[node][child];
                term[trie[parent][child]] |= term[trie[node][child]];
                q.push(trie[parent][child]);
            }
        }
    }
}

string toBin(ll val){
    if(val == 0) return "0";
    string ans = "";
    while(val > 0){
        ans = string(1, char('0' + val % 2ll)) + ans;
        val /= 2ll;
    }
    return ans;
}

string seq, num;
ll N, K, pd[MAXN][MAXN*MAXQ][2];
bool vis[MAXN][MAXN*MAXQ][2];

int T, Q;

int nextState(int root, int i){
    while(trie[root][i] == -1){
        root = fail[root];
    }
    return trie[root][i];
}

ll func(int id, int root, bool pref){
    if(term[root]){
        return 0;
    }else if(id == (int) num.size()){
        return 1ll;
    }else{
        ll ans = 0;
        if(!pref && pd[id][root][pref] != -1){
            return pd[id][root][pref];
        }
        ans = 0;
        for(int i = 0; i <= 1; ++i){
            if(!pref){
                ans += func(id+1, nextState(root, i), 0);
            }else{
                if(i <= (num[id] - '0'))
                    ans += func(id+1, nextState(root, i), pref && (i == (num[id] - '0')));
            }
        }
        if(!pref) {
            pd[id][root][pref] = ans;
        }
        return ans;
    }
}

int main(){
    cin >> T;
    while(T--){
        cin >> N >> K >> Q;
        memset(trie, -1, sizeof(trie));
        memset(term, 0, sizeof(term));
        memset(fail, -1, sizeof(fail));
        memset(pd, -1, sizeof(pd));
        INDEX = 1;
        for(int i = 0; i < Q; i++){
            cin >> seq;
            add(seq);
        }
        ahoCorasick();
        ll lo = 0, hi = (1ll << N) -1, ans = -1;
        while(lo <= hi){
            ll mid = lo + (hi - lo) / 2ll;
            num = toBin(mid);
            while((int)num.size() < N){
                num = "0" + num;
            }

            ll cnt = func(0, 0, 1);
            if(cnt >= K){
                hi = mid - 1;
                ans = mid;
            }else if(cnt < K){
                lo = mid + 1;
            }
        }
        cout << ans << endl;
    }
    return 0;
}
