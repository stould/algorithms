#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <math.h>
#include <string.h>
#include <stack>
#include <map>
#include <set>
#define ll long long
#define INF 0x3f3f3f3f
#define MAXN 1003

using namespace std;

string str;
int K;
int dp[MAXN][MAXN];

int t;
int main(void){
	cin >> t;
	while(t--){
		cin >> str >> K;
		int n = str.size();
		memset(dp,0,sizeof(dp));
		int ans = 1;
		for(int i = 0; i < n-1; i++){
			if(str[i] != str[i+1]){
				dp[i][i+1] = 1;
			}
			if(dp[i][i+1] <= K){
				ans = 2;
			}
		}
		for(int i = 3; i <= n; i++){
			for(int j = 0; j + i - 1 < n; j++){
				int L = j, R = j + i - 1;
				if(str[L] == str[R]){
					dp[L][R] = dp[L+1][R-1];
				}else{
					dp[L][R] = dp[L+1][R-1] + 1;
				}
				if(dp[L][R] <= K){
					ans = max(ans, R-L+1);
				}
			}
		}
		cout << ans << endl;
	}
	return 0;
}
