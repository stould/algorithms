
#include <bits/stdc++.h>
#define MX 34
using namespace std;

int l, r, c;

int dx[] = { 1, -1, 0, 0, 0, 0 }, dy[] = { 0, 0, 1, -1, 0, 0 }, dz[] = { 0, 0, 0, 0, 1, -1 };
char g[MX][MX][MX];
bool vis[MX][MX][MX];

struct tile {
	int x, y, z, mv;

	tile(int _x, int _y, int _z, int _mv) {
		x = _x, y = _y, z = _z;
		mv = _mv;
	}
};

tile s(0, 0, 0, 0), e(0, 0, 0, 0);

bool test(int x, int y, int z) {
	return (x >= 0 && x < r && y >= 0 && y < c && z >= 0 && z < l);
}

int bfs() {
	queue<tile> q;
	q.push(s);
	vis[s.z][s.x][s.y] = 1;
	while (!q.empty()) {
		tile ac = q.front(); q.pop();
		if (ac.x == e.x && ac.y == e.y && ac.z == e.z)
			return ac.mv;
		for (int i = 0; i < 6; i++) {
			int xx = ac.x + dx[i], yy = ac.y + dy[i], zz = ac.z + dz[i];
			if (test(xx, yy, zz) && !vis[zz][xx][yy] && g[zz][xx][yy] != '#'){
				vis[zz][xx][yy] = 1;
				q.push(tile(xx, yy, zz, ac.mv + 1));
			}
		}
	}
	return -1;
}

int main(void) {
	while (scanf("%d%d%d", &l, &r, &c) && l && r && c) {
		memset(vis,0,sizeof(vis));
		for (int i = 0; i < l; i++) {
			for (int j = 0; j < r; j++) {
				for (int k = 0; k < c; k++) {
					scanf(" %c",&g[i][j][k]);
					if (g[i][j][k] == 'S') {
						s.z = i, s.x = j, s.y = k;
					}else if (g[i][j][k] == 'E') {
						e.z = i, e.x = j, e.y = k;
					}
				}
			}
		}
		int ans = bfs();

		if (ans != -1)
			printf("Escaped in %d minute(s).\n", ans);
		else
			printf("Trapped!\n");
	}

	return 0;
}
