#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXM 1026
#define MAXN 20001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int T, d, n, line, col, cost;

int freq[MAXM][MAXM];

int main(void){
	scanf("%d", &T);
	while(T--){
		scanf("%d", &d);
		scanf("%d", &n);
		memset(freq,0,sizeof(freq));
		for(int i = 0; i < n; i++){
			scanf("%d%d%d", &line, &col, &cost);
			for(int dx = -d; dx <= d; dx++){
				for(int dy = -d; dy <= d; dy++){
					if(dx + line >= 0 && dx + line <= 1024 && dy + col >= 0 && dy + col <= 1024){
						freq[dx+line][dy+col] += cost;
					}
				}
			}
		}
		int ans = 0, ansl = 0, ansc = 0;
		for(int i = 0; i < MAXM; i++){
			for(int j = 0; j < MAXM; j++){
				if(freq[i][j] > ans){
					ans = freq[i][j];
					ansl = i;
					ansc = j;
				}
			}
		}
		printf("%d %d %d\n", ansl, ansc, ans);
   }
	return 0;
}
