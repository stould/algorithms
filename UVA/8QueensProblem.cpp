#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 10005
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int t, row[8], a, b, tm;

bool ok(int l, int c){
	for(int i = 0; i < c; i++){
		if(row[i] == l) return 0;
		int dy = abs(row[i] - l);
		int dx = abs(i - c);
		if(dx == dy) return 0;
	}
	return 1;
}

void track(int c){
	if(c == 8 && row[b] == a){
		printf("%2d      ",tm++);
		printf("%d", 1+row[0]);
		for(int i = 1; i < 8; i++){
			printf(" %d", 1+row[i]);
		}
		printf("\n");
	}else{
		for(int i = 0; i < 8; i++){
			if(ok(i, c)){
				row[c] = i;
				track(c+1);
				row[c] = -1;
			}
		}
	}
}

int main(void){
	scanf("%d", &t);
	while(t--){
		for(int i = 0; i < 8; i++) row[i] = -1;
		tm = 1;
		scanf("%d%d", &a, &b);
		a--;b--;
		printf("SOLN       COLUMN\n");
		printf(" #      1 2 3 4 5 6 7 8\n\n");
		track(0);
		if(t) printf("\n");
	}
}
