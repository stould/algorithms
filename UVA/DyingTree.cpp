#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 303
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int id[MAXN], height[MAXN];

int root(int node){
	if(id[node] == node){
		return node;
	}else{
		return id[node] = root(id[node]);
	}
}

void join(int u, int v){
	int ru = root(u);
	int rv = root(v);
	if(ru != rv){
		if(height[ru] > height[rv]){
			height[ru] += height[rv];
			id[rv] = ru;
		}else{
			height[rv] += height[ru];
			id[ru] = rv;
		}
	}
}

ll n,m,K,d,b,T;
vector<vector<pair<ll, ll> > > trees(MAXN);

vector<pair<ll, ll > > doctors;


void init(){
	for(int i = 0; i < MAXN; i++){
		id[i] = i;
		height[i] = 1;
		trees[i].clear();
	}
	doctors.clear();
}

ll distance(ll xa, ll ya, ll xb, ll yb){
	return (xa - xb) * (xa - xb) + (ya - yb) * (ya - yb);
}

int main(void){
	cin >> T;
	while(T--){
		cin >> n >> m >> K >> d;
		init();
		for(int j = 0; j < m; j++){
			pair<int, int > doc;
			cin >> doc.first >> doc.second;
			doctors.push_back(doc);
		}
		for(int i = 0; i < n; i++){
			cin >> b;
			for(int j = 0; j < b; j++){
				pair<int,int> tree;
				cin >> tree.first >> tree.second;
				trees[i].push_back(tree);
			}
		}
		for(int i = 0; i < n; i++){
			for(int j = i+1; j < n; j++) {
				bool united = 0;
				for(int k = 0; k < trees[i].size() && !united; k++){
					for(int l = 0; l < trees[j].size() && !united; l++){
						ll dist = distance(trees[i][k].first, trees[i][k].second, trees[j][l].first, trees[j][l].second);
						if(dist <= K*K){
							united = 1;
							join(i,j);
						}
					}
				}
			}
		}
		bool ans = 0;
		for(int i = 0; i < doctors.size() && !ans; i++){
			for(int j = 0; j < n && !ans; j++){
				for(int k = 0; k < trees[j].size() && !ans; k++){
					if(distance(doctors[i].first, doctors[i].second, trees[j][k].first, trees[j][k].second) <= d*d && root(0) == root(j)){
						ans = 1;
					}
				}
			}
		}
		if(ans){
			cout << "Tree can be saved :)\n";
		}else{
			cout << "Tree can’t be saved :(\n";
		}
	}
	return 0;
}
