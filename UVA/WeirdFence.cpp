#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 208
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int n,k;

int vis[MAXN], match[MAXN], can[MAXN];

vector<pair<int,int> > blue, red;

vector<vector<int> > g(MAXN);

bool aug(int u){
	if(vis[u]) return 0;
	vis[u] = 1;
	for(int i = 0; i < g[u].size(); i++){
		int v = g[u][i];
		if(match[v] == -1 || aug(match[v])){
			match[v] = u;
			return 1;
		}
	}
	return 0;
}

int flow(){
	memset(match, -1,sizeof(match));
	int mcbm = 0;
	for(int i = 0; i < n; i++){
		memset(vis,0,sizeof(vis));
		if(can[i]){
			mcbm += aug(i);
		}
	}
	return mcbm;
}

void mountGraph(int maxDist){
	for(int i = 0; i < MAXN; i++){
		g[i].clear();
		can[i] = 0;
	}
	for(int i = 0; i < blue.size(); i++){
		for(int j = 0; j < red.size(); j++){
			int dist = (blue[i].first - red[j].first)*(blue[i].first - red[j].first) + (blue[i].second - red[j].second)*(blue[i].second - red[j].second);
			if(dist <= (double) maxDist*maxDist){
				g[i].push_back(j+n+1);
				g[j+n+1].push_back(i);
				can[i] = 1;
			}
		}
	}
}

int t, px, py;

string type;

int main(void){
	cin >> t;
	while(t--){
		blue.clear();
		red.clear();
		cin >> n >> k;
		for(int i = 0; i < n; i++){
			cin >> px >> py >> type;
			pair<int,int> p = make_pair(px,py);
			if(type == "blue"){
				blue.push_back(p);
			}else{
				red.push_back(p);
			}
		}
		int lo = 0, hi = 10000000, ans = INF;
		while(lo <= hi){
			int mid = (lo+hi) >> 1;
			mountGraph(mid);
			int val = flow();
			if(val >= k){
				ans = min(ans,mid);
				hi = mid-1;
			}else{
				lo = mid+1;
			}
		}
		if(ans == INF){
			cout << "Impossible" << endl;
		}else{
			cout << ans << endl;
		}
	}
	return 0;
}
