#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 1005
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int t, n, m, g[1001][1001], pos[1001][1001], q, query[MAXN*MAXN];
int id[MAXN*MAXN];
int height[MAXN*MAXN];
map<int,int> answer;
bool isJoint[MAXN*MAXN];

set<int> roots;

int getPos(int L, int C){
	return m * L + C;
}

void clean(int n){
	for(int i = 0; i < n+1; i++){
		id[i] = i;
		height[i] = 1;

	}
}
 
int root(int x){
	if(x == id[x]){
		return x;
	}else{
		return id[x] = root(id[x]);
	}
}
 
bool unite(int a, int b){
	int roota = root(a);
	int rootb = root(b);
	if(roota != rootb){
		if(height[roota] >= height[rootb]){
			if(roots.find(rootb) != roots.end())
				roots.erase(rootb);
			roots.insert(roota);
			id[rootb] = roota;
			height[roota] += height[rootb];
		}else{
			if(roots.find(roota) != roots.end())
				roots.erase(roota);
			roots.insert(rootb);
			id[roota] = rootb;
			height[b] += height[roota];
		}

		return 1;
	}
	return 0;
}

void rootsOk(){
	for(set<int>::iterator i = roots.begin(); i != roots.end(); i++)
		cout << *i << " ";
	cout << endl;
}

struct p{
	int l,c,val;
	p(int a_,int b_, int c_): l(a_), c(b_), val(c_){}
	p(){}
	bool operator < (const p &o) const{
		return val < o.val;
	}
};

int dl[4] = {-1,0,1,0};
int dc[4] = {0,1,0,-1};

int main(void){
	scanf("%d", &t);
	while(t--){
		scanf("%d%d", &n, &m);
		clean(n*m);
		roots.clear();
		answer.clear();
		memset(isJoint,0,sizeof isJoint);
		vector<p> P;
		for(int i = 0; i < n; i++){
			for(int j = 0; j < m; j++){
				scanf("%d", &g[i][j]);
				pos[i][j] = getPos(i,j);
				P.push_back(p(i,j,g[i][j]));
			}
		}
		sort(P.begin(), P.end());
		scanf("%d", &q);
		for(int j = 0; j < q; j++){
			scanf("%d", &query[j]);
		}
		int idx = P.size()-1;
		answer[0] = 0;
		for(int i = q-1; i >= 0; i--){
			if(answer.find(query[i]) != answer.end()){
				continue;
			}
			while(idx >= 0 && P[idx].val > query[i]){
				rootsOk();
				int Line = P[idx].l;
				int Col = P[idx].c;
				bool alone = 1;
				for(int j = 0; j < 4; j++){
					int nLine = Line + dl[j];
					int nCol = Col + dc[j];

					if(nLine >= 0 && nCol >= 0 && nLine < n && nCol < m && g[nLine][nCol] >= g[Line][Col]){
						if(unite(pos[Line][Col], pos[nLine][nCol])){

						}
							alone = 0;
					}

				}
				if(alone){
					roots.insert(pos[Line][Col]);
				}
				idx--;
			}
			answer[query[i]] = roots.size();
		}
		printf("%d",answer[query[0]]);
		for(int i = 1; i < q; i++){
			printf(" %d",answer[query[i]]);
		}
		puts("");
	}
	return 0;
}
