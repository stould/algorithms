#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 10005
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int cnt[8], n;

string ord[4] = {"st", "nd", "rd", "th"};
vector<int> ans;

void humble(int n) {
	ans.push_back(1);
	while(n--){
		int a = ans[cnt[2]] * 2;
		int b = ans[cnt[3]] * 3;
		int c = ans[cnt[5]] * 5;
		int d = ans[cnt[7]] * 7;
		int lab = std::min(a,b);
		int lcd = std::min(c,d);
		int lall = std::min(lab,lcd);
		ans.push_back(lall);
		if(a == lall) cnt[2] ++;
		if(b == lall) cnt[3] ++;
		if(c == lall) cnt[5] ++;
		if(d == lall) cnt[7] ++;
	}
}

int main(void){
	humble(5842);
	while(scanf("%d", &n) && n){
		printf("The %d", n);
		if(n%10==1 && (n/10)%10!=1) printf("st");
		else if(n%10==2 && (n/10)%10!=1) printf("nd");
		else if(n%10==3 && (n/10)%10!=1) printf("rd");
		else printf("th");
		printf(" humble number is %d.\n",ans[n-1]);
	}
	return 0;
}
