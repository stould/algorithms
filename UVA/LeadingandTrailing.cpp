#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
#define MAXN 100001
#define LOGVAL 18
#define MOD 1000
#define ll long long
 
using namespace std;

int T;
ll n, k;

ll fastPow(ll b, int e){
	if(e == 0) {
		return 1;
	}else{
		if((e & 1) != 0){
			return (b*fastPow(b, e-1) % MOD) % MOD;
		}else{
			return fastPow((b*b) % MOD, e >> 1);
		}
	}
}

string fix(int val){
	string ans = "";
	while(val > 0){
		ans = string(1, '0' + val % 10) + ans;
		val /= 10;
	}
	while(ans.size() < 3) ans = "0" + ans;
	return ans;
}

int main(){
	scanf("%d", &T);
	while(T--){
		scanf("%lld%lld", &n, &k);
		double lognk = (log(n) / log(10)) * (double) k; //log(n^k) = k * log(n)
		double frac = lognk - ((double) ((int)(lognk)));
		int left = (int) (100*pow(10, frac));
		cout << fix(left) << "..." << fix(fastPow(n, k)) << endl;
	}
    return 0;
}
