#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100005
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

struct line{
	int l,r;
	line(){};
	line(int _l, int _r): l(_l), r(_r) {}

	bool operator < (const line &o) const{
		if(l == o.l) return r < o.r;
		return l < o.l;
    }
};

int t,n,m,l,r;
bool vis[MAXN];
line seg[MAXN];

int main(void){
	//	scanf("%d", &t);
	//	while(t--){
		n = 0;
		scanf("%d", &m);
		while(1){
			scanf("%d%d", &l, &r);
			if(l == 0 && r == 0) break;
			if(l == r || l > m || r < 0) continue;
			vis[n] = 1;
			seg[n++] = line(l,r);
		}
		sort(seg,seg+n);
		int end = 0, ans = 0, idxL = -1,lastTmpIdx=-1;
		for(int i = 0; i < n; i++){
			if(end >= m){
				vis[i] = 0;
				continue;
			}
			if(seg[i].l > end){
				ans = -1;
				break;
			}
			if(seg[i].r <= end){
				vis[i] = 0;
				continue;
			}else{
				if(ans == 0){
					ans++;
					idxL = i;
					end = seg[i].r;
					idxL = i;
				}else if(ans == 1){
					if(seg[i].l <= 0 && seg[idxL].l <= 0 && seg[i].r > seg[idxL].r){//getting longer right
						vis[idxL] = 0;
						end = seg[i].r;
						idxL = i;
					}else if(seg[i].r > end){//need to extend
						ans++;
						end = seg[i].r;
						lastTmpIdx = i;
					}
				}else{
					//testar se o elemento anterior ficou obsoleto.
					if(seg[i].l <= seg[idxL].r && seg[i].r > seg[lastTmpIdx].r){
						vis[lastTmpIdx] = 0;
					}else{
						ans++;
					}
					while(idxL < lastTmpIdx && (seg[i].l > seg[idxL].r || !vis[idxL])) {
							idxL++;
					}
					while(lastTmpIdx < i && !vis[lastTmpIdx]){
						lastTmpIdx++;
					}
					while(lastTmpIdx < i && (idxL == lastTmpIdx || !vis[lastTmpIdx])){
						lastTmpIdx++;
					}
					end = seg[i].r;
				}
			}
		}
		if(end < m || ans == -1){
			printf("No solution\n");
		}else{
			printf("%d\n", ans);
			for(int i = 0; i < n; i++){
				if(vis[i]){
					printf("%d %d\n", seg[i].l, seg[i].r);
				}
			}
		}
		//		if(t==0)break;
		//		puts("");
		//	}
	return 0;
}
