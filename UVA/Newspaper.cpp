#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXM 1026
#define MAXN 20001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int T, N, M, val;
char c;
int table[500];

int main(void){
	cin >> T;
	cin.ignore();
	while(T--){
		scanf("%d\n",&N);
		memset(table,0,sizeof(table));
		for(int i = 0; i < N; i++){
			cin >> c >> val;
			cin.ignore();
			table[(int)c] = val;
		}
		cin >> M;
		cin.ignore();
		unsigned long long ans = 0;
		for(int i = 0; i < M; i++){
			unsigned long long tmp;
			while(cin >> tmp && tmp != '\n'){
				ans += table[(int)tmp];
			}
		}
		printf("%.2lf$\n", ans / 100.);
	}
	return 0;
}
