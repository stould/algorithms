#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 30003
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int T, N, P, val[21];

int main(void){
	scanf("%d", &T);
	while(T--){
		scanf("%d%d", &N, &P);
		for(int i = 0; i < P; i++){
			scanf("%d", &val[i]);
		}
		set<int> pa,pb;
		for(int mask = 0; mask < (1 << P/2); mask++){
			int S = 0;
			for(int i = 0; i < P/2 && S >= 0; i++){
				if((mask & (1 << i))){
					S += val[i];
				}
			}
			pa.insert(S);
		}
		for(int mask = (1 << (P/2)); mask < (1 << P); mask++){
			int S = 0;
			for(int i = P/2; i < P && S >= 0; i++){
				if((mask & (1 << i))){
					S += val[i];
				}
			}
			pb.insert(S);
		}
		bool can = 0;
		for(set<int>::iterator i = pa.begin(); i != pa.end() && !can; i++){
			if(pb.find(N - *i) != pb.end()){
				can = 1;
			}
		}
		printf("%s\n", can || (N == 0) ? "YES" : "NO");
	}
	return 0;
}
