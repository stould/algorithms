#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 30003
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int id[MAXN], height[MAXN];

int root(int node){
	if(id[node] == node){
		return node;
	}else{
		return id[node] = root(id[node]);
	}
}

void unite(int u, int v){
	int ru = root(u);
	int rv = root(v);
	if(ru != rv){
		if(height[ru] > height[rv]){
			height[ru] += height[rv];
			id[rv] = ru;
		}else{
			height[rv] += height[ru];
			id[ru] = rv;
		}
	}
}

int N,M,K,num;

int main(void){
	while(scanf("%d%d", &N, &M)){
		if(N == 0 && M == 0) break;
		for(int i = 0; i < N; i++){
			id[i] = i;
			height[i] = 1;
		}
		for(int i = 0; i < M; i++){
			scanf("%d", &K);
			int begin;
			scanf("%d", &begin);
			for(int j = 1; j < K; j++){
				scanf("%d", &num);
				unite(num, begin);
			}
		}
		int ans = 0;
		for(int i = 0; i < N; i++){
			if(root(0) == root(i)){
				ans++;
			}
		}
		printf("%d\n", ans);
	}
	return 0;
}
