#include <bits/stdc++.h>
#define MAXN 101

using namespace std;

int t,n,m;
char g[MAXN][MAXN];
int dp[MAXN][MAXN];
int dl[4] = {0,1,0,-1};
int dc[4] = {1,0,-1,0};

int f1(int line, int col){
	if(line == n-1 && col == m-1){
		return g[line][col] == '*';
	}else{
		int &ans = dp[line][col];
		if(ans == -1){
			ans = 0;
			for(int i = 0; i < 2; i++){
				int nextL = line+dl[i];
				int nextC = col+dc[i];
				if(nextL < n && nextC < m && g[nextL][nextC] != '#'){
					int val = (int)g[line][col] == '*';
					ans = max(ans, val + f1(nextL, nextC));
				}
			}
		}
		return ans;
	}
}

int f2(int line, int col){
	if(line == 0 && col == 0){
		return g[line][col] == '*';
	}else{
		int &ans = dp[line][col];
		if(ans == -1){
			ans = 0;
			for(int i = 2; i < 4; i++){
				int nextL = line+dl[i];
				int nextC = col+dc[i];
				if(nextL >= 0 && nextC >= 0 && g[nextL][nextC] != '#'){
					int val = (int)g[line][col] == '*';
					ans = max(ans, val + f2(nextL, nextC));
				}
			}
		}
		return ans;
	}
}

void path(int line, int col){
	g[line][col] = '.';
	if(line == n-1 && col == m-1){
		g[line][col] = '.';
	}else{
		int bestL = -1, bestC = -1, v = -1;
		for(int i = 0; i < 2; i++){
			int nextL = line+dl[i];
			int nextC = col+dc[i];
			if(nextL < n && nextC < m && g[nextL][nextC] != '#' && f1(nextL, nextC) > v){
				bestL = nextL;
				bestC = nextC;
				v = f1(nextL, nextC);
			}
		}
		if(v != -1){
			path(bestL, bestC);
		}
	}
}


int main(void) {
	scanf("%d", &t);
	while(t--){
		scanf("%d%d", &m, &n);
		for(int i = 0; i < n; i++){
			for(int j = 0; j < m; j++){
				scanf(" %c", &g[i][j]);
				cout << g[i][j] << " ";
			}
			cout << endl;
		}cout << endl;
		memset(dp,-1,sizeof(dp));
		int ans = f1(0,0);
		path(0,0);
		for(int i = 0; i < n; i++){
			for(int j = 0; j < m; j++){
				cout << g[i][j] << " ";
			}
			cout << endl;
		}
		for(int i = 0; i < n; i++){
			for(int j = 0; j < m; j++)
				cout << dp[i][j] << " ";
			cout << endl;
		}
		memset(dp,-1,sizeof(dp));
		printf("%d\n", ans + f2(n-1,m-1));
	}
	return 0;
}
