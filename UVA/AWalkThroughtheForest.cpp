#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 1003
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define mp make_pair
using namespace std;

int N, M, U, V, C, dist[MAXN], dp[MAXN];
bool vis[MAXN];
vector<vector<pair<int,int> > > G(MAXN);
vector<vector<int> > SG(MAXN);

struct Less{
	bool operator () (int a, int b){
		return dist[a] > dist[b];
	}
};

void dijkstra(int S){
	dist[S] = 0;
	priority_queue<int, vector<int>, Less> q;
	q.push(S);
	while(!q.empty()){
		int top = q.top(); q.pop();
		for(int i = 0; i < (int) G[top].size(); i++){
			int next = G[top][i].first;
			int cost = G[top][i].second;
			if(dist[top] + cost < dist[next]){
				SG[next].clear();
				SG[next].push_back(top);
				dist[next] = dist[top] + cost;
				q.push(next);
			}else if(dist[top] + cost == dist[next]){
				SG[next].push_back(top);
			}
		}
	}
}

int f(int v){
	if(v == 1){
		return 1;
	}else{
		int &ans = dp[v];
		if(ans == -1){
			ans = 0;
			for(int i = 0; i < (int) SG[v].size(); i++){
				ans += f(SG[v][i]);
			}
		}
		return ans;
	}
}

int main(void){
	while(scanf("%d%d", &N, &M) && N > 0 && M > 0){
		for(int i = 0; i < MAXN; i++){
			G[i].clear();
			SG[i].clear();
			vis[i] = 0;
			dist[i] = INF;
			dp[i] = 0;
		}
		for(int i = 0; i < M; i++){
			scanf("%d%d%d", &U, &V, &C);
			G[U].push_back(make_pair(V,C));
			G[V].push_back(make_pair(U,C));
		}

		dijkstra(1);
		for(int i = 1; i <= N; i++){
			for(int j = 1; j <= N; j++) {
				if(i != j){
					
				}	
			}
		}
		cout << dp[1] << endl;
	}
	return 0;
}
