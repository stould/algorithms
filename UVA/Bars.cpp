#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 30003
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int T, N, P, val[21];

int main(void){
	scanf("%d", &T);
	while(T--){
		scanf("%d%d", &N, &P);
		for(int i = 0; i < P; i++){
			scanf("%d", &val[i]);
		}
		bool can = 0;
		for(int mask = 0; mask < (1 << P); mask++){
			int S = N;
			for(int i = 0; i < P && S >= 0; i++){
				if((mask & (1 << i))){
					S -= val[i];
				}
			}
			if(S == 0){
				can = 1;
				break;
			}
		}
		if(can){
			printf("YES\n");
		}else{
			printf("NO\n");
		}
	}
	return 0;
}
