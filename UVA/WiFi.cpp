#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100003
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int T, N, M, pos[MAXN];

bool can(int dist){
	int last = pos[0] + dist;
	int ant = N-1;
	for(int i = 1; i < M; i++){
		if(abs(pos[i] - last) > dist){
			last = pos[i] + dist;
			ant--;
		}
	}
	return ant >= 0;
}

int main(void){
	cin >> T;
	while(T--){
		cin >> N >> M;
		for(int i = 0; i < M; i++){
			cin >> pos[i];
			pos[i] *= 100;
		}
		sort(pos, pos + M);
		int lo = 0, hi = 1000000*100;
		while(lo <= hi){
			int mid = lo + (hi - lo) / 2.;
			if(can(mid)){
				hi = mid-1;
			}else{
				lo = mid + 1;
			}
		}
		if(lo % 100 < 10){
			printf("%d.0%d\n", lo/100, lo%100);
		}else{
			printf("%d.%d\n", lo/100, lo%100);
		}
	}
	return 0;
}
