#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 30003
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;


int N;

int main(void){
	bool into = 0;
	while(scanf("%d", &N) == 1 && N > 0){
		if(into) cout << endl;
		into = 1;
		bool can = 0;
		for(int esq = 1234; esq <= 98765 / N; esq++){
			int dir = esq * N;
			int tmp = esq;
			int mask = (esq < 10000);

			while(tmp > 0){
				mask |= 1 << (tmp % 10);
				tmp /= 10;
			}
			while(dir > 0){
				mask |= 1 << (dir % 10);
				dir /= 10;
			}
			if(mask == (1 << 10)-1){
				can = 1;
				if(esq < 10000){
					cout << esq * N << " / 0" << esq << " = " << N << endl;
				}else{
					cout << esq * N << " / " << esq << " = " << N << endl;
				}
			}
		}
		if(!can){
			cout << "There are no solutions for " << N << "." << endl;
		}

	}
	return 0;
}
