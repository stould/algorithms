#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 5001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int n;

ll val;

int main(void){
	while(cin >> n){
		priority_queue<ll> q;
		for(int i = 0; i < n; i++){
			cin >> val;
			q.push(-val);
		}

		ll ans = 0;
		while(q.size() > 1){
			ll topA = q.top(); q.pop();
			ll topB = q.top(); q.pop();
			ans += topA + topB;
			q.push(topA + topB);
		}
		cout << -ans << endl;
	}
	return 0;
}
