#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXM 1026
#define MAXN 20001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int T, S, A, F,s,a;

int main(void){
	scanf("%d", &T);
	while(T--){
		scanf("%d%d%d", &S, &A, &F);
		vector<int> street, avenue;
		for(int i = 0; i < F; i++){
			scanf("%d%d", &s, &a);
			street.push_back(s);
			avenue.push_back(a);
		}
		sort(street.begin(), street.end());
		sort(avenue.begin(), avenue.end());
		int ansS, ansA;
		if((F & 1) == 1){
			ansS = street[F/2];
			ansA = avenue[F/2];
		}else{
			ansS = street[(F-1)/2];
			ansA = avenue[(F-1)/2];
		}
		printf("(Street: %d, Avenue: %d)\n", ansS, ansA);

   }
	return 0;
}
