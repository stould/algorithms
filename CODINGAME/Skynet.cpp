#include <bits/stdc++.h>
#define MAXN 501
#define INF 0x3f3f3f3f

using namespace std;

    int N; // the total number of nodes in the level, including the gateways
    int L; // the number of links
    int E; // the number of exit gateways

int dfsct,  num[MAXN], low[MAXN], parent[MAXN], path[MAXN];
vector<vector<int> > graph(501);
vector<pair<int, int> > bridges;


bool isExit[MAXN];
bool vis[MAXN];
bool found_exit;

void bridge(int atual){
    num[atual] = low[atual] = dfsct++;
    for(int i = 0; i < graph[atual].size(); i++){
        int next = graph[atual][i];
        if(num[next] == -1){
            parent[next] = atual;
            bridge(next);
            if(low[next] > num[atual]){
                bridges.push_back(make_pair(atual, next));
            }
            low[atual] = min(low[atual], low[next]);
        }else if(next != parent[atual]){
            low[atual] = min(low[atual], num[next]);
        }
    }
}
 
void countBridges(){
    dfsct = 0;
    for(int i = 0; i < N; i++){
        num[i] = -1;
        parent[i] = 0;
    }
    for(int i = 0; i < N; i++){
        if(num[i] == -1) bridge(i);
    }
}



void dfsPreProc(int node, int root){
    if(found_exit == 1){
        return;   
    }
    vis[node] = 1;
    if(isExit[node]) found_exit = 1;
    for(int i = 0; i < graph[node].size(); i++){
        int next = graph[node][i];
        if(next == root || vis[next]) continue;
        dfsPreProc(next, root);
    }
}

int dist[MAXN];

void bfs(int S){
	queue<int> q;
	for(int i = 0; i < N; i++){
		dist[i] = INF;
		path[i] = -1;
	}
	dist[S] = 0;
	q.push(S);
	while(!q.empty()){
		int top = q.front(); q.pop();
		for(int i = 0; i < graph[top].size(); i++){
			int next = graph[top][i];
			if(dist[top] + 1 < dist[next]){
				path[next] = top;
				dist[next] = dist[top] + 1;
				q.push(next);
			}
		}
	}
}

void cutTrash(int node, int remove){
	for(int i = 0; i < graph[node].size(); i++){
		int next = graph[node][i];
		if(next == remove){
			graph[node].erase(graph[node].begin() + i);
			break;
		}
	}
}

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
int main(){
    cin >> N >> L >> E; cin.ignore();
	set<pair<int, int> > edges;
    for (int i = 0; i < L; i++) {
        int N1; // N1 and N2 defines a link between these nodes
        int N2;
        cin >> N1 >> N2; cin.ignore();
		edges.insert(make_pair(N1, N2));
		graph[N1].push_back(N2);
		graph[N2].push_back(N1);
    }
    for (int i = 0; i < E; i++) {
        int EI; // the index of a gateway node
        cin >> EI; cin.ignore();
		isExit[EI] = 1;
    }
	bool first = 1;
    // game loop
    while (1) {
        int SI; // The index of the node on which the Skynet agent is positioned this turn
        cin >> SI; cin.ignore();
		countBridges();
		bfs(SI);
		if(first == 1){
			first = 0;
			for(int i = 0; i < bridges.size(); i++){
				int A = bridges[i].first;
				int B = bridges[i].second;
				if(dist[A] > dist[B]){
					swap(A,B);
				}
				found_exit = 0;
				memset(vis,0,sizeof(vis));
				dfsPreProc(A,B);
				if(!found_exit){
					cutTrash(A,B);
				}
			}
		}
		
		int near = -1, val = INF;
		for(int i = 0; i < N; i++){
			if(i == SI) continue;
			if(val > dist[i]){
			    if(isExit[i]){
				    val = dist[i];
				    near = i;
			    }
			}
		}
		vector<int> cam(1, near);
		while(path[near] != -1){
			cam.push_back(path[near]);
			near = path[near];
		}
		reverse(cam.begin(), cam.end());
		if(val == INF){//all exists are protected

		}else{
			int A = SI;
			int B = cam[1];
			set<pair<int, int> >::iterator it = edges.find(make_pair(A,B));
			if(it == edges.end()){
				it = edges.find(make_pair(B,A));
			}			
			cout << (*it).first << " " << (*it).second << endl;
			edges.erase(it);
			cutTrash(A, B);
		}
        // Write an action using cout. DON'T FORGET THE "<< endl"
        // To debug: cerr << "Debug messages..." << endl;

    }
	return 0;
}
