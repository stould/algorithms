#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100007
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
 
using namespace std;
 
string toBin(ll val, bool flag){
	if(val == 0) return flag ? "" : "0";
	return toBin(val/2, 1) + string(1, char(val % 2+'0'));
}

string num;

ll dp[47][2][2],n,k;

ll func(int pos, bool last, bool prefix){
	if(pos == (int) num.size()){
		return 1LL;
	}else{
		ll &ans = dp[pos][last][prefix];
		if(ans == -1){
			ans = 0;
			ans += func(pos+1, 0, prefix && num[pos] == '0');
			if(last == 0){
				if(prefix == 0 || num[pos] == '1'){
					ans += func(pos+1, 1, prefix && num[pos] == '1');					
				}
			}
		}
		return ans;
	}
}

ll getNum(ll v){
	memset(dp,-1,sizeof(dp));
	num = toBin(v,0);
	while((int)num.size() < n) num = "0" + num;
	return func(0,0,1);
}

int main(void){
	while(cin >> n >> k){
		ll lo = 0, hi = 1LL << 45;
		string ans = "-1";
		while(lo <= hi){
			ll mid = (lo+hi) / 2LL;
			ll qtd = getNum(mid);
			if(num.size() > n){
				hi = mid-1;
			}else{
				if(qtd < k){
					lo = mid+1;
				}else if(qtd >= k){
					if(qtd == k){
						if(ans == "-1"){
							ans = num;
						}else{
							ans = min(ans, num);
						}
					}
					hi = mid-1;
				}
			}
		}
		cout << ans << endl;
	}
	return 0;
}
