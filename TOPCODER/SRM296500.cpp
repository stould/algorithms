#include <vector>
#include <list>
#include <map>
#include <set>
#include <queue>
#include <deque>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <string.h>

using namespace std;


class DollSets {
public:
	int maximumQuantity(vector <int> ds, int K) {
		sort(ds.begin(), ds.end());
		int ans = 0, l = 0, r = 0, n = ds.size();
		bool vis[n+1];
		memset(vis,0,sizeof(vis));
		while(r < n){
			if(l == r){
				r++;
			}else if(vis[l] == true){
				l++;
			}else if(vis[r] == true){
				r++;
			}else{
				if(ds[l] * K == ds[r]){
					ans++;
					vis[l] = vis[r] = true;
				}else{
					if(ds[l] * K < ds[r]){
						l++;
					}else{
						r++;
					}
				}
			}
		}
		return ans;
	}
};

int n, k;

int main(void){
	freopen("in.in", "r", stdin);
	DollSets doll;
	while(cin >> n){
		vector<int> tmp(n);
		for(int i = 0; i < n; i++){
			cin >> tmp[i];
		}
		cin >> k;
		cout << doll.maximumQuantity(tmp, k) << endl;	
	}
}


