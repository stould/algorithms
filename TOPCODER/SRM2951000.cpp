#include <vector>
#include <list>
#include <map>
#include <set>
#include <queue>
#include <deque>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <string.h>
#define ll long long

using namespace std;


class JimmyLightning {
public:


	struct Room {
		int val, t, idx, close;
		
		int sti(string v){
			int ans = 0, p = 1, n = v.size();
			for(int i = n-1; i >= 0; i--){
				ans += p * (v[i] - '0');
				p *= 10;
			}
			return ans;
		}
		
		Room(string r, vector <int> &doors){
			vector<int> pb;
			string tmp = "";
			for(int i = 0; i < r.size(); i++){
				if(r[i] == ' '){
					pb.push_back(sti(tmp));
					tmp = "";
				}else{
					tmp += string(1, r[i]);
				}
			}
			pb.push_back(sti(tmp));
			val = pb[0];
			t = pb[1];
			idx = pb[2]-1;
			close = doors[idx];
			cout << val << " " << t << " " << idx << " " << close << endl;
		}
		
		Room(){}
		
		bool operator < (const Room &o) const{
			if(close == o.close){
				if(val == o.val){
					return t < o.t;
				}
				return val > o.val;
			}
			return close < o.close;
		}

	};

	vector<Room> r;
	ll dp[1250][55];
	
	ll func(int timeWaste, int idx){
		if(idx >= r.size()){
			return 0;
		}else{
			ll &ans = dp[timeWaste][idx];
			if(ans == -1){
				ans = func(timeWaste, idx+1);
				if(timeWaste + r[idx].t < r[idx].close){
					ans = max(ans, (ll)r[idx].val + func(timeWaste + r[idx].t, idx));
				}
			}
			return ans;
		}
	}
	
	int robTheBank(vector <int> doors, vector <string> diamonds) {
		for(int i = 0; i < diamonds.size(); i++){
			r.push_back(Room(diamonds[i], doors));
		}
		sort(r.begin(), r.end());
		memset(dp, -1,sizeof(dp));
		return func(0,0);
	}
};


int main(void){
	freopen("in.in", "r", stdin);
	vector<int> doors(50);
	vector<string> diamonds(50);
	for(int i = 0; i < 50; i++){
		cin >> doors[i];
	}
	for(int i = 0; i < 50; i++){
		string a, b, c;
		cin >> a >> b >> c;
		diamonds[i] = a + " " + b + " " + c;
	}
	JimmyLightning jl;
	cout << jl.robTheBank(doors, diamonds);
	return 0;
}

//Powered by KawigiEdit 2.1.4 (beta) modified by pivanof!
