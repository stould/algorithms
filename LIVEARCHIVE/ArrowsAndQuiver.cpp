#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100009
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int t, n, m, c, target[MAXN], used[MAXN];

int reach(int quiver){
	queue<int> q;
	memset(used,0,sizeof(used));
	int ans = 0;
	for(int i = 0; i < m; i++){
		if(used[target[i]] == 0){
			if(q.size() + 1 > quiver){
				int tipo = q.front().tipo;
				q.pop();
				used[tipo] = 0;
			}
			used[target[i]] = 1;
			ans++;
		}
		q.push(Quiver(i,target[i]));
	}
	return ans;
}

int main(void){
	scanf("%d", &t);
	while(t--){
		//n = arrows
		//m = number of targets
		//c = time limit
		scanf("%d%d%d", &n, &m, &c);
		memset(target, 0, sizeof(target));
		memset(used, 0, sizeof(used));
		int total = 0;
		for(int i = 0; i < m; i++){
			scanf("%d", &target[i]);
			if(used[target[i]] == 0){
				total++;
				used[target[i]] = 1;
			}
		}
		int lo = 1, hi = m, ans = INF;
		while(lo <= hi){
			int mid = (lo+hi) >> 1;
			if(reach(mid) <= c){
				ans = min(ans, mid);
				hi = mid-1;
			}else{
				lo = mid+1;
			}
		}
		if(ans == INF || n > m){
				printf("%d\n", -1);
		}else{
			printf("%d\n", ans);
		}
	}
}
