#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100009
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int n,m,a,b,start,stop,cost;

struct street{
	int start, stop, cost;
	street(int a, int b, int c): start(a), stop(b), cost(c) {}
	street(){}
};

vector<vector<pair<int, vector<street> > > > g(MAXN);

string tmp;

vector<int> split(string& s) {
	vector<int> ans;
    istringstream ss(s, istringstream::in);
    int t;
    while(ss >> t) {
        ans.push_back(t);
    }
	return ans;
}

int main(void){

	bool S1 = 0, S2;
	while(1){
		if(!S1){
			cin >> n >> m;
		}
		if(n == 0 && m == 0) break;
		for(int i = 0; i < n; i++){
			g[i].clear();
		}
		vector<int> edge;
		S2 = 0;
		bool gone = 0;
		for(int i = 0; i < m && !gone; i++){
			if(!S2){
				cin >> a >> b;
			}
			pair<int, vector<street> > ab = make_pair(b, vector<street>());//a to b
			pair<int, vector<street> > ba = make_pair(a, vector<street>());//b to a
			while(1){
				getline(cin, tmp);
				edge = split(tmp);
				if(edge.size() == 2){
					a = edge[0];
					b = edge[1];
					if(i == m-1){
						n = a;
						m = b;
						gone = 1;
					}
					break;
				}else if(edge.size() == 3){
					cout << "LOL" << endl;
					start = edge[0];
					stop = edge[1];
					cost = edge[2];
					ab.second.push_back(street(start, stop, cost));
					ba.second.push_back(street(start, stop, cost));

				}
			}
			S2 = 1;
		}
		S1 = 1;
	}
	return 0;
}
