#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 103
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

vector<vector<int> > graph(MAXN);

struct Edge{
	int u,v;
	Edge(){}
	Edge(int FROM, int TO): u(FROM), v(TO){}
};

vector<vector<int> > comp;

int T,n,m,vis[MAXN],U,V,deg[MAXN],sum[MAXN];


void find(int u, int idx){
	comp[idx].push_back(u);
	sum[idx] += deg[u];
	vis[u] = 1;
	for(int i = 0; i < (int) graph[u].size(); i++){
		int v = graph[u][i];
		if(!vis[v]){
			find(v, idx);
		}
	}
}

void findComponents(){
	for(int i = 0; i < n; i++){
		if(!vis[i]){
			comp.push_back(vector<int>());
			find(i, comp.size()-1);
		}
	}
}

bool dfs(int u, int A, int B){
	vis[u] = 1;
	if(u == B){
		return 1;
	}else{
		for(int i = 0; i < graph[u].size(); i++){
			int v = graph[u][i];
			if(!vis[v]){
				if(v == B){
					if(u != A){
						if(dfs(v, A, B)){
							return 1;
						}
					}
				}else{
					if(dfs(v, A, B)){
						return 1;
					}
				}
			}
		}
	}
	return 0;
}

int cnt(int u, int A, int B){
	vis[u] = 1;
	int ans = 0;
	for(int i = 0; i < graph[u].size(); i++){
		int v = graph[u][i];
		if(!vis[v]){
			if(v == B){
				if(u != A){
					ans = 1 + cnt(v, A, B);
				}
			}else{
				ans = 1 + cnt(v,A,B);
			}
		}
	}
	return ans;
}

bool check[101][101];

int solve(){
	int ans = 0;
	findComponents();
	for(int idx = 0; idx < comp.size(); idx++){
		for(int i = 0; i < comp[idx].size(); i++){
			U = comp[idx][i];
			bool ok = 0;
			for(int j = 0; j < graph[U].size(); j++){
				V = graph[U][j];
				check[U][V] = 1;
				if(check[V][U] == 1){
					continue;
				}
				memset(vis,0,sizeof(vis));
				if(!dfs(U, U, V)){
					memset(vis,0,sizeof(vis));
					int ca = cnt(U,U,V)+1;
					int cb = cnt(V,V,U)+1;
					if(ca == cb && ca*(ca-1) == sum[idx]/2-1){
						ok = 1;
						ans++;
					}
				}
			}
			if(ok) break;
		}
	}
	return ans;
}

int main(void){
	cin >> T;
	for(int test = 1; test <= T; test++){
		cin >> n >> m;
		comp.clear();
		for(int i = 0; i < n; i++){
			graph[i].clear();
			vis[i] = 0;
			deg[i] = 0;
			sum[i] = 0;
			for(int j = 0; j < n; j++){
				check[i][j] = 0;
			}
		}
		for(int i = 0; i < m; i++){
			cin >> U >> V;
			U--;
			V--;
			graph[U].push_back(V);
			graph[V].push_back(U);
			deg[U]++;
			deg[V]++;
		}
		cout << "Case #"<<test<<": " << solve() << endl;
	}
	return 0;
}
