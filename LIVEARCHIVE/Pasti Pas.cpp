#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 50002
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define BASE1 29LL
#define BASE2 33LL
#define MOD 1000000007LL

using namespace std;

string str;
int t, n;

ll hash1[MAXN], pot1[MAXN], hash2[MAXN], pot2[MAXN];

ll mod(ll v){
	if(v >= MOD || v < 0)
		return (v % MOD + MOD) % MOD;
	return v;
}

void processHash1(){
        hash1[0] = str[0];
        hash2[0] = str[0];
        pot1[0] = 1;
        pot2[0] = 1;
        for(int i = 1; i < str.size(); i++){
                pot1[i] = (pot1[i-1] * BASE1);
                pot2[i] = (pot2[i-1] * BASE2);
                if(pot1[i] >= MOD) pot1[i] = mod(pot1[i]);
                if(pot2[i] >= MOD) pot2[i] = mod(pot2[i]);
                hash1[i] = str[i] * pot1[i];
                hash2[i] = str[i] * pot2[i];
                if(hash1[i] >= MOD) hash1[i] = mod(hash1[i]);
                if(hash2[i] >= MOD) hash2[i] = mod(hash2[i]);
                hash1[i] += hash1[i-1];
                hash2[i] += hash2[i-1];
                if(hash1[i] >= MOD) hash1[i] = mod(hash1[i]);
                if(hash2[i] >= MOD) hash2[i] = mod(hash2[i]);
        }
}

ll modpow(ll a, ll n) {
	ll res = 1LL;
	while (n) {
		if (n&1){
			res=(res*a);
			res = mod(res);
		}
        a=(a*1ll*a);
		a = mod(a);
		n >>=1;
    }
    return res;
}


ll binpow(ll b, ll e) {
	if(e == 0){
		return 1LL;
	}else if((e & 1LL) == 0){
		ll ans = binpow(b, (e >> 1LL));
		return mod(mod(ans) * mod(ans));
	}else{
		ll ans = mod(mod(b) * mod(binpow(b, e-1LL)));
	}
}
 

ll inverseMod(ll v){
	return modpow(v, MOD-2);
}

ll getHash1(int l, int r){
	 ll ans = hash1[r];
	 if(l-1 >= 0){
		 ans -= hash1[l-1];
	 }
	 ans = mod(ans * inverseMod(pot1[l]));
	 return ans;
}

ll getHash2(int l, int r){
	 ll ans = hash2[r];
	 if(l-1 >= 0){
		 ans -= hash2[l-1];
	 }
	 ans = mod(ans * inverseMod(pot2[l]));
	 return ans;
}

int main(void){
	ios::sync_with_stdio(0);
	cin >> t;
	for(int test = 1; test <= t; test++){
		cin >> str;
		processHash1();
		int n = str.size();
		int ans = 0;
		int LL = 0, LR = 0, RR = n-1, RL = n-1;
		bool use = 0;
		while(LR < RL){
			ll A = getHash1(LL, LR), AA = getHash2(LL, LR);
			ll B = getHash1(RL, RR), BB = getHash2(RL, RR);
			use = 1;
			if(A == B && AA == BB){
				ans += 2;
 				LL = LR+1;
				RR = RL-1;
				use = 0;
			}
			LR++;
			RL--;
		}
		ans += use | ((str.size() & 1) == 1);
		cout <<  "Case #" << test << ": "<< ans << endl;
	}
	return 0;
}
