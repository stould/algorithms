#include <bits/stdc++.h>
#define MAXN 101
#define MAXK 11
#define ALPHABET 10
#define ll long long

using namespace std;

int trie[MAXN*MAXK][ALPHABET];
int term[MAXN*MAXK], fail[MAXN*MAXK];
int INDEX;

void add(string &str){
    int root = 0;
    for(int i = 0; i < (int) str.size(); i++){
        int child = str[i] - '0';
        if(trie[root][child] == -1){
            trie[root][child] = INDEX++;
        }
        root = trie[root][child];
    }
    term[root] = 1;
}

void ahoCorasick(int root = 0){
    queue<int> q;
    for(int child = 0; child < ALPHABET; child++){
        if(trie[root][child] == -1){
            trie[root][child] = root;
        }else{
            q.push(trie[root][child]);
            fail[trie[root][child]] = root;
        }
    }
    while(!q.empty()){
        int parent = q.front(); q.pop();
        for(int child = 0; child < ALPHABET; child++){
            if(trie[parent][child] != -1){
                int node = fail[parent];
                while(trie[node][child] == -1){
                    node = fail[node];
                }
                fail[trie[parent][child]] = trie[node][child];
                term[trie[parent][child]] |= term[trie[node][child]];
                q.push(trie[parent][child]);
            }
        }
    }
}

int nextState(int root, int child){
    while(trie[root][child] == -1){
        root = fail[root];
    }
    return trie[root][child];
}

int T, K, N;
ll query;
ll pd[MAXK][MAXN*MAXK][2][2], mark[MAXK][MAXN*MAXK][2][2], state;
string opc;

string toStr(ll val){
    string ans = "";
    if(val == 0) ans = "0";
    while(val > 0){
        ans = string(1, char('0' + val % 10)) + ans;
        val /= 10ll;
    }
    return ans;
}

string num;

ll func(int id, int root, bool pref, bool seen){
    if(term[root]){
        return 0;
    }else if(id == (int) num.size()){
        return 1;
    }else{
        if(mark[id][root][pref][seen] == state){
            return pd[id][root][pref][seen];
        }else{
            mark[id][root][pref][seen] = state;
            ll ans = 0;
            int upperBound = pref ? num[id] - '0' : ALPHABET-1;
            for(int i = 0; i <= upperBound; i++){
                if(seen){
                    ans += func(id+1, nextState(root, i), pref && (i == upperBound), seen);
                }else{
                    if(i == 0){
                        ans += func(id+1, 0, pref && (i == upperBound), seen);
                    }else{
                        ans += func(id+1, nextState(0, i), pref && (i == upperBound), seen | 1);
                    }
                }
            }
            return pd[id][root][pref][seen] = ans;
        }
    }
}


int main(){
    cin >> T;
    state = 0;
    while(T--){
        INDEX = 1;
        memset(fail, 0, sizeof(fail));
        memset(trie, -1, sizeof(trie));
        memset(term, 0, sizeof(term));
        cin >> K;
        for(int i = 0; i < K; i++){
            cin >> opc;
            add(opc);
        }
        ahoCorasick();
        cin >> N;
        for(int i = 0; i < N; i++){
            cin >> query;query++;
            ll ans = -1;
            ll lo = 0, hi = (1ll << 34ll);
            while(lo <= hi){
                state++;
                ll mid = lo + (hi - lo) / 2ll;
                num = toStr(mid);
                ll cnt = func(0, 0, 1, 0);
                if(cnt >= query){
                    hi = mid - 1;
                    ans = mid;
                }else{
                    lo = mid + 1;
                }
            }
            cout << ans;
            if(i + 1 < N){
                cout << " ";
            }
        }
        puts("");

    }
    return 0;
}
