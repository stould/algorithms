#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100009
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

string seq;
int sat[1001];

int main(void){
	while(cin >> seq){
		if(seq.size() == 1 && seq[0] == '0') break;
		memset(sat,0,sizeof(sat));
		vector<int> ans;
		for(int i = 1; i < seq.size(); i+=1){
			if(seq[i] == '1'){
				if(sat[i] % 2 == 0){
					for(int j = 0; j <= seq.size(); j+=i){
						sat[j] ++;
					}
					ans.push_back(i);
				}
			}else{
				if(sat[i] % 2 == 1){
					for(int j = 0; j <= seq.size(); j+=i){
						sat[j] ++;
					}
					ans.push_back(i);
				}
			}
		}
		printf("%d", ans[0]);
		for(int i = 1; i < ans.size(); i++){
			printf(" %d", ans[i]);
		}
		printf("\n");
	}
	return 0;
}
