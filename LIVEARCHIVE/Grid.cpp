#include <bits/stdc++.h>
#define ll long long
 
using namespace std;
 
const int MAXN = 63;
const ll INF = 10000;
int dl[4] = {1,0,-1,0};
int dc[4] = {0,1,0,-1};
 
//Max Flow dinic O(V^2*E)
struct edge {
    int to,rev;
    ll cap;
    edge(int to, ll cap, int rev): to(to), cap(cap), rev(rev) {}
};
 
vector<edge> G[MAXN*MAXN+100];
int level[MAXN*MAXN+100];
int iter[MAXN*MAXN+100];
 
void add_edge(int from,int to,ll cap) {
    G[from].push_back(edge(to, cap, G[to].size()));
    G[to].push_back(edge(from, 0, G[from].size()-1));
}
 
void bfs(int s) {
    memset(level, -1, sizeof(level));
    queue<int> que;
    level[s] = 0;
    que.push(s);
    while(!que.empty()) {
        int v = que.front();
        que.pop();
        for (int i = 0; i < G[v].size(); i++) {
            edge& e = G[v][i];
            if(e.cap > 0 && level[e.to] < 0) {
                level[e.to] = level[v] + 1;
                que.push(e.to);
            }
        }
    }
}
 
ll dfs(int v, int t, ll f) {
    if(v == t) return f;
    for(int& i = iter[v]; i < (int) G[v].size(); i++) {
        edge &e = G[v][i];
        if(e.cap > 0 && level[v] < level[e.to]) {
            ll d = dfs(e.to, t, min(f, e.cap));
            if (d > 0) {
                e.cap -= d;
                G[e.to][e.rev].cap += d;
                return d;
            }
        }
    }
    return 0;
}
 
ll max_flow(int s, int t) {
    ll flow = 0;
    for( ; ; ) {
        bfs(s);
        if (level[t] < 0) {
            return flow;
        }
        memset(iter, 0, sizeof(iter));
        ll f;
        while ((f=dfs(s,t,INF)) > 0) {
            flow += f;
        }
    }
}
 
int n,m,T;
ll grid[MAXN][MAXN];
 
int main(){
    scanf("%d", &T);
    while(T--){
        for(int i = 0; i < MAXN*MAXN+100; i++){
            G[i].clear();
        }
        scanf("%d%d", &n, &m);
        ll sum = 0;
        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                scanf("%lld", &grid[i][j]);
                sum += grid[i][j];
            }
        }
        int S = 0, T = MAXN*MAXN+2;
        for(int i = 0; i < n; i++){
            for(int j = i%2; j < m; j+=2){
                for(int k = 0; k < 4; k++){
                    int nl = dl[k] + i;
                    int nc = dc[k] + j;
                    if(nl >= 0 && nc >= 0 && nl < n && nc < m){
                        int A = i*m+j+1, B = nl*m+nc+1;
                        add_edge(A,B,INF);
                    }
                }
            }
        }
 
        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                if((i + j) % 2 == 0){
                    add_edge(S, i*m+j+1, grid[i][j]);
                }else{
                    add_edge(i*m+j+1, T, grid[i][j]);
                }
            }
        }
        ll ans = sum - max_flow(S,T);
        printf("%lld\n", ans);
    }
    return 0;
}
