#include <bits/stdc++.h>

using namespace std;

vector<int> primes;

int pd[200][1001][4];

int func(int id, int bag, int used){
    if(used > 3){
        return 0;
    }
    if(bag == 0){
        return used == 3;
    }
    if(id >= primes.size()){
        return 0;
    }
    int &ans = pd[id][bag][used];
    if(ans == -1){
        ans = func(id+1, bag, used);
        if(bag - primes[id] >= 0){
            if((ans |= func(id, bag - primes[id], used+1))){
                return 1;
            }
        }
    }
    return ans;
    
}

vector<int> thePath;

void path(int id, int bag, int used){
    if(used > 3){
        return;
    }
    if(bag == 0){
        return;
    }
    if(id >= primes.size()){
        return;
    }
    if(bag - primes[id] >= 0){
        if(func(id, bag - primes[id], used+1)){
            thePath.push_back(primes[id]);
            path(id, bag - primes[id], used+1);
        }else{
            path(id+1, bag, used);
        }
    }
}

int T, K;

int main(){
    primes.push_back(2);
    for(int i = 3; i <= 1000; i++){
        int cnt = 0;
        for(int j = 1; j <= i; j++){
            cnt += i % j == 0 ? 1 : 0;
        }
        if(cnt == 2){
            primes.push_back(i);
        }
    }
    cin >> T;
    memset(pd,-1,sizeof(pd));
    func(0, 1000, 0);
    while(T--){
        cin >> K;
        path(0, K, 0);
        cout << thePath[0];
        for(int i = 1; i < 3; i++){
            cout << " " << thePath[i];
        }
        cout << endl;
        thePath.clear();
    }
    return 0;
}
