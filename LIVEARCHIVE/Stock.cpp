#include <bits/stdc++.h>
#define MAXN 1000001
#define ll long long

using namespace std;

int T, n;
ll val[MAXN];
ll best[MAXN];
ll pd[11][101];

int func(int id, int t){
    if(id == n){
        return 0;
    }else{
        ll &ans = pd[id][t];
        if(ans == -1){
            ans = func(id+1, t);
            ans = max(ans, - val[id] + func(id+1, t+1));
            ans = max(ans, val[id] * t + func(id+1, 0));
        }
        return ans;        
    }
}


int main(){
    scanf("%d", &T);
    while(T--){
        scanf("%d", &n);
        for(int i = 0; i < n; i++){
            scanf("%lld", &val[i]);
        }
        best[n-1] = val[n-1];
        for(int i = n-2; i >= 0; i--){
            best[i] = max(val[i], best[i+1]);
        }
        ll ans = 0, tmp = 0, t = 0;
        for(int i = 0; i < n; i++){
            if(val[i] == best[i]){
                ll total = t * val[i] - tmp;
                ans += total;
                tmp = t = 0;
            }else{
                t++;
                tmp += val[i];
            }
        }
        cout << ans << endl;
    }
    return 0;
}
