#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100009
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int n;

string text[1002];

int main(void){
	cin.tie(0);
	ios::sync_with_stdio(0);
	while(cin >> n && n > 0){
		cin.ignore();
		for(int i = 1; i <= n; i++){
			getline(cin, text[i]);
		}
		for(int i = 0; i <= n+1; i++){
			while(text[i].size() < 102){
				text[i].push_back(' ');
			}
		}
		int col = 0, fim = 0;
		for(int i = 0; i < n && !fim; i++){
			for(;;){
				if(text[i+1][col] == ' '){
					break;
				}else{
					col++;
				}
			}
		}
		cout << col+1 << endl;
	}
	return 0;
}
