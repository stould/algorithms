#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 20001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
 

using namespace std;
 
vector<vector<int> > g(MAXN);
int n, u, v, dist[MAXN], depth[MAXN], deg[MAXN];
const int root = 1;
 
int dfs(int node, int parent, int level){
    depth[node] = level;
    for(int i = 0; i < g[node].size(); i++){
        int next = g[node][i];
        if(next != parent){
            dist[node] += 1 + dfs(next, node, level+1);
        }
    }
    return dist[node];
}
 
ll calculate(int V, int root){
    ll ans = 0;
    vector<ll> child;
    for(int i = 0; i < g[V].size(); i++){
        int next = g[V][i];
        if(depth[next] > depth[V]){
            child.push_back(1LL+dist[next]);
        }else{
            child.push_back(dist[root] - dist[V]);
        }
    }
    for(int i = 0; i < child.size(); i++){
        for(int j = i+1; j < child.size(); j++){
            ans += child[i] * child[j];
        }
    }
    return ans;
}
 
int t;
 
 
int main(void){
    freopen("in.in", "r", stdin);
    scanf("%d", &t);
    for(int test = 1; test <= t; test++){
        scanf("%d", &n);
        for(int i = 0; i <= n; i++){
            g[i].clear();
            dist[i] = 0;
            depth[i] = 0;
            deg[i] = 0;
        }
        for(int i = 0; i < n-1; i++){
            scanf("%d%d", &u, &v);
            g[u].push_back(v);
            g[v].push_back(u);
            deg[u]++;
            deg[v]++;
        }
        dfs(root, -1, 0);
        ll ans = 0;
        for(int i = 1; i <= n; i++){
            if(deg[i] != 1){
                ans = max(ans, calculate(i, root));
            }
        }
        printf("Case #%d: %lld\n", test, ans);
    }
}
