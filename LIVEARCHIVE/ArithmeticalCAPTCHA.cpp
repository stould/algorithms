#include <bits/stdc++.h>
#define MAXN 201
#define ll long long int
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int T, n;
int graph[MAXN][MAXN];
int X[MAXN], Y[MAXN];

int dist(int X1, int Y1, int X2, int Y2){
    return (X2 - X1) * (X2 - X1) + (Y2 - Y1) * (Y2 - Y1);
}
vector<int> visible, notVisible, dst;



int main(){
    scanf("%d", &T);
    for(int test = 1; test <= T; test++){
        scanf("%d", &n);
        for(int i = 0; i < n; i++){
            scanf("%d", &X[i]);
        }
        visible.clear();
        notVisible.clear();
        dst.clear();
        dst.push_back(0);
        for(int i = 0; i < n; i++){
            scanf("%d", &Y[i]);
        }
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                scanf("%d", &graph[i][j]);
                int distance = dist(X[i], Y[i], X[j], Y[j]);
                if(graph[i][j]){
                    visible.push_back(distance);
                }else{
                    notVisible.push_back(distance);
                }
                dst.push_back(distance);
            }
        }
        sort(visible.begin(), visible.end());
        sort(notVisible.begin(), notVisible.end());
        int ansD = 0;
        int liers = INF;
        for(int i = 0; i < (int) dst.size(); i++){
            int lierVisible = visible.end() - lower_bound(visible.begin(), visible.end(), dst[i] + 1);
            int lierNotVisible = lower_bound(notVisible.begin(), notVisible.end(), dst[i] + 1) - notVisible.begin();
            int lieTotal = lierVisible + lierNotVisible;
            if(lieTotal < liers){
                liers = lieTotal;
                ansD = dst[i];
            }else if(lieTotal == liers){
                ansD = min(ansD, dst[i]);
            }
        }
        printf("Case #%d: %d %d\n", test, ansD, liers);
    }
    return 0;
}
