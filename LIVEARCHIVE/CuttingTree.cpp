#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100003
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int id[MAXN];

void unjoin(int what){
	id[what] = what;
}

int root(int idx){
	if(idx == id[idx]){
		return idx;
	}else{
		return root(id[idx]);
	}
}

int T,n,q,A,B;

char type;

int main(void){
	cin >> T;
	for(int test = 1; test <= T; test++){
		cin >> n >> q;
		for(int i = 0; i < MAXN; i++) id[i] = i;
		for(int i = 1; i <= n; i++){
			cin >> A;
			if(A != 0)
				id[i] = A;
		}
		printf("Case #%d:\n",test);
		while(q--){
			cin >> type;
			if(type == 'Q'){
				cin >> A >> B;
				if(root(A) == root(B)){
					cout << "YES\n";
				}else{
					cout << "NO\n";
				}
			}else if(type == 'C'){
				cin >> A;
				unjoin(A);
			}
		}
		
	}
	return 0;
}
