#include <bits/stdc++.h>

using namespace std;

int t, n, q[2][10001];

//dp[idx][idxQ2GreaterIdx][idxQ2GreaterIdx]
map<pair<int, pair<int, int> >, int> dp;

int func(int idx, int Q1Last, int Q2Last){
	if(idx == n){
		return 0;
	}else{
		pair<int, pair<int,int> > state = make_pair(idx,make_pair(Q1Last, Q2Last));
		if(dp.find(state) != dp.end()){
			return dp[state];
		}else{
			dp[state] = 0x3f3f3f3f;
			if(q[0][idx] <= Q1Last){
				if(q[1][idx] > Q1Last && q[0][idx] > Q2Last){
					dp[state] = min(dp[state], 1 + func(idx+1, q[1][idx], q[0][idx]));
				}
			}else if(q[1][idx] <= Q2Last){
				if(q[0][idx] > Q2Last && q[1][idx] > Q1Last){
					dp[state] = min(dp[state], 1 + func(idx+1, q[0][idx], q[1][idx]));
				}
			}else{
				dp[state] = min(dp[state], func(idx+1, q[0][idx], q[1][idx]));
				dp[state] = min(dp[state], 1 + func(idx+1, q[1][idx], q[0][idx]));
			}
			return dp[state];
		}
	}
}



int main() {
	cin >> t;
	while(t--){
		dp.clear();
		cin >> n;
		for(int i = 0; i < n; i++){
			cin >> q[0][i];
		}
		for(int i = 0; i < n; i++){
			cin >> q[1][i];
		}
		if(n == 1){
			printf("0\n");
		}else{
			int ans = func(1,q[0][0], q[1][0]);
			ans = min(ans, 1+func(1,q[1][0], q[0][1]));
			if(ans == 0x3f3f3f3f){
				ans = -1;
			}
			cout << ans << endl;
		}
	}
	return 0;
}
