#include <bits/stdc++.h>
#define MAXN 101
#define ll long long

using namespace std;

int T,n,m;

int wheel[MAXN], v[10];

int transf(){
    int P = 1;
    int ans = 0;
    for(int i = 0; i < m; i++){
        ans += P * v[m - i - 1];
        P *= 10;
    }
    return ans;
}

int main(){
    scanf("%d", &T);
    while(T--){
        scanf("%d%d", &n, &m);
        ll X, Y;
        for(int i = 0; i < m; i++){
            scanf("%d", &v[i]);
        }
        X = transf();
        for(int i = 0; i < m; i++){
            scanf("%d", &v[i]);
        }
        Y = transf();
        for(int i = 0; i < n; i++){
            scanf("%d", &wheel[i]);
        }
        int ans = 0;
        for(int i = 0; i < n; i++){
            int Z = 0, P = 1;
            for(int j = i + m - 1; j >= i; j--){
                Z += P * wheel[j % n];
                P *= 10;
            }
            if(X <= Z && Z <= Y){
                ans++;
            }
        }
        printf("%d\n", ans);
    }
    return 0;
}
