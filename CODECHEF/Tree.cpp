#include <bits/stdc++.h>
#define ll long long
#define MAXN 100007
#define LOGMAXVAL 19
#define INF 10000000

using namespace std;

vector<vector<int> > g(MAXN);
//For LCA:
int H[MAXN], L[MAXN << 1], E[MAXN << 1], idx, dp[MAXN*2][LOGMAXVAL], pre[MAXN*2], vis[MAXN], dist[MAXN];

struct LCA{
    int n;
    LCA(int n_){
        n = n_;
        build();
    }
 
    void build(){
        int base = 1;
        int pot = 0;
        for(int i = 0; i < 2*n; i++){
            if(i >= base * 2){
                pot++;
                base *= 2;
            }
            pre[i] = pot;
            dp[i][0] = i;
        }
        base = 2;
        pot = 1;
        while(base <= 2*n){
            for(int i = 0; i + base / 2 < 2*n; i++){
                int before = base / 2;
                if(L[dp[i][pot-1]] < L[dp[i + before][pot-1]]){
                    dp[i][pot] = dp[i][pot-1];
                }else{
                    dp[i][pot] = dp[i + before][pot-1];
                }
            }
            base *= 2;
            pot++;
        }
    }
 
    int getLca(int u, int v){
        int l = H[u];
        int r = H[v];
        if(l > r){
            swap(l,r);
        }
        int len = r-l+1;
        if(len == 1){
            return E[dp[r][0]];
        }else{
            int base = (1 << pre[len]);
            int pot = pre[len];
            if(L[dp[l][pot]] < L[dp[r-base+1][pot]]){
                return E[dp[l][pot]];
            }else{
                return E[dp[r-base+1][pot]];
            }
        }
    }

};

void dfs(int x, int depth){
    vis[x] = 1;//visited
    if(H[x] == -1) H[x] = idx;//mark first time the i'th node is visited
    L[idx] = depth;//when you visit a node you should mark the the depth you have found it.
    E[idx++] = x;//the i'th recursion, global variable
    for(int i = 0; i < (int) g[x].size(); i++){
        int next = g[x][i];
        if(!vis[next]){
            dist[next] = dist[x] + 1;
            dfs(next, depth+1);
            L[idx] = depth;
            E[idx++] = x;
        }
    }
}

void add(int a, int b){
    g[a].push_back(b);
    g[b].push_back(a);
}

int getDist(int U, int V, LCA &LCA){
    int lca = LCA.getLca(U,V);
    return dist[U] + dist[V] - 2 * dist[lca];
}


int T, n, U, V;
int nodes[MAXN];

int main(void){
    scanf("%d", &T);
    while(T--){
        scanf("%d", &n);
        idx = 0;
        for(int i = 0; i <= n; i++){
            g[i].clear();
            H[i] = -1;
            L[i] = E[i] = vis[i] = 0;
            dist[i] = 0;
        }
        for(int i = 2; i <= n; i++){
            scanf("%d", &nodes[i]);
            add(nodes[i], i);
        }
        dfs(1,0);
        LCA lca(n);
        int dist = 0, pa = 1, pb = 1;
        for(int i = 2; i <= n; i++){
            //trying move pa to newNode
            int dstMovePb = getDist(pa, i, lca);
            int dstMovePa = getDist(i, pb, lca);
            //cout << pa << " " << pb << endl;
            int big = max(dstMovePa, dstMovePb);
            if(big > dist){
                dist = big;
                if(dstMovePa > dstMovePb){
                    pa = i;
                }else{
                    pb = i;
                }
            }
            cout << dist << endl;
        }
        cout << endl;
    }
    return 0;
}
