#include <bits/stdc++.h>
#define MAXN 201
#define ll long long int
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;


bool esqDir[1000][1000];
bool dirEsq[1000][1000];
vector<string> words_;
set<vector<int> > ans;

void addAns(int a, int b){
    if(a > b) swap(a,b);
    vector<int> p;
    p.push_back(a);
    p.push_back(b);
    ans.insert(p);
}

struct Trie{
    map<int, Trie> child;
    int flag;
    vector<int> path;
    Trie() {
        flag = 0;
    }
 
    void addWord(int idx, int pos = 0) {
        if(pos == (int) words_[idx].size()) {
            flag = 1;
        }else{
            int letter_pos = words_[idx][words_[idx].size() - pos - 1] - 'a';
            if(child.find(letter_pos) == child.end()){
                child[letter_pos] = Trie();
            }
            child[letter_pos].path.push_back(idx);
            child[letter_pos].addWord(idx, pos + 1);
        }
    }
 
    void find(int idx, int pos = 0){
        //cout << idx << " " << pos << " "<< path.size() << ": ";

        //cout << endl;
        if(pos == (int) words_[idx].size()) {
            for(int i = 0; i < (int) path.size(); i++){
                //cout << path[i] << " " ;
                //cout << path[i] << endl;
                if(idx == path[i]) continue;

                //cout << idx << " " << path[i] << endl;
                if(words_[idx].size() == words_[path[i]].size()){
                    cout << "A = " << words_[idx] << " " << words_[path[i]] << endl;

                    addAns(idx, path[i]);
                }else if(words_[idx].size() > words_[path[i]].size()){
                    //cout << idx << " " << words_[idx];
                    if(dirEsq[idx][pos]){
                        cout << "B = " << words_[idx] << " " << words_[path[i]] << endl;
                        cout << pos << endl;
                        addAns(idx, path[i]);
                    }
                }else{

                    if(esqDir[path[i]][words_[(int)path[i]].size() - pos - 1]){
                        cout << "C = " << words_[idx] << " " << words_[path[i]] << endl;
                        cout << words_[(int)path[i]].size() - pos - 1 << endl;
                        addAns(idx, path[i]);
                    }
                }
            }
            return;
        }else{
            int letter_pos = words_[idx][pos] - 'a';
            /*
              cout << pos << " = " << words_[idx][pos] << ": " << endl;
              for(int i = 0; i < path.size(); i++){
              cout << path[i] << " ";
              }
              cout << endl;
            */
            if(child.find(letter_pos) != child.end()){
                child[letter_pos].find(idx, pos + 1);
            }
        }
    }
};

void preProcEsqDir(){
    for(int i = 0; i < (int) words_.size(); i++){
        int l = 1, r = words_[i].size() - 1, last = r;
        esqDir[i][0] = 1;
        while(l <= r && words_[i][l] == words_[i][l - 1]){
            esqDir[i][l] = 1;
            l++;
        }
        l = 0;
        while(r > 0){
            if(words_[i][l] == words_[i][r]){
                r--;
                l++;
                if(l >= r){
                    esqDir[i][last] = 1;
                    last = r;
                    l = 0;
                }
            }else{
                l = 0;
                r--;
                last = r;
            }
        }
    }
    /*
    for(int i = 0; i < words_[0].size(); i++){
        cout << esqDir[0][i] << " ";
    }
    cout << endl;
    */
}

void preProcDirEsq(){
    for(int i = 0; i < (int) words_.size(); i++){
        int m = words_[i].size();
        int l = 0, r = m - 2, last = l;
        dirEsq[i][r+1] = 1;
        while(r >= l && words_[i][r] == words_[i][r + 1]){
            dirEsq[i][r] = 1;
            r--;
        }
        r = m - 1;
        while(l < (int) words_[i].size()){
            if(words_[i][l] == words_[i][r]){
                r--;
                l++;
                if(l >= r){
                    dirEsq[i][last] = 1;
                    last = l;
                    r = m - 1;
                }
            }else{
                l++;
                r = m - 1;
                last = l;
            }
        }
    }
}

vector<vector<int> > palindromePairs(vector<string>& words) {
    Trie trie;
    words_ = words;
    for(int i = 0; i < (int) words_.size(); i++){
        trie.addWord(i);
    }
    preProcDirEsq();
    preProcEsqDir();
    for(int i = 0; i < words_.size(); i++){
        trie.find(i);
    }
    vector<vector<int> > r(ans.begin(), ans.end());
    return r;
}

int main(){
    vector<string> tmp;
    tmp.push_back("abcd");
    tmp.push_back("dcba");
    tmp.push_back("lls");
    tmp.push_back("s");
    tmp.push_back("sssll");

    palindromePairs(tmp);
    return 0;
}
