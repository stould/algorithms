class Solution {
public:
    map<int, int> cnt;
    vector<int> twoSum(vector<int>& num, int target) {
        vector<int> tmp = num;
        sort(num.begin(), num.end());
        for(int i= 0; i < num.size(); i++){
            cnt[num[i]] += 1;
        }
        vector<int> ans;
        for(int i = 0; i < num.size(); i++){
            vector<int>::iterator it = lower_bound(num.begin(), num.end(), target - num[i]);
            int pos = (it - num.begin());
            if(i != pos){
                if(*it + num[i] == target){
                    ans.push_back(min(num[i],num[pos]));
                    ans.push_back(max(num[i],num[pos]));
                    break;
                }
            }
        }
        if(ans.size() > 0){
            for(int i = 0; i < tmp.size(); i++){
                if(tmp[i] == ans[0]){
                    ans[0] = i+1;
                    break;
                }
            }
            for(int i = 0; i < tmp.size(); i++){
                if(tmp[i] == ans[1] && i != ans[0]-1){
                    ans[1] = i+1;
                    break;
                }
            }
        }
        sort(ans.begin(), ans.end());
        return ans;
    }
};
