class Solution {
public:
    int findDuplicate(vector<int>& nums) {
        map<int,int> ocur;
        for(int i = 0; i < nums.size(); i++){
            if(ocur[nums[i]] == 1) return nums[i];
            ocur[nums[i]] = 1;
        }
        return -1;
    }
};
