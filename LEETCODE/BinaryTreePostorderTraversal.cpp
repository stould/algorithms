/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<int> postorderTraversal(TreeNode* root) {
        vector<int> ans;
        if(root == NULL) return ans;
        stack<TreeNode*> st;
        st.push(root);
        int k = 0;
        TreeNode *curr = root;
        while(!st.empty()){
            TreeNode *top = st.top();
            st.pop();
            curr = top;
            ans.push_back(top->val);
            if(top->left != NULL){
                st.push(curr->left);
            }
            if(top->right != NULL){
                st.push(curr->right);
            }
        }
        reverse(ans.begin(), ans.end());
        return ans;
    }
};
