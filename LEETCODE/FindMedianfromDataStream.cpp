class MedianFinder {
public:
    
    priority_queue<int> qmax;
    priority_queue<int, vector<int>, greater<int> > qmin;
    
    // Adds a number into the data structure.
    void addNum(int num) {
        int sza = qmax.size(),  szb = qmin.size();
        if(sza == szb){
            qmax.push(num);
        }else{
            int A = qmax.top();
            if(A >= num){
                qmax.pop();
                qmin.push(A);
                qmax.push(num);
            }else{
                qmin.push(num);
            }
        }
        if(qmax.size() > 0 && qmin.size() > 0){
            int A = qmax.top();
            int B = qmin.top();
            if(A > B){
                qmax.pop();
                qmin.pop();
                qmax.push(B);
                qmin.push(A);
            }
        }
    }

    // Returns the median of current data stream
    double findMedian() {
        if(qmax.size() == 0 && qmin.size() == 0) return 0;
        if(qmax.size() == qmin.size()){
            return double(qmax.top() + qmin.top()) / 2.;
        }else{
            return qmax.top();
        }
    }
};


// Your MedianFinder object will be instantiated and called as such:
// MedianFinder mf;
// mf.addNum(1);
// mf.findMedian();
