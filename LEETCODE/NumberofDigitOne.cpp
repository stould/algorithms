class Solution {
public:


string num;
    int dp[70][70][2][2];
          
    string toStr(int val){
        string ans = "";
        if(val <=  0) ans = "0";
        while(val > 0){
            ans = string(1, '0'+(val % 10)) + ans;
            val /= 10;
        }
        return ans;
    }
     
     
    int func(int pos, int cnt, bool pref, bool seen,int what){
        if(pos == (int) num.size()){
            return cnt;
        }else{
            int &ans = dp[pos][cnt][pref][seen];
            if(ans == -1){
                ans = 0;
                for(int i = 0; i <= 9; i++){
                    if((i > num[pos] - '0' && pref)) continue;
                    if(!seen){
                        if(i > 0){
                            ans += func(pos+1, cnt + (i == what), pref && (num[pos] - '0' == i), i > 0, what);
                        }else{
                            ans += func(pos+1, 0, pref && (num[pos] - '0' == i), i > 0, what);
                        }
                    }else{
                        ans += func(pos+1, cnt + (i == what), pref && (num[pos] - '0' == i), seen, what);
                    }
                }
            }
            return ans;
        }
    }

    int countDigitOne(int n) {
        if(n < 0) return 0;
        num = toStr(n);
        memset(dp,-1,sizeof(dp));
        return func(0,0,1,0,1);
    }
};
