class Solution {
public:
    
    struct Trie{
        int end;
        map<char, Trie> child;
        
        Trie(){
            end = 0;
        }
        
        void add(string &s){
            add(s, 0);
        }
        
        void add(string &s, int idx){
            if(s.size() == idx){
                end = 1;
            }else{
                map<char, Trie>::iterator it = child.find(s[idx]-'a');
                if(it == child.end()){
                    child[s[idx]-'a'] = Trie();
                    it = child.find(s[idx]-'a');
                }
                (it->second).add(s, idx+1);
            }
        }
        
        int find(string &s){
            return find(s, 0);
        }
        
        int find(string &s, int idx){
            if(s.size() == idx){
                return end;
            }else{
                map<char, Trie>::iterator it = child.find(s[idx]-'a');
                if(it == child.end()){
                    return 2;
                }else{
                    return (it->second).find(s, idx+1);
                }
            }
        }
        
    };

    int dl[4] = {0,-1,0,1};
    int dc[4] = {-1,0,1,0};
    set<string> answer;
    vector<vector<char> > g;
    bool vis[30][30];
    Trie trie;
    
    void backtrack(int l, int c, string word){
        int query = trie.find(word);
        if(query == 2){
            return;
        }else{
            if(query == 1){
                answer.insert(word);
            }
            for(int i = 0; i < 4; i++){
                int nl = l + dl[i];
                int nc = c + dc[i];
                if(nl >= 0 && nc >= 0 && nl < g.size() && nc < g[nl].size() && !vis[nl][nc]){
                    vis[nl][nc] = 1;
                    backtrack(nl,nc, word + string(1,g[nl][nc]));
                    vis[nl][nc] = 0;
                }
            }
        }
        
    }
    

    vector<string> findWords(vector<vector<char> >& board, vector<string>& words) {
        
        //About Trie find method:
        //0 -> not match but it is a valid prefix
        //1 -> match and is a valid prefix
        //2 -> not match and it is not a valid prefix
        for(int i = 0; i < words.size(); i++){
            //cout << words[i] << endl;
            trie.add(words[i]);
        }
        g = board;
        for(int i = 0; i < g.size(); i++){
            for(int j = 0; j < board[i].size(); j++){
                memset(vis,0,sizeof(vis));
                vis[i][j] = 1;
                backtrack(i,j,string(1,board[i][j]));
            }
        }
        return vector<string>(answer.begin(), answer.end());
    }
};
