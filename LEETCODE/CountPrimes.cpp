class Solution {
public:
    
    int countPrimes(int n) {
        vector<int> isPrime(2000000,1);
        int MAXN = 2000000;
        isPrime[0] = isPrime[1] = 0;
        int ans = 0;
        for(int i = 2; i < MAXN; i++){
            if(isPrime[i]) {
                if(i < n) ans++;
                else break;
                for(int j = 2*i; j <= n; j += i){
                    isPrime[j] = 0;
                }
            }
        }
        return ans;
    }
};
