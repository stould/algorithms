#include <bits/stdc++.h>

using namespace std;

struct Node {
	Node *left;
	Node *right;
	int key, height;

	Node(int V){
		height = 1;
		left = right = NULL;
		key = V;
	}
	Node(){
		height = 1;
		left = right = NULL;
	}
};

struct AVLTree {
	Node *root;
	int size;

	AVLTree(){
		root = NULL;
		size = 0;
	}

	int height(Node *node){
		if(node != NULL){
			return node->height;
		}
		return 0;
	}

	Node *newNode(int key){
		Node *ans = new Node(key);
		return ans;
	}


	//find the leftmost child (or lower)
	Node *getLower(Node* subRoot){
		if(subRoot->left == NULL){
			return subRoot;
		}else{
			return getLower(subRoot->left);
		}
	}

	int getBalance(Node *node){
		if(node == NULL){
			return 0;
		}
		return height(node->left) - height(node->right);
	}

	//Single rotation LL
	Node *rightRotation(Node *subRoot){
		Node* newSubRoot = subRoot->left;
		subRoot->left = newSubRoot->right;
		newSubRoot->right = subRoot;
		subRoot->height = max(height(subRoot->left), height(subRoot->right)) + 1;
		newSubRoot->height = max(height(newSubRoot->left), height(newSubRoot->right)) + 1;
		return newSubRoot;
	}

	//Single rotation RR
	Node *leftRotation(Node *subRoot){
		Node* newSubRoot = subRoot->right;
		subRoot->right = newSubRoot->left;
		newSubRoot->left = subRoot;
		subRoot->height = max(height(subRoot->left), height(subRoot->right)) + 1;
		newSubRoot->height = max(height(newSubRoot->left), height(newSubRoot->right)) + 1;
		return newSubRoot;
	}

	//Double rotation LR
	Node *leftRightRotation(Node *subRoot){
		subRoot->left = leftRotation(subRoot->left);
		return rightRotation(subRoot);
	}

	//Double rotation RL
	Node *rightLeftRotation(Node *subRoot){
		subRoot->right = rightRotation(subRoot->right);
		return leftRotation(subRoot);
	}

	void insert(int key){
		root = insert(root, key);
	}

	Node *insert(Node *subRoot, int key){
		if(subRoot == NULL){
			size++;
			return newNode(key);
		}else{
			if(key < subRoot->key){
				subRoot->left = insert(subRoot->left, key);
			}else if(key > subRoot->key){
				subRoot->right = insert(subRoot->right, key);
			}
			subRoot->height = max(height(subRoot->left), height(subRoot->right)) + 1;
			int balance = getBalance(subRoot);
			//now we need to check if the unbalancing factor was caused by a inner or outer insertion
			//1) balance > 1 means that left subtree is higher than right subtree and we cought an unbalancing factor at node 'root'
			//2) balance < -1 means almost than 1) but, right subtree is greater than left subtree.
			
			//outer LL
			if(balance > 1 && key < subRoot->left->key){
				//case 1: single right rotation
				if(key == 16) cout << "A" << endl;
				return rightRotation(subRoot);
			}

			//outer RR
			if(balance < -1 && key > subRoot->right->key){
				//case 2: single left rotation
				if(key == 16) cout << "B" << endl;
				return leftRotation(subRoot);
			}

			//inner LR
			if(balance > 1 && key > subRoot->left->key){
				//case 3: left right rotation
				if(key == 16) cout << "C" << endl;
				return leftRightRotation(subRoot);
			}

			//inner RL
			if(balance < -1 && key < subRoot->right->key){
				//case 4: right left rotation
				if(key == 16) cout << "D" << endl;
				return rightLeftRotation(subRoot);
			}

			//Case of the tree is balanced or the tree already have the key, just returns the child.
			return subRoot;
		}
	}

	void erase(int key){
		root = erase(root, key);
	}

	Node *erase(Node* subRoot, int key){
		if(subRoot == NULL){
			return NULL;
		}else{
			if(key < subRoot->key){
				subRoot->left = erase(subRoot->left, key);
			}else if(key > subRoot->key){
				subRoot->right = erase(subRoot->right, key);
			}else{
				if((subRoot->left == NULL) || (subRoot->right == NULL)){
					//Case of 1 child or no child
					Node *temp = subRoot->left != NULL ? subRoot->left : subRoot->right;
					if(temp == NULL){
						//Case of No child
						temp = subRoot;
						subRoot = NULL;
						size--;
					}else{
						//Case of 1 child	
						*subRoot = *temp;
						size--;
					}
					free(temp);
				}else{
					//case of 2 children
					Node *lower = getLower(subRoot->right);
					subRoot->key = lower->key;
					subRoot->right = erase(subRoot->right, lower->key);
				}
			}
			if(subRoot == NULL){
				return subRoot;
			}else{
				subRoot->height = max(height(subRoot->left), height(subRoot->right)) + 1;
				int balance = getBalance(subRoot);
				//now we need to check if the unbalancing factor
				//We need to check the unbalanced node and check the balance of node's child too.
				
				//outer LL
				if(balance > 1 && getBalance(subRoot->left) >= 0){
					//Case 1: Unbalance at subRoot, and the balance of subRoot->left >= 0 means that the left subTree is higher or equal
					return rightRotation(subRoot);
				}
				
				//outer RR
				if(balance < -1 && getBalance(subRoot->right) <= 0){
					//Case 2: Unbalance at subRoot, and the balance of subRoot->right <= 0 means that the right subTre is higher or equal
					return leftRotation(subRoot);
				}

				//inner LR
				if(balance > 1 && getBalance(subRoot->left) < 0){
					//Case 3: Unbalance at subRoot, and the balance of subRoot->left < 0 means that the right subTree is higher
					return leftRightRotation(subRoot);
				}

				//inner RL
				if(balance < -1 && getBalance(subRoot->right) > 0){
					//Case 4: Unbalance at subRoot, and the balance of subRoot->right > 0 means that the left subTre is higher
					return rightLeftRotation(subRoot);
				}
				return subRoot;
			}
		}
	}

	void preOrder(){
		preOrder(root);
	}

	void preOrder(Node *subRoot){
		if(subRoot != NULL){
			printf("key = %d, height = %d\n", subRoot->key, subRoot->height);
			preOrder(subRoot->left);
			preOrder(subRoot->right);
		}
	}

	void inOrder(){
		inOrder(root);
	}

	void inOrder(Node *subRoot){
		if(subRoot != NULL){
			inOrder(subRoot->left);
			printf("key = %d, height = %d\n", subRoot->key, subRoot->height);
			inOrder(subRoot->right);
		}
	}
};

int main(){
	srand(time(NULL));
	AVLTree tree;
	tree.insert(10);
	tree.insert(5);
	tree.insert(15);
	tree.insert(3);
	tree.insert(4);
	tree.insert(17);
	tree.insert(16);
	tree.preOrder();
	cout << endl;
	return 0;
}
