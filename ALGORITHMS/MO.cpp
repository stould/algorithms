#include <bits/stdc++.h>
#define MAXN 100001
#define MAXV 1000001
#define SQRTN 320
#define INF 0x3f3f3f3f

using namespace std;

int n, q, ql, qr, val[MAXN];

int ans;
int cnt[MAXV];

void add(int idx){
	//	if(idx == 0 || idx > n) return;
	//cout << "add " << val[idx] << " idx = " << idx<< endl;
	int V = val[idx];
	cnt[V]++;
	if(cnt[V] == 3){
		ans += 1;
	}
}

void remove(int idx){
	//	if(idx == 0 || idx > n) return;
	//	cout << "remove " << val[idx] << " idx = " << idx << endl;
	int V = val[idx];

		cnt[V]--;
	
	if(cnt[V] == 2){
		ans -= 1;
	}
}


int brute(int L, int R){
	map<int, int> mp;
	for(int i = L; i <= R; i++){
		mp[val[i]]++;
	}
	int ans = 0;
	for(map<int,int>::iterator it = mp.begin(); it != mp.end(); it++){
		if(it->second > 2){
			ans++;
		}
	}
	return ans;
}
void MOs(){
	scanf("%d%d", &ql, &qr);
	q--;
	int l = ql, r = qr;
	for(int i = ql; i <= qr; i++){
		add(i);
	}
	cout << ans << " " << brute(ql, qr) << endl;
	while(q--){
		scanf("%d%d", &ql, &qr);
		while (r < qr) {
			r += 1;
			add(r);
		}
		while (l < ql) {
			remove(l);
			l += 1;
		}
		while (l > ql) {
			l -= 1;
			add(l);
		}
		cout << ans << " " << brute(ql, qr) << endl;
	}
}

int main(){
	scanf("%d", &n);
	for(int i = 1; i <= n; i++){
		scanf("%d", &val[i]);
	}
	ans = 0;
	scanf("%d", &q);
	MOs();
	return 0;
}
