#include <bits/stdc++.h>
#define INF 10000000
#define MAXCOST_ 10
#define MAXN_ 1001
#define MAXV_ 1001

using namespace std;

vector<vector<pair<int, int> > > graph(MAXN_);
int distSlow[MAXN_];
pair<int, list<int>::iterator > dist[MAXN_];
bool adj[MAXN_][MAXN_];

int N, M;

void gen(int n, int maxCost){
    for(int i = 0; i < MAXN_; i++){
        graph[i].clear();
        for(int j = 0; j < MAXN_; j++){
            adj[i][j] = 0;
        }
    }
    M = 0;
    for(int i = 0; i < n; i++){
        int arestas = 1 + rand() % n;
        for(int j = 0; j < arestas; j++){
            int next = rand() % n;
            int k = 30;
            while(k && adj[i][next]){
                k--;
                next = rand() % n;
            }
            if(k > 0){
                M++;
                adj[i][next] = 1;
                adj[next][i] = 1;
                graph[i].push_back(make_pair(next,rand() % maxCost + 1));
                graph[next].push_back(make_pair(i, rand() % maxCost + 1));
            }
        }
    }
}

int dijkstra(int s, int t){
    priority_queue<int> q;
    q.push(s);
    for(int i = 0; i < MAXN_; i++){
        distSlow[i] = INF;
    }
    distSlow[s] = 0;
    while(!q.empty()){
        int top = q.top();
        q.pop();
        for(int i = 0; i < (int) graph[top].size(); i++){
            int next = graph[top][i].first;
            int cost = graph[top][i].second;
            if(distSlow[top] + cost < distSlow[next]){
                q.push(next);
                distSlow[next] = distSlow[top] + cost;
            }
        }
    }
    return distSlow[t];
}

list<int> bucket[MAXCOST_*MAXV_+1];

int fastDijkstra(int s, int t){
    for(int i = 0; i < MAXN_; i++){
        dist[i].first = INF;
    }
    dist[s].first = 0;
    bucket[0].push_back(s);
    int pos = 0;
    while(1){
        while(bucket[pos].size() == 0 && pos < MAXCOST_ * M){
            pos++;
        }
        if(pos == MAXCOST_ * M){
            break;
        }else{
            int u = *bucket[pos].begin();
            bucket[pos].pop_front();
            for(int i = 0; i < (int) graph[u].size(); i++){
                int v = graph[u][i].first;
                int cost = graph[u][i].second;
                int du = dist[u].first;
                int dv = dist[v].first;
                if(du + cost < dv){
                    if(dv != INF){
                        //                        bucket[dv].erase(dist[v].second);
                    }
                    dist[v].first = du + cost;
                    dv = dist[v].first;
                    bucket[dv].push_back(v);
                    dist[v].second = bucket[dv].begin();
                }
            }
        }
    }
    return dist[t].first;
}

int main(){
    srand(time(NULL));
    int n = 101, maxCost = 10;
    gen(n, maxCost);
    cout << dijkstra(0, n-1) << endl;
    //cout << fastDijkstra(0, n-1) << endl;
    return 0;
}
