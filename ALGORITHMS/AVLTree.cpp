#include <bits/stdc++.h>
#define MAXN 100001
#define MAXV 1000001
#define SQRTN 320
#define INF 0x3f3f3f3f

using namespace std;

struct Node{
	Node *left;
	Node *right;
	
	int height;
	int key;

	Node(){
		left = NULL;
		right = NULL;
	}
};

//Using struct for simplicity
struct AVLTree{
	Node *mainRoot;
	int size;
	AVLTree(){
		size = 0;
		mainRoot = NULL;
	}
	

	void insert(int value){
		mainRoot = insert(mainRoot, value);
		size++;
	}

	int height(Node *root){
		int ans = 0;
		if(root != NULL){
			ans = root->height;
		}
		return ans;
	}

	Node *newNode(int value){
		Node *ans = new Node();
		ans->key = value;
		ans->height = 1;
		return ans;
	}

	int getBalance(Node *root){
		int ans = 0;
		if(root != NULL){
			ans = height(root->left) - height(root->right);
		}
		return ans;
	}

	void traverse(){
		traverse(mainRoot);
	}

	Node *seek(int val){
		return seek(mainRoot, val);
	}

	//Just a single rotation LL
	Node* rightRotation(Node *root){
		Node *newRoot = root->left;
		root->left = newRoot->right;
		newRoot->right = root;
		root->height = max(height(root->left), height(root->right)) + 1;
		newRoot->height = max(height(newRoot->left), height(newRoot->right)) + 1;
		return newRoot;
	}

	//Just a single rotation RR
	Node* leftRotation(Node *root){
		Node *newRoot = root->right;
		root->right =  newRoot->left;
		newRoot->left = root;
		root->height = max(height(root->left), height(root->right)) + 1;
		newRoot->height = max(height(newRoot->left), height(newRoot->right)) + 1;
		return newRoot;
	}

	//Double rotation LR
	Node* leftRightRotation(Node *root){
		//root->left receives the new root
		root->left = leftRotation(root->left);
		return rightRotation(root);
	}

	//Double rotation LR
	Node* rightLeftRotation(Node *root){
		//root->right receives the new root
		root->right = rightRotation(root->right);
		return leftRotation(root);
	}

	void traverse(Node *root){
		if(root == NULL){
			return;
		}else{
			traverse(root->left);
			cout << (root->key) << " ";
			traverse(root->right);
		}
	}
	
	Node *seek(Node *root, int val){
		if(val < root->key){
			return seek(root->left, val);
		}else if(val > root->key){
			return seek(root->right, val);
		}else{
			return root;
		}
	}

    Node *insert(Node *root, int key){
		if(root == NULL){
			return newNode(key);
		}else{
			if(key < root->key){
				root->left = insert(root->left, key);
			}else if(key > root->key){
				root->right = insert(root->right, key);
			}
			root->height = max(height(root->left), height(root->right)) + 1;
			int balance = getBalance(root);
			//now we need to check if the unbalancing was caused by a inner or outer insertion
			//1) balance > 1 means that left is higher than right and we cought an unbalancing factor at node 'root'
			//2) balance < -1 means almost than 1) but, right is greater than left.
			if(balance > 1 and key < root->left->key){
				//case 1: single right rotation
				return rightRotation(root);
			}
			if(balance < -1 and key > root->right->key){
				//case 2: single left rotation
				return leftRotation(root);
			}
			if(balance > 1 and key > root->left->key){
				//case 3: left right rotation
				return leftRightRotation(root);
			}
			if(balance < -1 and key < root->right->key){
				//case 4: right left rotation
				return rightLeftRotation(root);
			}
			return root;			
		}
	}

};

int main(){
	AVLTree tree;
	tree.insert(1);
	tree.insert(2);
	tree.insert(3);
	tree.insert(4);
	tree.insert(5);
	tree.insert(-1);
	tree.insert(-2);
	tree.insert(-3);
	tree.traverse();
	cout << endl;
	return 0;
}
