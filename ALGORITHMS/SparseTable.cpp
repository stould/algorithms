#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
#define MAXN 10
using namespace std;



int dp[MAXN][MAXN];
int val[MAXN];

void process2(int N) {
	int i, j;
   
	//initialize M for the intervals with length 1
	for (i = 0; i < N; i++){
		dp[i][0] = i;
	}
	//compute values from smaller to bigger intervals
	for (j = 1; 1 << j <= N; j++){
		for (i = 0; i + (1 << j) - 1 < N; i++){
			if (val[dp[i][j - 1]] < val[dp[i + (1 << (j - 1))][j - 1]]){
				dp[i][j] = dp[i][j - 1];
			}else{
				dp[i][j] = dp[i + (1 << (j - 1))][j - 1];
			}
		}
	}

	for (j = 1; 1 << j <= N; j++){
		for (i = 0; i + (1 << j) - 1 < N; i++){
			cout << dp[i][j] << " " ;
		}cout << endl;
	}
}  


void nsquare(){
	for(int i = 0; i < MAXN; i++){
		dp[i][i] = i;
		for(int j = i+1; j < MAXN; j++){
			dp[i][j] = val[dp[i][j-1]] <= val[j] ? dp[i][j-1] : j;
		}
	}
}

int main(){
	val[0] = 1;
	val[1] = 3;
	val[2] = 4;
	val[3] = 2;
	val[4] = 6;
	val[5] = 1;
	for(int i = 0; i < MAXN; i++){
		for(int j = i+1; j < MAXN; j++){
			int low = INF;
			for(int k = i; k <= j; k++){
				low = min(val[k], low);
			}
			cout << low << " ";
		}
	}
	process2(MAXN);

	return 0;
}
