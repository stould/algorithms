#include <bits/stdc++.h>
#define INF 0x3f3f3f3f

using namespace std;

struct Heap{

	vector<int> heap;

	Heap(){
		heap.push_back(0);
	}
	
	int size(){
		return heap.size() - 1;
	}

	bool empty(){
		return (int) heap.size() <= 1;
	}

	int top(){
		return heap[1];
	}

	void heapfy(int pos){
		if(pos == 1){
			return;
		}else{
			int parent = pos / 2;
			if(heap[pos] > heap[parent]){
				swap(heap[pos], heap[parent]);
				heapfy(parent);
			}
		}
	}
	
	void downHeap(int pos){
		int l = pos * 2;
		int r = pos * 2 + 1;
		int sz = heap.size();
		if(l < sz && r < sz){
			if(heap[l] == heap[r] && heap[pos] < heap[l]){
				swap(heap[l], heap[pos]);
				downHeap(l);
			}else{
				if(heap[l] < heap[r]){
					if(heap[pos] < heap[r]){
						swap(heap[r], heap[pos]);
						downHeap(r);
					}
				}else{
					if(heap[pos] < heap[l]){
						swap(heap[l], heap[pos]);
						downHeap(l);
					}
				}
			}
		}else{
			if(l < sz && heap[pos] < heap[l]){
				swap(heap[l], heap[pos]);
				downHeap(l);
			}else if(r < sz && heap[pos] < heap[r]){
				swap(heap[r], heap[pos]);
				downHeap(r);
			}
		}
	}
	
	void pop(){
		if(!empty()){
			swap(heap[1], heap[(int)heap.size() - 1]);
			heap.pop_back();
			downHeap(1);
		}
	}

	void push(int val){
		heap.push_back(val);
		heapfy(heap.size()-1);
	}
};

vector<int> kgreaterHeap(vector<int> &v, int k){
	Heap h;
	for(int i = 0; i < v.size(); i++){
		h.push(-v[i]);
		if(h.size() > k){
			h.pop();
		}
	}
	vector<int> ans;
	while(!h.empty()){
		ans.push_back(-h.top());
		h.pop();
	}
	reverse(ans.begin(), ans.end());
	return ans;
}

void update(vector<int> &list, int val){
	int k = list.size();
	for(int i = 0; i < k; i++){
		if(val > list[i]){
			for(int j = k-1; j > i; j--){
				list[j] = list[j-1];
			}
			list[i] = val;
			break;
		}
	}
}

vector<int> kgreaterSlow(vector<int> &v, int k){
	vector<int> list(k, - INF);
	for(int i = 0; i < v.size(); i++){
		update(list, v[i]);
	}
	vector<int> ans;
	for(int i = 0; i < min((int) v.size(), k); i++){
		ans.push_back(list[i]);
	}
	cout << endl;
	return ans;
}

int main(){
	srand(time(NULL));
	vector<int> arr;
	int n = 100000, k = 2284;
	for(int i = 0; i < n; i++){
		arr.push_back(1+rand() % 1000000);
	}
	vector<int> ans1 = kgreaterHeap(arr, k);
	vector<int> ans2 = kgreaterSlow(arr, k);
	for(int i = 0; i < ans1.size(); i++){
		//cout << ans1[i] << endl;
	}
	cout << endl;
		for(int i = 0; i < ans2.size(); i++){
		//cout << ans2[i] << endl;
		}
	return 0;
}
