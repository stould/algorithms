#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <cmath>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

bool vis[11][11],line[11],col[11];

using namespace std;

bool canPut(int a, int b, int n, int m){
  for(int i = 0; i < 11; i++){
	if(line[a] || col[b])return 0;
    if(a + i <= n && b + i <= m && vis[a+i][b+i]){
      return 0;
    }else if(a-i > 0 && b-i > 0 && vis[a-i][b-i]){
      return 0;
    }else if(a-i > 0 && b+i <= m && vis[a-i][b+i]){
     return 0; 
    }else if(a+i <= n && b-i > 0 && vis[a+i][b-i]){
      return 0;
    }
  }
  return 1;
}

int chess(std::string c, int n, int m) {
  	int ans = 0;
	if(c[0] == 'r') ans = std::min(m,n);
  	if(c[0] == 'k') ans = (int) std::ceil(n/2.0) * (int) std::ceil(m/2.0) + (int) std::floor(n/2.0) * (int) (m/2.0);
  	if(c[0] == 'K') ans = (n/2) * (m/2);
  if(c[0] == 'Q') {
    for(int i = 1; i <= n; i++){
      for(int j = 1; j <= m; j++){
        if(canPut(i,j,m,n)){
          vis[i][j] = line[i] = col[j] = 1;
          ans = std::max(ans, 1+chess(c,m,n));
          vis[i][j] = line[i] = col[j] = 0;
        }
      }
    }
  }
  return ans;
}
int A,B;
int main(void) {
	for(int i = 0; i < 7; i++){
		cin >> A>>B;
		cout << chess("Q", A,B) << endl;
	}
	return 0;
}
