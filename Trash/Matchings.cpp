#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

vector<int> path;
vector<vector<int> > g(MAXN);
int n,a,b;

int pd[MAXN][2];

int func(int at,int pai, int f){
	
	if(pd[at][f]!=-1)
		return pd[at][f];
	int resp = 0;
	if(f == 1){
		for(int i = 0;i< g[at].size();i++){
			int y = g[at][i];
			if(y == pai)continue;
			resp+=func(y,at,0);
		}
	}else{
		int todos = func(at,pai,1);
		resp = todos;
		for(int i = 0;i< g[at].size();i++){
			int y = g[at][i];
			if(y == pai)continue;
			resp = max(resp,todos - func(y,at,0) + func(y,at,1) +1);
		}
	}
	return pd[at][f] = resp;
}

int main(void) {
	cin >> n;
	for(int i = 0; i < n-1; i++){
		cin >> a >> b;
		g[a].push_back(b);
		g[b].push_back(a);
	}
	memset(pd,-1,sizeof(pd));
	cout << func(1,-1,0) << endl;
	return 0;
}
