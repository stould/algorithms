#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 10005
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int pd[1 << 11][10], omask;

int func(int mask, int press, int bits){
  if(press == 0){
    return mask == omask;
  }
  int &ans = pd[mask][press];
  if(ans == -1){
    ans = 0;
    for(int i = 0; i < 10; i++){
      if(omask & (1 << i)){
		if(bits == 0 && i == 0){
			continue;
		}else{
	        ans += func(mask | (1<< i), press-1, bits+1);
		}
      }
    }
  }
  return ans;
}

int secretCode(std::vector< bool > a) {
    omask = 0;
  for(int i = 0; i < (1 << 11); i++){
    for(int j = 0; j < 10; j++){
		pd[i][j] = -1;
    }
  }
    for(int i = 0; i < a.size(); i++){
        if(a[i]){
            omask |= (1 << i);
        }
    }
    return func(0,8,0);
}

int main(void){
	vector<bool> a;
	a.push_back(1);
	a.push_back(0);
	a.push_back(0);
	a.push_back(0);
	a.push_back(0);
	a.push_back(0);
	a.push_back(0);
	a.push_back(1);
	a.push_back(1);
	a.push_back(0);
	cout << secretCode(a) << endl;
	return 0;
}
