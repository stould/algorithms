#include <stdio.h>
#include <string.h>
#include <iostream>
using namespace std;
int n, coins[] = {1, 5, 10, 25, 50};
int dp[8000];

int coin(int t) {
	memset(dp, 0, sizeof(dp));
	dp[0] = 1;
	for(int i = 0; i < 5; i++) {
		int c = coins[i];
		for(int j = c; j <= n; j++) {
			dp[j] += dp[j - c];
		}
	}
	return dp[n];
}

int main() {
   while(scanf("%d", &n) != EOF) {
      cout << coin(n) << endl;
   }

   return 0;
}
