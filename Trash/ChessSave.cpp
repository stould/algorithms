#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <cmath>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1



using namespace std;

int row[11], ans, temp;

bool ok(int l, int c){
	for(int i = 0; i < c; i++){
		if(row[i] == l) return 0;
		int dy = abs(row[i] - l);
		int dx = abs(i - c);
		if(dx == dy) return 0;
	}
	return 1;
}

void track(int c, int n, int m){
	if(c == m){
		ans = std::max(ans, temp);
	}else{
		for(int i = 0; i < n; i++){
			if(ok(i, c)){
				row[c] = i;
				temp++;
				track(c+1, n, m);
				row[c] = -1;
				temp--;
			}
		}
	}
}

int chess(std::string c, int m, int n) {
	ans = temp = 0;
	if(c[0] == 'r') ans = std::min(m,n);
  	if(c[0] == 'k') ans = (int) std::ceil(n/2.0) * (int) std::ceil(m/2.0) + (int) std::floor(n/2.0) * (int) (m/2.0);
  	if(c[0] == 'K') ans = ((n+1)/2) * ((m+1)/2);
	if(c[0] == 'Q') {
		for(int i = 0; i < 11;i++) row[i] = -1;
		track(0, m,n);
	}
  return ans;
}

int A,B;
int main(void) {
	for(int i = 0; i < 1; i++){
		cin >> A>>B;
		cout << chess("Q", A,B) << endl;
	}
	return 0;
}
