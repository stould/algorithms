#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <cmath>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1
#define ull unsigned long long

using namespace std;

    bool wordPattern(string pattern, string str) {
        int pos = 0, last = 0, i = 0;
        map<char, string> mpa;
        map<string, char> mpb;
        while(1){
			if(pos == (int)str.size()-1){
                string tmp = str.substr(last);
                if(mpa.find(pattern[i]) == mpa.end()){
					mpa[pattern[i]] = tmp;
                    if(mpb.find(tmp) == mpb.end()){
                        mpb[tmp] = pattern[i];
                    }else{
                        if(mpb[tmp] != pattern[i]){
                            return 0;
                        }
                    }
                }else{
                    if(mpa[pattern[i]] != tmp){
                        return 0;
                    }
                }
				break;
			}else if(str[pos] == ' '){
                string tmp = str.substr(last, pos-last);
                if(mpa.find(pattern[i]) == mpa.end()){
                    mpa[pattern[i]] = tmp;
                    if(mpb.find(tmp) == mpb.end()){
                        mpb[tmp] = pattern[i];
                    }else{
                        if(mpb[tmp] != pattern[i]){
                            return 0;
                        }
                    }
                }else{
                    if(mpa[pattern[i]] != tmp){
                        return 0;
                    }
                }
                i++;
                last = pos+1;
            }
			pos++;
        }
        return 1;
    }
int main(){
	cout << wordPattern("abba", "dog dog dog dog") << endl;
	return 0;
}

