#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>

using namespace std;

int N,K;

string to(int x){
    stringstream ss;
    ss << x;
    return ss.str();
}
void func(int n,int k){

    if(n>N)
        return;
    if(k == K){
		//cout << "OK" << endl;
        return;
    }

    for(int i=n+1;i<=N;i++){
        func(i,k+1);
    }

}

int main(){
    N = 28;
    K = 22;
    func(0,0);
	return 0;
}
