#include <stdio.h>
#include <string.h>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

//extract greater value less than N
int f(int k, std::vector<int> &v){
  int lo = 0, hi = v.size()-1, ans = 0;
  while(lo <= hi){
    int mid = (lo+hi) / 2;
    if(v[mid] == k){
      return v[mid];
    }else if(v[mid] > k){
      hi = mid-1;
    }else{
      ans = std::max(ans, v[mid]);
      lo = mid+1;
    }
  }
  return ans;
}

std::vector< int > fibonacciSum(int N) {
	std::vector<int> fib(2),ans;
  	fib[0] = fib[1] = 1;
  	for(int i = 2; fib[i-1] < N; i++){
    	fib.push_back(fib[i-1] + fib[i-2]);
  	}
  while(N){
    int K = f(N, fib);
    ans.insert(ans.begin(), K);
    N -= K;
  }
  //std::reverse(ans.begin(), ans.end());
  return ans;
}

int sum(int n){
	int ans = 0;
	for(int i = 1; i <= n+1; i++){
		for(int j = 1; j <= n+1; j++){
			ans += i+j;
		}
	}
	return ans == n*n*n + n*n + 3*n*n + 5*n+2;
}


int main() {
	cout << sum(3) << endl;
	return 0;
}
