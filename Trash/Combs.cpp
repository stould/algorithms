#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 3017
#define ll long long
#define INF 0xffffff
#define PI acos(-1)

using namespace std;

int N = 28;
int K = 8;
int cnt, comb;
void backTrack(int pos, int mask, int bits){
	if(N - pos < K - bits) {
		cnt++;
		return;
	}
	if(pos == N && bits != K) return;
	if(bits == K){
		comb++;
		//cout << "OK" << endl;
	}else{
		backTrack(pos+1, mask, bits);
		backTrack(pos+1, mask | (1 << pos), bits+1);
	}
}

int main(void){
	cnt = 0;
	comb = 0;
	backTrack(0, 0, 0);
	cout << comb << endl;
	cout << cnt << endl;
    return 0;
}

