#include <bits/stdc++.h>
using namespace std;
 
int count(int *num, int n, int s) {
   unsigned mask = 1, aux = 1;
   int ans = 0, sum = 0, a = 0;
   if(s == 0)
      ans++;
   while(aux <= (1 << n)-1) {
      a = 0; mask = 1;
      while(mask <= aux) {
         if(mask & aux) {
            sum += num[a];
         }
         a++;
         mask <<= 1;
      }
      if(sum == s)
         ans++;
      sum = 0;
      aux++;
   }
   return ans;
}
 
int main() {
   int n, s;
 
   while(cin >> n){
 
	   int num[n];
 
	   for(int i=0; i<n; i++) {
		   scanf("%d", &num[i]);
	   }
	   cin >> s;
 
	   cout << count(num, n, s) << endl;
 
   }
   return 0;
}
