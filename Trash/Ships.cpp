#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

int d,t,M,I,J,i,j,a,v[101][101];

void D(int l, int c, std::vector< std::string > &G){
	t ++;
	d += G[l][c] == '@';
	v[l][c] = 1;
	for(i=-1; i < 2; i++){
		for(j=-1; j < 2; j++){
			int L = l + i, C = c + j;
			if(L < M && C < M && L >= 0 && C >= 0 && G[L][C] != '.' && !v[L][C]){
				D(L,C,G);
			}
		}
	}
}

int battleships(int N, std::vector< std::string > G) {
	M=N;
	a = 0;
  for(I = 0; I < N; I++){
    for(J = 0; J < N; J++){
      if(G[I][J] != '.' && !v[I][J]){
		d = t = 0;
    	D(I,J,G);
        a += d < t;
      }
    }
  }
  return a;
}

int main(void) {
	vector<string> G;
	G.push_back("@.@.");
	G.push_back("@.x.");
	G.push_back("@.@.");
	G.push_back("....");
	cout << battleships(4,G) << endl;
	return 0;
}
