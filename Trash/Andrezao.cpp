

#include <bits/stdc++.h>

using namespace std;
 
std::string waysToTear(std::vector< int > Arr) {
  long long n = Arr[0], a = Arr[1], b = Arr[2], c = Arr[3], MOD = Arr[4], BIAS = Arr[5];
	
  int sum[n], cnt[n], can[n], suffix[n],v[n];

  suffix[n] = 0;
  sum[0] = 0;
  for(int i = 0; i < n; i++){
	v[i] = ((a * (long long)i * (long long)i + b * (long long)i + c) % MOD) - BIAS;
    sum[i] = v[i];
    if(i) sum[i] += sum[i-1];
  }
  for(int i = n-1; i >= 0; i--){
	suffix[i] = v[i] + suffix[i+1];
  }
  long long top = sum[n-1] / 3, ans = 0; 
  for(int i = 0; i < n; i++){
	if(sum[i] == top) cnt[i] = 1;
	else cnt[i] = 0;
	if(i) cnt[i] += cnt[i-1];
  }
  for(int i = 2; i < n; i++){
	if(suffix[i] == top){
		ans += cnt[i-2];
	}
  }
  if(ans == 0) return "0";
  std::string r = "";
  while(ans){
    r = char(ans % 10 + '0') + r;
    ans /= 10;
  }
  return r;
}




int main(void) {
	vector<int> a,b,c;
	a.push_back(3);
	a.push_back(0);
	a.push_back(0);
	a.push_back(0);
	a.push_back(1);
	a.push_back(0);

	b.push_back(6);
	b.push_back(0);
	b.push_back(1);
	b.push_back(1);
	b.push_back(2);
	b.push_back(1);

	c.push_back(80000);
	c.push_back(395);
	c.push_back(192);
	c.push_back(73);
	c.push_back(187);
	c.push_back(84);

	cout << waysToTear(a) << endl;
	cout << waysToTear(b) << endl;
	cout << waysToTear(c) << endl;
    return 0;
}


