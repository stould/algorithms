#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

int n, v[20], s;

int main(void) {
	while(cin >> n){
		for(int i = 0; i < n; i++){
			cin >> v[i];
		}
		cin >> s;
		int ans = 0;
		for(int i = 0; i < (1 << n); i++){
			int sum = 0;
			for(int j = 0; j < n; j++){
				if((i & (1 << j)) != 0){
					sum += v[j];
				}
			}
			if(sum == s) ans++;
		}
		printf("%d\n", ans);
	}
}
