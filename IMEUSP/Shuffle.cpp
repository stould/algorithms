#include <bits/stdc++.h>

using namespace std;



deque<char> shuffle(deque<char> &deck){
    deque<char> l,r;
    
    while(!deck.empty()){
        if(l.size() <= r.size()){
            l.push_back(deck.back());
        }else{
            r.push_back(deck.back());
        }
        deck.pop_back();
    }
    while(l.size() > 0){
        r.push_back(l.front());
        l.pop_front();
    }
    return r;
}

string str;
int K;

int main(void){
    deque<char> deck;
    cin >> str;
    for(int i = 0; i < (int) str.size(); i++){
        deck.push_back(str[i]);
    }
    cin >> K;
    while(K--){
        deck = shuffle(deck);
    }
    while(deck.size() > 0){
        cout << deck.front();
        deck.pop_front();
    }
    cout << endl;
    return 0;
}
