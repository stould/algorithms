#include <bits/stdc++.h>
#define MAXNM 201
using namespace std;


vector<vector<int> > freq(MAXNM, vector<int>(MAXNM,0));
int n,m,id;
char grid[MAXNM][MAXNM];
int dl[4] = {0,1,0,-1};
int dc[4] = {1,0,-1,0};
int vis[MAXNM][MAXNM];
int matrix[MAXNM][MAXNM];

struct rec{
    int l1, c1, l2, c2;
    rec(int l1_, int c1_, int l2_, int c2_){
        l1 = l1_;
        c1 = c1_;
        l2 = l2_;
        c2 = c2_;
    };
    rec(){}
};

void dfs(int l, int c, int id){
    vis[l][c] = id;
    for(int i = 0; i < 4; i++){
        int nl = dl[i] + l;
        int nc = dc[i] + c;
        if(nl >= 1 && nc >= 1 && nl <= n && nc <= m && vis[nl][nc] == -1 && grid[nl][nc] == '#'){
            dfs(nl,nc, id);
        }
    }
}


int getArea(int l1, int c1, int l2, int c2){
    return freq[l2][c2] - freq[l1-1][c2] - freq[l2][c1-1] + freq[l1-1][c1-1];
}

void preProcess(){
    for(int i = 1; i <= n; i++){
        for(int j = 1; j <= m; j++){
            freq[i][j] = freq[i][j-1] + matrix[i][j];
                
        }
    }
    for(int i = 1; i <= m; i++){
        for(int j = 2; j <= n; j++){
            freq[j][i] += freq[j-1][i];
        }
    }
}

pair<int, int> expand(int l, int c, int id){
    //cout << "X = " << l << " " << "Y = " << c <<  " id = " << id << endl;
    int tmpl = l, tmpc = c, bigArea = 0;
    //cout << "1" << endl;
    while(l + 1 <= n && vis[l + 1][c] == id){
        int token = getArea(tmpl, tmpc, l+1, c);
        int area = (((l+1) - tmpl) + 1) * ((c - tmpc) + 1);
        if(token != area) break;
        l++;
    }
    while(c + 1 <= m && vis[l][c + 1] == id){
        int token = getArea(tmpl, tmpc, l, c + 1);
        int area = ((l - tmpl) + 1) * (((c + 1) - tmpc) + 1);
        if(token != area) break;
        c++;
    }
    bigArea = ((l - tmpl) + 1) * ((c  - tmpc) + 1);
    int L = l, C = c;
    l = tmpl;
    c = tmpc;
    while(c + 1 <= m && vis[l][c + 1] == id){
        int token = getArea(tmpl, tmpc, l+1, c);
        int area = ((l - tmpl) + 1) * (((c + 1) - tmpc) + 1);
        if(token != area) break;
        c++;
    }
    while(l + 1 <= n && vis[l + 1][c] == id){
        int token = getArea(tmpl, tmpc, l, c + 1);
        int area = (((l+1) - tmpl) + 1) * ((c - tmpc) + 1);
        if(token != area) break;
        l++;
    }
    if(((l - tmpl) + 1) * ((c  - tmpc) + 1) > bigArea){
        return make_pair(l, c);
    }else{
        return make_pair(L,C);
    }
}

pair<int, int> expand2(int l, int c, int id){
    int tmpl = l, tmpc = c, bigArea = 0;
    //cout << l  << " " << c << " "<<endl;
    while(l + 1 <= n && vis[l + 1][c] == id && grid[l + 1][c] == '#'){
        int token = getArea(tmpl, tmpc, l+1, c);
        int area = (((l+1) - tmpl) + 1) * ((c - tmpc) + 1);
        //cout << l + 1 << " " << c << " "<<endl;
        //cout << "area = " << token << " " << area << endl;
        if(token != area) break;
        l++;
    }
    
    while(c + 1 <= m && vis[l][c + 1] == id && grid[l][c + 1] == '#'){
        int token = getArea(tmpl, tmpc, l, c + 1);
        int area = ((l - tmpl) + 1) * (((c + 1) - tmpc) + 1);
        //cout << "area = " << token << " " << area << endl;
        if(token != area) break;
        c++;
    }
    bigArea = ((l - tmpl) + 1) * ((c  - tmpc) + 1);
    int L = l, C = c;
    l = tmpl;
    c = tmpc;
    while(c + 1 <= m && vis[l][c + 1] == id && grid[l][c + 1] == '#'){
        int token = getArea(tmpl, tmpc, l, c + 1);
        int area = ((l - tmpl) + 1) * (((c + 1) - tmpc) + 1);
        //cout << "area = " << token << " " << area << endl;
        if(token != area) break;
        c++;
    }
    while(l + 1 <= n && vis[l + 1][c] == id && grid[l + 1][c] == '#'){
        int token = getArea(tmpl, tmpc, l+1, c);
        int area = (((l+1) - tmpl) + 1) * ((c - tmpc) + 1);
        //cout << l + 1 << " " << c << " "<<endl;
        //cout << "area = " << token << " " << area << endl;
        if(token != area) break;
        l++;
    }
    

    if(((l - tmpl) + 1) * ((c  - tmpc) + 1) > bigArea){
        return make_pair(l, c);
    }else{
        return make_pair(L,C);
    }
}

bool intersect(rec r, int l, int c){
    if (l >= r.l1 && l <= r.l2 && c >= r.c1 && c <= r.c2) {
        return true;
    }
    return false;
}

pair<int, int> expand3(int l, int c, int id, rec r){
    int tmpl = l, tmpc = c, bigArea = 0;
    //cout << "3" << endl;
    while(l + 1 <= n && vis[l + 1][c] == id && !intersect(r, l+1,c)){
        int token = getArea(tmpl, tmpc, l+1, c);
        int area = (((l+1) - tmpl) + 1) * ((c - tmpc) + 1);
        if(token != area) break;
        l++;
    }
    while(c + 1 <= m && vis[l][c + 1] == id && !intersect(r , l,c + 1)){
        int token = getArea(tmpl, tmpc, l, c + 1);
        int area = ((l - tmpl) + 1) * (((c + 1) - tmpc) + 1);
        //cout << "area = " << token << " " << area << endl;
        if(token != area) break;
        c++;
    }

    bigArea = ((l - tmpl) + 1) * ((c  - tmpc) + 1);
    int L = l, C = c;
    l = tmpl;
    c = tmpc;
    while(c + 1 <= m && vis[l][c + 1] == id && !intersect(r , l,c + 1)){
        int token = getArea(tmpl, tmpc, l, c + 1);
        int area = ((l - tmpl) + 1) * (((c + 1) - tmpc) + 1);
        //cout << "area = " << token << " " << area << endl;
        if(token != area) break;
        c++;
    }
    while(l + 1 <= n && vis[l + 1][c] == id && !intersect(r, l+1,c)){
        int token = getArea(tmpl, tmpc, l+1, c);
        int area = (((l+1) - tmpl) + 1) * ((c - tmpc) + 1);
        if(token != area) break;
        l++;
    }

    if(((l - tmpl) + 1) * ((c  - tmpc) + 1) > bigArea){
        return make_pair(l, c);
    }else{
        return make_pair(L,C);
    }
}

void paint(int l1, int c1, int l2, int c2, char color){
    for(int i = l1; i <= l2; i++){
        for(int j = c1; j <= c2; j++){
            grid[i][j] = color;
        }
    }
}

int main(void){
    cin >> n >> m;
    for(int i = 1; i <= n; i++){
        for(int j = 1; j <= m; j++){
            cin >> grid[i][j];
            if(grid[i][j] == '#'){
                matrix[i][j] = 1;
            }
            vis[i][j] = -1;
        }
    }
    preProcess();
    //cout << getArea(1,1,n,m) << endl;
    int comp = 0;
    for(int i = 1; i <= n; i++){
        for(int j = 1; j <= m; j++){
            //cout << freq[i][j] << " ";
            if(grid[i][j] == '#' && vis[i][j] == -1 && comp <= 2){
                dfs(i, j, comp);
                comp++;
            }
        }
        //cout << endl;
    }
    //cout << endl;
    if(comp > 2){
        cout << "NO" << endl;
    }else{
        if(comp == 2){
            int X = -1, Y = -1, cnt = 0;
            for(int i = 1; i <= n; i++){
                for(int j = 1; j <= m; j++){
                    if(vis[i][j] == 0){//comp 0
                        if(X == -1){
                            X = i;
                            Y = j;
                        }
                        cnt++;
                    }
                }
            }
            //cout << X << " " << Y << endl;
            pair<int, int> expanded = expand(X,Y,0);
            //cout << expanded.first << " "<<expanded.second << endl;
            //cout << (abs(X - expanded.first) + 1) * (1 + abs(Y - expanded.second )) << endl;
            if((abs(X - expanded.first) + 1) * (1 + abs(Y - expanded.second )) != cnt){
                cout << "NO" << endl;
            }else{
                rec rec1(X,Y, expanded.first,expanded.second);
                X = -1, Y = -1, cnt = 0;
                for(int i = 1; i <= n; i++){
                    for(int j = 1; j <= m; j++){
                        if(vis[i][j] == 1){//comp 0
                            if(X == -1){
                                X = i;
                                Y = j;
                            }
                            cnt++;
                        }
                    }
                }
                expanded = expand(X,Y,1);
                if((abs(X - expanded.first) + 1) * (1 + abs(Y - expanded.second ))  != cnt){
                    cout << "NO" << endl;
                }else{
                    rec rec2(X,Y, expanded.first,expanded.second);
                    cout << "YES" << endl;
                    paint(rec1.l1, rec1.c1, rec1.l2, rec1.c2, 'a');
                    paint(rec2.l1, rec2.c1, rec2.l2, rec2.c2, 'b');
                    for(int i = 1; i <= n; i++){
                        for(int j = 1; j <= m; j++){
                            cout << grid[i][j];
                        }
                        cout << endl;
                    }
                }
            }
        }else if(comp == 1){
            int X = -1, Y = -1, cnt = 0;
            for(int i = 1; i <= n; i++){
                for(int j = 1; j <= m; j++){
                    if(vis[i][j] == 0){//comp 0
                        if(X == -1){
                            X = i;
                            Y = j;
                        }
                        cnt++;
                    }
                }
            }
            pair<int, int> expanded = expand2(X,Y, 0);
            if((abs(X - expanded.first) + 1) * (1 + abs(Y - expanded.second )) == cnt){
                if(cnt > 1){
                    int difl = abs(X - expanded.first);
                    int difc = abs(Y - expanded.second);
                    if(difl > difc){
                        rec rec1(X,Y, expanded.first,expanded.second);
                        X = expanded.first;
                        rec rec2(X,Y, expanded.first,expanded.second);
                        cout << "YES" << endl;
                        paint(rec1.l1, rec1.c1, rec1.l2, rec1.c2, 'a');
                        paint(rec2.l1, rec2.c1, rec2.l2, rec2.c2, 'b');
                        for(int i = 1; i <= n; i++){
                            for(int j = 1; j <= m; j++){
                                cout << grid[i][j];
                            }
                            cout << endl;
                        }
                    }else{
                        rec rec1(X,Y, expanded.first,expanded.second);
                        Y = expanded.second;
                        rec rec2(X,Y, expanded.first,expanded.second);
                        cout << "YES" << endl;
                        paint(rec1.l1, rec1.c1, rec1.l2, rec1.c2, 'a');
                        paint(rec2.l1, rec2.c1, rec2.l2, rec2.c2, 'b');
                        for(int i = 1; i <= n; i++){
                            for(int j = 1; j <= m; j++){
                                cout << grid[i][j];
                            }
                            cout << endl;
                        }
                    }
                }else{
                    cout << "NO" << endl;
                }
            }else{
                rec rec1(X,Y, expanded.first,expanded.second);
                X = -1, Y = -1;
                for(int i = 1; i <= n; i++){
                    for(int j = 1; j <= m; j++){
                        if(vis[i][j] == 0 && !intersect(rec1, i, j)){//comp 0
                            if(X == -1){
                                X = i;
                                Y = j;
                            }
                        }
                    }
                }
                expanded = expand3(X,Y, 0, rec1);
                rec rec2(X,Y, expanded.first,expanded.second);
                paint(rec1.l1, rec1.c1, rec1.l2, rec1.c2, 'a');
                paint(rec2.l1, rec2.c1, rec2.l2, rec2.c2, 'b');
                bool can = 1;
                for(int i = 1; i <= n; i++){
                    for(int j = 1; j <= m; j++){
                        if(grid[i][j] == '#'){
                            can = 0;
                        }
                    }
                }
                if(can){
                    cout << "YES" << endl;
                    for(int i = 1; i <= n; i++){
                        for(int j = 1; j <= m; j++){
                            cout << grid[i][j];
                        }
                        cout << endl;
                    }
                }else{
                    cout << "NO" <<endl;
                }
            }
        }else{
            cout << "NO" << endl;
        }
    }
    return 0;
}
