#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
#define MAXN 100001
#define MAXV 1000001
#define LOGVAL 20
#define ll long long

using namespace std;

int n, U, V, deg[MAXN], depth[MAXN];
ll R[MAXN], sum[MAXN];
vector<vector<int> > g(MAXN);

int dfs(int u, int parent, int d){
	depth[u] = d;
	for(int i = 0; i < g[u].size(); i++){
		int next = g[u][i];
		if(next != parent){
			sum[u] += 1 + dfs(next, u, d+1);
		}
	}
	return sum[u];
	
}

ll calc(int node){
	vector<ll> c;
	vector<ll> s;
	ll last = 0;
	for(int i = 0; i < g[node].size(); i++){
		if(depth[node] < depth[g[node][i]]){
			c.push_back(sum[g[node][i]] + 1);
			s.push_back(last + sum[g[node][i]] + 1);
			last = last + sum[g[node][i]] + 1;
		}
	}
	ll ans = 0;
	for(int i = 0; i < c.size(); i++){
		ans += c[i] * (last - s[i]);
	}
	return ans + last;
}



int main(){
	//ios::sync_with_stdio(0);
	freopen("lca.in", "r", stdin);
	freopen("lca.out", "w", stdout);
	cin >> n;

	for(int i = 0; i < n-1; i++){
		cin >> U >> V;
		U--;
		V--;
		g[U].push_back(V);
		g[V].push_back(U);
		deg[U]++;
		deg[V]++;
	}

	dfs(0, -1, 0);
	ll bigger = 0;
	for(int i = 0; i < n; i++){
		R[i] = calc(i);
		bigger = max(bigger, R[i]);
	}
	vector<int> ans;
	for(int i = 0; i < n; i++){
		if(R[i] == bigger){
			ans.push_back(i+1);
		}
	}
	cout << ans.size() << endl;	
	for(int i = 0; i < ans.size(); i++){
		cout << ans[i] << " ";
	}
	cout << endl;
	
    return 0;
}
