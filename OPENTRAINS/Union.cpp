#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
#define MAXN 100001
#define MAXV 1000001
#define LOGVAL 20
using namespace std;
 
int n, q, U, V, C, root[MAXN];
vector<vector<pair<int, int> > > g(MAXN);
 
//LCA
int idx;
int H[MAXN];
int L[MAXN * 2];
int E[MAXN * 2];
int vis[MAXN];
int dp[MAXN*2][LOGVAL];
int pre[MAXN*2];
 
//Persistent SegTree
int INDEX;
int Lef[MAXN*4*LOGVAL];
int Rig[MAXN*4*LOGVAL];
int S[MAXN*4*LOGVAL];
 
struct SegTreePersistent{
 
    SegTreePersistent(){
        build(0, 0, MAXV);
    }
 
    void build(int node, int l, int r){
        if(l == r){
            return;
        }else{
            int mid = (l+r) / 2;
            Lef[node] = INDEX++;
            Rig[node] = INDEX++;
            build(Lef[node], l, mid);
            build(Rig[node], mid+1, r);
        }
    }
 
    int query(int node, int l, int r, int K){
        if(l > K){
            return 0;
        }else if(r <= K){
            return S[node];
        }else{
            int mid = (l+r) / 2;
            int left = query(Lef[node], l, mid, K);
            int right = query(Rig[node], mid+1, r, K);
            return left + right;
        }
    }
 
    int update(int node, int l, int r, int pos){
        int next = INDEX++;
        Lef[next] = Lef[node];
        Rig[next] = Rig[node];
        S[next] = S[node];
        if(l == r){
            S[next]++;
        }else{
            int mid = (l+r) / 2;
            if(pos <= mid){
                Lef[next] = update(Lef[node], l, mid, pos);
            }else{
                Rig[next] = update(Rig[node], mid+1, r, pos);
            }
            S[next] = S[Lef[next]]+S[Rig[next]];
        }
        return next;
    }
};
 
struct LCA{
   
    LCA(){
        build();
    }
 
    void build(){
		int base = 1;
		int pot = 0;
		for(int i = 0; i < 2*MAXN; i++){
			if(i >= base * 2){
				pot++;
				base *= 2;
			}
			pre[i] = pot;
			dp[i][0] = i;
		}
		base = 2;
		pot = 1;
		while(base <= 2*n){
			for(int i = 0; i + base / 2 < 2*n; i++){
				int before = base / 2;
				if(L[dp[i][pot-1]] < L[dp[i + before][pot-1]]){
					dp[i][pot] = dp[i][pot-1];
				}else{
					dp[i][pot] = dp[i + before][pot-1];
				}
			}
			base *= 2;
			pot++;
		}
    }
 
    int getLca(int u, int v){
        int l = H[u];
        int r = H[v];
        if(l > r){
            swap(l,r);
        }
		int len = r-l+1;
		if(len == 1){
			return E[dp[r][0]];
		}else{
			int base = (1 << pre[len]);
			int pot = pre[len];
			if(L[dp[l][pot]] < L[dp[r-base+1][pot]]){
				return E[dp[l][pot]];
			}else{
				return E[dp[r-base+1][pot]];
			}
		}
    }
 
};
 
void dfs(int x, int depth, SegTreePersistent &tree){
    vis[x] = 1;
    if(H[x] == -1) H[x] = idx;
    L[idx] = depth;
    E[idx++] = x;
    for(int i = 0; i < g[x].size(); i++){
        int next = g[x][i].first;
        int cost = g[x][i].second;
        if(!vis[next]){
            root[next] = tree.update(root[x], 0, MAXV, cost);
            dfs(next, depth+1, tree);
            L[idx] = depth;
            E[idx++] = x;
        }
    }
}
 
 
int main(){
    freopen("union.in", "r", stdin);
    freopen("union.out", "w", stdout);
    memset(H,-1,sizeof(H));
    INDEX = 1;
    idx = 0;
    scanf("%d", &n);
    for(int i = 0; i < n-1; i++){
        scanf("%d%d%d", &U, &V, &C);//from U to V with cost C -> Two hands
        U--;
        V--;
        g[U].push_back(make_pair(V, C));
        g[V].push_back(make_pair(U, C));
    }
    scanf("%d", &q);
    SegTreePersistent segtree;
    dfs(0,0,segtree);
    LCA lca;
    while(q--){
        scanf("%d%d%d", &U, &V, &C);
        U--;
        V--;
        int parent = lca.getLca(U,V);
        int ans = segtree.query(root[U], 0, MAXV, C) + segtree.query(root[V], 0, MAXV, C) -2*segtree.query(root[parent], 0, MAXV, C);
        printf("%d\n", ans);
    }
    return 0;
}
