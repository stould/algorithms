#include <bits/stdc++.h>
#define MAXN 101
#define INF 0x3f3f3f3f

using namespace std;

int dp[MAXN][MAXN], w[MAXN];
int C;

int freq[MAXN];

int getCost(int a, int b){
    return (freq[b] - freq[a-1]) * (b - a + 1);
}

int func(int l, int r){
    if(l == r){
        return 0;
    }else{
        int &ans = dp[l][r];
        if(ans == -1){
            ans = INF;
            for(int i = l; i < r; i++){
                int left = getCost(l, i);
                int right = getCost(i+1, r);
                ans = min(max(0, left + func(l, i) + right + func(i+1, r) - C), ans);
            }
        }
        return ans;
    }
}

int N, T;

int main() {
    cin >> N >> T;
    for(int i = 1; i <= N; i++){
        cin >> w[i];
    }
    for(int i = 1; i <= N; i++){
        freq[i] = w[i] + freq[i-1];
    }
    int lo = 0, hi = 1000001;
    while(lo <= hi){
        int mid = lo + (hi - lo) / 2;
        C = mid;
        memset(dp, -1, sizeof(dp));
        if(func(1, N) > T){
            lo = mid+1;
        }else{
            hi = mid-1;
        }
    }
    cout << lo << endl;
    return 0;
}
