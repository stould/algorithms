#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int t,n;

int main() {
    cin >> t;
    while(t--){
        cin >> n;
        if(n <= 3){
            cout << n << endl;
        }else{
            if(n % 2 == 0){
                cout << 3 << endl;
            }else{
                cout << 4 << endl;
            }
        }
    }
    return 0;
}
