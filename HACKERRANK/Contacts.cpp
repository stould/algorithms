#include <bits/stdc++.h>
#define MAXN 100001*22

using namespace std;

int trie[MAXN][27];
int cnt[MAXN];
int id = 1;

void addWord(string &s) {
    int root = 0;
    for(int i = 0; i < s.size(); i++){
        int node = s[i] - '0';
        cnt[root]++;
        if(trie[root][node] == -1){
            trie[root][node] = id;
            root = id++;
        }else{
            root = trie[root][node];
        }
    }
    cnt[root]++;
}

int find(string &s){
    int root = 0;
    for(int i = 0; i < s.size(); i++){
        int node = s[i] - '0';
        //cout << cnt[root] << endl;
        if(trie[root][node] == -1){
            return 0;
        }else{
            root = trie[root][node];
        }
    }
    //cout << endl;
    return cnt[root];
}

string op, word;
int n;
int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    memset(trie,-1,sizeof(trie));
    cin >> n;
    for(int i = 0; i < n; i++){
        cin >> op >> word;
        if(op == "add"){
            addWord(word);
        }else if(op == "find"){
            int total = find(word);
            cout << total << endl;
        }
    }
    return 0;
}
