#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main(){
    int n;
    int m;
    cin >> n >> m;
    vector<string> topic(n);
    for(int topic_i = 0;topic_i < n;topic_i++){
       cin >> topic[topic_i];
    }
    //could be solved faster with trie.
    int big = 0, countBig = 0;
    for(int i = 0; i < n; i++){
        for(int j = i+1; j < n; j++){
            int ones = 0;
            for(int k = 0; k < m; k++){
                if(topic[i][k] == '1' || topic[j][k] == '1'){
                    ones++;
                }
            }
            if(ones > big){
                big = ones;
                countBig = 1;
            }else if(ones == big){
                countBig++;
            }
        }
    }
    cout << big << endl << countBig << endl;
    return 0;
}
