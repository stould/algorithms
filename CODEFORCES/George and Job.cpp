#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <math.h>
#include <string.h>
#include <stack>
#include <map>
#include <set>
#define ll long long
#define INF 12345678910121LL

using namespace std;



int n,m,k;
ll v[7000], dp[5001][5001];
ll sum[7000];

ll func(int idx, int used){
	if(used == 0){
		return 0;
	}else if(idx > n){
		return -INF;
	}else{
		ll &ans = dp[idx][used];
		if(ans == -1){
			ans = 0;
			ans = max(ans, func(idx+1, used));
			ans = max(ans, (sum[idx] - sum[idx-m]) + func(idx+m, used-1));
			
		}
		return ans;
	}
}

int main(void){
	
	while(cin >> n >> m >> k){
		memset(dp,-1,sizeof(dp));
		sum[0] = 0;
		for(int i = 1; i <= n; i++){
		    cin >> v[i];
			sum[i] = sum[i-1] + v[i];
		}
		cout << func(m, k) << endl;
	}
	return 0;
}
