#include <bits/stdc++.h>
#define MAXN 100001
#define ll long long int

using namespace std;


map<int, int> cnt[MAXN];
int id[MAXN];
int mx[MAXN];
ll sum[MAXN], ans[MAXN];
vector<vector<int> > graph(MAXN);

bool vis[MAXN];

//into the merge step, you have to do what the problems asks
void merge(int u, int v){
    //we have to merge the smaller into the greater
    if((int) cnt[ id[u] ].size() < (int) cnt[ id[v] ].size()){
        swap(id[u], id[v]);
    }
    int root = id[u];
    for(map<int, int>::iterator it = cnt[id[v]].begin(); it != cnt[id[v]].end(); it++){
        int col = (*it).first;
        int tot = (*it).second;
        cnt[root][col] += tot;
        if(cnt[root][col] > mx[root]){
            mx[root] = cnt[root][col];
            sum[root] = col;
        }else if(cnt[root][col] == mx[root]){
            sum[root] += col;
        }
    }
}

void dfs(int u){
    vis[u] = 1;
    for(int i = 0; i < (int) graph[u].size(); i++){
        int v = graph[u][i];
        if(!vis[v]){
            dfs(v);
            merge(u, v);
        }
    }
    ans[u] = sum[id[u]];
}

int n, color[MAXN];

int main(void){
    cin >> n;
    
    for(int i = 1; i <= n; i++){
        cin >> color[i];
        cnt[i][color[i]] = 1;
        mx[i] = 1;
        id[i] = i;
        sum[i] = color[i];
    }

    for(int i = 1; i <= n; i++){
        int u,v;
        cin >> u >> v;
        graph[u].push_back(v);
        graph[v].push_back(u);
    }
    //root = 1
    dfs(1);
    cout << ans[1];
    for(int i = 2; i <= n; i++){
        cout << " " << ans[i];
    }
    cout << endl;
    return 0;
}
