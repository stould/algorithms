#include <bits/stdc++.h>
#define MAXN 500001
#define ll long long int

using namespace std;

int sz[MAXN], height[MAXN], cntF[MAXN];
vector<int>  graph[MAXN];
bool heavy[MAXN], cntP[MAXN][26], ans[MAXN];

string num;

void getSz(int u, int parent, int depth = 1){
    sz[u] = 1;
    height[u] = depth;
    for(int i = 0; i < (int) graph[u].size(); i++){
        int v = graph[u][i];
        getSz(v, u, depth + 1);
        sz[u] += sz[v];
    }
}


void add(int u){
    cntF[height[u]] -= cntP[height[u]][num[u]-'a'];
    cntP[height[u]][num[u]-'a'] ^= 1;
    cntF[height[u]] += cntP[height[u]][num[u]-'a'];
    for(int i = 0; i < (int)graph[u].size(); i++){
        int v = graph[u][i];
        if(!heavy[v]){
            add(v);
        }
    }
}

void remove(int u){
    cntF[height[u]] -= cntP[height[u]][num[u]-'a'];
    cntP[height[u]][num[u]-'a'] ^= 1;
    cntF[height[u]] += cntP[height[u]][num[u]-'a'];
    for(int i = 0; i < (int)graph[u].size(); i++){
        int v = graph[u][i];
        if(!heavy[v]){
            remove(v);
        }
    }    
}

vector<pair<int, int> > query[MAXN];

void dfs(int u, bool keep){
    int bigChild = -1, cnt = -1;
    for(int i = 0; i < (int)graph[u].size(); i++){
        int v = graph[u][i];
        if(sz[v] > cnt){
            cnt = sz[v];
            bigChild = v;
        }
    }
    for(int i = 0; i < (int)graph[u].size(); i++){
        int v = graph[u][i];
        if(v != bigChild){//goes down the light nodes
            dfs(v, 0);
        }
    }
    if(bigChild != -1){//goes down the heavy node only once
        dfs(bigChild, 1);
        heavy[bigChild] = 1;
    }
    add(u);
    for(int i = 0; i < (int)query[u].size(); i++){
        ans[query[u][i].first] = cntF[query[u][i].second] <= 1;
    }
    if(bigChild != -1){
        heavy[bigChild] = 0;
    }
    if(keep == 0){
        remove(u);
    }
}

int n, m, v, d;

int main(void){
    ios::sync_with_stdio(0);
    cin.tie(0);
    cin >> n >> m;
    for(int u = 2; u <= n; u++){
        cin >> v;
        graph[v].push_back(u);
    }
    cin >> num;
    num = "0"+num;
    for(int i = 0; i < m; i++){
        cin >> v >> d;
        query[v].push_back(make_pair(i, d));
    }
    getSz(1,-1);
    dfs(1, 0);
    for(int i = 0; i < m; i++){
        cout << (ans[i] ?  "Yes" : "No") << "\n";
    }
    return 0;
}
