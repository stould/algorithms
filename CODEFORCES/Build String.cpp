#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 555
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
 
using namespace std;
 
 
int vparent[MAXN], eparent[MAXN], dist[MAXN], capparent[MAXN];
 
struct Edge{
 
    int v, c, cap, idrev;
 
    Edge(int V, int C, int F, int ID){
        v = V;
        c = C;
        cap = F;
        idrev = ID;
    }
    Edge(){}
};
 
vector<vector<Edge> > g(MAXN);
 
void addEdge(int u, int v, int c, int f){
    g[u].push_back(Edge(v,c,f,g[v].size()));
    g[v].push_back(Edge(u,-c,0,g[u].size()-1));
}
 
struct Less{
    bool operator () (int a, int b) {
        return dist[a] > dist[b];
    }
};
 
pair<int, int> mflow(int S, int T, int goal){
    int go = 0;
    int flowAns = 0, costAns = 0;
    while(flowAns < goal){
        go = 0;
        memset(eparent,-1,sizeof(eparent));
        memset(vparent,-1, sizeof(vparent));
        memset(dist,63,sizeof(dist));
        for(int i = 0; i < T; i++){
            eparent[i] = vparent[i] = -1;
            dist[i] = INF;
        }
        priority_queue<int, vector<int>, Less> q;
        q.push(S);
        dist[S] = 0;
        while(!q.empty()){
            int top = q.top(); q.pop();
            for(int i = 0; i < g[top].size(); i++){
                Edge &neigh = g[top][i];
                int next = neigh.v;//Vizinho
                int cost = neigh.c;//Custo
                int cap = neigh.cap;//capacidade
                if(dist[top] + cost < dist[next] && cap > 0){
                    vparent[next] = top;
                    eparent[next] = i;
                    capparent[next] = cap;
                    dist[next] = dist[top] + cost;
                    q.push(next);
 
                }
            }
        }
        if(dist[T] == INF){
            flowAns = -1;
            break;
        }
        int totalFlow = INF;
        for(int i = T; i != S; i = vparent[i]){
            totalFlow = min(capparent[i], totalFlow);
        }
        int from = T;
        while(from != S){
            Edge &to = g[vparent[from]][eparent[from]];
            g[to.v][to.idrev].cap += totalFlow;
            to.cap -= totalFlow;
            from = vparent[from];
        }
        flowAns += totalFlow;
        costAns += dist[T];
 
    }
    return make_pair(flowAns, costAns);
}
 
string str;
string SS[MAXN];

const int S = 0, T = 450;
int n, maxFold[MAXN], cnt[MAXN], mp[MAXN], idx = 145;

int main(void){
	cin >> str >> n;
	for(int i = 0; i < str.size(); i++){
		cnt[str[i]-'a']++;
	}
	memset(mp,-1,sizeof(mp));
	for(int i = 1; i <= n; i++){
		cin >> SS[i] >> maxFold[i];
//		cout << "From " << S << " to " << i << ", cost = " << 0 << endl;
		addEdge(S, i, 0, maxFold[i]);
		for(int j = 0; j < SS[i].size(); j++){
			if(mp[SS[i][j]-'a'] == -1){
				mp[SS[i][j]-'a'] = idx++;
			}
//			cout << "From " << i << " to " << SS[i][j] << ", cost = " << j+1 << endl;
			addEdge(i,mp[SS[i][j]-'a'],i,1);
		}
	}
	for(int i = 0; i < 26; i++){
		if(mp[i] != -1){
//			cout << "From " << mp[i] << " to " << T << ", cost = " << 0 << endl;
			addEdge(mp[i],T,0,cnt[i]);
		}
	}
	pair<int,int> ans = mflow(S,T,str.size());
	if(ans.first == -1){
		cout << ans.first << endl;
	}else{
		cout << ans.second << endl;
	}
	return 0;
}








