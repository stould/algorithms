#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>

#define MAXN 2000003
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

int n;

int v, freq[MAXN];

int main(){
	ios::sync_with_stdio(0);
	cin >> n;
	for(int i = 0; i < n; i++){
		cin >> v;
		freq[v]++;
	}
	int ans = 0;
	for(int i = 0; i <= 2000000; i++){		
		freq[i+1] += freq[i] / 2;
		ans += freq[i] % 2;
	}
	cout << ans << endl;
	return 0;
}
