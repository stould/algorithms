#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 207
#define MAXM 1007
#define ll long long
#define INF 0x3f3f3f3f3fLL
#define PI acos(-1)
 
using namespace std;

int n, sz[MAXN];
ll dp[MAXN][MAXM], g[MAXN][MAXM];

void path(int skill){
	if(skill < 0) return;
	int idx = -1;
	ll v = INF;
	for(int i = 0; i < sz[skill]; i++){
		if(dp[skill][i] < v){
			v = dp[skill][i];
			idx = i;
		}
	}
	path(skill-1);
	printf("%d ", idx+1);
}

int main(void){
	scanf("%d", &n);
	for(int i = 0; i < n; i++){
		scanf("%d", &sz[i]);
	}
	for(int i = 0; i < n; i++){
		for(int j = 0; j < sz[i]; j++){
			scanf("%lld", &g[i][j]);		
		}
	}
	for(int i = 1; i < MAXN; i++){
		for(int j = 0; j < MAXM; j++){
			dp[i][j] = INF;
		}
	}
	vector<ll> aux(1001);
	for(int i = 1; i < n; i++){
		for(int j = 0; j < sz[i]; j++){
			for(int k = 0; k < sz[i-1]; k++){//from i-j to 
				if(max(dp[i-1][k], abs(g[i-1][k] - g[i][j])) < dp[i][j]){
					dp[i][j] = max(dp[i-1][k], abs(g[i-1][k] - g[i][j]));
					aux[j] = max(g[i][j], g[i-g[i][j] = aux[j];
	}
	for(int i = 0; i < n; i++){
		for(int j = 0; j < sz[i]; j++){
			cout << g[i][j] << " ";
		}
		cout << endl;
	}
	ll ans = INF;
	for(int i = 0; i < 1001; i++){
		if(dp[n-1][i] < ans){
			ans = dp[n-1][i];
		}
	}
	printf("%lld\n", ans);
	path(n-1);
	return 0;
}
