#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1
#define MOD 1000000007

using namespace std;

int game[100003*2];

pair<int,int> f(int last, int A, int B){
	int sum = A+B;
	game[0] = last;
	for(int i = 1; (A+B) > 0; i++){
		//		cout << A << " " << B << endl;
		if(A == 0){
			B--;
			game[i] = 1;
		}else if(B == 0){
			A--;
			game[i] = 0;
		}else{
			if(i % 2 == 1){//petya - distinct
				if(game[i-1] == 0) {//if last = A
					B--;
					game[i] = 1;
				}else{
					A--;
					game[i] = 0;
				}
			}else{
				if(game[i-1] == 0){//vasya
					A--;
					game[i] = 0;
				}else{
					B--;
					game[i] = 1;
				}
			}
		}
	}

	pair<int,int> ans = make_pair(0,0);
	for(int i = 1; i <= sum; i++){

		if(game[i] == game[i-1]){
			ans.first += 1;
		}else{
			ans.second += 1;
		}
	}

	return ans;
}

int R,B;

int main(void) {
	cin >> R >> B;
	pair<int,int> startA = f(0,R-1,B);
	//	cout << startA.first << " " << startA.second << endl;
	pair<int,int> startB = f(1,R,B-1);
	//cout << startB.first << " " << startB.second << endl;
	vector<pair<int,int> > tmp;
	tmp.push_back(startA);
	tmp.push_back(startB);
	sort(tmp.begin(), tmp.end());
	cout << tmp[1].first << " " << tmp[1].second << endl;
	return 0;
}
