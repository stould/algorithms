#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>

#define MAXN 100003
#define MAXK 13
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

int n, k;
ll ft[MAXN][MAXK], dp[MAXN][MAXK];
int val[MAXN];

struct FenwickTree{
    int sz;
 
    FenwickTree(int N){
        for(int i = 0; i < MAXN; i++){
			for(int j = 0; j < MAXK; j++){
				ft[i][j] = 0;
			}

        }
        sz = N;
    }
 
    ll query(int idx, int K){
        ll ans = 0;
        while(idx > 0){
            ans += ft[idx][K];
            idx -= (idx & -idx);
        }
        return ans;
    }
 
    ll query(int a, int b, int K){
        if(a == 1){
            return query(b, K);
        }else{
            return query(b, K) - query(a-1, K);
        }
    }
 
    void update(int idx, ll val, int K){
        while(idx <= sz){
            ft[idx][K] += val;
            idx += (idx & -idx);
        }
    }
};



int main(void) {
	scanf("%d%d", &n, &k);
	FenwickTree bit(n);
	ll ans = 0;
	for(int i = 1; i <= n; i++){
		scanf("%d", &val[i]);
	}
	for(int j = 1; j <= n; j++){
		bit.update(val[j], 1, 0);
		for(int i = 1; i <= k; i++){
			dp[j][i] = bit.query(val[j]-1, i-1);
			bit.update(val[j], dp[j][i], i);
		}
	}
	ans = bit.query(n, k);
	cout << ans << endl;
	return 0;
}
