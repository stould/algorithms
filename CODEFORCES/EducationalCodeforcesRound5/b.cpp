#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 101
#define ll long long
#define INF 0x3f3f3f3f3f3f3fLL
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

int n, m;

ll g[MAXN][MAXN];

int main(void) {
	cin >> n >> m;
	for(int i = 0; i < n; i ++){
		for(int j = 0; j < m; j++){
			cin >> g[i][j];
		}
	}
	ll ans = 0;
	for(int i = 0; i < n; i++){
		ll low = 100000000000LL;
		for(int j = 0; j < m; j++){
			low = min(low, g[i][j]);
		}
		ans = max(low, ans);
	}
	cout << ans << endl;
}
