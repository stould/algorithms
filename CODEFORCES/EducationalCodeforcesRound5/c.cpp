#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>

#define MAXN 1001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

int n, m;



bool vis[MAXN][MAXN];
int id[MAXN*MAXN];
int height[MAXN*MAXN];
int sum[MAXN*MAXN];
char grid[1001][1001];
int dl[4] = {1,0,-1,0};
int dc[4] = {0,1,0,-1};

int pos(int line, int col){
	return m * line + col;
}

void init(){
	for(int i = 0; i < MAXN*MAXN; i++){
		id[i] = i;
		height[i] = 1;
	}
	memset(vis,0,sizeof(vis));
}
 
int root(int x){
	if(x == id[x]){
		return x;
	}else{
		return id[x] = root(id[x]);
	}
}
 
bool find(int a, int b){
	return root(a) == root(b);
}
 
void unite(int a, int b){
	int roota = root(a);
	int rootb = root(b);
	if(roota != rootb){
		if(height[roota] >= height[rootb]){
			id[rootb] = roota;
			height[roota] += height[rootb];
			sum[roota] += sum[rootb];
		}else{
			id[roota] = rootb;
			height[b] += height[roota];
			sum[rootb] += sum[roota];
		}
	}
}



void dfs(int L, int C){
	vis[L][C] = 1;
	sum[root(pos(L,C))]++;
	for(int i = 0; i < 4; i++){
		int nl = dl[i] + L;
		int nc = dc[i] + C;
		if(nl >= 0 && nc >= 0 && nl < n && nc < m){
			if(!vis[nl][nc] && grid[nl][nc] == '.'){
				unite(pos(L,C), pos(nl, nc));
				dfs(nl, nc);
			}
		}
	}
}


int main(void) {
	scanf("%d%d", &n, &m);
	init();
	for(int i = 0; i < n; i++){
		for(int j = 0; j < m; j++){
			scanf(" %c", &grid[i][j]);
		}
	}
	for(int i = 0; i < n; i++){
		for(int j = 0; j < m; j++){
			if(!vis[i][j] && grid[i][j] == '.'){
				dfs(i,j);
			}
		}
	}
	for(int i = 0; i < n; i++){
		for(int j = 0; j < m; j++){
			if(grid[i][j] == '*'){
				int ans = 0;
				set<int> idx;
				for(int k = 0; k < 4; k++){
					int nl = dl[k] + i;
					int nc = dc[k] + j;
					if(nl >= 0 && nc >= 0 && nl < n && nc < m && grid[nl][nc] == '.'){
						idx.insert(root(pos(nl, nc)));
					}
				}
				for(set<int>::iterator k = idx.begin(); k != idx.end(); k++){
					ans += sum[(*k)];
				}
				printf("%d",(1+ans) % 10);
			}else{
				printf(".");
			}
		}
		puts("");
	}
	return 0;
}
