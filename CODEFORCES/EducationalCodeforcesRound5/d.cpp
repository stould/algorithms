#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 1000001
#define ll long long
#define INF 0x3f3f3f3f3f3f3fLL
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

int n, k;

int cnt[MAXN];

int val[MAXN/2+1];

bool can(int mid, bool ans){
	int diff = 0;
	memset(cnt,0,sizeof(cnt));
	for(int i = 0; i < mid; i++){
		if(cnt[val[i]] == 0){
			diff++;
		}
		cnt[val[i]]++;
	}
	if(diff <= k) {
		if(ans) printf("%d %d\n", 1, mid);
		return 1;
	}
	for(int i = 1; i + mid <= n; i++){
		cnt[val[i-1]]--;
		if(cnt[val[i-1]] == 0){
			diff--;
		}
		if(cnt[val[i+mid-1]] == 0){
			diff++;
		}
		cnt[val[i+mid-1]]++;
		if(diff <= k){
			if(ans) printf("%d %d\n", i+1, i+mid);
			return 1;
		}

	}
	return 0;
}

int main(void) {
	scanf("%d%d",&n, &k);
	for(int i = 0; i < n; i++){
		scanf("%d", &val[i]);
	}
	int lo = 0, hi = n, ans = 0;
	while(lo <= hi){
		int mid = (lo+hi) / 2;
		if(can(mid, 0)){
			lo = mid+1;
			ans = max(ans, mid);
		}else{
			hi = mid-1;
		}
	}
	can(ans, 1);
	return 0;
}
