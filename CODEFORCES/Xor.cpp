#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
#define MAXN 100001
#define LOGVAL 18
#define mp make_pair
#define ll long long
 
using namespace std;

int n, q, k, val[MAXN];
int BLOCK_SIZE = 320;
int sum[MAXN];

struct pt{
	int l, r, id;
	
	pt(){}
	pt(int L, int R, int ID){
		l = L;
		r = R;
		id = ID;
	}

	bool operator < (const pt &o) const{
		return r < o.r;
	}
};

vector<vector<pt> > cons(BLOCK_SIZE);
ll resp[MAXN];
int seen[1 << 20];
ll answer;

void add(int pos){
	answer += seen[sum[pos] ^ k];
	seen[sum[pos]]++;
}

void remove(int pos){
	seen[sum[pos]]--;
	answer -= seen[sum[pos] ^ k];
}

void process(int pos){
	int l = pos*BLOCK_SIZE, r = pos*BLOCK_SIZE, ql, qr, id;
	answer = 0;
	sort(cons[pos].begin(), cons[pos].end());
	for(int i = 0; i < cons[pos].size(); i++){
		ql = cons[pos][i].l;
		qr = cons[pos][i].r;
		id = cons[pos][i].id;
		while(r < qr){
			add(r);
			r++;
		}
		while(l < ql){
			remove(l);
			l++;
		}
		while(l > ql){
			l--;
			add(l);
		}
		resp[id] = answer;

	}
	for(int j = l; j < r; j++){
		remove(j);
	}
}

int main(){
	ios::sync_with_stdio(0); cin.tie(0);
	cin >> n >> q >> k;
	for(int i = 0; i < n; i++){
		cin >> val[i];
		resp[i] = -1;
	}
	for(int i = 1; i <= n; i++){
		sum[i] = sum[i-1] ^ val[i - 1];
	}
	for(int i = 0; i < q; i++){
		pt newQ;
		cin >> newQ.l >> newQ.r;
		newQ.id = i;
		newQ.l--;
		newQ.r++;
		cons[newQ.l / BLOCK_SIZE].push_back(newQ);
	}

	for(int i = 0; i < BLOCK_SIZE; i++){
		if(cons[i].size()){
			process(i);
		}
	}
	
	for(int i = 0; i < q; i++){
		cout << resp[i] << endl;
	}
	
    return 0;
}
