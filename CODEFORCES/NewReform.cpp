#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
#define MAXN 100003
#define ll long long

using namespace std;

int n, m, U, V;

bool active[MAXN];
vector<vector<int> > graph(MAXN);
map<pair<int, int>, int> edge;

void removeEdge(int from, int to, int i){
    graph[from].erase(graph[from].begin() + i);
    for(int j = 0; j < (int) graph[to].size(); j++){
        if(graph[to][j] == from){
            graph[to].erase(graph[to].begin() + j);
            break;
        }
    }
}

void removeEdge(int from, int to){
    for(int j = 0; j < (int) graph[from].size(); j++){
        if(graph[from][j] == to){
            graph[from].erase(graph[from].begin() + j);
            break;
        }
    }
    for(int j = 0; j < (int) graph[to].size(); j++){
        if(graph[to][j] == from){
            graph[to].erase(graph[to].begin() + j);
            break;
        }
    }
}

int getBest(int vertex){//best is the soon with greater degree
    int best = 0, ans = -1;
    for(int i = 0; i < (int) graph[vertex].size(); i++){
        int next = graph[vertex][i];
        if((int)graph[next].size() > best){
            best = (int) graph[next].size();
            ans = i;
        }
    }
    return ans;
}

int main(){
    scanf("%d%d", &n, &m);
    for(int i = 0; i < m; i++){
        scanf("%d%d", &U, &V);
        graph[U].push_back(V);
        graph[V].push_back(U);
    }
    set<pair<int, int> > q;
    for(int i = 1; i <= n; i++){
        if((int) graph[i].size() > 0){
            q.insert(make_pair(graph[i].size(), i));
        }
    }
    while(!q.empty()){
        int top = (*q.begin()).second;
        //cout << top << ", size = " << q.size() << endl;
        int bestVertex = getBest(top);
        int go = graph[top][bestVertex];
        q.erase(*q.begin());
        q.erase(make_pair(graph[go].size(), go));
        removeEdge(top, go, bestVertex);
        if(active[top]) active[go] = 1;
        else active[top] = 1;
        if(graph[top].size() > 0){
            q.insert(make_pair(graph[top].size(), top));
        }
        if(graph[go].size() > 0){
            q.insert(make_pair(graph[go].size(), go));
        }
    }
    int ans = 0;
    for(int i = 1; i <= n; i++){
        if(active[i]) ans++;
    }
    printf("%d\n", n-ans);
    return 0;
}
