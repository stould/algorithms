#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <math.h>
#include <string.h>
#include <stack>
#include <map>
#include <set>
#define ll long long
#define INF 0x3f3f3f3f
#define MAXN 100001

using namespace std;

string str;

int main(void){
	cin >> str;
	ll MOD = 1009;
	ll p = 1;
	ll num = 0;
	for(int i = str.size() -1; i >= 0; i--){
		num += (str[i] - '0') * p;
		num %= MOD;
		p *= 10;
		p %= MOD;
		if(num % MOD == 0){
			cout << i << "\n";
			break;
		}
	}
	return 0;
}
