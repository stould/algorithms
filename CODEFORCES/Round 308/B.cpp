#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#include <cmath>

#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f3f3f3fLL
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;
ll n;

int main(void) {
	cin >> n;
	ll ans = 0;
	if(n == 1000000000){//10e9
		ans += 10;
		n--;
	}
	if(n >= 100000000){//10e8
		ans += (n - 100000000 + 1) * 9;
		n = 100000000-1;
	}
	if(n >= 10000000){//10e7
		ans += (n - 10000000 + 1) * 8;
		n = 10000000-1;
	}	
	if(n >= 1000000){//10e6
		ans += (n - 1000000 + 1) * 7;
		n = 1000000-1;
	}
	if(n >= 100000){//10e5
		ans += (n - 100000 + 1) * 6;
		n = 100000-1;
	}
	if(n >= 10000){//10e4
		ans += (n - 10000 + 1) * 5;
		n = 10000-1;
	}
	if(n >= 1000){//10e3
		ans += (n - 1000 + 1) * 4;
		n = 1000-1;
	}
	if(n >= 100){//10e2
		ans += (n - 100 + 1) * 3;
		n = 100-1;
	}
	if(n >= 10){//10e1
		ans += (n - 10+1) * 2;
		n = 10-1;
	}
	ans += n;
			   
	cout << ans << endl;
	return 0;
}
