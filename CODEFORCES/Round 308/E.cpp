#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f3f3f3fLL
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

bool vis[5200];

ll parser(string &s){
	ll ans = 0;
	ll tmp = 0;

	memset(vis,0,sizeof(vis));
	for(int i = 1; i < s.size();){
		while(s[i] == '*'){
			if(tmp == 0){
				vis[i-1] = vis[i+1] = 1;
				tmp = (s[i-1] - '0')*(s[i+1] - '0');
			}else{
				vis[i+1] = 1;
				tmp *= s[i+1]-'0';
			}
			i+=2;
		}
		ans += tmp;
		tmp = 0;
		if(s[i] == '+'){
			if(i+2 < s.size()){
				if(s[i+2] == '+'&& !vis[i-1]){
					ans += s[i-1] - '0';
					vis[i-1] = 1;
				}
				i+=2;
			}else if(i+2 >= s.size()){
				if(i-2 >= 0 && s[i-2] == '+' && !vis[i-1]){
					vis[i-1] = vis[i+1] = 1;
					ans += s[i-1] - '0' + s[i+1] - '0';
				}else{
					vis[i+1] = 1;
					ans +=  s[i+1] - '0';

				}
				break;
			}
		}
	}
	for(int i = 0; i < s.size(); i++){
		if(!vis[i] && s[i] >= '0' && s[i] <= '9'){
			ans += s[i] - '0';
		}
	}
	return ans;
}

ll parse(string s){
	string toAns = "";
	string a = "";
	bool open = 0;
	for(int i = 0; i < s.size(); i++){
		if(open){
			a += string(1,s[i]);
		}else{
			if(s[i] == '('){
				open = 1;
			}else if(s[i] == ')'){
				open = 0;
			}else{
				toAns += string(1,s[i]);
			}
		}
	}
	
}

int main(void) {

	return 0;
}
