#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f3f3f3fLL
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

int XX,YY,n;

pair<int,int> p[2001];


int cross(pair<int, int> &A, pair<int, int> &B, pair<int, int> &C) {
    return (B.first-A.first) * (C.second-A.second) - (B.second-A.second) * (C.first-A.first);
}

int main(void) {
	cin >> n;
	for(int i = 0; i < n; i++){
		cin >> XX >> YY;
		p[i] = make_pair(XX,YY);
	}
	pair<int,int> A,B,C;
	ll ans = 0;
	for(int i = 0; i < n; i++){
		for(int j = i+1; j < n; j++){
			for(int k = j+1; k < n; k++){
				if(cross(p[i],p[j],p[k]) != 0) ans++;
			}
		}
	}
	cout << ans << endl;
	return 0;
}
