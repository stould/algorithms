#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f3f3f3fLL
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

int n,r1,c1,r2,c2;
int board[103][103];

int main(void) {
	cin >> n;
	for(int i = 0; i < n; i++){
		cin >> r1 >> c1 >> r2 >> c2;
		for(int j = min(r1,r2); j <= max(r1,r2); j++){
			for(int k = min(c1,c2); k <= max(c1,c2); k++){
				board[j][k]++;
			}
		}
	}
	int ans = 0;
	for(int i = 1; i <= 101; i++){
		for(int j = 1; j <= 101; j++){
			ans += board[i][j];

		}
	}
	cout << ans << endl;
	return 0;
}
