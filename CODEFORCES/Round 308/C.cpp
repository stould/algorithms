#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f3f3f3fLL
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

vector<ll> pos;
vector<int> sum;
ll w,m,n;

map<pair<pair<int,int>, int>,int> dp;

bool func(int idx, ll L, ll R){

	if(L == R) return 1;
	if(idx == pos.size()) return 0;
	bool ans  = func(idx+1,L,R);
	if(ans) return 1;
	ans |= func(idx+1,L+pos[idx],R);
	if(ans) return 1;
	ans |= func(idx+1, L, R+pos[idx]);
	if(ans) return 1;
	return ans;
}

int main(void) {
	/*cin >> w >> m;
	
	if(w <= 3 || w == m+1 || w == m || w == m-1 || m == 1){
		cout << "YES" << endl;
	}else{
		cout << "NO" << endl;
		}*/
	
		
	   
	for(int I = 4; I <= 4; I++){
		for(int J = 1; J <= 300; J++){
			w= I;
			m = J;
			sum.clear();
			pos.clear();
			dp.clear();
			int idx = 0;
			for(ll i = 0; ; i++){
				ll v = pow(w,i);
				sum.push_back(v);
				pos.push_back(v);
				if(sum.size() > 1) sum[i] += sum[i-1];
				if(v > m){
					break;
				}
			}
			n = idx;
			cout << I << " " << J << " = " << func(0,0,m) << endl;
		}

		}
	   
	return 0;
}
