#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

ll n, l, a, b, v[MAXN];

void print(set<ll> p){
	for(set<ll>::iterator it = p.begin(); it != p.end(); it++){
		cout << *it << " ";
	}
	cout << endl;
}

vector<ll> test(set<ll> p, ll A, ll B){
	vector<ll> ans(3);
	if(p.find(A) != p.end()){
		if(p.find(B) != p.end()){
			ans.clear();
			return ans;
		}else{
			ans.push_back(B);
			return ans;
		}
	}else{
		for(int i = 0; i < n; i++){
			vector<ll> tmp;
			if(v[i] - A >= 0 && p.find(v[i] - A) == p.end()){
				print(p);
				p.insert(v[i] - A);
				print(p);
				tmp.push_back(v[i] - A);
				
				if(p.find(v[i] - B) == p.end() && p.find(B) == p.end() && p.find(v[i] + B) == p.end()){
					tmp.push_back(B);
					cout << "added " << v[i]+A << " and no found " << v[i] - B << " or " << B << " or " << v[i] + B << endl;					
				}
				if(ans.size() > tmp.size()){
					ans = tmp;
				}
				p.erase(v[i] - A);
			}
			tmp.clear();
			if(v[i] + A <= l && p.find(v[i] + A) == p.end()){
				print(p);
				p.insert(v[i] + A);
				print(p);
				tmp.push_back(v[i] + A);
				if(p.find(v[i] - B) == p.end() && p.find(B) == p.end() && p.find(v[i] + B) == p.end()){
					tmp.push_back(B);	
					cout << "added " << v[i]+A << " and no found " << v[i] - B << " or " << B << " or " << v[i] + B << endl;				
				}
				if(ans.size() > tmp.size()){
					ans = tmp;
				}
				p.erase(v[i] + A);
			}
			cout << endl;
		}
	}
	return ans;
}

int main(void) {
	cin >> n >> l >> a >> b;
	set<ll> p;
	for(int i = 0; i < n; i++){
		cin >> v[i];
		p.insert(v[i]);
	}

	vector<ll> ans(3);
	vector<ll> tmp1 = test(p, a, b);cout << endl;
	vector<ll> tmp2 = test(p, b, a);
	if(tmp1.size() < ans.size()){
		ans = tmp1;
	}
	if(tmp2.size() < ans.size()){
		ans = tmp2;
	}
	cout << ans.size() << endl;
	for(int i = 0; i < ans.size(); i++){
		cout << ans[i] << " ";
	}
}
