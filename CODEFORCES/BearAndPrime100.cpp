#include <bits/stdc++.h>

using namespace std;

vector<int> primes;

int main(){
    for(int i = 2; i <= 100; i++){
        int divs = 0;
        for(int j = 1; j <= i; j++){
            if(i % j == 0){
                divs++;
            }
        }
        if(divs == 2){
            primes.push_back(i);
        }
    }
    string judge_ans;
    int divisible = 0;
    for(int i = 0; i < 15; i++){
        cout << primes[i] << endl;
        fflush(stdout);
        cin >> judge_ans;
        if(judge_ans == "yes"){
            divisible++;
        }
    }
    for(int i = 0; primes[i] < 5 && primes[i] * primes[i] <= 100; i++){
        cout << primes[i]*primes[i] << endl;
        fflush(stdout);
        cin >> judge_ans;
        if(judge_ans == "yes"){
            divisible++;
        }
    }
    if(divisible <= 1){
        cout << "prime" << endl;
    }else{
        cout << "composite" << endl;
    }
    return 0;
}
