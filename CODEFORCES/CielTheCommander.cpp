#include <iostream>
#include <vector>
#include <cmath>
#include <stdio.h>
#include <queue>
#include <algorithm>
#include <stdlib.h>
#include <sstream>
#include <map>
#include <string.h>
#include <time.h>
#include <utility>
#include <bitset>
#include <set>
#define MAXN 100100
#define INF 9999999
#define LOGN 25
#define ll long long
using namespace std;

int pai[MAXN], subSize[MAXN], treeSize;
vector<set<int> > graph(MAXN);

void dfsCnt(int node, int parent){
    treeSize++;
    subSize[node] = 1;
    for(auto next : graph[node]){
        if(next != parent){
            dfsCnt(next, node);
            subSize[node] += subSize[next];
        }
    }
}

int getCentroid(int node, int parent){
    for(auto next : graph[node]){
        if(next != parent && 2 * subSize[next] >= treeSize){
            return getCentroid(next, node);
        }
    }
    return node;
}

vector<vector<int> > tree(MAXN);
int treeRoot;

void centroid(int node, int lastCen){
    treeSize = 0;
    dfsCnt(node, -1);
    int centroidAt = getCentroid(node, -1);
    if(lastCen == -1){
        pai[centroidAt] = centroidAt;
        treeRoot = centroidAt;
    }else{
        tree[centroidAt].push_back(lastCen);
        tree[lastCen].push_back(centroidAt);
        pai[centroidAt] = lastCen;
    }
    for(auto next : graph[centroidAt]){
        graph[next].erase(graph[next].find(centroidAt));
        centroid(next, centroidAt);
    }
    graph[centroidAt].clear();
}

char color[MAXN];

vector<vector<int> > byLevel(LOGN);

void dfs(int node, int parent, int level = 0){
    byLevel[level].push_back(node);
    for(auto next : tree[node]){
        if(next != parent){
            dfs(next, node, level + 1);
        }
    }
}

int n, u, v;

int main(){
    scanf("%d", &n);
    for(int i = 0; i < n - 1; i++){
        scanf("%d%d", &u, &v);
        graph[u].insert(v);
        graph[v].insert(u);
    }
    centroid(1, -1);
    dfs(treeRoot, -1);
    for(int i = 0; i < LOGN; i++){
        for(int j = 0; j < (int) byLevel[i].size(); j++){
            color[byLevel[i][j]] = i + 'A';
        }
    }
    for(int i = 1; i <= n; i++){
        printf("%c ", color[i]);
    }
    puts("");
    return 0;
}
