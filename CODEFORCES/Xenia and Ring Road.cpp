#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100002
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int n, m, v[100001];


int main(void){
	while(scanf("%d%d", &n, &m) == 2){
		for(int i = 1; i <= m; i++){
			scanf("%d", &v[i]);
		}
		int bigger = 1;
		ll ans = 0;
		for(int i = 1; i <= m; i++){
			if(v[i] == bigger) continue;
			if(v[i] > bigger){
				ans += (ll) v[i] - bigger;
				bigger = v[i];
			}else if(v[i] < bigger){
				ans += (n - bigger) + v[i];
				bigger = v[i];
			}
		}
		cout << ans << endl;
	}
}
