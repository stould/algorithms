#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>

#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;


string s;

int L, R, K, Q;

void rotate(){
	string newStr(".", R - L + 1);
	for(int i = 0; i <= (R-L); i++){
		newStr[(i+K) % (R-L+1)] = s[L+i];
	}
	for(int i = L; i <= R; i++){
		s[i] = newStr[i-L];
	}
}

int main(void) {
	cin >> s;
	cin >> Q;
	while(Q--){
		cin >> L >> R >> K;
		L--;
		R--;
		rotate();
	}
	cout << s << endl;
	return 0;
}
