#include <bits/stdc++.h>
#define MAXN 1003
using namespace std;

int T, n,k;
string s;

//0=rock
//1=paper
//2=scisor

int func(int idx){
	if(idx == n){
		return 0;
	}else{
		int ans = func(idx+1)+1;
		bool alter = 1;
		for(int i = 1; i < k && idx+i < n; i++){
			if(i && s[i+idx] == s[idx-i-1]){
				alter = 0;
			}
			if(!alter){
				ans = min(ans, 1+func(idx+k));
			}
		}
		return ans;
	}
}

int main(void) {
	cin >> T;
	while(T--){
		cin >> n >> k >> s;
		s = " " + s;
		int dp[MAXN];
		memset(dp,63,sizeof(dp));
		dp[0] = 0;
		for(int i = 1; i <= n; i++){
			bool alter = 1;
			for(int j = i; j < i+k; j++){
				if(s[j] == s[j+1]){
					alter = 0;
				}
				if(!alter || j == i){
					dp[i] = min(dp[i] , dp[j - 1] + 1);
				}
			}
		}
		cout << dp[n-1] << endl;
	}
    return 0;
}
