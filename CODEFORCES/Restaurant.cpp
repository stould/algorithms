#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>

#define MAXN 500001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

struct p{
	int l, r;
	p(){}
	p(int a,int b): l(a), r(b){}
	bool operator < (const p &o) const{
		if(r == o.r){
			return l < o.l;
		}
		return r < o.r;
	}
};

int n, l, r, dp[MAXN];

int main(void) {
	scanf("%d", &n);
	vector<p> val;
	for(int i = 0; i < n; i++){
		scanf("%d%d", &l, &r);
		val.push_back(p(l,r));
	}
	sort(val.begin(), val.end());
	dp[0] = 1;
	for(int i = 1; i < n; i++){
		dp[i] = dp[i-1];
		int lo = 0, hi = i-1, near = -1;
		while(lo <= hi){
			int mid = lo + (hi - lo) / 2;
			if(val[mid].r < val[i].l){
				near = max(near, mid);
				lo = mid+1;
			}else{
				hi = mid-1;
			}
		}
		if(near > -1){
			dp[i] = max(dp[i], 1+dp[near]);
		}
	}
	printf("%d\n", dp[n-1]);
	return 0;
}
