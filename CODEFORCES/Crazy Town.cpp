#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <math.h>
#include <string.h>
#include <stack>
#include <map>
#include <set>
#define ll long long
#define INF 0x3f3f3f3f
#define MAXN 100001

using namespace std;

ll X1,Y1,X2,Y2,a,b,c;
int n;
int main(void){
	cin >> X1 >> Y1 >> X2 >> Y2 >> n;
	int ans = 0;
	while(n--){
		cin >> a >> b >> c;
		ll p1 = a*X1 + b*Y1 + c;
		ll p2 = a*X2 + b*Y2 + c;
		if(p1 > 0LL && p2 < 0LL || p1 < 0LL && p2 > 0LL) ans++;
	}
	cout << ans << endl;
	return 0;
}
