#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>

#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

int base[MAXN];

int n;


int main(void) {
	cin >> n;
	for(int i = 0; i < n; i++){
		cin >> base[i];
	}
	int low = 0, big = 0;
	int ans = 1;
	for(int i = 1; i < n; i++){
		if(base[i] < base[low]){
			low = i;
		}
		if(base[i] > base[big]){
			big = i;
		}
		while(abs(base[i] - base[low]) > 1){
			low++;
		}
		while(abs(base[i] - base[big]) > 1){
			big++;
		}
		ans = max(ans, i - min(low, big)  + 1);
	}
	cout << ans << endl;
	return 0;
}
