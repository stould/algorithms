#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

int n,m,k;


ll satis[20];
ll dp[18][1 << 18];
ll pratos[20][20];

ll func(int last, int mask, int bits){
	if(bits == m){
		return 0;
	}else{
		ll &ans = dp[last][mask];
		if(ans == -1){
			ans = 0;
			for(int i = 0; i < n; i++){
				if(!(mask & (1 << i))){
					int rizes = pratos[last][i];
					ans = max(ans, rizes + satis[i] + func(i, mask | (1 << i), bits+1));
				}
			}
		}
		return ans;
	}
}

int main(void) {
	cin >> n >> m >> k;
	for(int i = 0; i < n; i++){
		cin >> satis[i];
	}
	memset(pratos,0,sizeof(pratos));
	for(int i = 0; i < k; i++){
		ll A,B,C;
		cin >> A >> B >> C;
		pratos[A-1][B-1] = C;
	}
	memset(dp,-1,sizeof(dp));
	ll ans = 0;
	for(int i = 0; i < n; i++){
		ans = max(ans, satis[i] + func(i, (1 << i), 1));
	}
	cout << ans << endl;
	return 0;
}
