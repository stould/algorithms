#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>

#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

int n,m, v[MAXN];

struct P{
	ll money, fri;
	P(){}
	P(ll A, ll B): money(A), fri(B){}
	bool operator < (const P &o) const{
		if(money == o.money){
			return fri > o.fri;
		}
		return money < o.money;
	}
};

vector<P> person;

int lower(ll key){
	int lo = 0, hi = n-1, ans = INF;
	while(lo <= hi){
		int mid = lo + (hi-lo) / 2;
		if(person[mid].money >= key){
			hi = mid-1;
			ans = min(ans, mid);
		}else{
			lo = mid+1;
		}
	}
	return ans;
}

int upper(ll key){
	int lo = 0, hi = n-1, ans = -1;
	while(lo <= hi){
		int mid = lo + (hi-lo) / 2;
		if(person[mid].money <  key){
			lo = mid+1;
			ans = max(ans, mid);
		}else{
			hi = mid-1;
		}
	}
	return ans;
}

ll freq[MAXN];

int main(void) {
	cin >> n >> m;
	
	for(int i = 0; i < n; i++){
		P tmp(0,0);
		cin >> tmp.money >> tmp.fri;
		person.push_back(tmp);
	}
	sort(person.begin(), person.end());
	freq[0] = person[0].fri;

	for(int i = 1; i < n; i++){
		freq[i] = freq[i-1] + person[i].fri;
	}
	ll ans = 0;
	for(int i = 0; i < n; i++){
		int hi = upper(person[i].money+m);
		if(hi == -1){
			ans = max(person[i].fri, ans);
		}else{
			if(i  == 0){
				ans = max(freq[hi], ans);
			}else{
				ans = max(ans, freq[hi] - freq[i-1]);
			}
		}
	}
	cout << ans << endl;
	return 0;
}
