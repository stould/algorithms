#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>

#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

int n,m, ans = 0;
vector<vector<int> > G(MAXN);
bool isLeaf[MAXN], hasCat[MAXN];
int deg[MAXN];

void dfs(int u, int parent, int cats){
	if(isLeaf[u]){
		ans++;
	}else{
		for(int i = 0; i < (int) G[u].size(); i++){
			int v = G[u][i];
			if(v != parent){
				if(hasCat[v]){
					if(cats + 1 <= m){
						dfs(v, u, cats+1);
					}
				}else{
					dfs(v, u, 0);
				}
			}
		}
	}
}

int U,V;

int main(void) {
	cin >> n >> m;
	for(int i = 0; i < n; i++){
		cin >> hasCat[i];
	}
	for(int i = 0; i < n-1; i++){
		cin >> U >> V;
		U--;
		V--;
		G[U].push_back(V);
		G[V].push_back(U);
		deg[U]++;
		deg[V]++;
	}
	for(int i = 1; i < n; i++){
		if(deg[i] == 1){
			isLeaf[i] = 1;
		}
	}
	dfs(0, -1, hasCat[0]);
	cout << ans << endl;
	return 0;
}
