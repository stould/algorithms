#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100007
#define ll long long int
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define EPS 1e-7
using namespace std;

int n;
ll w, v[MAXN*2];

double can(double X){
	double total = 0;
	for(int i = 0; i < n; i++){
		total += X;
		if(X > v[i]) return -1;
	}
	for(int i = 0; i < n; i++){
		total += 2.*X;
		if(X*2. > v[n+i]) return -1;
	}
	return total;
}

int main(void){
	cin >> n >> w;
	for(int i = 0 ; i < 2 * n; i++){
		cin >> v[i];
	}
	sort(v, v+(2*n));
	double lo = 0, hi = (double) w, ans = 0;
	for(int i = 0; i < 100; i++){
		double mid = (lo+hi) / 2.;
		double total = can(mid);
		if(total > -1){
			ans = max(ans, min(total, (double)w));
			lo = mid;
		}else{
			hi = mid;
		}
	}
	printf("%.7lf\n", ans);
	return 0;
}
