#include <iostream>
#include <vector>
#include <cmath>
#include <stdio.h>
#include <queue>
#include <algorithm>
#include <stdlib.h>
#include <sstream>
#include <map>
#include <string.h>
#include <time.h>
#include <utility>
#include <bitset>
#include <set>
#define MAXN 100100
#define INF 0x3f3f3f3f
#define LOGN 20
#define ll long long
using namespace std;

int pai[MAXN], subSize[MAXN], level[MAXN], lca[MAXN][LOGN], treeSize;
vector<set<int> > graph(MAXN);

void dfs(int node, int parent){
    lca[node][0] = parent == -1 ? node : parent;
    for(int i = 1; i < LOGN; i++){
        lca[node][i] = lca[lca[node][i-1]][i-1];
    }
    for(auto next : graph[node]){
        if(next != parent){
            level[next] = level[node] + 1;
            dfs(next, node);
        }
    }
}

int getLca(int a, int b){
    if(level[a] < level[b]){
        swap(a,b);
    }
    for(int i = LOGN-1; i >= 0; i--){
        if(level[a] - (1 << i) >= level[b]){
            a = lca[a][i];
        }
    }
    if(a == b) return a;
    for(int i = LOGN-1; i >= 0; i--){
        if(lca[a][i] != lca[b][i]) {
            a = lca[a][i]; b = lca[b][i];
        }
    }
    return lca[a][0];
}

void dfsCnt(int node, int parent){
    treeSize++;
    subSize[node] = 1;
    for(auto next : graph[node]){
        if(next != parent){
            dfsCnt(next, node);
            subSize[node] += subSize[next];
        }
    }
}

int dist(int a, int b){
    return level[a] + level[b] - 2*level[getLca(a,b)];
}

int getCentroid(int node, int parent){
    for(auto next : graph[node]){
        if(next != parent && 2 * subSize[next] >= treeSize){
            return getCentroid(next, node);
        }
    }
    return node;
}

void centroid(int node, int lastCen){
    treeSize = 0;
    dfsCnt(node, -1);
    int centroidAt = getCentroid(node, -1);
    if(lastCen == -1){
        pai[centroidAt] = centroidAt;
    }else{
        pai[centroidAt] = lastCen;
    }
    for(auto next : graph[centroidAt]){
        graph[next].erase(graph[next].find(centroidAt));
        centroid(next, centroidAt);
    }
    graph[centroidAt].clear();
}

vector<multiset<int> > minDist(MAXN);

void update(int node){
    int u = node;
    while(1){
        int d1 = dist(u, node);
        minDist[u].insert(d1);
        if(u == pai[u]) break;
        u = pai[u];
    }
}

int query(int node){
    int ans = INF;
    int u = node;
    while(1){
        if( (int) minDist[u].size() > 0){
            int d1 = dist(node, u);
            int d2 = *minDist[u].begin();
            ans = min(ans, d1 + d2);
        }
        if(u == pai[u]) break;
        u = pai[u];
    }
    return ans;
}

int n, u, v, T, q;

int main(){
    scanf("%d%d", &n, &q);
    for(int i = 0; i < n - 1; i++){
        scanf("%d%d", &u, &v);
        graph[u].insert(v);
        graph[v].insert(u);
    }
    dfs(1, -1);
    centroid(1, -1);
    update(1);
    for(int i = 0; i < q; i++){
        cin >> T >> v;
        if(T == 1){
            update(v);
        }else{
            cout << query(v) << endl;
        }
    }
    return 0;
}
