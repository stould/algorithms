#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>

#define MAXN 1001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

int n, m, k;

char grid[1001][1001];

int beg, END;
pair<int, int> myqueue[MAXN*MAXN];
bool vis[MAXN][MAXN];
int id[MAXN*MAXN];
int height[MAXN*MAXN];
int sum[MAXN*MAXN];

int pos(int line, int col){
	return m * line + col;
}

void add(int l, int c){
	myqueue[END++] = make_pair(l,c);
}

pair<int,int> front(){
	return myqueue[beg];
}

void pop(){
	beg++;
}

bool empty(){
	return beg == END;
}

void init(){
	beg = END = 0;
	for(int i = 0; i < MAXN*MAXN; i++){
		id[i] = i;
		height[i] = 1;
	}
}
 
int root(int x){
	if(x == id[x]){
		return x;
	}else{
		return id[x] = root(id[x]);
	}
}
 
bool find(int a, int b){
	return root(a) == root(b);
}
 
void unite(int a, int b){
	int roota = root(a);
	int rootb = root(b);
	if(roota != rootb){
		if(height[roota] >= height[rootb]){
			id[rootb] = roota;
			height[roota] += height[rootb];
			sum[roota] += sum[rootb];
		}else{
			id[roota] = rootb;
			height[b] += height[roota];
			sum[rootb] += sum[roota];
		}
	}
}


int dl[4] = {1,0,-1,0};
int dc[4] = {0,1,0,-1};

void bfs(int S, int T){
	add(S,T);
	vis[S][T] = 1;
	while(!empty()){
		int topL = front().first;
		int topC = front().second;
		pop();
		if(!find(pos(S,T), pos(topL, topC))){
			unite(pos(S,T), pos(topL, topC));
		}
		//	cout << pos(S,T) << " with " << pos(topL, topC) << " root = " << root(pos(S,T)) << endl;
		for(int i = 0; i < 4; i++){
			int nL = topL + dl[i];
			int nC = topC + dc[i];
			if(nL >= 0 && nL < n && nC >= 0 && nC < m && !vis[nL][nC]){
				if(grid[nL][nC] == '*'){
					sum[root(pos(S,T))] += 1;
				}else{
					add(nL,nC);
					vis[nL][nC] = 1;
				}
			}
		}
	}
}

int L,C;

int main(void) {
	ios::sync_with_stdio(0);
	cin >> n >> m >> k;
	init();
	for(int i = 0; i < n; i++){
		for(int j = 0; j < m; j++){
			cin >> grid[i][j];
		}
	}
	for(int i = 1; i < n-1; i++){
		for(int j = 1; j < m-1; j++){
			if(!vis[i][j] && grid[i][j] == '.'){
			   END = beg = 0;
				bfs(i,j);
			}
		}
	}
	for(int i = 0; i < k; i++){
		cin >> L >> C;
		L--;
		C--;
		cout << sum[root(pos(L,C))] << endl;
	}
	return 0;
}
