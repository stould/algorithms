#include <bits/stdc++.h>
#define MAXN 100001
#define ll long long int

using namespace std;

int max_color[MAXN], sz[MAXN];
ll sum, bigger, ans[MAXN];
vector<vector<int> > graph(MAXN);
int n, color[MAXN];
bool heavy[MAXN];

void getSz(int u, int parent){
    sz[u] = 1;
    for(int i = 0; i < (int) graph[u].size(); i++){
        int v = graph[u][i];
        if(v == parent) continue;
        getSz(v, u);
        sz[u] += sz[v];
    }
}

void reval(int col){
    max_color[col]++;
    if(bigger < max_color[col]){
        bigger = max_color[col];
        sum = col;
    }else if(bigger == max_color[col]){
        sum += col;
    }
}

void add(int u, int parent){
    reval(color[u]);
    for(int i = 0; i < (int)graph[u].size(); i++){
        int v = graph[u][i];
        if(!heavy[v] && v != parent){
            add(v, u);
        }
    }
}

void remove(int u, int parent){
    max_color[color[u]]--;
    for(int i = 0; i < (int)graph[u].size(); i++){
        int v = graph[u][i];
        if(!heavy[v] && v != parent){
            remove(v, u);
        }
    }    
}

void dfs(int u, int parent, bool keep){
    int bigChild = -1, cnt = -1;
    for(int i = 0; i < (int)graph[u].size(); i++){
        int v = graph[u][i];
        if(sz[v] > cnt && v != parent){
            cnt = sz[v];
            bigChild = v;
        }
    }
    for(int i = 0; i < (int)graph[u].size(); i++){
        int v = graph[u][i];
        if(v != parent && v != bigChild){//goes down the light nodes
            dfs(v, u, 0);
        }
    }
    if(bigChild != -1){//goes down the heavy node only once
        dfs(bigChild, u, 1);
        heavy[bigChild] = 1;
    }
    add(u, parent);
    ans[u] = sum;
    if(bigChild != -1){
        heavy[bigChild] = 0;
    }
    if(keep == 0){
        remove(u, parent);
        bigger = 0;
        sum = 0;
    }
}

int u,v;

int main(void){

    cin >> n;
    
    for(int i = 1; i <= n; i++){
        cin >> color[i];
    }
    for(int i = 1; i < n; i++){

        cin >> u >> v;
        graph[u].push_back(v);
        graph[v].push_back(u);
    }
    getSz(1,-1);
    dfs(1, -1, 0);
    cout << ans[1];
    for(int i = 2; i <= n; i++){
        cout << " " << ans[i];
    }
    cout << endl;
    return 0;
}
