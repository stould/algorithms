#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100002
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;


struct Edge{
	int balance;
	int last;
	int used;
	Edge *parent;
	Edge(){}
	Edge(int a, int b, int c){
		balance = a;
		last = b;
		used = c;
		parent = NULL;
	}
};


void path(Edge *e){
	if(e->parent == NULL) return;
	printf("%d ", e->last);
	path(e->parent);
}


string str;
int m, v[12], n;
bool ok;

int main(void){
	while(cin >> str){
		cin >> m;
		n = 0;
		for(int i = 0; i < str.size(); i++){
			if(str[i] == '1') v[n++] = i+1;
		}
		ok = 0;
		stack<Edge*> st;
		st.push(new Edge(0, -1, 0));
		map<Edge*, bool> vis;
		while(!st.empty()){
			Edge *top = st.top(); st.pop();
			if(top->used == m) {
				printf("YES\n");
				path(top);
				printf("\n");
				ok = 1;
				break;
			}else{
				for(int i = 0; i < n; i++){
					if(v[i] != top->last){
						if(top->used % 2 == 0){
							if(top->balance - v[i] < 0){
								Edge *next = new Edge(top->balance - v[i], v[i], top->used+1);
								if(vis[next]) continue;
								vis[next] = 1;
								next->parent = top;
								st.push(next);
							}
						}else{
							if(top->balance + v[i] > 0){
								Edge *next = new Edge(top->balance + v[i], v[i], top->used+1);
								if(vis[next]) continue;
								vis[next] = 1;
								next->parent = top;
								st.push(next);
							}
						}
					}
				}
			}
		}
		if(!ok){
			printf("NO\n");
		}
	}
}
