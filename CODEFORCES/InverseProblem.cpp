#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <math.h>
#include <string.h>
#include <stack>
#include <map>
#include <set>
#define ll long long
#define INF 12345678910121LL
#define MAXN 2003

using namespace std;

struct UnionFind {
    int N, *id, *sz;

    UnionFind(int _N) {
        id = new int[_N];
        sz = new int[_N];
        for(int i = 0; i < _N; i++) {
            id[i] = i;
            sz[i] = 1;
        }
        N = _N;
    }
    int root(int i) {
        while(i != id[i]) {
            id[i] = id[id[i]];
            i = id[i];
        }
        return i;
    }
    bool find(int p, int q) {
        return root(p) == root(q);
    }
    void unite(int p, int q) {
        int i = root(p);
        int j = root(q);
        if(i == j) return;
        if(sz[i] < sz[j]) {
            id[i] = j; sz[j] += sz[i];
        } else {
            id[j] = i; sz[i] += sz[j];
        }
    }
};

struct edge {
    int from, to, cost;
    edge() {}
    edge(int from, int to, int cost): from(from), to(to), cost(cost) {};

    bool operator<(const edge& e) const {
		return cost < e.cost;
    }
};

int H[MAXN], L[MAXN << 1], E[MAXN << 1], vis[MAXN], tree[MAXN * 8], idx, dst[MAXN];
vector<vector<pair<int, int> > > g(MAXN);

void dfs(int x, int depth){
	vis[x] = 1;//visited
	if(H[x] == -1) H[x] = idx;//mark first time the i'th node is visited
	L[idx] = depth;//when you visit a node you should mark the the depth you have found it.
	E[idx++] = x;//the i'th recursion, global variable
	for(int i = 0; i < g[x].size(); i++){
		int next = g[x][i].first;
		if(!vis[next]){
			dst[next] = dst[x] + g[x][i].second;
			dfs(next, depth+1);
			L[idx] = depth;
			E[idx++] = x;
		}
	}
}

//NlogN build the segtree and minimize the height of the I'th visited node
void build(int node, int l, int r){
	if(l > r) return;
	if(l == r){
		tree[node] = l;
	}else{
		int mid = (l+r) >> 1;
		build(node*2, l, mid);
		build(node*2+1, mid+1, r);
		int A = tree[node*2];
		int B = tree[node*2+1];
		if(L[A] <= L[B]){
			tree[node] = A;
		}else{
			tree[node] = B;
		}
	}
}

//Get the vertex with the minimum height, then it will be the LCA of A and B.
int rmq(int node, int l, int r, int ra, int rb){
	if(l > rb || r < ra){
		return -1;
	}else if(l >= ra && r <= rb){
		return tree[node];
	}else{
		int mid = (l+r) >> 1;
		int q1 = rmq(node*2, l, mid, ra, rb);
		int q2 = rmq(node*2+1, mid+1, r, ra, rb);
		if(q1 == -1){
			return q2;
		}else if(q2 == -1){
			return q1;
		}else{
			if(L[q1] <= L[q2]){
				return q1;
			}else{
				return q2;
			}
		}
	}
}



int n, d, dist[MAXN][MAXN];

int main(void){
	while(scanf("%d", &n) == 1){
		vector<edge> edges;
		idx = 0;
		for(int i = 0; i <= n; i++){
			g[i].clear();
			H[i] = -1;
			L[i] = E[i] = vis[i] = 0;
			dst[i] = 0;
		}
		bool can = 1;
		for(int i = 0; i < n; i++){
			for(int j = 0; j < n; j++){
				scanf("%d", &dist[i][j]);
			}
		}
		for(int i = 0; i < n && can; i++){
			for(int j = i; j < n && can; j++){
				if(i == j){
					if(dist[i][j] != 0){
						can = 0;
					}
				}else{
					if(dist[i][j] == 0 || dist[i][j] != dist[j][i]){
						can = 0;
					}
					edges.push_back(edge(i,j,dist[i][j]));
				}
			}
		}
		if(!can){
			cout << "NO" << endl;
			continue;
		}
		sort(edges.begin(), edges.end());
		UnionFind uf(1 + n);
		int ans = 0;
		for(int i = 0; i < edges.size(); i++) {
			edge &edg = edges[i];
			if(!uf.find(edg.from, edg.to)) {
				uf.unite(edg.from, edg.to);
				g[edg.from].push_back(make_pair(edg.to, edg.cost));
				g[edg.to].push_back(make_pair(edg.from, edg.cost));
			}
		}
		dfs(0,0);
		build(1, 0, 2*n-1);
		for(int i = 0; i < n && can; i++){
			for(int j = i+1; j < n && can; j++){
				int goFrom = H[i];
				int goTo = H[j];
				if(goFrom > goTo){
					swap(goFrom, goTo);
				}
				int lcaAB = E[rmq(1, 0, 2*n-1, goFrom, goTo)]; //is the LCA of A and B;
				//				cout << dist[i][j] << endl;
				//				cout << dst[i] << "+" << dst[j] << "- 2*" << dst[lcaAB] << endl;
				if(dist[i][j] != dst[i] + dst[j] - 2*dst[lcaAB]) can = 0;
			}
		}
		if(can){
			cout << "YES\n";
		}else{
			cout << "NO\n";
		}
	}
	return 0;
}
