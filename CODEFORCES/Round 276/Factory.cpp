#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;


int gcd(int a, int b){
	if(b == 0) return a;
	return gcd(b, a % b);
}

ll a, b;

int main(void) {
	cin >> a >> b;
	bool stop = 0;
	for(int v = 0; v <= 100000; v++){
		if(a%b == 0){ stop = 1; break; }
		a += (a%b);
	}
	if(!stop){
		cout << "No\n";
	}else{
		cout << "Yes\n";
	}
	return 0;
}
