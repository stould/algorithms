#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f3f3f3fLL
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

int n;


int main(void) {
	cin >> n;
	ll minorx = INF, minory = INF;
	ll majorx = -INF, majory= -INF;
	for(int i = 0; i < n; i++){
		ll a,b;
		cin >> a >> b;
		minorx = min(minorx, a);
		minory = min(minory, b);
		majorx = max(majorx, a);
		majory = max(majory, b);
	}
	ll sq = max(abs(minorx-majorx), abs(minory-majory));
	cout << sq*sq << endl;
}
