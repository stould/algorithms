#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

ll n,L,R;

ll brute(){
	ll ans = 0;
	int bits = 0;
	for(ll i = L; i <= R; i++){
		int b = __builtin_popcount(i);
		if(b > bits){
			ans = i;
			bits = b;
		}
	}
	return ans;
}


int main(void) {
	cin >> n;
	while(n--){
		cin >> L >> R;
		for(ll i = 0; (1LL << i) <= R; i++){
			if(!(L & (1LL << i))){
				if((L | (1LL << i)) <= R){
					L |= (1LL << i);
				}else{
					break;
				}
			}
		}
		cout << L << endl;
	}
}
