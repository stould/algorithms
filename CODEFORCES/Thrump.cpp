#include <bits/stdc++.h>

using namespace std;

const int INF = 10001010;

const int MAXN = 301;
const int MAX_ALPHA = 10;
const int MAX_AHO = 351;

int N, M;
string A[16], B[16];
string ans;
int T[MAX_AHO], term[MAX_AHO], sig[MAX_AHO][MAX_ALPHA], cnt;
int state = 1;
int goalMask = 0;
int P[16];
int SUM;

void add(string& arg, int id) {
    int x = 0, n = (int) arg.size();
    for (int i = 0; i < n; i++){
        int c = (int) arg[i] - 'a';
        if (sig[x][c] == 0) {
            term[cnt] = 0;
            sig[x][c] = cnt++;
        }
        x = sig[x][c];
    }
    
    term[x] |= 1 << id;
}


void aho() { 
    queue <int> q;
    for (int i = 0; i < MAX_ALPHA; i++){
        int v = sig[0][i];

        if (v > 0) {
            q.push(v);
            T[v] = 0;
        }
    }

    while (!q.empty()){
        int u = q.front();
        q.pop();
        
        for (int i = 0; i < MAX_ALPHA; i++){
            int x = sig[u][i];

            if (x == 0) {
                continue;
            }
            
            int v = T[u];

            while (sig[v][i] == 0 && v != 0) {
                v = T[v];
            }

            int y = sig[v][i];
            q.push(x);
            
            T[x] = y;
            term[x] |= term[y];
        }
    }
}

int dist[MAX_AHO][1 << 15];
int visited[MAX_AHO][1 << 15];
pair<int, pair<int, int> > pai[MAX_AHO][1<<15];

void bfs(){
    queue<pair<int, int> > q;//root, mask
    q.push(make_pair(0, 0));
    memset(dist, 63, sizeof(dist));
    memset(pai, -1, sizeof(pai));
    dist[0][0] = 0;
    visited[0][0] = 1;
    while(!q.empty()){
        int root = q.front().first; 
        int mask = q.front().second;
        visited[root][mask] = 0;
        q.pop();
        if(goalMask == mask){
            int U = root, uMask = mask;
            vector<char> ans;
            while(U != -1){
                if(pai[U][uMask].first >= 0 && pai[U][uMask].first <= 9){
                    ans.push_back((char(pai[U][uMask].first + 'a')));
                }
                int tmpU = U;
                U = pai[U][uMask].second.first;
                uMask = pai[tmpU][uMask].second.second;
            }
            reverse(ans.begin(), ans.end());
            for(int i = 0; i < (int) ans.size(); i++){
                cout << ans[i];
            }
            cout << endl;
            break;
        }
        for(int i = 0; i < MAX_ALPHA; i++){
            int next = root;
            while (next != 0 && sig[next][i] == 0) {
                next = T[next];
            }
            next = sig[next][i];
            if((mask | term[next]) <= goalMask){
                if(dist[root][mask] + 1 <= SUM && dist[root][mask] + 1 < dist[next][mask | term[next]]){
                    dist[next][mask | term[next]] = dist[root][mask] + 1;
                    pai[next][mask | term[next]] = make_pair(i, make_pair(root, mask));
                    
                    q.push(make_pair(next, mask | term[next]));
                    visited[next][mask | term[next]] = 1;
                    
                }
            }
        }
    }
}

int main() {
    cin.tie(0);
    ios_base::sync_with_stdio(false);

    cin >> N >> M;

    cnt = 1;
    
    for (int i = 0; i < N; i++) {
        cin >> A[i];
        SUM += A[i].size();
        add(A[i], i);
        goalMask |= (1 << i);
    }

    for (int i = 0; i < M; i++) {
        cin >> B[i];
        add(B[i], N + i);
    }
    
    aho();
    bfs();

    return 0;
}
