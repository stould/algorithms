#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 5001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

vector<pair<ll, ll> > exam;
ll a,b;
int n;
int main(void) {
	cin >> n;
	for(int i = 0; i < n; i++){
		cin >> a >> b;
		exam.push_back(make_pair(a, b));
	}
	sort(exam.begin(), exam.end());
	ll ans = 0;
	for(int i = 0; i < n; i++){
		if(exam[i].second >= ans){
			ans = exam[i].second;
		}else{
			ans = exam[i].first;
		}
	}
	cout << ans << endl;
}
