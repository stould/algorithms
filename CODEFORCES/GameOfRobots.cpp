#include <bits/stdc++.h>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

ll n,k,val[MAXN];

int main(void) {
    cin >> n >> k;
    for(int i = 0; i < n; i++){
        cin >> val[i];
    }
    ll lo = 1, hi = n;
    ll ans = -1;
    while(lo <= hi){
        int mid = (lo + hi) / 2;
        ll sum = (ll)mid * ((ll)mid+1ll) / 2ll;
        if(sum >= k){
            hi = mid-1;
        }else{
            lo = mid+1;
        }
    }
    lo--;
    k -= lo * (lo+1)/2;
    cout << val[k-1] << endl;
    return 0;
}
