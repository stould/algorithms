#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100007
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
 
using namespace std;
 
int n,X0,Y0,X1,Y1,r,a,b,idx = 1,S,T,dist[MAXN];

int dl[8] = {-1,-1,-1,0,0,1,1,1};
int dc[8] = {-1,0,1,-1,1,-1,0,1};

map<pair<int,int>, int> mapper;
vector<vector<int> > graph(MAXN);

int main(void){
	scanf("%d%d%d%d%d", &X0, &Y0,&X1,&Y1,&n);
	S = idx;
	mapper[make_pair(X0,Y0)] = idx++;
	T = idx;
	mapper[make_pair(X1,Y1)] = idx++;
	for(int i = 0; i < n; i++){
		scanf("%d%d%d",&r,&a,&b);
		for(int col = a; col <= b; col++){
			pair<int,int> field = make_pair(r,col);
			if(mapper.find(field) == mapper.end()){
				mapper[field] = idx++;
			}
		}
	}
	queue<pair<int,int> > q;
	q.push(make_pair(X0,Y0));
	for(int i = 0; i <= idx; i++) dist[i] = INF;
	dist[S] = 0;
	while(!q.empty()){
		pair<int,int> top = q.front();q.pop();
		int topx = top.first;
		int topy = top.second; 
		for(int k = 0; k < 8; k++){
			pair<int,int> next = make_pair(topx+dl[k],topy+dc[k]);
			if(mapper.find(next) != mapper.end() && dist[mapper[top]] + 1 < dist[mapper[next]]){
				dist[mapper[next]] = dist[mapper[top]] + 1;
				q.push(next);
			}
		}		
	}
	if(dist[T] == INF){
		printf("-1\n");
	}else{
		printf("%d\n", dist[T]);
	}
	return 0;
}
