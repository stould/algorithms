#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
#define MAXN 103
#define ll long long

using namespace std;

ll gcd(ll a, ll b){
    if(b == 0){
        return a;
    }else{
        return gcd(b, a % b);
    }
}

ll lcm(ll a, ll b){
    return (a*b) / gcd(a,b);
}

struct Frac{
    ll up, down;
    Frac(){}
    Frac(ll up_, ll down_){
        up = up_;
        down = down_;
    }
    bool operator < (const Frac &o) const{
        return up * o.down < o.up * down;
    }
};

Frac sum(Frac &a, Frac &b){
    ll down = lcm(a.down, b.down);
    ll up = (down / a.down) * a.up + (down / b.down) * b.up;
    ll mdc = gcd(down, up);
    return Frac(up / mdc, down / mdc);
}

Frac div(Frac &a, Frac &b){
    ll up = a.up * b.down;
    ll down = a.down * b.up;
    ll mdc = gcd(up, down);
    return Frac(up / mdc, down / mdc);
}

int n,m,b;
Frac oldm[MAXN][MAXN], newm[MAXN][MAXN];

int dl[9] = {-1,0,1,0,-1,1,1,-1,0};
int dc[9] = {0,1,0,-1,1,1,-1,-1,0};
Frac fix(9,1);
int main(){
    cin >> m >> n >> b;
    for(int i = 0; i < n; i++){
        for(int j = 0; j < m; j++){
            int v;
            cin >> v;
            oldm[i][j].up = v;
            oldm[i][j].down = 1;
        }
    }
    while(b--){
        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                Frac average(0,1);
                for(int k = 0; k < 9; k++){
                    int nl = i + dl[k];
                    int nc = j + dc[k];
                    if(nl < 0){
                        nl = n-1;
                    }
                    if(nl >= n){
                        nl = 0;
                    }
                    if(nc < 0) {
                        nc = m-1;
                    }
                    if(nc >= m){
                        nc = 0;
                    }
                    average = sum(average, oldm[nl][nc]);
                }
                
                newm[i][j] = div(average, fix);
            }
        }
        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                oldm[i][j] = newm[i][j];
            }
        }        
    }
    set<Frac> ans;
    for(int i = 0; i < n; i++){
        for(int j = 0; j < m; j++){
            ans.insert(newm[i][j]);
        }
    }
    cout << (int) ans.size() << endl;
    return 0;
}
