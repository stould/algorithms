#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
#define MAXN 100001
using namespace std;

int n;

struct Name_{
    string first;
    string second;
    Name_(){}
    Name_(string first_, string second_){
        first = first_;
        second = second_;
    }
    bool operator < (const Name_ &o) const{
        if(second == o.second){
            return first < o.first;
        }else{
            return second < o.second;
        }
    }
};

string f,s;

int main(){
    vector<Name_> names;
    cin >> n;
    for(int i = 0; i < n; i++){
        cin >> f >> s;
        names.push_back(Name_(f,s));
    }
    sort(names.begin(), names.end());
    for(int i = 0; i < n; i++){
        cout << names[i].first << " " << names[i].second << endl;
    }
    return 0;
}
