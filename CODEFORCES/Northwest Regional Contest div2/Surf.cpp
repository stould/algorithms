#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
#define MAXN 2000003
#define ll long long

using namespace std;

ll tree[MAXN*4];

ll rmq(int node, int l, int r, int ra, int rb){
    if(l > rb || r < ra){
        return -1;
    }else if(l >= ra && r <= rb){
        return tree[node];
    }else{
        int mid = (l+r) >> 1;
        ll q1 = rmq(node*2, l, mid, ra, rb);
        ll q2 = rmq(node*2+1, mid+1, r, ra, rb);
        return max(q1, q2);
    }
}
       
void update(int node, int l, int r, int pos, int value) {
    if (l > r) return;     
    if (l == r) {
        tree[node] = value;
    } else {
        int m = (l + r) >> 1;
        if (pos <= m) {
            update(2 * node, l, m, pos, value);
        } else {
            update(2 * node + 1, m + 1, r, pos, value);
        }
        tree[node] = max(tree[node*2], tree[node*2+1]);
    }
}

int n;

struct Range{
    int l, r;
    ll val;
    Range(){}
    Range(int L, int R, ll V): l(L), r(R), val(V){}
    bool operator < (const Range &o) const{
        if(l == o.l) return r < o.r;
        return l < o.l;
    }
};

int main(){
    cin >> n;
    vector<Range> v;
    for(int i = 0; i < n; i++){
        Range tmp;
        cin >> tmp.l >> tmp.val >> tmp.r;
        tmp.r += tmp.l;
        v.push_back(tmp);
    }
    sort(v.begin(), v.end());

    ll ans = 0;
    for(int i = 0; i < n; i++){
        ll last = rmq(1,0,MAXN-1,0,v[i].l);
        ans = max(ans, last + v[i].val);
        update(1,0,MAXN-1,v[i].r, max(v[i].val + last, rmq(1,0,MAXN-1, v[i].r, v[i].r)));
    }
    cout << ans << endl;
    return 0;
}
