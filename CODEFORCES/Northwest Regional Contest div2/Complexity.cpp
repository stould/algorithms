#include <bits/stdc++.h>
#define INF 1000000
#define MAXN 501
using namespace std;

string ss;

int freq[30];

int main(){
  cin >> ss;
  for(int i = 0; i < ss.size(); i++){
    freq[ss[i] - 'a']++;
  }
  vector<int> vv;
  for(int i = 0; i < 30; i++){
    if(freq[i] > 0){
      vv.push_back(freq[i]);
    }
  }
  sort(vv.begin(), vv.end());
  int ans = 0;
  for(int i = 0; i < vv.size() - 2; i++){
    ans += vv[i];
  }
  cout << ans << endl;
  return 0;
}
