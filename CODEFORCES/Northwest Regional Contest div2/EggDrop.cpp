#include <bits/stdc++.h>
#define INF 1000000
using namespace std;

int n,k, flor;
string what;

int can_pass[102];

int main(){
  cin >> n >> k;
  can_pass[1] = 1;
  can_pass[k] = 2;
  
  for(int i = 0; i < n; i++){
    cin >> flor >> what;
    if(what == "SAFE"){
      for(int j = 1; j <= flor; j++){
	can_pass[j] = 1;
      }
    }else{
      for(int j = flor; j <= k ; j++){
	can_pass[j] = 2;
      }
    }
  }
  
  int lb = k, hs = 0;
  while(lb > 0){
    if(can_pass[lb] == 1){
      lb++;
      break;
    }
    lb--;
  }
  for(int i = 1; i <= k; i++){
    if(can_pass[i] == 2){
      hs = i-1;
      break;
    }
  }
  cout << lb << " " << hs << endl;
  return 0;
}
