#include <bits/stdc++.h>

using namespace std;

string op[11];
int val[11], n;
int main(){
  cin >> n;
  for(int i = 0; i < n; i++){
    cin >> op[i] >> val[i];
  }
  int ans = 0;
  for(int i = 1; i <= 100; i++){
    int sum = i;
    bool can = 1;
    for(int j = 0; j < n && can; j++){
      if(op[j] == "ADD") sum += val[j];
      if(op[j] == "SUBTRACT") sum -= val[j];
      if(op[j] == "MULTIPLY") sum *= val[j];
      if(op[j] == "DIVIDE"){
	if(sum % val[j] != 0){
	  can = 0;
	}
	sum /= val[j];
      }
      if(sum < 0){
	can = 0;
      }
    }
    if(!can) ans++;
  }
  cout << ans << endl;
  return 0;
}
