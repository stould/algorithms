#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
#define MAXN 100001
using namespace std;

int n, val[MAXN];

int main(){
    cin >> n;
    for(int i = 0; i < n; i++){
        cin >> val[i];
    }
    sort(val, val+n);
    int ans = INF;
    for(int i = 0; i < n / 2; i++){
        //cout << val[i] << " + " << val[n-i-1] << endl;
        ans = min(ans, val[i] + val[n-i-1]);
    }
    cout << ans << endl;
    return 0;
}
