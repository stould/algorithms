#include <bits/stdc++.h>
#define INF 1000000
#define MAXN 501
using namespace std;

int n,m;
string matrix[MAXN];
int dist[MAXN][MAXN];
int dl[4], dc[4];

void fixMove(int l, int c){
  dl[0] = -(matrix[l][c] - '0');
  dc[0] = 0;
  
  dl[1] = 0;
  dc[1] = (matrix[l][c] - '0');
  
  dl[2] = (matrix[l][c] - '0');
  dc[2] = 0;
  
  dl[3] = 0;
  dc[3] = -(matrix[l][c] - '0');
}

int main(){
  cin >> n >> m;
  for(int i = 0; i < n; i++){
    cin >> matrix[i];
    for(int j = 0; j < m; j++){
      dist[i][j] = INF;
    }
  }
  queue<pair<int, int> > q;
  q.push(make_pair(0,0));
  dist[0][0] = 0;
  while(!q.empty()){
    int ltop = q.front().first;
    int ctop = q.front().second;
    q.pop();
    fixMove(ltop, ctop);
    for(int i = 0; i < 4; i++){
      int nl = ltop + dl[i];
      int nc = ctop + dc[i];
      if(nl >= 0 && nl < n && nc >= 0 && nc < m){
	if(dist[ltop][ctop] + 1 < dist[nl][nc]){
	  dist[nl][nc] = dist[ltop][ctop] + 1;
	  q.push(make_pair(nl,nc));
	}
      }
    }
  }
  if(dist[n-1][m-1] == INF){
    cout << "IMPOSSIBLE" << endl;
  }else{
    cout << dist[n-1][m-1] << endl;
  }
  return 0;
}
