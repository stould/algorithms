#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
#define MAXN 2000003
#define ll long long

using namespace std;

int v;

int main(){
    vector<int> sizea;
    vector<int> sizeb;
    for(int i = 0; i < 3; i++){
        cin >> v;
        sizea.push_back(v);
    }
    for(int i = 0; i < 3; i++){
        cin >> v;
        sizeb.push_back(v);
    }
    sort(sizea.begin(), sizea.end());
    sort(sizeb.begin(), sizeb.end());
    bool can = 1;
    for(int i = 0; i < 3; i++){
        if(sizea[i] != sizeb[i]) can = 0;
    }
    if(can){
        can = 0;
        do{
            if(sizea[0] * sizea[0] + sizea[1] * sizea[1] == sizea[2] * sizea[2]){
                can = 1;
            }
        }while(!can && next_permutation(sizea.begin(), sizea.end()));
    }
    cout << (can ? "YES" : "NO") << endl;
    return 0;
}
