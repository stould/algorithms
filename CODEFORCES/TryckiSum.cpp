#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>

#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

int t;

ll n;

int main(void) {
	cin >> t;
	while(t--){
		cin >> n;
		ll dig = n*(n+1)/2;
		ll sum = 1;
		while(sum <= n){
			dig -= sum;
			sum *= 2;
		}
		cout << - sum +  dig + 1 << endl;
	}
	return 0;
}
