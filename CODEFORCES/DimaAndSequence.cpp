#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100003
#define ll long long
#define INF 0xffffff
#define PI acos(-1)
 
using namespace std;

int seq[100];

void toBin(int v){
	if(v <= 0){
		return;
	}else{
		toBin(v/2);
		cout << v % 2 << " ";
	}
}

ll v;
ll s[101];
ll n;

int main(void){
	
	cin >> n;
	for(int i = 0; i < n; i++){
		cin >> v;
		s[__builtin_popcount(v)]++;
	}

	ll ans = 0;
	for(int i = 0; i < 101; i+=1){
		ans += (s[i] * (s[i] - 1)) >> 1;
	}
	cout << ans << endl;
	return 0;
}
