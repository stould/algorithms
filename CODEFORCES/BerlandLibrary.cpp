#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <math.h>
#include <string.h>
#include <stack>
#include <map>
#include <set>
#define ll long long
#define INF 0x3f3f3f3f
#define MAXN 100001

using namespace std;

char op[301];
int n, id[301];

vector<int> other(){
	set<int> plus;
	vector<int> ans;
	for(int i = 0; i < n; i++){
		if(op[i] == '-'){
			if(plus.find(id[i]) == plus.end()){
				ans.push_back(id[i]);
			}
		}else{
			plus.insert(id[i]);
		}
	}
	return ans;
}

int main(void){
	cin >> n;
	set<int> st;
	int ans = 0;
	for(int i = 0; i < n; i++){
		cin >> op[i] >> id[i];
	}
	vector<int> generalId = other();
	vector<char> generalOp;
	for(int i = 0; i < generalId.size(); i++){
		generalOp.push_back('+');
	}
	for(int i = 0; i < n; i++){
		generalId.push_back(id[i]);
		generalOp.push_back(op[i]);
	}
	for(int i = 0; i < generalId.size(); i++){
		if(generalOp[i] == '+'){
			st.insert(generalId[i]);
		}else{
			st.erase(generalId[i]);
		}
		ans = max(ans, (int)st.size());
	}
	cout << ans << endl;
	return 0;
}
