#include <iostream>
#include <vector>
#include <cmath>
#include <stdio.h>
#include <queue>
#include <algorithm>
#include <stdlib.h>
#include <sstream>
#include <map>
#include <string.h>
#include <time.h>
#include <utility>
#include <bitset>
#include <set>
#define MAXN 100100
#define INF 9999999
#define ll long long
using namespace std;

long double EPS = 1e-9;

struct p{
    long double x,y,angle,idx;
    bool operator<(p other)const{
        return angle < other.angle;
    }
};

long double pe(p A,p B){
    return ((long double)A.x * (long double)B.x) + ((long double)A.y * (long double)B.y);
}
long double pv(p A,p B){
    return sqrt(((long double)A.x * (long double)A.x + (long double)A.y * (long double)A.y ) * ((long double)B.x * (long double)B.x + (long double)B.y * (long double)B.y ));
}

int main(){
    long double PI=acos((long double)(-1));
    priority_queue<p> q1,q2;
    int N;
    cin >> N;
    p BL = (p){(long double)-10001.0,(long double)0.0,0,0};
    p BR = (p){(long double)10001.0,(long double)0.0,0,0};
    for(int i=0;i<N;i++){
        long double x,y;
        cin >> x >> y;
        p A = (p){x,y,0,0};
        long double angle;
        if(y >= 0){
            angle = acos(pe(A,BL)/pv(A,BL));
        }else{
            angle = PI + acos(pe(A,BR)/pv(A,BR));
        }
        q1.push((p){x,y,-angle,i+1});
    }

    vector<p> vec;
    while(!q1.empty()){
        p p1 = q1.top(); q1.pop();
        abs(p1.angle);
        vec.push_back(p1);
    }
    std::cout.precision(15);
    long double x1,y1;
    long double minn = INF;
    long double diff;
    vec.push_back(vec[0]);
    for(int i=0;i<vec.size()-1;i++){
        diff = abs(atan2(vec[i].x,vec[i].y) - atan2(vec[i+1].x,vec[i+1].y));
        diff =  min(diff,	(2.0*PI)-abs((atan2(vec[i].x,vec[i].y) - atan2(vec[i+1].x,vec[i+1].y))));
        if(diff < minn){
            minn = diff;
            x1 = vec[i].idx;
            y1 = vec[i+1].idx;
        }
    }
    cout << x1 << " " << y1 << endl;
    return 0;
}
