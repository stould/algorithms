#include <algorithm>
#include <vector>
#include <iostream>
#include <cmath>

using namespace std;

int main(){
    double vp, vd, t, f, c;
    cin >> vp >> vd >> t >> f >> c;
	if(vp >= vd){
		cout << 0 << endl;
		return 0;
	}
    double s0p = vp * t;
	int ans = 0;
	while(1){
		double spent = (s0p) / (vd - vp);
		double dist = s0p + vp*spent;
		s0p = dist + (spent + f) * vp;
		if(c-dist <= 0) break;
		ans++;
	}
	cout << ans << endl;
    return 0;
}
