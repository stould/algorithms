#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <map>
#include <math.h>
#include <string.h>
#define MAXN 103
#define INF 0x3f3f3f3fLL
#define ll long long int
#define MOD 100000000
 
using namespace std;

bool zeros(int s){
  int ans = 0;
  if(s == 0) ans = 1;
  while(s){
    if(s%10 == 0) return 1;
    s/=10;
    ans++;
  }
  return ans;
}

int k, v[101];

int main(void){
	cin >> k;
	for(int i = 0; i < k; i++) cin >> v[i];
	sort(v,v+k);
	reverse(v,v+k);
	vector<int> path;
	//tentar achar 4;
	if(v[0] == 100){
		path.push_back(100);
		bool F = 0;
		for(int i = 10; i < 100 && !F; i += 10){
			for(int j = 1; j < k; j++){
				if(i == v[j]){
					F = 1;
					path.push_back(i);
					for(int L = j+1; L < k; L++){
						if(v[L] > 0 && v[L] < 10){
							path.push_back(v[L]);
							break;
						}
					}
					break;
				}
			}
		}
		if(!F){
			for(int i = 1; i < k; i++){
				if(v[i] % 10 != 0 && v[i] != 0){
					path.push_back(v[i]);
					break;
				}
			}
		}
		if(v[k-1] == 0){
			path.push_back(v[k-1]);
		}
	}else{
		bool F = 0;
		for(int i = 10; i < 100 && !F; i+= 10){
			for(int j = 0; j < k; j++){
				if(i == v[j]){
					F = 1;
					path.push_back(i);
					for(int L = j+1; L < k; L++){
						if(v[L] > 0 && v[L] < 10){
							path.push_back(v[L]);
							break;
						}
					}
					break;
				}
			}
		}

		if(!F){
			for(int i = 0; i < k; i++){
				if(v[i] % 10 != 0 && v[i] != 0){
					path.push_back(v[i]);
					break;
				}
			}
		}
		if(v[k-1] == 0){
			path.push_back(v[k-1]);
		}		
	}
	cout << path.size() << endl;
	for(int i = 0; i < path.size(); i++) cout << path[i] << " ";
	return 0;
}
