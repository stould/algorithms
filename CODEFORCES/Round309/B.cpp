#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

string grid[101];
int n;
int main(void) {
	cin >> n;
	for(int i = 0; i < n; i++){
		cin >> grid[i];
	}
	int ans = 0;
	for(int i = 0; i < n; i++){
		int cnt = 0;
		for(int j = 0; j < n; j++){
			if(grid[i][j] == '1') cnt++;
		}
		if(cnt == n) ans++;
	}
	for(int i = 0; i < n; i++){
		//lets clean this line
		for(int j = 0; j < n; j++){
			if(grid[i][j] == '0'){
				for(int k = 0; k < n; k++){
					if(grid[k][j] == '0') grid[k][j] = '1';
					else grid[k][j] = '0';
				}
			}
		}
		int clean = 0;
		for(int j = 0; j < n; j++){
			int cnt = 0;
			for(int k = 0; k < n; k++){
				cnt += grid[j][k] == '1';
			}
			if(cnt == n) clean++;
		}
		ans = max(ans, clean);
		//lets clean this line
		for(int j = 0; j < n; j++){
			if(grid[i][j] == '1'){
				for(int k = 0; k < n; k++){
					if(grid[k][j] == '1') grid[k][j] = '0';
					else grid[k][j] = '1';
				}
			}
		}
	
	}
	cout << ans << endl;
	return 0;
}
