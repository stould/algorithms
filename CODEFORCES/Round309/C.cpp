#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1
#define MOD 1000000007

using namespace std;

ll n, cnt[1001];
ll dp[1001][1001];

ll func(int id, int ball) {
	if (id == 1) {
		return 1;
	} else if (ball == 0) {
		return 1;
	} else {
		ll &resp = dp[id][ball];
		if (resp == -1) {
			resp = ((func(id - 1, ball)%MOD) + (func(id, ball - 1)%MOD))%MOD;		
		}
		return resp;
	}
}

int main(void) {
	cin >> n;
	for(int i = 0; i < n; i++){
		cin >> cnt[i];
	}
	memset(dp,-1,sizeof(dp));
	ll ans = 1, balls = 1;
	for(int i = 0; i < n; i++){
		ans *= func(balls, cnt[i]-1);
		ans %= MOD;
		balls += cnt[i];
	}
	cout << ans << endl;
	return 0;
}
