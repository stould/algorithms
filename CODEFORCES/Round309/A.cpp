#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;
	
string in;


int main(void) {
	cin >> in;
	set<string> st;
	for(int ch = 0; ch < 26; ch++){
		for(int i = 0; i <= in.size(); i++){
			string l = "",r="";
			for(int j = 0; j < i; j++){
				l += string(1, in[j]);
			}
			
			for(int j = i; j < in.size(); j++){
				r += string(1, in[j]);
			}
			//cout << l+string(1, ch+'a')+r << endl;
			st.insert(l+string(1, ch+'a')+r);
		}
	}
	cout << st.size() << endl;
	return 0;
}
