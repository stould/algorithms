#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <math.h>
#include <string.h>
#include <stack>
#include <map>
#include <set>
#define ll long long
#define INF 0x3f3f3f3f
#define MAXN 2007
using namespace std;

int n,t,vis[MAXN][MAXN];
double p, dp[MAXN][MAXN];

double func(int people, int timePast){
	if(people == n){
		return (double) people;
	}else if(timePast == t){
		return (double) people;
	}else{
		if(vis[people][timePast] == 0){
			vis[people][timePast] = 1;
			dp[people][timePast] = (double) p * func(1+people, timePast+1);
			dp[people][timePast] += (double) (1.0-p) * func(people, timePast+1);
		}
		return dp[people][timePast];
	}
}

double func2(){
	dp[0][0] = 1;
	for(int i = 0; i <= t; i++){//tempo
		for(int j = 0; j <= n; j++){//pessoas
			dp[i+1][j+1] += dp[i][j] * p;
			dp[i+1][j] += dp[i][j] * (1.-p);
		}
		dp[i+1][n] += dp[i][n];
	}
	double ans = 0;
	for(int i = 0; i <= n; i++) ans += dp[t][i] * i;
	return ans;
}

int main(void){
	//n = people
	//p = prob
	//t = seconds
	//poeple could move sec in sec
	cin >> n >> p >> t;
	memset(vis,0,sizeof(vis));
	cout << func2() << endl;
	return 0;
}
