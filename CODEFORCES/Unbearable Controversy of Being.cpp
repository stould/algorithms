#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 3001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
 
 
using namespace std;
 
int n, m, u, v;
int pascal[MAXN];
bool g[MAXN][MAXN];
vector<vector<int> > gl(MAXN);

int main(void){
	ios::sync_with_stdio(0);
	cin >> n >> m;
	pascal[0] = pascal[1] = 0;
	pascal[2] = 1;
	for(int i = 3; i <= n; i++){
		pascal[i] += pascal[i-1] + i - 1;
	}
	for(int i = 0; i < m; i++){
		cin >> u >> v;
		g[u][v] = 1;
		gl[u].push_back(v);
	}
	ll ans = 0;
	for(int i = 1; i <= n; i++){
		for(int j = 1; j <= n; j++){
			if(i != j){
				int paths = 0;
				for(int k = 0; k < gl[i].size(); k++){
					int nextA = gl[i][k];
					for(int l = 0; l < gl[nextA].size(); l++){
						if(gl[nextA][l] == j) paths++;
					}
				}
				ans += pascal[paths];
			}
		}
	}
	cout << ans << endl;
	return 0;
}
