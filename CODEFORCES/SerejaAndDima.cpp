#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 1003
#define ll long long int
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define MOD 1000000007LL
  
using namespace std;

int n, val[MAXN];

int main(){
	scanf("%d", &n);
	for(int i = 0; i < n; i++){
		scanf("%d", &val[i]);
	}
	int L = 0, R = n-1;
	int sum[2];
	sum[0] = sum[1] = 0;
	bool player = 0;
	while(L != R){
		if(val[L] > val[R]){
			sum[player] += val[L++];
		}else{
			sum[player] += val[R--];
		}
		player ^= 1;
	}
	sum[player] += val[L];
	printf("%d %d\n", sum[0], sum[1]);
    return 0;
}
