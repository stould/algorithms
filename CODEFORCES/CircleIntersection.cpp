#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>

#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

long double X1, X2, Y1, Y2;
long double r;
long double R;
long double d;



int main(void) {
	cin >> X1 >> Y1 >> r;
	cin >> X2 >> Y2 >> R;
	d = hypot(X2-X1, Y2-Y1);
	cout.precision(12);
	if(R < r){
		swap(R,r);
	}
	long double ans;
	if(d >= r+R){
		cout << "LOL" << endl;
		ans = 0;
	}else if(d+r <= R){
		cout << "AHAH" << endl;
		ans = min(acos((long double) -1) * r * r, acos((long double) -1) * R * R);
	}else{
		long double part1 = r*r*acos((d*d + r*r - R*R)/((long double)2.*d*r));
		long double part2 = R*R*acos((d*d + R*R - r*r)/((long double)2.*d*R));
		long double part3 = (long double)0.5*sqrt((-d+r+R)*(d+r-R)*(d-r+R)*(d+r+R));
		ans = part1 + part2 - part3;
	}
	cout << ans << endl;
	return 0;
}
