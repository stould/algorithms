#include <bits/stdc++.h>
#define MAXN 1003
using namespace std;

int T, n;
string s;

//0=rock
//1=paper
//2=scisor

int freq[MAXN][3];

int main(void) {
    cin >> T;
    while(T--){
        memset(freq,0,sizeof(freq));
        cin >> n >> s;
		s = " "+s;
        for(int i = 1; i <= n; i++){
            int pos;
            switch(s[i]){
                case 'R': pos = 0;break;
                case 'P': pos = 1;break;
                case 'S': pos = 2;break;
            }
            freq[i][pos]++;
            for(int j = 0; j < 3; j++) freq[i][j] += freq[i-1][j];
        }
        int ans = 0;
        for(int i = 0; i <= n; i++){
            for(int j = 0; i + j <= n; j++){
				int ties = 0;
                int winsRock = freq[i][2];
				ties += freq[i][0];

				int winsPaper = freq[i+j][0] - freq[i][0];
				ties += freq[i+j][1] - freq[i][1];

				int winsScisors = freq[n][1] - freq[i+j][1];
				ties += freq[n][2] - freq[i+j][2];

                if((winsRock+winsPaper+winsScisors)*2 > n-ties){
                    ans++;
                }
            }
        }
        cout << ans << endl;
    }
    return 0;
}
