#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100003
#define ll long long
#define INF 0xffffff
#define PI acos(-1)
#define RIGHT 0
#define LEFT 1
 
using namespace std;
 
ll v, tree[MAXN << 2], lazy[MAXN << 2];
 
void propagate(int node, int l, int r){
    if(lazy[node] > 0){
        int L = (node << 1);
        int R = L | 1;
        tree[node] = lazy[node];
        if(l != r){
            lazy[L] = lazy[node];
            lazy[R] = lazy[node];
        }
        lazy[node] = 0;
    }
}
 
ll query(int node, int l, int r, int bl, int br){
    if(l > r || l > br ||  r < bl){
        return 0LL;
    }else{
        propagate(node, l, r);
        if(l >= bl && r <= br){
            return tree[node];
        }else{
            int mid = (l + r) >> 1;
            int L = (node << 1);
            int R = L | 1;
            return max(query(L, l, mid, bl, br), query(R, mid+1, r, bl, br));
        }
    }
}
 
void update(int node, int l, int r, int bl, int br, ll v){
    propagate(node, l, r);
    if(l > r || l > br ||  r < bl){
        return;
    }else{
        if(l >= bl && r <= br){
            lazy[node] = v;
            propagate(node,l,r);
            return;
        }else{
            int mid = (l + r) >> 1;
            int L = (node << 1);
            int R = L | 1;
            update(L, l, mid, bl, br,v);
            update(R, mid+1, r, bl, br,v);
            tree[node] = max(tree[L], tree[R]);
        }
    }
}

int n,m,w,h;

int main(void){
	cin >> n;
	
	for(int i = 0; i < n; i++){
		cin >> h;
		update(1,0,n-1,i,i,h);
	}
	cin >> m;
	vector<ll> ans;
	for(int i = 0; i < m; i++){
		cin >> w >> h;
		ll maxH = query(1,0,n-1,0,w-1);
		ans.push_back(maxH);
		update(1,0,n-1,0,w-1,maxH+h);
	}
	for(int i = 0; i < (int)ans.size(); i++){
		cout << ans[i] << " ";
	}
	return 0;
}
