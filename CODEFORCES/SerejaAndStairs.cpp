U#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100003
#define ll long long int
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define MOD 1000000007LL
  
using namespace std;

int n, val[MAXN], escada[MAXN];
int cnt[5001];

int main(){
	scanf("%d", &n);
	for(int i = 0; i < n; i++){
		scanf("%d", &val[i]);
		cnt[val[i]]++;
	}
	int idx = 0;
	for(int i = 1; i <= 5000; i++){
		if(cnt[i] > 0){
			cnt[i]--;
			escada[idx++] = i;
		}
	}
	for(int i = escada[idx-1] - 1; i > 0; i--){
		if(cnt[i]){
			escada[idx++] = i;
		}
	}
	cout << idx << endl;
	for(int i = 0; i < idx; i++){
		cout << escada[i] << " ";
	}
	cout << endl;
    return 0;
}
