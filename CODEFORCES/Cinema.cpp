#include <bits/stdc++.h>
#define MAXN 2000001
#define ll long long int
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

ll n, m;
ll v[MAXN], audio[MAXN], sub[MAXN];

map<ll, ll> sl;

int main(void) {
    cin >> n;
    for(int i = 0; i < n; i++){
        cin >> v[i];
        if(sl.find(v[i]) == sl.end()){
            sl[v[i]] = 1;
        }else{
            sl[v[i]]++;
        }
    }
    cin >> m;
    for(int i = 0; i < m; i++){
        cin >> audio[i];
    }
    for(int i = 0; i < m; i++){
        cin >> sub[i];
    }
    ll bestAudio = -1, bestSub = -1;
    ll idx = 0;
    for(int i = 0; i < m; i++){
        ll muitoSatisfeito = sl[audio[i]];
        ll quaseSatisfeito = sl[sub[i]];
        if(muitoSatisfeito > bestAudio){
            bestAudio = muitoSatisfeito;
            bestSub = quaseSatisfeito;
            idx = i;
        }else if(muitoSatisfeito == bestAudio && quaseSatisfeito > bestSub){
            bestSub = quaseSatisfeito;
            idx = i;
        }
    }
    cout << idx+1 << endl;
}
