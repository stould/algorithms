#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f3f3f3fLL
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

int n,na,nb,v;
set<pair<deque<int>,deque<int> > > vis;

pair<int,int> simul(deque<int> a, deque<int> b, int round){
	pair<deque<int>, deque<int> > state = make_pair(a,b);
	if(vis.find(state) != vis.end()){
		return make_pair(-1,-1);
	}else{
		vis.insert(state);
		if(a.size() == 0){
			return make_pair(round, 2);
		}else if(b.size() == 0){
			return make_pair(round, 1);
		}else{
			int topA = a.back();
			a.pop_back();
			int topB = b.back();
			b.pop_back();
			if(topA > topB){
				a.push_front(topB);
				a.push_front(topA);				
			}else{
				b.push_front(topA);
				b.push_front(topB);
			}
			return simul(a,b,round+1);
		}
	}
}

int main(void) {
	cin >> n >> na;
	deque<int> A,B;
	for(int i = 0; i < na; i++){
		cin >> v;
		A.push_front(v);
	}
	cin >> nb;
	for(int i = 0; i < nb; i++){
		cin >> v;
		B.push_front(v);
	}
	pair<int,int> ans = simul(A, B, 0);
	if(ans.first == -1){
		cout << -1 << endl;
	}else{
		cout << ans.first << " " << ans.second << endl;
	}
	return 0;
}
