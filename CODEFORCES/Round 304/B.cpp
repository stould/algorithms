#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f3f3f3fLL
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

int n, v[3001];


int main(void) {
	while(cin >> n){
		for(int i = 0; i < n; i++) cin >> v[i];
		sort(v,v+n);
		int ans = 0;
		for(int i = 1; i < n; i++){
			while(v[i] <= v[i-1]) {
				ans ++;
				v[i]++;			
			}
		}
		cout << ans << endl;
	}
	return 0;
}
