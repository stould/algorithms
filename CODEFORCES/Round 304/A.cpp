#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f3f3f3fLL
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

ll k,n,w;

int main(void) {
	cin >> k >> n >> w;
	ll V = 0;
	for(int i = 1; i <= w; i++){
		V += k*i;
	}
	if(V > n){
		cout << V - n << endl;
	}else{
		cout << 0 << endl;
	}
	return 0;
}
