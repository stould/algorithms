#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100003
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int n;
ll tree[MAXN*4], dp[MAXN];

struct Evt{
	ll d, t, v;
	int idx;
	Evt(){}
	Evt(ll D, int T, int V, int IDX){
		d = D;
		t = T;
		v = V;
		idx = IDX;
	}
	bool operator < (const Evt &o) const{
		if(d == o.d){
			return idx < o.idx;
		}else{
			return d < o.d;
		}
	}
};

vector<Evt> ori, sor;
map<Evt, int> mp;

ll query(int node, int l, int r, int bl, int br){
	if(l > r || l > br || r < bl){
		return 0;
	}else if (l >= bl && r <= br){
		return tree[node];
	}else{
		int mid = l + (r-l) / 2;
		return max(query(node*2, l, mid, bl, br), query(node*2+1, mid+1, r, bl, br));
	}
}

void update(int node, int l, int r, int idx, ll val){
	if(l > r) return;
	if (l == r){
		tree[node] = val;
	}else{
		int mid = l + (r-l) / 2;
		if(idx <= mid){
			update(node*2, l, mid, idx, val);
		}else{
			update(node*2+1, mid+1, r, idx, val);
		}
		tree[node] = max(tree[2*node], tree[2*node+1]);
	}
}

int lower(ll val){
	int lo = 0, hi = n-1, ans = n-1;
	while(lo <= hi){
		int mid = lo + (hi-lo) / 2;
		if(sor[mid].d >= val){
			ans = min(ans, mid);
			hi = mid-1;
		}else{
			lo = mid+1;
		}
	}
	return ans;
}

int upper(ll val){
	int lo = 0, hi = n-1, ans = 0;
	while(lo <= hi){
		int mid = lo + (hi-lo) / 2;
		if(sor[mid].d <= val){
			ans = max(mid, ans);
			lo = mid+1;
		}else{
			hi = mid-1;
		}
	}
	return ans;
}

int main(void){
	cin >> n;
	for(int i = 0; i < n; i++){
		Evt tmp;
		tmp.idx = i;
		cin >> tmp.d >> tmp.v >> tmp.t;
		ori.push_back(tmp);
		sor.push_back(tmp);
	}
	sort(sor.begin(),sor.end());
	for(int i = 0; i < n; i++){
		mp[sor[i]] = i;
	}
	ll ans = 0;
	for(int i = 0; i < n; i++){
		int lo = lower(ori[i].d-ori[i].t);
		int hi = upper(ori[i].d);
		ll best = query(1,0, n-1, lo, hi);
		ans = max(best + ori[i].v, ans);
		update(1,0, n-1, mp[ori[i]], best + ori[i].v);
	}
	cout << ans << endl;
	return 0;
}
