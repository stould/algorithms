#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>

#define MAXN 100003
#define MAXK 13
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;


ll k, l, r;

int main(void) {
	cin >> k >> l >> r;
	unsigned ll ans = 0;
	if(r < 0){
		l = abs(l);
		r = abs(r);
		swap(l,r);
	}
	if(r >= 0){
		if(l <= 0){
			l = abs(l);
			ans = l / k + 1;
		}else{
			l--;
			ans = l / k;
		}
	}
	cout << ans << endl;
	return 0;
}
