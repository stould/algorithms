#include <bits/stdc++.h>
#define MAXN 501
#define ll long long int
#define INF 10101010101010101ll

using namespace std;


int n, k;

ll val[MAXN], pd[MAXN][MAXN][2];

string intToStr(int val){
    string ans = "";
    if(val == 0){
        return "0";
    }
    while(val > 0){
        ans = string(1, '0' + val % 10) + ans;
        val /= 10;
        
    }
    return ans;
}

int bigger(int a, int b){
    return intToStr(a) < intToStr(b) ? a : b;
}

ll func(int pos, int restam, bool first){
    if(restam == 0){
        return 0;
    }else{
        ll &ans = pd[pos][restam][first];
        if(ans == -1){
            ans = -INF;
            for(int i = pos + first; i < n; i++){
                ll pen = abs(val[i] - val[pos]) * (i - pos - 1);
                ll profit = val[i] + (first == 0 ? 0 : -pen);
                ans = max(ans, profit + func(i, restam - 1, 1));
            }
        }
        return ans;
    }
}

void path(int pos, int restam, bool first){
    if(restam == 0){
        return;
    }else{
        ll ans = -INF, id = -1;
        for(int i = pos + first; i < n; i++){
            ll pen = abs(val[i] - val[pos]) * (i - pos - 1);
            ll profit = val[i] + (first == 0 ? 0 : -pen);
            ll next = profit + func(i, restam - 1, 1);
            if(next > ans){
                ans = next;
                id = i;
            }else if(next == ans){
                if(intToStr(id+1) > intToStr(i+1)){
                    id = i;
                }
            }
        }
        cout << id + 1 << " ";
        path(id, restam - 1, 1);
    }
    
}


int main(void){
    cin >> n >> k;
    for(int i = 0; i < n; i++){
        cin >> val[i];
    }
    memset(pd, -1,sizeof(pd));
    cout << func(0, k, 0) << endl;
    path(0, k, 0);
    cout << endl;
    return 0;
}
