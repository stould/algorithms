#include <bits/stdc++.h>
#define MAXN 100103
#define INF 92233720368547758LL
#define ll long long
using namespace std;

ll cost[MAXN+5];


struct st{
    int id;
    ll cost;
    string s;
    bool operator < (const st &o) const{
        return s < o.s;
    }
};

int n;
string str;

vector<vector<int> > pocket(MAXN+5);
vector<vector<pair<int, ll> > > graph(MAXN*2+5);
ll dist[MAXN*2+5];

struct Less{
    bool operator () (int a, int b){
        return dist[a] > dist[b];
    }
};

ll dijstra(int s, int t){
    priority_queue<int, vector<int>, Less> q;
    q.push(s);
    for(int i = 0; i < MAXN*2+5; i++){
        dist[i] = INF;
    }
    dist[s] = 0;
    while(!q.empty()){
        int top = q.top(); q.pop();
        for(auto it : graph[top]){
            int next = it.first;
            int cost = it.second;
            if(dist[top] + cost < dist[next]){
                dist[next] = dist[top] + cost;
                q.push(next);
            }
        }
    }
    return dist[t];
}


int main(){
    cin >> n;
    for(int i = 0; i < n; i++){
        cin >> cost[i];
    }
    vector<st> tmp;
    for(int i = 0; i < n; i++){
        cin >> str;
        string rev = str;
        reverse(rev.begin(), rev.end());
        st cara1, cara2;
        if(str == rev){
            cara1.id = i+1, cara1.cost = 0;
            cara1.s = str;
            tmp.push_back(cara1);
        }else{
            cara1.id = i+1, cara1.cost = 0;
            cara1.s = str;
            tmp.push_back(cara1);
            cara2.id = i+1, cara2.cost = cost[i];
            cara2.s = rev;
            tmp.push_back(cara2);
        }
    }
    sort(tmp.begin(), tmp.end());
    for(int i = 0; i < (int) tmp.size(); i++){
        pocket[tmp[i].id].push_back(i);
    }
    for(int i = 1; i <= n; i++){
        if(i == n) break;
        for(int j = 0; j < (int)pocket[i].size(); j++){
            for(int k = 0; k < (int) pocket[i+1].size(); k++){
                if(pocket[i+1][k] >= pocket[i][j]){
                    graph[pocket[i][j]].push_back(make_pair(pocket[i+1][k], tmp[pocket[i+1][k]].cost));
                    graph[pocket[i+1][k]].push_back(make_pair(pocket[i][j], tmp[pocket[i+1][k]].cost));
                }
            }
        }
    }
    int S = 2*n+2, T = 2*n+3;
    for(int i = 0; i < (int)pocket[1].size(); i++){
        graph[S].push_back(make_pair(pocket[1][i], tmp[pocket[1][i]].cost));
        graph[pocket[1][i]].push_back(make_pair(S, tmp[pocket[1][i]].cost));
    }
    for(int i = 0; i < (int)pocket[n].size(); i++){
        graph[pocket[n][i]].push_back(make_pair(T, 0));
        graph[T].push_back(make_pair(pocket[n][i], 0));
    }
    ll ans = dijstra(S,T);
    cout << (ans == INF ? -1 : ans) << endl;
    return 0;
}
