#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>

#define MAXN 200003
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

int n, m, v;
vector<int> a,b;

int search(int val){
	int lo = 0, hi = n-1, ans = 0;
	while(lo <= hi){
		int mid = lo + (hi-lo) / 2;
		if(a[mid] <= val){
			lo = mid+1;
			ans = max(ans, mid);
		}else{
			hi = mid-1;
		}
	}
	return ans;
}

int main(void) {
	scanf("%d%d", &n, &m);
	for(int i = 0; i < n; i++){
		scanf("%d", &v);
		a.push_back(v);
	}
	for(int i = 0; i < m; i++){
		scanf("%d", &v);
		b.push_back(v);
	}
	sort(a.begin(), a.end());
	for(int i = 0; i < m; i++){
		cout << search(b[i]) + 1 << " ";
		//cout << (upper_bound (a.begin(), a.end(), b[i]) - a.begin())  << " ";
	}
	cout << endl;
	return 0;
}
