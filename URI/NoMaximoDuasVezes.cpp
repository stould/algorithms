#include <stdio.h> 
#include <stdlib.h> 
#include <iostream> 
#include <vector> 
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h> 
#include <set>
#include <map> 
#include <iostream>
#include <sstream>
#define MAXN 10001
#define ll long long
#define PI acos(-1)
#define INF 0x3f3f3f3f

using namespace std;

string num;

ll dp[20][1 << 10][1 << 10][2], ans;

ll ppow(ll base, ll exp){
	ll ans = 1;
	for(int i = 0; i < exp; i++) ans *= base;
	return ans;
}

bool func(int pos, int m1, int m2, int pref){
	if(pos == num.size()){
		return true;
	}else{
		ll &ans = dp[pos][m1][m2][pref];
		if(ans == -1){
			ans = 0;
			for(int i = 9; i >= 0; i--){
				if(pref && (i > num[pos] - '0')){
					continue;
				}
				if(m1 & (1 << i)){
					if(m2 & (1 << i)){
						continue;
					}else{
						ans |= func(pos+1, m1, m2 | (1 << i), pref && (i == num[pos] - '0'));
					}
				}else{
					ans |= func(pos+1, m1 | (1 << i), m2, pref && (i == num[pos] - '0'));
				}
			}
		}
		return ans;
	}
}

void path(int pos, int m1, int m2, int pref){
	if(pos == num.size()){
		return;
	}else{
		for(int i = 9; i >= 0; i--){
			if(pref && (i > num[pos] - '0')){
				continue;
			}
			if(m1 & (1 << i)){
				if(m2 & (1 << i)){
					continue;
				}else{
					if( func(pos+1, m1, m2 | (1 << i), pref && (i == num[pos] - '0'))){
						ans += i * ppow(10, num.size() - 1 - pos);
						path(pos+1, m1, m2 | (1 << i), pref && (i == num[pos] - '0'));
						break;
					}
				}
			}else{
				if(func(pos+1, m1 | (1 << i), m2, pref && (i == num[pos] - '0'))){
					ans += i * ppow(10, num.size() - 1 - pos);
					path(pos+1, m1 | (1 << i), m2, pref && (i == num[pos] - '0'));
					break;
				}
			}
		}
	}
}

int main(void) {
	cin >> num;
	memset(dp, -1, sizeof(dp));
	func(0,0,0,1);
	ans = 0;
	path(0,0,0,1);
	cout << ans << endl;
	return 0;
}
