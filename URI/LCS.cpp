

#include <bits/stdc++.h>
using namespace std;
char a[1004], b[1004];
int dp[1004][1004], n, m;
 
int lcs(int i, int j) {
    if(i > n || j > m) return 0;
    if(dp[i][j] != -1) return dp[i][j];
    else {
        if(a[i-1] == b[j-1])
            dp[i][j] = 1 + lcs(i+1, j+1);
        else
            dp[i][j] = max(lcs(i+1, j), lcs(i, j+1));
 
        return dp[i][j];
    }
}
 

int main(void) {
    while(cin >> a) {
		n = strlen(a);
		cin >> b;
		m = strlen(b);
        memset(dp, -1, sizeof(dp));
        cout << lcs(0, 0) - 1 << endl;
    }
   
    return 0;
}


