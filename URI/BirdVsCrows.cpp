#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <map>
#include <math.h>
#include <string.h>
#define MAXN 103
#define INF 0x3f3f3f3f
#define ll long long
#define MOD 100000000

using namespace std;

int t,n,p[10],c[10];

int gcd(int a, int b){
	if(b == 0) return a;
	return gcd(b, a % b);
}

int lcm(int a, int b){
	return (a * b) / gcd(a,b);
}

int dp[1 << 10][10];

//the main idea: i'm trying to use the idx-th crow and the mask will tell me when a tree is free or not
int f(int mask, int idx){
	if(idx >= n){
		if(mask == (1 << n) -1){
			return 0;
		}else{
			return INF;
		}
	}else{
		int &ans = dp[mask][idx];
		if(ans == -1){
			ans = f(mask, idx+1);
			for(int i = 0; i < n; i++){
				if(!(mask & (1 << i))){//checking if the i-th tree is free
					int cost = lcm(p[i], c[idx]);
					ans = min(ans, max(cost, f(mask | (1 << i), idx+1)));
				}
			}
		}
		return ans;
	}
}

int main(void){
	scanf("%d", &t);
	for(int test = 1; test <= t; test++){
		scanf("%d", &n);
		for(int i = 0; i < n; i++){
			scanf("%d", &p[i]);
		}
		for(int i = 0; i < n; i++){
			scanf("%d", &c[i]);
		}
		memset(dp,-1,sizeof(dp));
		printf("Caso #%d: %d\n",test, f(0,0));
	}
    return 0;
}

