#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#include <time.h>

#define MAXN 103
#define ll long long int
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define MOD 1000000007LL
 
using namespace std;
 
bool vis[MAXN][MAXN];

int main(){
	srand(time(NULL));
	for(int test = 1; test <= 15; test++){
		int n = max(3, rand() % 100);
		int m = max(1, n / 2 + n / 3);
		cout << n << " " << m << endl;
		for(int i = 0; i < m; i++){
			int U = max(1, rand() % (n+1)), V = max(1, rand() % (n+1));
			while(vis[U][V] || U == V){
				U = max(1, rand() % (n+1));
				V = max(1, rand() % (n+1));
			}
			vis[U][V] = vis[V][U] = 1;
			cout << U << " " << V << endl;
		}
	}
	cout << "-1 -1" << endl;
    return 0;
}
