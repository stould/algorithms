#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100003
#define ll long long
#define INF 0x3f3f3f3f
 
using namespace std;
 
 
map<pair<int,int>, int> mp;
set<pair<int,int> > points;
vector<vector<int> > G(MAXN);
 
int n, X[MAXN], Y[MAXN], color[MAXN];
map<int, int> comp;
 
bool dfs(int node, int c) {
    if(color[node] != 0) {
        if(color[node] == c) {
            return true;
        } else {
            return false;
        }
    }
    comp[c]++;
    color[node] = c;
    for(int i = 0; i < (int) G[node].size(); i++){
        int next = G[node][i];
        if(!dfs(next, -c)) {
            return false;
        }
    }
    return true;
}
 
int dist(int xa, int ya, int xb, int yb){
    return (xa-xb)*(xa-xb) + (ya-yb)*(ya-yb);
}
 
int main(void){
    while(scanf("%d", &n) == 1){
		mp.clear();
		points.clear();
		for(int i = 0; i <= n; i++){
			G[i].clear();
			color[i] = 0;
		}
		for(int i = 0; i < n; i++){
			scanf("%d%d", &X[i], &Y[i]);
			points.insert(make_pair(X[i],Y[i]));
			mp[make_pair(X[i],Y[i])] = i;
		}
		for(int i = 0; i < n; i++){
			set<int> idx;
			for(int j = -5; j <= 5; j++){
				for(int k = -5; k <= 5; k++){
					if(points.find(make_pair(X [i] + j, Y[i] + k)) != points.end() && dist(X[i], Y[i], X[i] + j, Y[i] + k) <= 25){
						int pos = mp[make_pair(X[i] + j, Y[i] + k)];
						if(i != pos){
							G[i].push_back(pos);
						}
					}
				}
			}
		}
		int ans = 0;
		for(int i = 0; i < n; i++){
			if(color[i] == 0){
				comp.clear();
				dfs(i,1);
				ans += min(comp[1], comp[-1]);
			}
		}
		printf("%d\n", ans);
	}
    return 0;
}
