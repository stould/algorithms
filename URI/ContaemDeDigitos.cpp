#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#include <limits.h>
#define ll long long
     
using namespace std;
     
ll a,b;
string num;
ll dp[70][70][2][2];
     
string toStr(ll val){
	string ans = "";
	if(val <=  0) ans = "0";
	while(val > 0){
		ans = string(1, '0'+(val % 10LL)) + ans;
		val /= 10LL;
	}
	return ans;
}


ll func(int pos, int cnt, bool pref, bool seen,int what){
	if(pos == (int) num.size()){
		return cnt;
	}else{
		ll &ans = dp[pos][cnt][pref][seen];
		if(ans == -1){
			ans = 0;
			for(int i = 0; i <= 9; i++){
				if((i > num[pos] - '0' && pref)) continue;
				if(!seen){
					if(i > 0){
						ans += func(pos+1, cnt + (i == what), pref && (num[pos] - '0' == i), i > 0, what);
					}else{
						ans += func(pos+1, 0, pref && (num[pos] - '0' == i), i > 0, what);
					}
				}else{
					ans += func(pos+1, cnt + (i == what), pref && (num[pos] - '0' == i), seen, what);
				}
			}
		}
		return ans;
	}
}
     
ll digits[10];

int main(void){
	while(cin >> a >> b && a > 0 && b > 0){
		memset(digits,0,sizeof(digits));
		num = toStr(b);
		for(int i = 0; i <= 9; i++){
			memset(dp,-1,sizeof(dp));
			digits[i] = func(0,0,1,0,i);
		}
		num = toStr(a-1);
		for(int i = 0; i <= 9; i++){
			memset(dp,-1,sizeof(dp));
			digits[i] -= func(0,0,1,0,i);
		}
		cout << digits[0];
		for(int i = 1; i <= 9; i++){
			cout << " " << digits[i];
		}
		cout << endl;
	}
	return 0;
}

