#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#define MAXN 10001
#define INF 0x3f3f3f3f
#define ll long long

using namespace std;

int N, V;
int distSoFar[2];
int vertexSoFar[2];
int dist[MAXN];
vector<vector<int> > graph(MAXN);

void dfs(int U, int parent, int who){
	if(dist[U] > distSoFar[who]){
		distSoFar[who] = dist[U];
		vertexSoFar[who] = U;
	}
	for(int i = 0; i < graph[U].size(); i++){
		int next = graph[U][i];
		if(next != parent){
			dist[next] = dist[U] + 1;
			dfs(next, U, who);
		}
	}
}

int main(void){
	while(scanf("%d", &N) && N > -1){
		distSoFar[0] = distSoFar[1] = 0;
		for(int i = 1; i <= N; i++){
			graph[i].clear();
		}
		for(int i = 1; i < N; i++){
			scanf("%d", &V);
			graph[i+1].push_back(V);
			graph[V].push_back(i+1);
		}
		memset(dist,0,sizeof(dist));
		dfs(1,-1, 0);
		memset(dist,0,sizeof(dist));
		dfs(vertexSoFar[0], -1, 1);
		int ans = INF;
		for(int i = 1; i <= N; i++){
			ans = min(ans, max(abs(dist[vertexSoFar[0]] - dist[i]), abs(dist[vertexSoFar[1]] - dist[i])));
		}
		printf("%d\n", ans );
	}
    return 0;
}

