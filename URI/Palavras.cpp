#include <bits/stdc++.h>
#define MAXN 10001

using namespace std;

struct Trie{
    int id;
    int trie[MAXN][2];
    int value[MAXN+2];
    
    Trie(){
    }

    void clear(){
        id = 1;
        memset(trie,-1, sizeof(trie));
        memset(value, -1, sizeof(value));
    }
    
    void add(string &str, int root = 0){
        int n = (int) str.size();
        for(int i = 0; i < n; i++){
            int next = str[i] - '0';
            if(trie[root][next] == -1){
                trie[root][next] = id;
                root = id++;
                value[root] = 0;
            }else{
                root = trie[root][next];
            }
        }
        value[root] = 1;
    }
};

Trie t1, t2;
bool ans;

void dfs(int root1, int root2, int depth = 0){
    if(depth > 30){
        return;
    }
    if(t1.value[root1] == 1 && t2.value[root2] == 1){
        ans = 1;
    }else{
        if(t1.value[root1] == 1){
            dfs(0, root2, depth+1);
        }
        if(t2.value[root2] == 1){
            dfs(root1, 0, depth+1);
        }        
        for(int i = 0; i < 2 && ans == 0; i++){
            if(t1.trie[root1][i] != -1 && t2.trie[root2][i] != -1){
                int next1 = t1.trie[root1][i];
                int next2 = t2.trie[root2][i];
                dfs(next1, next2, depth+1);
            }
        }
    }
}

int n,m;
string tmp;

int main(){
    while(cin >> n >> m){
        ans = 0;
        t1.clear();
        t2.clear();
        for(int i = 0; i < n; i++){
            cin >> tmp;
            t1.add(tmp);
        }
        for(int i = 0; i < m; i++){
            cin >> tmp;
            t2.add(tmp);
        }
        dfs(0, 0);
        cout << (ans ? "S" : "N") << endl;
    }
    return 0;
}

