#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 1009
#define ll long long
#define PI acos(-1)
  
using namespace std;
  
int n,m,a,b,t;
  
vector<vector<pair<int, int> > > graph(MAXN);
   
stack<int> S;
int num[MAXN], low[MAXN], parent[MAXN], bridges, dfsct;
bool instack[MAXN], vis[MAXN];
  
void dfs(int u, bool dir){
    vis[u] = 1;
    for(int i = 0; i < graph[u].size(); i++){
        int next = graph[u][i].first;
		int hand = graph[u][i].second;
		if(!vis[next]){
			if(dir == 0){//undirected
				dfs(next, dir);
			}else{
				if(hand != 0){
					dfs(next, dir);
				}
			}
		}
    }
}

void bridge(int atual, int lvl){
	if(bridges) return;
	num[atual] = low[atual] = lvl;
	for(int i = 0; i < graph[atual].size(); i++){
		int next = graph[atual][i].first;
		int hand = graph[atual][i].second;		
		if(num[next] == -1){
			parent[next] = atual;
			bridge(next, lvl+1);
			if(low[next] > num[atual] && hand != 2){
				bridges++;
			}
			low[atual] = min(low[atual], low[next]);
		}else if(next != parent[atual]){
			low[atual] = min(low[atual], num[next]);
		}
	}
}

int main(void) {
    while(scanf("%d%d", &n, &m) == 2){
        while(S.size()) S.pop();
        for(int i = 1; i <= n; i++){
			vis[i] = 0;
            graph[i].clear();
        }
		int stA, stB;
        scanf("%d%d%d", &stA, &stB, &t);
        for(int i = 1; i < m; i++){
            scanf("%d%d%d", &a, &b, &t);
            if(t == 1){
				graph[a].push_back(make_pair(b,1));
				graph[b].push_back(make_pair(a,0));
			}else{
				graph[a].push_back(make_pair(b,2));
				graph[b].push_back(make_pair(a,2));				
			}
        }
		dfs(stA,0);
		if(!vis[stB]){
			printf("*\n");
		}else{
			memset(vis,0,sizeof(vis));
			dfs(stA,1);
			if(vis[stB]){
				memset(vis,0,sizeof(vis));
				dfs(stB,1);
				if(vis[stA]){
					printf("-\n");
					continue;
				}
			}
			dfsct = bridges = 0;
			//seeking for bridges
			for(int i = 1; i <= n; i++){
				num[i] = -1;
				low[i] = 0;
				parent[i] = 0;
				instack[i] = 0;
			}
			for(int i = 1; i <= n && bridges == 0; i++){
				if(num[i] == -1){
					bridge(i,0);
				}
			}
			if(bridges == 0){
				printf("1\n");
			}else{
				printf("2\n");
			}
		}
	}
    return 0;
}
