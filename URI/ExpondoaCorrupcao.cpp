#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 1007
#define ll long long int
#define INF 0xffffff
#define PI acos(-1)
#define MOD 1000000007LL

using namespace std;

int D, P, R, B, U, V;
int S[MAXN], T[MAXN];
int ga[MAXN*10], gb[MAXN*10], cntA[MAXN], cntB[MAXN], idx;
bool vis[MAXN];
int dp[150][10001];

vector<vector<int> > graph(MAXN);

void dfs(int node){
	vis[node] = 1;
	if(node < 100){
		ga[idx] += S[node];
		cntA[idx]++;
	}else{
		gb[idx] += T[node-100];
		cntB[idx]++;
	}
	for(int i = 0; i < graph[node].size(); i++){
		int next = graph[node][i];
		if(!vis[next]){
			dfs(next);
		}
	}
}


//maximize in B
int func_PPP(int pos, int money){
	if(pos == idx){
		return 0;
	}else{
		int &ans = dp[pos][money];
		if(ans == -1){
			ans = cntB[pos] + func_PPP(pos+1, money);
			if(money >= ga[pos] + gb[pos]){
				ans = max(ans, cntA[pos] + func_PPP(pos+1, money - (ga[pos] + gb[pos])));
			}
		}
		return ans;
	}
}

//maximize in A
int func_DSP(int pos, int money){
	if(pos == idx){
		return 0;
	}else{
		int &ans = dp[pos][money];
		if(ans == -1){
			ans = cntA[pos] + func_DSP(pos+1, money);
			if(money >= ga[pos] + gb[pos]){
				ans = max(ans, cntB[pos] + func_DSP(pos+1, money - (ga[pos] + gb[pos])));
			}
		}
		return ans;
	}
}

int main(){
	cin >> D >> P >> R >> B;
	idx = 0;
	for(int i = 0; i < D; i++){
		cin >> S[i];
	}
	for(int i = 0; i < P; i++){
		cin >> T[i];
	}
	for(int i = 0; i < R; i++){
		cin >> U >> V;
		U--;
		V--;
		graph[U].push_back(V+100);
		graph[V+100].push_back(U);
	}
	for(int i = 0; i < D; i++){
		if(vis[i]){
			continue;
		}
		if(graph[i].size() == 0){
			vis[i] = 1;
			cntA[idx] = 1;
			ga[idx++] = S[i];
		}else{
			dfs(i);
			idx++;
		}
	}
	for(int i = 0; i < P; i++){
		if(vis[i+100]){
			continue;
		}
		if(graph[i+100].size() == 0){
			vis[i+100] = 1;
			cntB[idx] = 1;
			gb[idx++] = T[i];
		}else{
			dfs(i+100);
			idx++;
		}
	}
	memset(dp,-1,sizeof(dp));	
	int DSP = func_DSP(0,B);
	memset(dp,-1,sizeof(dp));
	int PPP = func_PPP(0,B);
	cout << DSP << " " << PPP << endl;
	return 0;
}
