#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 103
#define ll long long int
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define MOD 1000000007LL
  
using namespace std;
  
  
int n,m,U,V;
  
int id[MAXN], height[MAXN];
  
int dfsct, num[MAXN], low[MAXN], parent[MAXN];
  
int isBridge[MAXN][MAXN], vis[MAXN];
  
int dangerVertex[MAXN], deg[MAXN], cntLeaves;
  
vector<vector<int> > tree(MAXN), g(MAXN);

int root(int x){
    if(x == id[x]){
        return x;
    }else{
        return id[x] = root(id[x]);
    }
}

bool find(int a, int b){
    return root(a) == root(b);
}
void unite(int a, int b){
    int roota = root(a);
    int rootb = root(b);
    if(roota != rootb){
        if(height[roota] >= height[rootb]){
            id[rootb] = roota;
            height[roota] += height[rootb];
        }else{
            id[roota] = rootb;
            height[b] += height[roota];
        }
    }
}

//dfs for bridges 
void bridge(int atual){
    num[atual] = low[atual] = dfsct++;
    for(int i = 0; i < g[atual].size(); i++){
        int next = g[atual][i];
        if(num[next] == -1){
            parent[next] = atual;
            bridge(next);
            if(low[next] > num[atual]){
                isBridge[next][atual] = isBridge[atual][next] = 1;
            }
            low[atual] = min(low[atual], low[next]);
        }else if(next != parent[atual]){
            low[atual] = min(low[atual], num[next]);
        }
    }
}
void countBridges(){
    dfsct = 0;
    for(int i = 1; i <= n; i++){
        num[i] = -1;
        parent[i] = 0;
    }
    for(int i = 1; i <= n; i++){
        if(num[i] == -1) bridge(i);
    }
}


//dfs to compress the nodes with no bridges between
void dfs(int u){
    vis[u] = 1;
    for(int i = 0; i < g[u].size(); i++){
        int next = g[u][i];
        if(!vis[next]){
            if(!dangerVertex[next] && !dangerVertex[u]){
                unite(u,next);
            }
            dfs(next);
        }
    }
}

//set all 'darger' vertex, those with all the edges being bridges
void setDanger(){
    for(int i = 1; i <= n; i++){
        int cnt = 0;
        for(int j = 0; j < g[i].size(); j++){
            int next = g[i][j];
            if(isBridge[i][next]){
                cnt++;
            }
        }
        dangerVertex[i] = (cnt == (int) g[i].size());
    }
}

//Making the tree, make edge (U - V) when the root are different
void mountTree(){
    for(int i = 1; i <= n; i++){
        int rootu = root(i);
        for(int j = 0; j < g[i].size(); j++){
            int next = g[i][j];
            int rootv = root(next);
            if(rootu != rootv){
                tree[rootu].push_back(rootv);
            }
        }
    }  
}
 

//dfs to count the leaves of the tree 
void dfsLeaves(int u){
    if(tree[u].size() == 1 && dangerVertex[u]){
        cntLeaves++;
    }
    vis[u] = 1;
    for(int i = 0; i < tree[u].size(); i++){
        int next = tree[u][i];
        if(!vis[next]){
            dfsLeaves(next);
        }
    }
}

//getting the asnwer
int getAnswer(){
	set<int> cagados;
    memset(vis,0,sizeof(vis));
    int ans = 0;
    for(int i = 1; i <= n; i++){
        int r = root(i);
		if(dangerVertex[i]){
			cagados.insert(i);
		}
        if(!vis[r] && dangerVertex[r]){
            if(tree[r].size() == 0){
                ans += 2;
                vis[r] = 1;
            }else{
                cntLeaves = 0;
                dfsLeaves(r);
                ans += cntLeaves;
            }
        }
    }
	//treating some cornercases
	int corner = 0;
	if(cagados.size() == 2){
		int cnt = 0;
		for(set<int>::iterator it = cagados.begin(); it != cagados.end(); it++){
			if(tree[*it].size() == 0 && g[*it].size() == 0){
				cnt++;
			}
		}
		if(cnt == cagados.size()) corner = 1;
	}else if(cagados.size() == 1){
		int cara = *cagados.begin();
		if(tree[cara].size() == 0){
			if(g[cara].size() == 0){
				corner = 1;
			}
		}
	}
    return (ans+1) / 2 + corner;
}

//eliminate the leaves while they are not dangerous
//the tree must have all leaves dangerous
void ajeita(){
    queue<int> q;
    memset(vis,0,sizeof(vis));
    for(int i = 1; i <= n; i++){
        if((int)tree[root(i)].size() == 1 && !dangerVertex[root(i)] && !vis[root(i)]){
            vis[root(i)] = 1;
            q.push(root(i));
        }
    }
    while(!q.empty()){
        int top = q.front(); q.pop();
        deg[top] = 0;
        for(int i = 0; i < tree[top].size(); i++){
            int next = tree[top][i];
            for(int j = 0; j < tree[next].size(); j++){
                if(tree[next][j] == top){
                    tree[top].erase(tree[top].begin());
                    tree[next].erase(tree[next].begin() + j);
                    i = -1;
                    break;
                }
            }
            if(dangerVertex[next]) continue;
            q.push(next);
        }
  
    }
}

inline void rd(int &x) {
    register int c = getchar_unlocked();    
    x = 0;
    int neg = 0;

    for (; ((c<48 || c>57) && c != '-'); c = getchar_unlocked());

    if (c=='-') {
        neg = 1;
        c = getchar_unlocked();
    }

    for ( ; c>47 && c<58 ; c = getchar_unlocked()) {
        x = (x<<1) + (x<<3) + c - 48;
    }

    if (neg) {
        x = -x;
    }
}

int main(){
    while(1){
		rd(n);
		rd(m);
		if(n == -1 && m == -1) break;
        for(int i = 1; i <= n; i++){
            g[i].clear();
            tree[i].clear();
            vis[i] = 0;
            deg[i] = 0;
            dangerVertex[i] = 0;
            for(int j = 1; j <= n; j++){
                isBridge[i][j] = 0;
            }
            id[i] = i;
            height[i] = 1;
        }
        for(int i = 0; i < m; i++){
            rd(U); rd(V);
            g[U].push_back(V);
            g[V].push_back(U);
        }
        countBridges();
        setDanger();
        for(int i = 1; i <= n; i++){
            if(!vis[i]){
                dfs(i);
            }
        }
        mountTree();
        ajeita();
        printf("%d\n", getAnswer());
    }
    return 0;
}
