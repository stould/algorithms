#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 1000002
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int n, dp[MAXN], stop;

string seq;

int func(int idx, int bag, bool hasNumber){
	if(idx > stop){
		if((bag % 3) == 0 && hasNumber > 0){
			return 1;
		}
		return 0;
	}else{

			dp[idx] = func(idx+1, 0, 0);
			if(!hasNumber){
				dp[idx] += func(idx+1, (seq[idx] - '0'), 1);

			}else{
				dp[idx] += func(idx+1, bag + seq[idx] - '0', 1);
				dp[idx] += func(idx+1, 0, 0);
			}

		return dp[idx];
	}
}

int main(void){
	cin >> seq;
	int ans = 0;
	int st = -1;
	memset(dp,-1,sizeof(dp));
	for(int i = 0; i < seq.size(); i++){
		if(seq[i] < '0' || seq[i] > '9'){
			if(st == -1){
				st = 0;
			}
			stop = max(i-1,0);
			cout << "from " << st << " to " << stop << endl;
			st = i+1;
		}
	}
	st = max(0,st);
	stop = seq.size();
	cout << "from " << st << " to " << seq.size()-1 << endl;
	ans += func(st,0,0);
	cout << ans << endl;
}
