#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 10005
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int vis[MAXN], low[MAXN], num[MAXN], sat[MAXN],dfs_counter, scc_counter, n, m, t, u, v;
vector<vector<int> > graph(MAXN);
stack<int> st;


void tarjan(int u, int depth) {
    low[u] = num[u] = depth;
    st.push(u);
    vis[u] = 1;
    for(int i = 0; i < graph[u].size(); i++) {
        int v = graph[u][i];
        if(num[v] == -1){
            tarjan(v, depth+1);
        }
        if (vis[v]){
            low[u] = min(low[u], low[v]);
        }
    }
    if(low[u] == depth) {
        while(1) {
            int next = st.top();st.pop();
            sat[next] = scc_counter;
            vis[next] = 0;
            if(u == next) break;
        }
        ++scc_counter;
    }
}


int main(void){
	scanf("%d", &t);
	while(t--){
		dfs_counter = scc_counter = 0;
		scc_counter = 1;
		while(st.empty() == 0) st.pop();
		scanf("%d%d", &n, &m);
		for(int i = 0; i <= n; i++){
			graph[i].clear();
            vis[i] = 0;
            num[i] = -1;
            sat[i] = -1;
            low[i] = 0;
		}
		for(int i = 0; i < m; i++){
			scanf("%d%d", &u, &v);
			graph[u].push_back(v);
		}
		for(int i = 0; i < n; i++){
			if(num[i] == -1){
				tarjan(i, 0);
			}
		}
		set<int> st;
		for(int i = 0; i < n; i++){
			if(sat[i] != 0)
			st.insert(sat[i]);
			cout << sat[i] << " ";
		}
		cout << endl;
		if(abs((int)st.size() - n) >= 2) printf("YES\n");
		else printf("NO\n");
	}
}
