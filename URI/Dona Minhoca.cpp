#include <stdio.h> 
#include <stdlib.h> 
#include <iostream> 
#include <vector> 
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h> 
#include <set>
#include <map> 
#include <iostream>
#include <sstream>
#define MAXN 10001
#define ll long long
#define PI acos(-1)
#define INF 0x3f3f3f3f

using namespace std;

int n,m,q,U,V,c,startNode,wormSize,dist[MAXN],cycleCost[MAXN],cycleIdx[MAXN],idx, deg[MAXN];
bool vis[MAXN];
vector<vector<pair<int, int> > > g(MAXN);
stack<int> st;

struct Less{
	bool operator () (int from, int to){
		return dist[from] > dist[to];
	}
};

void dfs(int node, int parent){
	vis[node] = 1;
	st.push(node);
	for(int i = 0; i < g[node].size(); i++){
		int next = g[node][i].first;
		deg[node] --;
		if(vis[next]){
			if(next != parent && cycleIdx[node] == -1){
				int tmpTo = next;
				while(!st.empty()){
					int top = st.top(); st.pop();
					cycleIdx[top] = idx;
					for(int j = 0; j < g[top].size(); j++){
						if(g[top][j].first == tmpTo){
							cycleCost[idx] += g[top][j].second;
							break;
						}
					}
					tmpTo = top;
					if(top == next) break;
				}
				idx++;
			}
			
		}else{
			dfs(next, node);
			while(!st.empty() && !deg[st.top()]){
				st.pop();
			}
		}
	}
}

void dijkstra(int s){
	for(int i = 1; i <= n; i++){
		dist[i] = INF;
	}
	dist[s] = 0;
	priority_queue<int, vector<int>, Less> q;
    q.push(s);
    while(!q.empty()){
        int top = q.top(); q.pop();
        for(int i = 0; i < g[top].size(); i++){
            int next = g[top][i].first;
            int cost = g[top][i].second;
            if(dist[top] + cost < dist[next]){
                dist[next] = dist[top] + cost;
                q.push(next);
            }
        }
    }
}

int main(void) {
	while(scanf("%d%d",&n, &m) == 2){
		idx = 0;
		while(!st.empty()){
			st.pop();
		}
		for(int i = 0; i <= n; i++){
			g[i].clear();
			vis[i] = 0;
			cycleCost[i] = 0;
			cycleIdx[i] = -1;
			deg[i] = 0;
		}
		for(int i = 0; i < m; i++){
			scanf("%d%d%d", &U, &V, &c);
			deg[U] ++;
			deg[V] ++;
			g[U].push_back(make_pair(V,c));
			g[V].push_back(make_pair(U,c));
		}
		dfs(1, -1);
		scanf("%d", &q);
		for(;q--;){
			scanf("%d%d", &startNode, &wormSize);
			dijkstra(startNode);
			int ans = INF;
			for(int i = 1; i <= n; i++){
				if(cycleIdx[i] != -1 && cycleCost[cycleIdx[i]] >= wormSize){
					ans = min(ans, cycleCost[cycleIdx[i]] + 2 * dist[i]);
				}
			}
			printf("%d\n",ans == INF ? -1 : ans);
		}
		
	}
	return 0;
}
