#include <bits/stdc++.h>
#define INF 100101010
using namespace std;
int m, n, g, k, ans = INF;
int mana[1014], dmg[1014], dp[1014], dist[1004];
vector<vector<int> > gr, mob;
 
struct oper {
    bool operator()(int x, int y) {
	return dist[x] > dist[y];
    }
};

int dij() {
    priority_queue<int, vector<int>, oper> q;

    dist[0] = 0;
    q.push(0);

    while(!q.empty()) {
		int at = q.top();
		q.pop();
		for(int i=0; i < gr[at].size(); i++) {
			int next = gr[at][i], cost = 0;
			for(int j = 0; j < mob[next].size(); j++) cost += dp[mob[next][j]];
			int aux = dist[at] + cost, aux2 = dist[next];
			if(aux < aux2) {
				q.push(gr[at][i]);
				dist[gr[at][i]] = aux;
			}
		}
    }
    return dist[n];
}
 
void calc() {
    for (int i = 0; i <= 1000; i++)
        dp[i] = dist[i] = INF;

    dp[0] = 0;
    for (int i = 1; i <= 1000; i++) {
        for (int j = 0; j<m; j++) {
            if (i - dmg[j] >= 0 && dp[i - dmg[j]] + mana[j] < dp[i])
                dp[i] = dp[i - dmg[j]] + mana[j];
        }
    }
    for (int i = 0; i <= 1000; i++){
        for (int j = i + 1; j <= 1000; j++){
            dp[i] = min(dp[i], dp[j]);
        }
    }
 
}
 
int main(void) {
    while (cin >> m >> n >> g >> k && (m + n + g + k)) {
        gr.resize(n + 1), mob.resize(n + 1);

	gr[0].push_back(1);
        for (int i = 0; i<m; i++) {
            cin >> mana[i] >> dmg[i];
        }
 
        int a, b;
        for (int i = 0; i<g; i++) {
            cin >> a >> b;
            gr[a].push_back(b);
            gr[b].push_back(a);
        }
	
        for (int i = 0; i<k; i++) {
            cin >> a >> b;
            mob[a].push_back(b);
        }
 
        calc();
		ans = dij();

        if (ans != INF)
            cout << ans << "\n";
        else
            cout << "-1\n";
 
        gr.clear(), mob.clear();
    }
 
    return 0;
}
