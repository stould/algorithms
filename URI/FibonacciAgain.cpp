#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <map>
#include <math.h>
#include <string.h>
#define MAXN 103
#define INF 0x3f3f3f3f
#define ll long long

using namespace std;

ll dp[1055505];


int cicleLength(ll MOD){
	int len = 2;
	dp[0] = 0;
	dp[1] = 1;
	while(1){
		dp[len] = (dp[len-1] + dp[len-2]) % MOD;
		if(dp[len-1] % MOD == 0 && dp[len] % MOD == 1){
			break;
		}else{
			len++;
		}
	}
	return len-1;
}

int fib(int n, ll MOD){
	dp[0] = 0;
	dp[1] = 1;
	for(int i = 2; i <= n; i++){
		dp[i] = (dp[i-1] + dp[i-2]) % MOD;
	}
	return dp[n];
}

int main(void){
	int cl = cicleLength(2);
	cout << fib(fib(5,cl), 2) << endl;
    return 0;
}


