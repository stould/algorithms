#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <map>
#include <math.h>
#include <string.h>
#define MAXN 101
#define INF 0x3f3f3f3f
#define ll long long
#define MOD 100000000
 
using namespace std;
 
struct task{
    int l, r;
    task(){}
    task (int a, int b){
        l = a; r = b;
    }
    bool operator < (const task &o) const{
        return l < o.l;
    }
};
 
int n,m,a,b;
 
task ci[MAXN];
 
map<pair<int, pair<int,int> >, ll > dp;
 
ll func(int idx, int anterior, int where){
    if(where > m){
        return 1;
    }else if(idx >= n){
        return 0;
    }else{
        pair<int,pair<int,int> > state = make_pair(where, make_pair(idx,anterior));
        if(dp.find(state) == dp.end()){
            dp[state] = func(idx + 1, anterior, where);
            if(ci[idx].r >= where && ci[idx].l <= where){
                dp[state] += func(idx+1,idx,ci[idx].r+1) % MOD;
            }
        }
        return dp[state] % MOD;
    }
}
 
int main(void){
    while(scanf("%d%d", &m, &n) == 2 && n > 0 && m > 0){
        dp.clear();
        for(int i = 0; i < n; i++){
            scanf("%d%d", &a, &b);
            ci[i] = task(a,b);
        }
        sort(ci, ci+n);
		ll ans = 0;
		for(int i = 0; i < n; i++){
			if(ci[i].l == 0){
				ans += func(0, i,ci[i].r+1) % MOD;
			}else{break;}
		}
		printf("%lld\n", ans % MOD);
    }
    return 0;
}
