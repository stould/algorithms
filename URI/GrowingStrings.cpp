#include <bits/stdc++.h>
#define MAXN 10001
#define MAXLEN 1001
#define ALPHABET 27

using namespace std;

int trie[MAXN*MAXLEN][ALPHABET], fail[MAXN*MAXLEN], T[MAXN*MAXLEN], INDEX = 1;

vector<set<int> > term(MAXN*MAXLEN);

void add(string &s,  int id, int root = 0){
    for(int i = 0; i < (int) s.size(); i++){
        int child = s[i] - 'a';
        if(trie[root][child] == -1){
            trie[root][child] = INDEX++;
        }
        root = trie[root][child];
    }
    
    term[root].insert(id);
    
    T[root] = 1;
}

void ahoCorasick(int root = 0){
    queue<int> q;
    for(int child = 0; child < ALPHABET; child++){
        if(trie[root][child] == -1){
            trie[root][child] = 0;
        }else{
            fail[trie[root][child]] = root;
            q.push(trie[root][child]);
        }
    }
    while(!q.empty()){
        int parent = q.front(); q.pop();
        for(int child = 0; child < ALPHABET; child++){
            if(trie[parent][child] != -1){
                int node = fail[parent];
                while(trie[node][child] == -1){
                    node = fail[node];
                }
                fail[trie[parent][child]] = trie[node][child];
                term[trie[parent][child]].insert(term[trie[node][child]].begin(), term[trie[node][child]].end());
                T[trie[parent][child]] |= T[trie[node][child]];
                q.push(trie[parent][child]);
            }
        }
    }
}

vector<vector<int> > graph(MAXN);

void seek(string &s, int from, int idx = 0, int root = 0){
    for(auto next : term[root]){
        if(next != from)
            graph[from].push_back(next);
    }
    if(idx < (int) s.size()){
        seek(s, from, idx+1, trie[root][s[idx] - 'a']);
    }
}

int N;
string words[MAXN];

int pd[MAXN];

int func(int node){
    if(graph[node].size() == 0){
        return 0;
    }
    int &ans = pd[node];
    if(ans == -1){
        ans = 0;
        for(auto next : graph[node]){
            ans = max(ans, 1 + func(next));
        }
        return ans;
    }
}

int main(){
    while(cin >> N && N != 0){
        INDEX = 1;
        memset(trie, -1, sizeof(trie));
        memset(T, 0, sizeof(T));
        memset(fail, -1, sizeof(fail));
        memset(pd, -1, sizeof(pd));
        for(int i = 0; i < MAXN*MAXLEN; i++){
            term[i].clear();
        }
        for(int i = 0; i < MAXN; i++){
            graph[i].clear();
        }
        for(int i = 0; i < N; i++){
            cin >> words[i];
            add(words[i], i);
        }
        ahoCorasick();
        for(int i = 0; i < N; i++){
            seek(words[i], i);
        }
        int ans= 0;
        for(int i = 0; i < N; i++){
            ans = max(ans, func(i));
        }
        cout << ans +1 << endl;
    }
    return 0;
}
