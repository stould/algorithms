import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

public class diamonds {

    public static void main(String[] args) throws IOException {
		InputStreamReader ir = new InputStreamReader(System.in);
        BufferedReader in = new BufferedReader(ir);
		int numLin = Integer.parseInt(in.readLine());
        ArrayList<Integer> result = new ArrayList();
        while (numLin-- > 0) {
            int qtde = 0;
            String c = in.readLine();
            for (int i = 0; i < c.length() - 1; i++) {
                char ch = c.charAt(i);
                if ('<' == ch) {
                    int j = i + 1;
                    char ch2 = c.charAt(j);
                    while (ch2 != '>' && j < c.length() - 1) {
                        ch2 = c.charAt(++j);
                    }
                    if (ch2 == '>') {
                        char[] c2 = c.toCharArray();
                        c2[j] = '.';
                        c = String.valueOf(c2);
                        qtde++;
                    }
                }
            }
            result.add(qtde);
        }
        for (int i = 0; i < result.size(); i++) {
            System.out.println(result.get(i));
        }
    }
}
