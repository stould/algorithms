#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 103
#define ll long long int
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define MOD 1000000007LL

using namespace std;

int N,M,U,V,dp[MAXN][MAXN],cycleIdx[MAXN],idx, deg[MAXN];
bool vis[MAXN];
vector<vector<int> > g(MAXN);
stack<int> st;
vector<vector<int> > comp;
bool inCycle[MAXN];

void dfs(int node, int parent){
    vis[node] = 1;
    st.push(node);
    for(int i = 0; i < g[node].size(); i++){
        int next = g[node][i];
        deg[node] --;
        if(vis[next]){
            if(next != parent && cycleIdx[node] == -1){
                while(!st.empty()){
                    int top = st.top(); st.pop();
                    cycleIdx[top] = idx;
                    if(top == next) break;
                }
                idx++;
            }
             
        }else{
            dfs(next, node);
            while(!st.empty() && !deg[st.top()]){
                st.pop();
            }
        }
    }
}


void printComps(){
	for(int i  = 0; i < comp.size(); i++){
		for(int j = 0; j < comp[i].size(); j++){
			cout << comp[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

void dofloyd(){
	for(int k = 1; k <= N; k++){
		for(int i = 1; i <= N; i++){
			for(int j = 1; j <= N; j++){
				dp[i][j] = min(dp[i][j], dp[i][k] + dp[k][j]);
			}
		}
	}
}

void floyd(){
	memset(dp, 63, sizeof(dp));
	for(int i = 1; i <= N; i++){
		dp[i][i] = 0;
		for(int j = 0; j < g[i].size(); j++){
			dp[i][g[i][j]] = 1;
			dp[g[i][j]][i] = 1;
			deg[g[i][j]]++;
			deg[i]++;
		}
	}
	dofloyd();
}



void dfscomp(int u, int I){
	vis[u] = 1;
	comp[I].push_back(u);
	for(int i = 0; i < (int)g[u].size(); i++){
		int next = g[u][i];
		if(!vis[next] && !inCycle[next]){
			dfscomp(next, I);
		}
	}
}


bool mergeTwo(int X, int Y){
	bool ans = 0;
	int distA = -1, distB = -1;
	vector<int> &a = comp[X];
	vector<int> &b = comp[Y];

	pair<int,int> A,B;
	for(int i = 0; i < a.size(); i++){
		if(inCycle[a[i]]) continue;
		for(int j = i; j < a.size(); j++){
			if(dp[a[i]][a[j]] > distA && !inCycle[a[j]]){
				distA = dp[a[i]][a[j]];
				A.first = a[i];
				A.second = a[j];
			}
		}
	}
	for(int i = 0; i < b.size(); i++){
		if(inCycle[b[i]]) continue;
		for(int j = i; j < (int)b.size(); j++){
			if(dp[b[i]][b[j]] > distB && !inCycle[b[j]]){
				distB = dp[b[i]][b[j]];
				B.first = b[i];
				B.second = b[j];
			}
		}
	}

	g[A.first].push_back(B.first);
	g[B.first].push_back(A.first);

	g[A.second].push_back(B.second);
	g[B.second].push_back(A.second);

	idx = 0;
	while(st.size()) st.pop();
	for(int i = 1; i <= N; i++){
		vis[i] = 0;
		cycleIdx[i] = -1;
	}
	dfs(A.first, -1);
	a.clear();

	for(int i = 1; i <= N; i++){
		if(cycleIdx[i]  != -1){
			a.push_back(i);
			ans = 1;
		}
	}
	
	for(int i = 0; i < a.size(); i++){
		bool ok = 1;
		for(int j = 0;ok && j < (int) g[a[i]].size(); j++){
			if(cycleIdx[g[a[i]][j]] == -1 ) ok = 0;
		}
		inCycle[a[i]] = ok;
		if(ok){
			for(int j = 0; j < g[a[i]].size();){
				int next = g[a[i]][j];
				bool find = 0;
				for(int k = 0; k < g[next].size(); k++){
					if(g[next][k] == a[i]){
						g[a[i]].erase(g[a[i]].begin() + j);
						g[next].erase(g[next].begin() + k);
						find = 1;
						break;
					}
				}
				if(find){
					j = 0;
				}else{
					j++;
				}
			}
		}
	}

	memset(vis,0,sizeof(vis));
	comp.clear();
	idx = 0;
	for(int i = 1; i <= N; i++){
		if(!vis[i]  && !inCycle[i]){
			comp.push_back(vector<int>());
			dfscomp(i, idx);
			idx++;
		}
	}
	return ans;
}

bool mergeOne(int X){
	bool ans = 0;
	int distA = -1;
	vector<int> &a = comp[X];

	pair<int,int> A;
	for(int i = 0; i < a.size(); i++){
		if(inCycle[a[i]]) continue;
		for(int j = i; j < a.size(); j++){
			if(dp[a[i]][a[j]] > distA && !inCycle[a[j]]){
				distA = dp[a[i]][a[j]];
				A.first = a[i];
				A.second = a[j];
			}
		}
	}
	cout << "link:" <<endl;
	cout << A.first << " " << A.second << endl;
	g[A.first].push_back(A.second);
	g[A.second].push_back(A.first);

	idx = 0;
	while(st.size()) st.pop();
	for(int i = 1; i <= N; i++){
		vis[i] = 0;
		cycleIdx[i] = -1;
	}
	dfs(A.first, -1);
	a.clear();
	
	for(int i = 1; i <= N; i++){
		if(cycleIdx[i]  != -1){
			cout << i << " " << cycleIdx[i] << " " << endl;
			a.push_back(i);
			ans = 1;
		}
	}
	cout << endl;

	
	for(int i = 0; i < a.size(); i++){
		bool ok = 1;
		for(int j = 0;ok && j < (int) g[a[i]].size(); j++){
			if(cycleIdx[g[a[i]][j]] == -1 ) ok = 0;
		}
		inCycle[a[i]] = ok;
		if(ok){
			for(int j = 0; j < g[a[i]].size();){
				int next = g[a[i]][j];
				bool find = 0;
				for(int k = 0; k < g[next].size(); k++){
					if(g[next][k] == a[i]){
						g[a[i]].erase(g[a[i]].begin() + j);
						g[next].erase(g[next].begin() + k);

						find = 1;
						break;
					}
				}
				if(find){
					j = 0;
				}else{
					j++;
				}
			}
		}
	}

	cout << "CYCLE" << endl;
	for(int i = 0; i < a.size(); i++){
		cout << a[i] << " ";
	}
	cout << endl;
	memset(vis,0,sizeof(vis));
	comp.clear();
	idx = 0;
	for(int i = 1; i <= N; i++){
		if(!vis[i]  && !inCycle[i]){
			comp.push_back(vector<int>());
			dfscomp(i, idx);
			idx++;
		}
	}
	return ans;
}

bool cleanComp(int X){
	bool ans = 0;
	vector<int> &a = comp[X];
	idx = 0;
	while(st.size()) st.pop();
	for(int i = 1; i <= N; i++){
		vis[i] = 0;
		cycleIdx[i] = -1;
	}
	dfs(a[0], -1);
	a.clear();

	for(int i = 1; i <= N; i++){
		if(cycleIdx[i]  != -1){
			a.push_back(i);
			ans = 1;
		}
	}
	cout << "cleaning up"<<endl;
	for(int i = 0; i < a.size(); i++){
		cout << a[i] << endl;
		bool ok = 1;
		for(int j = 0;ok && j < (int) g[a[i]].size(); j++){
			if(cycleIdx[g[a[i]][j]] == -1 ) ok = 0;
		}
		inCycle[a[i]] = ok;
		if(ok){
			for(int j = 0; j < g[a[i]].size();){
				int next = g[a[i]][j];
				bool find = 0;
				for(int k = 0; k < g[next].size(); k++){
					if(g[next][k] == a[i]){
						g[a[i]].erase(g[a[i]].begin() + j);
						g[next].erase(g[next].begin() + k);
						find = 1;
						break;
					}
				}
				if(find){
					j = 0;
				}else{
					j++;
				}
			}
		}
	}
	
	memset(vis,0,sizeof(vis));
	comp.clear();
	idx = 0;
	for(int i = 1; i <= N; i++){
		if(!vis[i]  && !inCycle[i]){
			comp.push_back(vector<int>());
			dfscomp(i, idx);
			idx++;
		}
	}*/
	for(int i = 1; i <= N; i++){
		for(int j = 0; j < g[i].size(); i++){
			cout << g[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
	return ans;
}



int main(){
	while(scanf("%d%d", &N, &M) && N != -1 && M != -1){
		for(int i = 1; i <= N; i++){
			for(int j = 1; j <= N; j++){
				dp[i][j] = INF;
			}
		}
		comp.clear();
		for(int i = 1; i <= N; i++){
			g[i].clear();
			vis[i] = 0;
			cycleIdx[i] = -1;
			deg[i] = 0;
			inCycle[i] = 0;
		}
		for(int i = 0; i < M; i++){
			scanf("%d%d", &U, &V);
			g[U].push_back(V);
			g[V].push_back(U);
			deg[U] ++;
            deg[V] ++;
			dp[U][V] = 1;
			dp[V][U] = 1;
		}

		idx = 0;
		for(int i = 1; i <= N; i++){
			dp[i][i] = 0;
			if(!vis[i]){
				comp.push_back(vector<int>());
				dfscomp(i, idx);
				idx++;
			}
		}

		bool go = 0;
		
		for(int i = 0; i < comp.size(); ){
			floyd();
			if(comp[i].size() < 3){
				i++;
				continue;
			}
			go = cleanComp(i);
			if(go) i = 0;
			else i++;
		}
		
		//	printComps();
		int ans = 0;
		while(comp.size() > 0){
			floyd();
			int low = INF, big = -INF, idxlow, idxbig;
			for(int i = 0; i < (int) comp.size(); i++){
				if((int)comp[i].size() < low){
					low = (int) comp[i].size();
					idxlow = i;
				}
			}
			for(int i = 0; i < (int) comp.size(); i++){
				if((int) comp[i].size() > big && i != idxlow){
					idxbig = i;
					big = (int) comp[i].size();
				}
			}
			if(low != INF && big != -INF){
				bool can = mergeTwo(idxlow, idxbig);
				ans+=2;
				if(!can) break;
				//	cout << "ADD 2 " << endl;
			}else{
				go = INF;
				if(low != INF){
					go = idxlow;
				}else if(big != -INF){
					go = idxbig;
				}else{
					break;
				}
				if(go == INF) break;
				ans++;
				if(comp[go].size() <= 2){
					break;
				}else{
					mergeOne(go);
				}
			}
			//printComps();
			
		}
		cout << ans << endl;
	}
	return 0;
}
