#include <stdio.h> 
#include <stdlib.h> 
#include <iostream> 
#include <vector> 
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h> 
#include <set>
#include <map> 
#include <iostream>
#include <sstream>
#define MAXN 501
#define ll long long
#define PI acos(-1)

using namespace std;

int n,m,v[MAXN][MAXN],q,l,u;

int sLine(int X, int line){
	int lo = 0, hi = m-1;
	bool find = 0;
	while(lo <= hi){
		int mid = (lo+hi) >> 1;
		if(v[line][mid] < X){
			lo = mid+1;
		}else if(v[line][mid] >= X){
			find = 1;
			hi = mid-1;
		}
	}
	return find == 1 ? lo : -1;
}

int sDiag(int X, int line, int col){
	int lo = col, hi, ans = -1;
	if(n-line > m-col){
		hi = col+m-col-1;
	}else{
		hi = col+n-line-1;
	}
	while(lo <= hi){
		int mid = (lo+hi) >> 1;
		if(v[line+(mid-col)][mid] <= X){
			lo = mid+1;
			ans = max(mid-col, ans);
		}else{
			hi = mid-1;
		}
	}
	return ans == -1 ? 0 : 1+ans;
}

int main(void) {
	while(scanf("%d%d", &n, &m) && n > 0 && m > 0){
		for(int i = 0; i < n; i++){
			for(int j = 0; j < m; j++){
				scanf("%d", &v[i][j]);
			}
		}
		scanf("%d", &q);
		while(q--){
			scanf("%d%d", &l, &u);
			int ans = 0;
			for(int line = 0; line < n; line++){
				if(ans > (n-line)) break;
				int col = sLine(l, line);
				if(col == -1) continue;
				int v = sDiag(u, line, col);
				ans = max(ans, v);
			}
			printf("%d\n", ans);
		}
		printf("-\n");
	}
	return 0;
}
