#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#define MAX 500010
  
using namespace std;
  
int n, Array[MAX], dummyArray[MAX];
  
long long mergesort(int start, int end)
{
    if (start == end)
    {
        return 0;
    }
  
    int mid = start + (end - start) / 2;
  
    long long ret1 = mergesort(start, mid);
    long long ret2 = mergesort(mid + 1, end);
  
    int i = start;
    int j = mid + 1;
    int k = start;
    long long ret3 = 0;
  
    while(i <= mid && j <= end)
    {
        if (Array[i] < Array[j])
        {
            dummyArray[k++] = Array[i++];
        }
        else
        {
            ret3 += (mid - i) + 1;
            dummyArray[k++] = Array[j++];
        }
    }
    while(i <= mid)
    {
        dummyArray[k++] = Array[i++];
    }
    while(j <= end)
    {
        dummyArray[k++] = Array[j++];
    }
  
    memcpy(Array+start,dummyArray+start,sizeof(Array[0])*(end-start+1));
    return (ret1 + ret2 + ret3);
}
  
  
  
int main()
{
    ios::sync_with_stdio(0);
    while(1 == scanf("%d",&n) && n)
    {
        for(int i = 0; i < n; i++) scanf("%d", &Array[i]);
        int tot = mergesort(0, n-1);
        if(tot % 2 == 0) cout << "Carlos\n";
        else cout << "Marcelo\n";
    }
  
    return 0;
}
