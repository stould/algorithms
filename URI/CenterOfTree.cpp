#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 20350
#define ll long long int
#define INF 0xffffff
#define PI acos(-1)
#define MOD 1000000007LL
 
using namespace std;
 
vector<vector<int> > graph(MAXN);
int n, U, V;
int deg[MAXN], dist[MAXN];
 
void addEdge(int U_, int V_){
	graph[U_].push_back(V_);
	graph[V_].push_back(U_);
	deg[U_]++;
	deg[V_]++;
}
 
vector<int> findCenter(){
	queue<int> q;
	//pushing the leaves
	for(int i = 0; i < n; i++){
		dist[i] = 0;
		if(deg[i] == 1){
			q.push(i);
		}
	}
	int further = 0;
	while(!q.empty()){
		int top = q.front(); q.pop();
		for(int i = 0; i < graph[top].size(); i++){
			int next = graph[top][i];
			deg[next]--;
			if(deg[next] == 1){
				q.push(next);
				dist[next] = dist[top] + 1;
				further = max(further, dist[next]);
			}
		}
	}
	vector<int> ans;
	for(int i = 0; i < n; i++){//all reacleable nodes with the maximum distance, E {center}
		if(dist[i] == further){
			ans.push_back(i);
		}
	}
	return ans;
}

int main(){
	
	return 0;
}
