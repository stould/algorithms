#include <bits/stdc++.h>
#define MAXN 103
#define MAXM 17
#define INF 0x3f3f3f3f
#define ll long long
#define MOD 100000000
#define ALPHABET 300
using namespace std;

int trie[MAXM*MAXN][ALPHABET], INDEX;
int fail[MAXN*MAXM];

int term[MAXN*MAXM];

void add(string &s){
    int root = 0;
    for(int i = 0; i < (int) s.size(); i++){
        int child = s[i];
        if(trie[root][child] == -1){
            trie[root][child] = INDEX++;
        }
        root = trie[root][child];
    }
    term[root] = 1;
}

void ahCorasick(int root = 0){
    queue<int> q;
    for(int child = 0; child < ALPHABET; child++){
        if(trie[root][child] == -1){
            trie[root][child] = 0;
        }else{
            q.push(trie[root][child]);
            fail[trie[root][child]] = root;
        }
    }
    while(!q.empty()){
        int parent = q.front(); q.pop();
        for(int child = 0; child < ALPHABET; child++){
            if(trie[parent][child] != -1){
                int node = fail[parent];
                while(trie[node][child] == -1){
                    node = fail[node];
                }
                fail[trie[parent][child]] = trie[node][child];
                term[trie[parent][child]] |= term[trie[node][child]];
                q.push(trie[parent][child]);
            }
        }
    }
}

set<int> resp;

void seek(string &s, int idx = 0, int root = 0){
    if(idx == (int) s.size()){
        return;
    }else{
        int child = s[idx];
        if(trie[root][(int)s[idx]] == -1){
            int node = fail[root];
            while(trie[node][child] == -1){
                node = fail[node];
            }
            return seek(s, idx+1, trie[node][child]);
        }else{
            return seek(s, idx+1, trie[root][child]);
        }
    }
}

int N,M;
string text;

int pd[MAXN][MAXN*MAXM];

int func(int pos, int root){
    if(pos == (int) text.size()){
        return 0;
    }else{
        int &ans = pd[pos][root];
        if(ans == -1){
            ans = INF;
            ans = min(ans, 1 + func(pos+1, 0));
            int child = text[pos];
            if(trie[root][child] == -1){
                int node = fail[root];
                while(trie[node][child] == -1){
                    node = fail[node];
                }
                if(!term[trie[node][child]] ){
                    ans = min(ans, func(pos+1, trie[node][child]));
                }
            }else{
                if(!term[trie[root][child]] ){
                    ans = min(ans, func(pos+1, trie[root][child]));
                }
            }
        }
        return ans;
    }
}

string emoticon;

int main(void){
    while((cin >> N >> M) && (N > 0 && M > 0)){
        for(int i = 0; i < MAXN*MAXM; i++){
            term[i] = 0;
        }
        INDEX = 1;
        memset(trie, -1, sizeof(trie));
        memset(fail, -1, sizeof(fail));
        memset(term, 0, sizeof(term));
        cin.ignore();
        for(int i = 0; i < N; i++){
            getline(cin, emoticon);
            add(emoticon);
        }
        ahCorasick();
        int ans = 0;
        for(int i = 0; i < M; i++){
            memset(pd, -1, sizeof(pd));
            getline(cin, text);
            ans += func(0,0);
        }
        cout << ans << endl;
    }
    return 0;
}

