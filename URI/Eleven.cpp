#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 50007
#define ll long long int
#define INF 0xffffff
#define PI acos(-1)
#define MOD 1000000007LL

using namespace std;

string num;

int n, even, odd,counter[11];
ll pascal[60][60], dp[11][60][60][11];

void triangle(int n){
	pascal[0][0] = 1;
	for(int i = 1; i < n; i++){
		pascal[i][0] = 1;
		pascal[i][i] = 1;
		for(int j = 1; j < i; j++){
			pascal[i][j] = (pascal[i-1][j-1] + pascal[i-1][j]) % MOD;
		}
	}
}

ll backTrack(int sum, int qtdEven, int qtdOdd, int id){
	if(qtdEven < 0 || qtdOdd < 0 || sum < 0) return 0;
	if(id == 10){
		return (ll) (sum % 11 == 0 && qtdEven == 0 && qtdOdd == 0);
	}else{
		if(dp[sum][qtdEven][qtdOdd][id] == -1){
			dp[sum][qtdEven][qtdOdd][id] = 0;
			for(int i = 0; i <= counter[id]; i++){
				ll evenComb = pascal[qtdEven][i];
				ll oddComb = pascal[qtdOdd][counter[id] - i];
				if(id == 0){
					evenComb = pascal[qtdEven-1][i];
				}
				int A = ((counter[id] - i)*id) % 11;
				int B = (i*id) % 11;
				int C = abs(B-A+11) % 11;
				int next = (sum+C) % 11;
				ll comb = (evenComb * oddComb) % MOD;
			    dp[sum][qtdEven][qtdOdd][id] = (dp[sum][qtdEven][qtdOdd][id] + (comb * (backTrack(next, qtdEven - i, qtdOdd - (counter[id] - i), id+1) % MOD)) % MOD) % MOD;
			}
		}
		return dp[sum][qtdEven][qtdOdd][id];
	}
}


int main(){
	ios::sync_with_stdio(0);
	cin.tie(0);
	triangle(60);
	while(cin >> num){
		memset(dp,-1,sizeof(dp));
		memset(counter,0,sizeof(counter));
		n = num.size();
		even = (n+1) / 2;
		odd = n/2;
		for(int i = 0; i < (int) n; i++){
			counter[num[i] - '0'] += 1;
		}
		cout << backTrack(0,even,odd, 0) << endl;
	}
	return 0;
}
