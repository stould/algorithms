#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <map>
#include <math.h>
#include <string.h>
#define MAXN 103
#define INF 0x3f3f3f3f
#define ll long long
#define MOD 100000000

using namespace std;

struct task{
    int l, r;
    task(){}
    task (int a, int b){
        l = a; r = b;
    }
    bool operator < (const task &o) const{
        if(l == o.l) return r < o.r;
        return l < o.l;
    }
};

int n,m,a,b;

task ci[MAXN];

int dp[MAXN][MAXN];

//a pd é o seguinte, estou usando idx e last, vou tentar usar mais um:
int func(int ante, int atual){
    if(ci[atual].r == m){
        return 1;
    }else{
        int &ans = dp[ante][atual];
        if(ans == -1){
            ans = 0;
            for(int i = 1+atual; i < n; i++){
                int next = i;
                if(ante != atual){
                    if((ci[next].l <= ci[atual].r && ci[next].r > ci[atual].r && ci[ante].r < ci[next].l)){
                        ans += func(atual,next);
                        if(ans >= MOD) ans -= MOD;
                    }
                }else{
                    if(!(ci[ante].l >= ci[next].l && ci[ante].r <= ci[next].r) && (ci[next].l <= ci[atual].r && ci[next].r > ci[atual].r)){
                        ans += func(atual,next);
                        if(ans >= MOD) ans -= MOD;
                    }
                }
            }
        }
        return ans;
    }
}

int main(void){
    while(scanf("%d%d", &m, &n) == 2 && n > 0 && m > 0){
        for(int i = 0; i < n; i++){
            scanf("%d%d", &a, &b);
            ci[i] = task(a,b);
        }
        memset(dp,-1,sizeof (dp));
        sort(ci, ci+n);
        int ans = 0;
        for(int i = 0; i < n; i++){
            if(ci[i].l == 0){
                ans += func(i, i);
                if(ans >= MOD) ans -= MOD;
            }else{break;}
        }
        printf("%d\n", ans);
    }
    return 0;
}

