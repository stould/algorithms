#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 50007
#define ll long long int
#define INF 0xffffff
#define PI acos(-1)
#define MOD 1000000007LL

using namespace std;

int n;
ll v;

int main(){
	cin >> n;
	ll sum = 0;
	map<ll, int> seen;
	seen[0] = 1;
	ll ans = 0;
	for(int i = 0; i < n; i++){
		cin >> v;
		ans += seen[sum+v];
		sum += v;
		seen[sum] ++;
	}
	cout << ans << endl;
	return 0;
}
