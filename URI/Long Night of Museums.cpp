#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#define MAXN 21
#define ll long long
 
using namespace std;
 
int n, TIMEX[MAXN], dist[MAXN][MAXN], dp[1<<21][21];
 
void floyd(){
    for(int k = 0; k < n; k++){
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                dist[i][j] = min(dist[i][j], dist[i][k] + dist[k][j]);
            }
        }
    }
}
 
int func(int bag, int mask, int idx){
    if(dp[mask][idx] != -1){
        return dp[mask][idx];
    }else{
        dp[mask][idx] = 0;
        for(int j = 0; j < n; j++){
            if(bag-(dist[idx][j]+TIMEX[j]) >= 0 && !(mask & (1 << j))) {
                dp[mask][idx] = max(dp[mask][idx], 1 + func(bag-(dist[idx][j]+TIMEX[j]), mask | (1 << j), j));
            }
        }
        return dp[mask][idx];
    }
}
 
int main(void){
 
    while(scanf("%d", &n) && n){
        for(int i = 0; i < n; i++){
            scanf("%d", &TIMEX[i]);
        }
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                scanf("%d", &dist[i][j]);
            }
        }
        floyd();
        int k = (1<<n);
        memset(dp,-1,sizeof(dp));
        int maX = 0;
        for(int i = 0; i < n; i++){
            if (420 - TIMEX[i] >= 0) {
                maX = max(maX, 1+func(420-TIMEX[i], 1 << i, i));
            }
        }
        printf("%d\n", maX);
    }
    return 0;
}
