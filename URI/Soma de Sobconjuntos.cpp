#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#define MAXN 10001
#define INF 0x3f3f3f3f
#define ll long long

using namespace std;

int T, N;
ll val[MAXN];

int main(void){
	scanf("%d", &T);
	while(T--){
		scanf("%d", &N);
		for(int i = 0; i < N; i++){
			scanf("%lld", &val[i]);
		}
		sort(val, val+N);
		ll ans = 1;
		//if we can sum numbers up to 2^i-1, then we need to check if the next number in array is equal to 2^i + 1
		for(int i = 0; i < N; i++){
			if(val[i] > ans){
				break;
			}else{
				ans += val[i];
			}
		}
		printf("%lld\n", ans);
	}
    return 0;
}

