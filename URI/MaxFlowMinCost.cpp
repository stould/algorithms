#include <stdio.h> 
#include <stdlib.h> 
#include <iostream> 
#include <vector> 
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h> 
#include <set>
#include <map> 
#include <iostream>
#include <sstream>
#define MAXN 105
#define ll long long

#define PI acos(-1)

using namespace std;

const ll INF = 0x3f3f3f3f3f3f3fLL;

int vparent[MAXN], eparent[MAXN];
ll dist[MAXN], parentflow[MAXN];

struct Edge {

    int v, idrev;
    ll cap, c;
    Edge(int V, ll C, ll F, int ID) {
        v = V;
        c = C;
        cap = F;
        idrev = ID;
    }
    Edge() {}
};
vector < vector < Edge > > g(MAXN);

void addEdge(int u, int v, ll c, ll f) {
    g[u].push_back(Edge(v, c, f, g[v].size()));
    g[v].push_back(Edge(u, -c, 0, g[u].size() - 1));
}

struct Less {
  bool operator()(int a,int & b) {
        return dist[a] > dist[b];
    }
};

pair < int, ll > mflow(int S, int T, ll data) {
  ll flowAns = 0LL, costAns = 0LL;
    
    while (data > 0) {
        memset(eparent, -1, sizeof(eparent));
        memset(vparent, -1, sizeof(vparent));

        for(int i = 0; i <= T; i++){
          eparent[i] = vparent[i] = -1;
          parentflow[i] = -1;
          dist[i] = INF;
        }
        
        priority_queue < int , vector < int > , Less > q;
        q.push(S);
        dist[S] = 0;
        while (!q.empty()) {
            int top = q.top();
            q.pop();
            for (int i = 0; i < g[top].size(); i++) {
                Edge & neigh = g[top][i];
                int next = neigh.v;
                ll cost = neigh.c;
                ll cap = neigh.cap;
                if (dist[top] + cost < dist[next] && cap > 0) {
                    vparent[next] = top;
                    eparent[next] = i;
                    parentflow[next] = cap;
                    dist[next] = dist[top] + cost;
                    q.push(next);
                }
            }
        }
        if(dist[T] == INF){
          costAns = INF;
          break;
        }
        ll totalFlow = INF;
        for(int i = T; i != S; i = vparent[i]){
          totalFlow = min(totalFlow, parentflow[i]);
        }
        int from = T;

        while (from != S) {
            Edge & to = g[vparent[from]][eparent[from]];
            g[to.v][to.idrev].cap += totalFlow;
            to.cap -= totalFlow;
            from = vparent[from];
        }

        if (data - totalFlow < 0) {
          totalFlow = data;
        }
        
        flowAns += totalFlow;
        costAns += dist[T] * totalFlow;
        data -= totalFlow;
    }
    
    return make_pair(flowAns, costAns);
}

int n, m, u, v, FROM[5001], TO[5001];
ll c, data, flow, COST[5001];

int main(void) {
  while(scanf("%d%d", &n, &m) == 2){
    for(int i = 0; i <= n; i++) g[i].clear();
    for(int i = 0; i < m; i++){
      scanf("%d%d%lld", &FROM[i], &TO[i], &COST[i]);
    }
    scanf("%lld%lld", &data, &flow);
    for(int i = 0; i < m; i++){
      addEdge(FROM[i], TO[i], COST[i], flow);
      addEdge(TO[i], FROM[i], COST[i], flow);
    }
    ll ans = mflow(1, n, data).second;
    if(ans == INF){
      printf("Impossible.\n");
    }else{
      printf("%lld\n", ans);
    }
  }
  return 0;
}
