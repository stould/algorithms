#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100003
#define ll long long
#define INF 0x3f3f3f3f
 
using namespace std;

ll gcd(ll a, ll b) {
	if(b == 0) return a;
	return gcd(b, a % b);
}

struct Reta {
	ll Bx, By;
	ll Px, Py;
	ll A, B;
	Reta(){}

	Reta(ll X1, ll Y1, ll X2, ll Y2){
		Bx = X1;
		By = Y1;
		Px = X2;
		Py = Y2;
		A = By-Py;
		B = Bx-Px;
		ll g = gcd(abs(A), abs(B));
		A /= g;
		B /= g;
	}

	pair<ll,ll> getY(ll X){
		pair<ll,ll> ans;
		ll F = (-A*Bx + B*By);
		ll g = gcd(abs(F), abs(B));

		ans.first = F/g;
		ans.second = B/g;
		return ans;
	}
};

vector<ll> points;

ll lower(ll A, ll B){
	ll lo = 0, hi = points.size()-1, ans = 0;
	while(lo <= hi){
		ll mid = lo + (hi-lo) / 2;
		if((double)points[mid] < (double)A/(double)B){
			ans = max(ans, mid);
			lo = mid+1;
		}else if((double)points[mid] > (double)A/(double)B){
			hi = mid-1;
		}else{
			return mid;
		}
	}
	return ans;
}

ll dist(ll xa, ll ya, ll xb, ll yb){
    return (xa-xb)*(xa-xb) + (ya-yb)*(ya-yb);
}
 
int T, P, G, X, Y,Bx,By;

set<pair<int,int> > mp;

int main(void){
	cin >> T;
	while(T--){
		points.clear();
		mp.clear();
		cin >> Bx >> By;
		cin >> P;
		for(int i = 0; i < P; i++){
			cin >> X >> Y;
			points.push_back(X);
			points.push_back(Y);
			mp.insert(make_pair(X,Y));
		}
		sort(points.begin(), points.end());
		cin >> G;
		int ans = 0;
		for(int i = 0; i < G; i++){
			cin >> X >> Y;
			Reta r(Bx,By,X,Y);
			pair<ll,ll> p = r.getY(0);
			ll val = lower(p.first,p.second);
			cout << double(p.first)  / double(p.second) << endl;
			if(val < (int) points.size()-2){
				//cout << points[val] << " " << points[val+1] << endl;
				if(mp.find(make_pair(points[val], points[val+1])) != mp.end()){
					ans++;
				}
			}
		}
		printf("%d\n",ans);
	}
    return 0;
}
