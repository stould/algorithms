#include <bits/stdc++.h>
#define inf 30000
using namespace std;
 
int n, m, k, p;	
int manut[2005], sell[2005], dp[2005][2005];
 
short func (int i, int d){
    if (d > n) return 0;
    if (i > m) return inf;
    if (dp[i][d] != -1) return dp[i][d];
    short ans = inf;
    return dp[i][d] = ans = min ( func (i+1, d+1) + manut[i], 
                                  func (1, d+1) + manut[0] + p - sell[i] );	
}

void path (int i, int d){
    if (d > n) return;
    if (i > m) return;
    if(func (i+1, d+1) + manut[i] < func (1, d+1) + manut[0] + p - sell[i]){
        path(i+1, d+1);
    }else{
        cout << d << " ";
        path(1, d+1);
    }
}

 
int main (void){
    while (scanf("%d%d%d%d", &n, &k, &m, &p) != EOF){
        for (int i = 0; i < m; i++)
            scanf ("%d", &manut[i]);
        manut[m] = inf;
        for (int i = 1; i <= m; i++)
            scanf("%d", &sell[i]);
        memset(dp, -1, sizeof dp);
        printf("%d\n", func(k, 1));
        path(k, 1);
        cout << endl;
    }
    return 0;
}
