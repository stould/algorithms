#include <bits/stdc++.h>
#define MAXN 55
#define ll long long
using namespace std;
//up, right, down, left
int dx[4] = {-1,0,1,0};
int dy[4] = {0,1,0,-1};

struct cell{
    int x, y;
    cell(){}
    cell(int X, int Y): x(X), y(Y){}
    bool operator < (const cell &o) const{
        if(x == o.x){
            return y < o.y;
        }
        return x < o.x;
    }
};

vector<vector<vector<cell> > > poliminos(12);
unordered_map<ll, bool> vis;

const ll BASE = 33ll;
const ll MOD = (ll) (10e9) + 7ll;

ll myHash(vector<cell> &ref){
    ll ans = 0ll, P = 1ll;
    for(int i = 0; i < (int) ref.size(); i++){
        ans = ((((ref[i].x) * P) % MOD)+ans) % MOD;
        P *= BASE;
        ans = ((((ref[i].y) * P) % MOD)+ans) % MOD;
        P *= BASE;
    }
    return ans;
}

void generatePoly(const int LIM){
    poliminos[1].push_back(vector<cell> (1, cell(0,0)));
    for(int len = 2; len <= LIM; len++){
        for(auto i : poliminos[len-1]){
            //now iterating through the j-th polimino and generating new polimino
            for(auto j : i){
                int x = j.x, y = j.y;
                for(int k = 0; k < 4; k++){
                    vector<cell> newPoli;
                    int newX = x + dx[k];
                    int newY = y + dy[k];
                    if(newX < 0){
                        for(auto cells : i){
                            newPoli.push_back(cell(cells.x+1, cells.y));
                        }
                        newX++;
                    }else if(newY < 0){
                        for(auto cells : i){
                            newPoli.push_back(cell(cells.x, cells.y+1));
                        }
                        newY++;
                    }else{
                        for(auto cells : i){
                            newPoli.push_back(cell(cells.x, cells.y));
                        }
                    }
                    bool can = 1;
                    for(int i = 0; i < (int) newPoli.size() && can; i++){
                        if(newPoli[i].x == newX && newPoli[i].y == newY){
                            can = 0;
                        }
                    }
                    if(can){
                        newPoli.push_back(cell(newX, newY));
                        sort(newPoli.begin(), newPoli.end());
                        ll theHash = myHash(newPoli);
                        if((int) newPoli.size() == len && !vis[theHash]){
                            poliminos[len].push_back(newPoli);
                            vis[theHash] = 1;
                        }
                    }else{
                        newPoli.clear();
                    }
                }
            }
        }
    }
}

int N,M;

int grid[MAXN][MAXN];

int main(){
    generatePoly(10);
    while(scanf("%d%d", &N, &M) == 2){
        for(int i = 0; i < N; i++){
            for(int j = 0; j < N; j++){
                scanf("%d", &grid[i][j]);
            }
        }
        int ans = 0;
        for(int row = 0; row < N; row++){
            for(int col = 0; col < N; col++){
                //testing all poliminoes with center in (rol, col)
                for(int i = 0; i < (int)poliminos[M].size(); i++){
                    bool can = 1;
                    int sum = 0;
                    for(int j = 0; j < (int) poliminos[M][i].size(); j++){
                        int nextRow = row + poliminos[M][i][j].x;
                        int nextCol = col + poliminos[M][i][j].y;
                        if(nextRow >= N || nextCol >= N){
                            can = 0;
                            break;
                        }else{
                            sum += grid[nextRow][nextCol];
                        }
                    }
                    if(can){
                        ans = max(ans, sum);
                    }
                }
            }
        }
        printf("%d\n", ans);
    }
    return 0;
}

