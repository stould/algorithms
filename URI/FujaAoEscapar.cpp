#include <bits/stdc++.h>

#define ull unsigned long long
#define MAXN 100002
#define BASE (ull) 71
#define BASE2 (ull) 33

using namespace std;

string str;

ull HASH[MAXN];
ull pot[MAXN];
ull C[MAXN];

void preProcess(){
    pot[0] = 1;
    C[0] = 1;
    HASH[0] = 0;
    for(int i = 0; i < (int) str.size(); i++){
        pot[i+1] = pot[i] * BASE;
        C[i+1] = C[i] * BASE2;
    }
    for(int i = 0; i < (int) str.size(); i++){
        HASH[i+1] = HASH[i] * BASE + (str[i] - '0'+ 1);
    }
}

ull getHash(int l, int r){
    return HASH[r+1] - HASH[l] * pot[r - l + 1] + C[l-r+1];
}

bool can(int size){
    set<ull> cmp;
    int MAX = (1 << size);
    for(int i = 0; i + size <= (int) str.size() && (int)cmp.size() < MAX; i++){
        cmp.insert(getHash(i, i + size-1));
    }

    if((int)cmp.size() < MAX){
        return 1;
    }
    return 0;
}

int main(){
    ios::sync_with_stdio(0);
    cin.tie(0);
    cin >> str;
    preProcess();
    for(int size = 1; ; size++){
        if(can(size)){
            cout << size << endl;
            break;
        }
    }
    return 0;
}
