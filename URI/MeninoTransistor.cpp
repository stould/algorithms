#include <bits/stdc++.h>

std::map<std::pair<int,int>, int> m;

int S,R,k,I;

bool s(std::vector< int > &N, int i, int b) {
	if(b == 0) return 1;
	if(i >= S) return 0;
	std::pair<int,int> x = std::make_pair(i, b);
	if(m.find(x) == m.end()) m[x] |= s(N, i+1, b+N[i]) | s(N, i+1, b);
	return m[x];
}

bool SubsetSum(std::vector< int > N) {
  	S=N.size(),R=0;
	for(I = 0; I < S && !R; I++){
		k = N[I];
		N[I] = 0;
		R |= s(N, 0, k);
		N[I] = k;
	}
	return R;
}


int main(void) {
	std::vector<int> v;
	for(int i = -10; i < 20; i++) if(i != 0) v.push_back(i);
	std::cout << SubsetSum(v) << std::endl;
    return 0;
}


