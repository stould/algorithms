#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <stack>
#include <math.h>
#include <string.h>
#include <string>
#define ll long long
#define MAXN 30001
 
using namespace std;
 
string word, code;
int t;
 
int v[MAXN], pointer, posCode, posIn, pindex;
 
 
void action(int idx){
   if(code[idx] == '>'){
        pointer++;
    }else if(code[idx] == '<'){
        pointer--;
    }else if(code[idx] == '+'){
        v[pointer]++;
    }else if(code[idx] == '-'){
        v[pointer]--;
    }else if(code[idx] == '.'){
        cout << (char)v[pointer];
    }else if(code[idx] == ','){
        if(posIn >= word.size()){
            v[pointer] = 0;
        }else{
            v[pointer] = (int) word[posIn++];
        }
    }else if(code[idx] == '#'){
        for(int i = 0; i < 10; i++){
            cout << (char) v[i];
        }
    }
}
 
void openB(){
    int b = 1;
    if(v[pointer] == 0){
        do{
            pindex++;
            if(code[pindex] == '['){
                b++;
            }else if(code[pindex] == ']'){
                b--;
            }
        }while(b != 0);
    }
}
 
void closeB(){
    int b = 0;
    do{
        if(code[pindex] == '['){
            b++;
        }else if(code[pindex] == ']'){
            b--;
        }
        pindex--;
    }while(b != 0);
}
 
void process(int n){
    pindex = 0;
    while(pindex < n){
        //cout << pindex << endl;
        if(code[pindex] == '['){
            openB();
        }else if(code[pindex] == ']'){
            closeB();
        }else{
            action(pindex);
        }
        pindex++;
    }
}
 
int main(void) {
//    freopen("in.in", "r", stdin);
    cin >> t;
    cin.ignore();
    for(int i = 1; i <= t; i++){
        cout << "Instancia " << i << endl;
        getline(cin, word);
        getline(cin, word);
        getline(cin, code);
        memset(v, 0, sizeof(v));
        pointer = posCode = posIn = 0;
        process(code.size());
        cout << endl << endl;
    }
    return 0;
}
