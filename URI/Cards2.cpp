#include <bits/stdc++.h>
#define MAXN 10001
typedef long long ll;

using namespace std;

int n;
ll val[MAXN];
ll pd[MAXN][2];

int main() {
    while (scanf("%d", &n) == 1) {
        for(int i = 1; i <= n; i++){
            scanf("%lld", & val[i]);
        }
        for(int i = 1; i <= n; i++){
            pd[i][0] = max(val[i], val[i+1]);
        }
        for (int len = 4; len <= n; len += 2) {
            for (int i = 1, j = len; j <= n; i++, j++) {
                ll nexta = val[i] + max(pd[i+1][0], pd[i+2][0]);
                ll nextb = val[j] + max(pd[i][0], pd[i+1][0]);
                pd[i][1] = max(nexta, nextb);
            }
            for (int i = 1, j = len; j <= n; i++, j++) {
                pd[i][0] = pd[i][1];
            }
        }
        printf("%lld\n", pd[1][0]);
    }
    return 0;
}
