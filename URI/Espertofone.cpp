#include <bits/stdc++.h>
#define MAXN 201
#define ll long long int
#define MOD 1000000007LL
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

bool isColinear(int x1, int y1, int x2, int y2, int x3, int y3){
    //    (y1 - y2) / (x1 - x2) = (y3 - y2) / (x3 - x2)
    if((y1- y2)*(x3 - x2) - (y3 - y2) * (x1 - x2) == 0){
        return 1;
    }
    return 0;
}

void genGraph(int n, vector<vector<ll> > &graph){
    for(int i = 0; i < n; i++){
        for(int j = 0; j < n; j++){
            for(int k = 0; k < n; k++){
                for(int l = 0; l < n; l++){
                    int dl = abs(i - k);
                    int dc = abs(j - l);
                    if(i == k && dc > 1) continue;
                    if(j == l && dl > 1) continue;
                    if(dl == dc && dl != 1) continue;
                    //graph[i * n + j][k * n + l] = 1;
                    bool colinear = 0;
                    for(int ax = min(i, k); ax <= max(i, k) && ! colinear; ax++){
                        for(int ay = min(j, l); ay <= max(j, l) && !colinear; ay++){//lets check if (i,j) can see (k,l) look for (ax, ay)
                            if(isColinear(i, j, k, l, ax, ay) && !((ax == i && ay == j) || (ax == k && ay == l))){
                                colinear = 1;
                            }
                        }
                    }
                    if(!colinear){
                        graph[i * n + j][k * n + l] = 1;
                    }
                }
            }
        }
    }
}

ll setMod(ll a, ll b){
    if(a >= MOD)
        a %= MOD;
    if(b >= MOD)
        b %= MOD;
    ll ans = (a*b);
    if(ans > MOD) ans %= MOD;
    return ans;
}

vector<vector<ll> > multiply(vector<vector<ll> > a, vector<vector<ll> > b) {
    vector<vector<ll> > res(a.size(), vector<ll>(a.size()));
    for(int i = 0; i < a.size(); i++) {
        for(int j = 0; j < a.size(); j++) {
            ll sum = 0;
            for (int k = 0; k < a.size(); k++) {
                sum += setMod(a[i][k], b[k][j]);
            }
            res[i][j] = sum;
        }
    }
    return res;
}

vector<vector<ll> > binPow(vector<vector<ll> > a, ll n) {
    if (n <= 1) {
        return a;
    } else if ((n & 1) != 0) {
        return multiply(a, binPow(a, n - 1));
    } else {
        vector<vector<ll> > b = binPow(a, n / 2);
        return multiply(b, b);
    }
}

int n;
ll k;
vector<vector<ll > > dimension[6];

int main(){
    for(int i = 2; i <= 5; i++){
        vector<vector<ll > > graph(i*i, vector<ll>(i*i));
        dimension[i] = graph;
        genGraph(i, dimension[i]);
    }
    while(scanf("%d%lld", &n, &k) == 2){
        if(k == 0){
            printf("%d\n", n*n);
        }else{
            vector<vector<ll > > ans = binPow(dimension[n], k);
            ll sum = 0;
            for(int i = 0; i < n*n; i++){
                for(int j = 0; j < n*n; j++){
                    sum = (sum + ans[i][j]) % MOD;
                }
            }
            printf("%lld\n", sum);
        }
    }
    return 0;
}
