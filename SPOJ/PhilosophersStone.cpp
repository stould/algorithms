#include <stdio.h>
#include <string>
#include <vector>
#include <algorithm>
#include <map>
#include <math.h>
#include <string.h>
#include <iostream>
 
using namespace std;
 
int T, n, m;
long long pd[100][100], matrix[100][100];
 
long long func(long long r, long long c, int n, int m){
    if(pd[r][c] > -1){
        return pd[r][c];
    }else{
        if(r == n-1){
            return matrix[r][c];
        }else{
            long long ans = 0;
                if(c >= 0 && c < m)
                    ans += matrix[r][c] + max(max(func(r+1, c-1,n,m), func(r+1,c+1,n,m)),func(r+1,c,n,m));
            pd[r][c] = ans;
        }
    }
    return pd[r][c];
}
 
int main(void){
 
    scanf("%d", &T);
    for(int x = 0; x < T; x++){
        scanf("%d%d", &n ,&m);
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m; j++)
                scanf("%lld", &matrix[i][j]);
        long long maX = 0;
        memset(pd, -1, sizeof(pd));
        for(int i = 0; i < m; i++){
            maX = max(maX, func(0,i,n,m));
        }
        printf("%lld\n", maX);
    }
    return 0;
}
