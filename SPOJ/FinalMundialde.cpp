#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <map>
#include <math.h>
#include <string.h>
#define MAXN 103
#define INF 1000000
#define ll long long
#define MOD 100000000

using namespace std;

int n,m,c,o,d,t,dp[MAXN][MAXN][MAXN],test=1;

int main(void){
	while(scanf("%d%d", &n, &m) == 2){
		for(int i = 0; i <= 100; i++){
			for(int j = 0; j <= 100; j++){
				for(int k = 0; k <= 100; k++){
					dp[i][j][k] = INF;
					
				}
			}
		}
		for(int i = 0; i < m; i++){
			scanf("%d%d%d", &o, &d, &t);
			dp[o][d][0] = min(dp[o][d][0], t);
		}
		for(int k = 1; k <= n; k++){
			for(int i = 1; i <= n; i++){
				for(int j = 1; j <= n; j++){
					dp[i][j][k] = dp[i][j][k-1];
					dp[i][j][k] = min(dp[i][j][k], dp[i][k][k-1] + dp[k][j][k-1]);
				}
			}
		}
		printf("Instancia %d\n", test++);
		scanf("%d", &c);
		for(int i = 0; i < c; i++){
			scanf("%d%d%d", &o, &d, &t);
			int ans = INF;
			printf("%d\n", dp[o][d][t] == INF || t == 0 ? -1 : dp[o][d][t]);
		}
		puts("");
	}
    return 0;
}

