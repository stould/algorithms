#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 101
#define ll long long
#define INF 0x3f3f3f3f
 
using namespace std;
 
int k, r, m, n, Xs[MAXN], Ys[MAXN], people[MAXN],Xr[22], Yr[22];
vector<vector<int> > near(21);
 
bool vis[MAXN];
 
vector<int> generateMasks(){
    vector<int> masks;
    int stop = m - 1;
    for(int i = k-1; i >= 0; i--){
        int pos = i;
        int mask = 0;
        for(int j = 0; j <= i; j++){
            mask |= (1 << j);
        }
        for(int j = m-1; j > m-1-(k-i-1); j--){
            mask |= (1 << j);
        }
        masks.push_back(mask);
        while(pos < stop){
            mask = (~(1 << (pos)) & mask) | (1 << (pos+1));
            pos++;
            masks.push_back(mask);
        }
        stop--;
    }
    return masks;
}
 
string bin(int v){
    string ans = "";
    while(v > 0){
        ans =string(1, '0' + (v % 2)) + ans;
        v /= 2;
    }
    return ans;
}
 
 
int main(void){
    while(scanf("%d%d", &k, &r) == 2){
        scanf("%d", &m);
		for(int i = 0; i <= m; i++){
			near[i].clear();
		}
        for(int i = 0; i < m; i++){
            scanf("%d%d", &Xr[i], &Yr[i]);
        }
        scanf("%d", &n);
        for(int i = 0; i < n; i++){
            scanf("%d%d%d", &Xs[i], &Ys[i], &people[i]);
            for(int j = 0; j < m; j++){
                double dist = hypot(double(Xs[i] - Xr[j]), double(Ys[i] - Yr[j]));
                if(dist <= r){
                    near[j].push_back(i);
                }
            }
        }
		int ans = 0;
		for(int i = 0; i < (1 << m);i++){
			if(__builtin_popcount(i) == k){
				memset(vis,0,sizeof(vis));
				int total = 0;
				for(int j = 0; j < m; j++){
					if((i & (1 << j))){
						for(int p = 0; p < near[j].size(); p++){
							int next = near[j][p];
							if(!vis[next]){
								total += people[next];
								vis[next] = 1;
							}
						}
					}
				}
				ans = max(ans, total);
			}
		}
		printf("%d\n", ans);
    }
    return 0;
}
 
