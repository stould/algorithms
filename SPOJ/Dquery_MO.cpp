#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
#define MAXN 30001
#define MAXVAL 1000002
#define MAXQ 200001
#define mp make_pair
#define ll long long
 
using namespace std;
 
int n, q, val[MAXN];
int BLOCK_SIZE = sqrt(MAXN)+5;
 
int resp[MAXQ];
int seen[MAXVAL];
int answer;
 
struct pt{
	int l, r, id;
	
	pt(){}
	pt(int L, int R, int ID){
		l = L;
		r = R;
		id = ID;
	}
 
	bool operator < (const pt &o) const{
		return r < o.r;
	}
};
 
vector<vector<pt> > cons(BLOCK_SIZE);
 
void add(int pos){
	if(seen[val[pos]] == 0){
		answer++;
	}
	seen[val[pos]]++;
}
 
void remove(int pos){
	if(seen[val[pos]] == 1){
		answer--;
	}
	seen[val[pos]]--;
}
 
void process(int pos){
	int l = pos*BLOCK_SIZE, r = pos*BLOCK_SIZE, ql, qr, id;
	answer = 0;
	sort(cons[pos].begin(), cons[pos].end());
	for(int i = 0; i < cons[pos].size(); i++){
		ql = cons[pos][i].l;
		qr = cons[pos][i].r;
		id = cons[pos][i].id;
		while(r < qr){
			add(r);
			r++;
		}
		while(l < ql){
			remove(l);
			l++;
		}
		while(l > ql){
			l--;
			add(l);
		}
		resp[id] = answer;
 
	}
	for(int j = l; j < r; j++){
		remove(j);
	}
}
 
inline void rd(int &x) {
    register int c = getchar_unlocked();
    x = 0;
    int neg = 0;
 
    for (; ((c<48 || c>57) && c != '-'); c = getchar_unlocked());
 
    if (c=='-') {
        neg = 1;
        c = getchar_unlocked();
    }
 
    for ( ; c>47 && c<58 ; c = getchar_unlocked()) {
        x = (x<<1) + (x<<3) + c - 48;
    }
 
    if (neg) {
        x = -x;
    }
}
 
int main(){
	rd(n);
	for(int i = 0; i < n; i++){
		rd(val[i]);
		resp[i] = -1;
	}
	rd(q);
	for(int i = 0; i < q; i++){
		pt newQ;
		rd(newQ.l); rd(newQ.r);
		newQ.id = i;
		newQ.l--;
		cons[newQ.l / BLOCK_SIZE].push_back(newQ);
	}
 
	for(int i = 0; i < BLOCK_SIZE; i++){
		if(cons[i].size()){
			process(i);
		}
	}
	
	for(int i = 0; i < q; i++){
	    printf("%d\n", resp[i]);
	}
	
    return 0;
}
