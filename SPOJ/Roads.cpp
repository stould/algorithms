#include <bits/stdc++.h>
#define MAXN 101
#define MAXCOIN 10001
#define INF 0x3f3f3f3f
#define mp make_pair
using namespace std;

struct road{
    int to, len, toll;
    road(){}
    road(int to_, int len_, int toll_): to(to_), len(len_), toll(toll_){}
};

vector<vector<road> > graph(MAXN);

int dijkstra(int S, int T, int change){
    priority_queue<pair<pair<int, int>, int>, vector<pair<pair<int, int>, int> >, greater< pair<pair<int, int>, int> > > q;
    q.push( mp( mp(0,0), S ) );
    int ans = INF;
    while(!q.empty()){
        int len = q.top().first.first;
        int toll = q.top().first.second;
        int u = q.top().second;
        q.pop();
        if(u == T){
            ans = min(len, ans);
            break;
        }
        for(int i = 0; i < (int) graph[u].size(); i++){
            int nextLen = graph[u][i].len;
            int nextToll = graph[u][i].toll;
            int v = graph[u][i].to;
            if(toll + nextToll <= change){
                q.push( mp( mp(len + nextLen, toll + nextToll), v) );
            }
        }
    }
    return ans;
}

int tests, K, N, R, U, V, L, T;

int main(){
    scanf("%d", &tests);
    while(tests--){
        scanf("%d%d%d", &K, &N, &R);
        for(int i = 0; i < MAXN; i++){
            graph[i].clear();
        }
        for(int i = 0; i < R; i++){
            scanf("%d%d%d%d", &U, &V, &L, &T);
            if(U == V) continue;
            graph[U].push_back(road(V, L, T));
        }
        int dist = dijkstra(1, N, K);
        printf("%d\n", dist == INF ? -1 : dist);
    }
    return 0;
}
