#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 1000002
#define ll long long
#define INF 0x3f3f3f3f

using namespace std;

int t,a,b,c;

int main(void){
	scanf("%d", &t);
	while(t--){
		scanf("%d%d%d", &a, &b, &c);
		if(a > b) swap(a,b);
		if(c > b || (a == b && c != a)){
			printf("-1\n");
		}else if(a == c || b == c){
			printf("1\n");
		}else if(b-a == c){
			printf("2\n");
		}else{
			int ans = -1;
			if(c % a == 0 && c > a){
				int div = c/a;
				ans = (div)*2;
			}else{
				int div = b/a;
				int leftOnA = a-(b-div*a);
				//1)tentar encher ao maximo o pote B e depois colocar o restante do A em B
				if(leftOnA == c){
					ans = (div+1)*2;
				}else if(leftOnA+a < b && leftOnA+a == c){
					ans = (div+2)*2;
				}else{
					//encheu
					int vezes = 1;
					while(b>c){
						//tacou no barril B
						vezes++;
						b -= a;
						if(b == c){
							break;
						}
						//limpa barril A
						vezes++;
					}
					if(b == c){
						ans = vezes;
					}
				}
			}
			printf("%d\n", ans);
		}
	}
    return 0;
}

