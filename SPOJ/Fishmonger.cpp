include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 1001
#define ll long long
#define INF 0x3f3f3f3f
 
using namespace std;
 
int n, t, travel[51][51], toll[51][51], bestTime;
 
struct state{
	int cost, time;
	bool vis;
	state(int a, int b){
		cost = a;
		time = b;
		vis = 0;
	}
	state(){
		cost = INF;
		time = 0;
		vis = 0;
	}
};
 
 
state dp[51][1001];
 
state func(int v, int waste){
	if(v == n-1){
		return state(0,waste);
	}else{
		state &ans = dp[v][waste];
		if(ans.vis == 0){
			ans.vis = 1;
			for(int i = 0; i < n; i++){
				if(waste + travel[v][i] <= t && v != i){
					
					state next = func(i, waste + travel[v][i]);
					int cost = toll[v][i] + next.cost;
					int time = next.time;
					//cout << waste << endl;
					if(cost < ans.cost){
						ans.cost = cost;
						ans.time = time;
					}else if(cost == ans.cost){
						ans.time = min(ans.time, time);
					}
				}
			}
		}
		return ans;
	}
}
 
int main(void){
	while(scanf("%d%d", &n, &t) && (n > 0 && t > 0)){
		for(int i = 0; i < n; i++){
			for(int j = 0; j < n; j++){
				scanf("%d", &travel[i][j]);
			}
		}
		for(int i = 0; i < n; i++){
			for(int j = 0; j < n; j++){
				scanf("%d", &toll[i][j]);
			}
		}
		for(int i = 0; i <= 50; i++){
			for(int j = 0; j <= 1000; j++){
				dp[i][j] = state(INF,0);
			}
		}
		bestTime = INF;
		state ans = func(0,0);
		
		cout << ans.cost << " " << ans.time << endl;
	}
	return 0;		
} 
