#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <math.h>
#include <string.h>
#include <stack>
#define MAXN 100003
 
using namespace std;
 
int k, l, m, dp[1000001];
 
int main(void){
 
    while(cin >> k >> l >> m){
        dp[0] = 0;
        for(int i = 1; i < 1000001; i++){
            if(dp[i-1] == 0){
                dp[i] = 1;
                continue;
            } else{
                dp[i] = 0;
            }
            if(i - k >= 0){
                if(dp[i-k] == 0){
                    dp[i] = 1;
                }
            }
            if(i - l >= 0){
                if(dp[i-l] == 0){
                    dp[i] = 1;
                }
            }
        }
 
        for(int i = 0; i < m; i++){
            int var;
            cin >> var;
            if(dp[var] == 1){
                printf("A");
            }else{
                printf("B");
            }
        }
        printf("\n");
    }
    return 0;
}
