
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 503
#define ll long long
#define INF 0x3f3f3f3f
 
using namespace std;
 
int t,n;
string s;
 
int dp[201][201], freq[201];
 
int sweet(int l, int r){
	if(l == 0){
		return freq[r];
	}else{
		return freq[r] - freq[l-1];
	}
}
 
int sour(int l, int r){
	if(l == 0){
		return r - l + 1 - freq[r];
	}else{
		return r - l + 1 - (freq[r] - freq[l-1]);
	}
}
 
int func(int l, int r){
	if(l == r){
		return s[l] - '0';
	}else{
		int &ans = dp[l][r];
		if(ans == -1){
			if(sweet(l,r) > sour(l,r)){
				ans = r-l+1;
			}else{
				ans = 0;
			}
			for(int i = l; i < r; i++){
				//part [l,i]
				//part [i+1,r]
				int sweetl = sweet(l,i), sourl = sour(l,i);
				int sweetr = sweet(i+1,r), sourr = sour(i+1,r);
				ans = max(func(l,i) + func(i+1, r), ans);
				if(sweetl > sourl) {
					ans = max(ans, i-l+1 + func(i+1,r));
				}
				if(sweetr > sourr) {
					ans = max(ans, r-i + func(l,i));
				}
			}
		}
		return ans;
	}
}
 
int main(void){
	cin >> t;
	while(t--){
		cin >> n;
		cin >> s;
		freq[0] = s[0] - '0';
		for(int i = 1; i < n; i++){
			freq[i] = freq[i-1] + (s[i] - '0');
		}
		memset(dp,-1,sizeof(dp));
		cout << func(0,n-1) << endl;
	}
    return 0;
}
