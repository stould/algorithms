#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
#define MAXN 70
#define ll long long
 
using namespace std;

int t, c, p;
ll dp[MAXN][10];

ll func(int idx, int last){
	if(idx == p){
		return 1;
	}else{
		ll &ans = dp[idx][last];
		if(ans == -1){
			ans = 0;
			for(int i = last; i <= 9; i++){
				ans += func(idx+1, i);
			}
		}
		return ans;
	}
}

void func2(){
	for(int i = 0; i <= 9; i++){
		dp[1][i] = 1;
	}
	for(int i = 2; i < MAXN; i++){
		for(int j = 0; j <= 9; j++){
			for(int k = 0; k <= j; k++){
				dp[i][j] += dp[i-1][k];
			}
		}
	}
}

int main(){
	scanf("%d", &t);
	func2();
	for(int test = 1; test <= t; test++){
		scanf("%d%d", &c, &p);
		ll ans = 0;
		for(int i = 0; i <= 9; i++){
			ans += dp[p][i];
		}
		printf("%d %lld\n",c, ans);
	}
    return 0;
}
