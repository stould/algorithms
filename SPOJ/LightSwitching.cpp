#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <map>
#include <math.h>
#include <string.h>
#define MAXN 100004
 
using namespace std;
 
int n, m, a, b, cmd, tree[MAXN << 2], lazy[MAXN << 2];
 
void propagate(int &node, int &l, int &r) {
    if(lazy[node] != 0){
        int L = (node << 1);
        int R = (node << 1) + 1;
        tree[node] = (r - l + 1) - tree[node];
        if(l != r){
            lazy[L] ^= 1;
            lazy[R] ^= 1;
        }
        lazy[node] = 0;
    }
}
 
void update(int node, int l, int r, int bl, int br){
    propagate(node,l,r);
    if(l > br || r < bl || l > r) {
        return;
    } else if(l >= bl && r <= br) {
        lazy[node] ^= 1;
        if(lazy[node] != 0){
            tree[node] = (r - l + 1) - tree[node];
            if(l != r){
                lazy[(node << 1)] ^= 1;
                lazy[(node << 1) + 1] ^= 1;
            }
            lazy[node] = 0;
        }
        return;
    } else {
        int mid = (l+r) >> 1;
        int L = node << 1;
        int R = (node << 1) + 1;
        update(L, l, mid, bl, br);
        update(R, mid+1, r, bl, br);
        tree[node] = tree[L] + tree[R];
        return;
    }
}
 
int query(int node, int l, int r, int bl, int br){
    if(l > br || r < bl || l > r){
        return 0;
    }else{
        propagate(node,l,r);
        if(l >= bl && r <= br){
            return tree[node];
        }else{
            int mid = (l+r) >> 1;
            int L = node << 1;
            int R = (node << 1) + 1;
            int vl = query(L, l, mid, bl, br);
            int vr = query(R, mid+1, r, bl, br);
            return vl + vr;
        }
    }
}
 
int main(void){
 
    while(~scanf("%d%d", &n, &m)){
        for(int i = 0; i < m; i++){
            scanf("%d%d%d", &cmd, &a, &b);
            if(cmd == 0){
                update(1,0,n-1,a-1,b-1);
            }else{
                printf("%d\n", query(1,0,n-1,a-1,b-1));
            }
        }
    }
    return 0;
}
