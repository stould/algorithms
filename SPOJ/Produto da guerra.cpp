#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100002
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int n, u, v, degree[MAXN], vis[MAXN], t;

vector<vector<int> > g(MAXN);

struct Less{
	bool operator () (int a, int b){
		return degree[a] > degree[b];
	}
};


int main(void){
	scanf("%d", &t);
	while(scanf("%d", &n) != EOF){
		priority_queue<int, vector<int>, Less> q;
		for(int i = 0; i <= n; i++){
			g[i].clear();
			vis[i] = degree[i] = 0;
		}
		for(int i = 0; i < n - 1; i++){
			scanf("%d%d", &u, &v);
			g[u].push_back(v);
			g[v].push_back(u);
			degree[u]++;
			degree[v]++;
		}
		for(int i = 1; i <= n; i++){
			q.push(i);
		}
		int ans = 0;
		while(!q.empty()){
			int top = q.top(); q.pop();
			bool ok = 1;
			for(int i = 0; i < degree[top] && ok; i++){
				int next = g[top][i];
				if(vis[next]){
					ok = 0;
				}
			}
			if(ok){
				ans++;
				vis[top] = 1;
			}
		}
		printf("%d\n", ans);
	}
}
