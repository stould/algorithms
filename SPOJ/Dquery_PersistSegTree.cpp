#include <bits/stdc++.h>
#define INF 30002
#define MAXN 30005
#define MAXQ 200001

#define LOGMAXVAL 18
 
using namespace std;
 
int n, q, ql[MAXQ], qr[MAXQ], v[MAXQ];
 
//Persistent SegTree
int root[MAXQ];
int INDEX;
int Lef[MAXN*4*LOGMAXVAL];
int Rig[MAXN*4*LOGMAXVAL];
int S[MAXN*4*LOGMAXVAL];

struct PersistentSegTree{

    PersistentSegTree(){
        INDEX = 1;
        build(0, 0, MAXN);
    }
	
    //build the initial and empty tree
    void build(int node, int l, int r){
        if(l == r){
            return;
        }else{
            int mid = (l+r) / 2;
            Lef[node] = INDEX++;
            Rig[node] = INDEX++;
            build(Lef[node], l, mid);
            build(Rig[node], mid+1, r);
        }
    }
 
    /*query to count how many elements are > K
      here is the key of the problem.*/
    int query(int node, int l, int r, int K){
        if(r <= K){
            return 0;
        }else if(l > K){
            return S[node];
        }else{
            int mid = (l+r) / 2;
            return query(Lef[node], l, mid, K) + query(Rig[node], mid+1, r, K);
        }
    }
 
    /*add a new node, we just need to copy log(n) nodes 
      from the previus tree add add the new one*/
    int update(int node, int l, int r, int pos){
        int next = INDEX++;
        Lef[next] = Lef[node];
        Rig[next] = Rig[node];
        S[next] = S[node];
        if(l == r){
            S[next] += 1;
        }else{
            int mid = (l+r) / 2;
            if(pos <= mid){
                Lef[next] = update(Lef[node], l, mid, pos);
            }else{
                Rig[next] = update(Rig[node], mid+1, r, pos);
            }
            S[next] = S[Lef[next]] + S[Rig[next]];
        }
        return next;
    }
};

int go[MAXN];
int last[MAXN];
map<int, int> vis;

inline void rd(int &x) {
    register int c = getchar_unlocked();    
    x = 0;
    int neg = 0;
 
    for (; ((c<48 || c>57) && c != '-'); c = getchar_unlocked());
 
    if (c=='-') {
        neg = 1;
        c = getchar_unlocked();
    }
 
    for ( ; c>47 && c<58 ; c = getchar_unlocked()) {
        x = (x<<1) + (x<<3) + c - 48;
    }
 
    if (neg) {
        x = -x;
    }
}
 
int main(){
    rd(n);
    for(int i = 1; i <= n; i++){
        rd(v[i]);
    }    
    cin >> q;
    for(int i = 0; i < q; i++){
        rd(ql[i]); rd(qr[i]);
    }


    int idx = 1;
    for(int i = 0; i < MAXN; i++){
        go[i] = INF;
    }
    for(int i = 1; i <= n; i++){
        if(vis[v[i]] == 0){
            vis[v[i]] = idx++;
            last[vis[v[i]]] = i;
        }else{
            go[last[vis[v[i]]]] = i;
            last[vis[v[i]]] = i;
        }
    }
    for(int i = 1; i <= n; i++){
        cout << go[i] << endl;
    }cout << endl;
    PersistentSegTree tree;
    for(int i = 1; i <= n; i++){
        root[i] = tree.update(root[i-1], 0, MAXN-1, go[i]);
    }
    for(int i = 0; i < q; i++){
        int ans = tree.query(root[qr[i]], 0, MAXN-1, qr[i]);
        ans -= tree.query(root[ql[i]-1], 0, MAXN-1, qr[i]);
        printf("%d\n", ans);
    }
    return 0;
}
