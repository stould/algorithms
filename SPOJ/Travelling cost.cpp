#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 503
#define ll long long
#define INF 0x3f3f3f3f

using namespace std;

vector<vector<pair<int,int> > > g(MAXN);
int n,u,v,w,q,source,destination,dist[MAXN];

struct Less{
	bool operator () (int a, int b){
		return dist[a] > dist[b];
	}
};

void dijkstra(int s){
	priority_queue<int, vector<int>, Less> q;
	q.push(s);
	dist[s] = 0;
	while(!q.empty()){
		int top = q.top(); q.pop();
		for(int i = 0; i < g[top].size(); i++){
			int next = g[top][i].first;
			int cost = g[top][i].second;
			if(dist[top] + cost < dist[next]){
				dist[next] = dist[top] + cost;
				q.push(next);
			}
		}
	}
}

int main(void){
	while(scanf("%d", &n) == 1){
		for(int i = 0; i < MAXN; i++){
			dist[i] = INF;
			g[i].clear();
		}
		for(int i = 0; i < n; i++){
			scanf("%d%d%d", &u, &v, &w);
			g[u].push_back(make_pair(v,w));
			g[v].push_back(make_pair(u,w));
		}
		scanf("%d%d", &source, &q);
		dijkstra(source);
		while(q--){
			scanf("%d", &destination);
			if(dist[destination] == INF){
				printf("NO PATH\n");
			}else{
				printf("%d\n", dist[destination]);
			}
		}
	}
    return 0;
}
