#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <map>
#include <math.h>
#include <string.h>
#define MAXN 50001
#define INF 500000000
#define ll long long
 
using namespace std;
 
int n, m, from, to;
 
int interval[MAXN];
int tree[MAXN*4+1];
 
void build(int idx, int a, int b) {
    if (a > b) {
        return;
    } else if(a == b){
        tree[idx] = interval[a];
    }else{
        int mid = (a+b) >> 1;
        build(idx*2, a, mid);
        build(idx*2+1, mid + 1, b);
        tree[idx] = max(tree[2 * idx], tree[2 * idx + 1]);
    }
}
 
int query(int idx, int idxa, int idxb, int a, int b){
    if(a > idxb || b < idxa) return -INF;
    if(idxa >= a && idxb <= b){
        return tree[idx];
    }
    int mid = (idxa+idxb) >> 1;
    int q1 = query(2 * idx, idxa, mid, a, b);
    int q2 = query(2 * idx + 1, mid+1, idxb, a, b);
    return max(q1, q2);
}
 
int main(void){
    ios::sync_with_stdio(0);
    while(cin >> n >> m){
        for(int i = 0; i < n; i++){
            cin >> interval[i];
        }
        build(1, 0, n - 1);
        int ans = 0;
        for(int i = 0; i < m; i++){
            cin >> from >> to;
            int tmp = query(1, 0, n-1, from-1, to-2);
            if(tmp <= interval[from-1])
                ans++;
        }
        cout << ans << endl;
    }
    return 0;
}
