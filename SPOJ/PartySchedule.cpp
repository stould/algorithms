#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 503
#define ll long long
#define INF 0x3f3f3f3f
 
using namespace std;
 
int budget, parties, w[MAXN], v[MAXN];
 
int dp[101][MAXN];
 
int main(void){
	while(scanf("%d%d", &budget, &parties) && budget != 0 && parties != 0){
		for(int i = 1; i <= parties; i++){
			scanf("%d%d", &w[i], &v[i]);
		}
		memset(dp,0,sizeof(dp));
		for(int i = 1; i <= parties; i++){
			for(int j = 0; j <= budget; j++){
				if(j >= w[i]){
					dp[i][j] = max(dp[i-1][j], dp[i-1][j-w[i]] + v[i]);
				}else{
					dp[i][j] = dp[i-1][j];
				}
			}
		}
		int lo = 0, hi = budget;
		while(lo <= hi){
			int mid = (lo+hi) / 2;
			if(dp[parties][mid] >= dp[parties][budget]){
				hi = mid-1;
			}else{
				lo = mid+1;
			}
		}
		printf("%d %d\n", lo, dp[parties][budget]);
	}
    return 0;
}
