#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#define MAXN 100001
#define INF 999999999
#define ll long long
 
using namespace std;
 
bool isPrime[32000];
vector<int> primes;
int seq[110001];
 
void sieve(int N){
    memset(isPrime,1,sizeof(isPrime));
    isPrime[0] = isPrime[1] = 0;
    isPrime[2] = 1;
    for(int i = 2; i < N; i++){
        if(isPrime[i]){
            for(int j = 2*i; j < N; j += i){
                isPrime[j] = 0;
            }
        }
    }
    for(int i = 0; i < N; i++){
        if(isPrime[i]){
            primes.push_back(i);
        }
    }
}
 
bool verify(int N){
    if(N <= 1) return 0;
    if(N == 2) return 1;
    if(N % 2 == 0) return 0;
    int m = (int) sqrt(N);
    for(int i = 3; i <= N; i++){
        if(N % i == 0) return 0;
    }
    return 1;
}
 
int search(int key, int lower, int higher){
    ll low = 2, high = higher;
    ll ans = 1000000000;
    while(low <= high){
        ll mid = (low+high) >> 1;
        if(key * mid < lower){
            low = mid+1;
        }else if(key * mid > lower){
            high = mid-1;
            ans = min(ans, mid);
        }else{
            return mid;
        }
    }
    return (int)ans;
}
 
void generate(int &low, int &high){
    memset(seq,1,sizeof(seq));
    for(int i = 0; primes[i]*primes[i] <= high; i++){
        int p = primes[i] * search((ll)primes[i],(ll)low, (ll)high);
        for(int j = p; j <= high; j += primes[i]){
            seq[j-low] = 0;
        }
    }
    for(int i = 0; i <= high-low; i++){
        if(seq[i]){
            printf("%d\n", i+low);
        }
    }
    printf("\n");
}
 
int a, b, t;
 
int main(void){
    //freopen("in.in.c", "r", stdin);
    sieve(32000);
    scanf("%d\n", &t);
    while(t--){
        ~scanf("%d%d", &a, &b);
        if(a < 2) a = 2;
        generate(a, b);
    }
    return 0;
}
