#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <cmath>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100001
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)
#define LEFT 0
#define RIGHT 1

using namespace std;

int n;
ll h[MAXN];

int main(void) {
	while(cin >> n && n){
		for(int i = 0; i < n; i++){
			cin >> h[i];
		}
		ll ans = 0;
		stack<int> st;
		for(int i = 0; i < n; i+=1){
			if(st.empty() || h[i] >= h[st.top()]){
				st.push(i);
			}else{
				int top = st.top(); st.pop();
				ans = max(ans, h[top] * (ll)(i-top-1));
				while(!st.empty() && h[i] < h[st.top()]){
					top = st.top(); st.pop();
					ans = max(ans, h[top] * (ll)(i-top));
				}
				st.push(i);
			}
		}
		while(!st.empty() && h[n-1] <= h[st.top()]){
			int top = st.top(); st.pop();
			ans = max(ans, h[top] * (ll)(n-top));
		}
		cout << ans << endl;
	}
	return 0;
}
