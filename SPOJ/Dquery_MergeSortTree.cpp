#include <bits/stdc++.h>
#define MAXN 30003
#define INF INT_MAX
#define ll long long
 
using namespace std;
 
int n, q, ql, qr;
int v[MAXN], vv;
int go[MAXN];
int idx, last[MAXN];
map<int,int> vis;
vector<int> tree[MAXN << 2];
 
int lower_search(vector<int> &arr, int key){
    int lo = 0, hi = arr.size() - 1, ans = INF;
    while(lo <= hi){
        int mid = (lo + hi) >> 1;
        if(arr[mid] >= key){
            ans = min(ans, mid);
            hi = mid-1;
        }else{
            lo = mid+1;
        }
    }
    return ans;
}
 
vector<int> merge(vector<int> &l, vector<int> &r){
    vector<int> ans;
    int idxl = 0;
    int idxr = 0;
    while(idxl < l.size() && idxr < r.size()){
        if(l[idxl] < r[idxr]){
            ans.push_back(l[idxl++]);
        }else if(l[idxl] > r[idxr]){
            ans.push_back(r[idxr++]);
        }else{
            ans.push_back(l[idxl++]);
            ans.push_back(r[idxr++]);
        }
    }
    while(idxl < l.size()){
        ans.push_back(l[idxl++]);
    }
    while(idxr < r.size()){
        ans.push_back(r[idxr++]);
    }
    return ans;
}
 
void build(int node, int l, int r){
    if(l > r) return;
    if(l == r){
        tree[node] = vector<int>(1, go[l]);
    }else{
        int mid = (r+l) >> 1;
        build(node << 1, l, mid);
        build((node << 1) | 1, mid+1, r);
        tree[node] = merge(tree[node << 1], tree[(node << 1) | 1]);
    }
}
 
int query(int node, int l, int r, int bl, int br){
    if(l > br || r < bl || l > r){
        return 0;
    }else if(l >= bl && r <= br){
        //int greater = upper_bound(tree[node].begin(), tree[node].end(), br) - tree[node].begin();
        int greater = lower_search(tree[node], br+1);
        if(greater == INT_MAX){
            return 0;
        }else{
            return tree[node].size() - greater;
        }
    }else{
        int mid = (l+r) >> 1;
        int lq = query(node << 1, l, mid, bl, br);
        int rq = query((node << 1) | 1, mid+1, r, bl, br);
        return lq + rq;
    }
}
 
inline void rd(int &x) {
    register int c = getchar_unlocked();
    x = 0;
    int neg = 0;
 
    for (; ((c<48 || c>57) && c != '-'); c = getchar_unlocked());
 
    if (c=='-') {
        neg = 1;
        c = getchar_unlocked();
    }
 
    for ( ; c>47 && c<58 ; c = getchar_unlocked()) {
        x = (x<<1) + (x<<3) + c - 48;
    }
 
    if (neg) {
        x = -x;
    }
}
 
int main(){
    //freopen("in.in","r", stdin);
    //freopen("out", "w", stdout);
    rd(n);
    idx = 1;
    for(int i = 0; i < MAXN; i++){
        go[i] = INF;
    }
    for(int i = 0; i < n; i++){
        rd(vv);
        if(vis[vv] == 0){
            vis[vv] = idx++;
            last[vis[vv]] = i;
        }else{
            go[last[vis[vv]]] = i;
            last[vis[vv]] = i;
        }
    }
    rd(q);
    build(1, 0, n-1);
    while(q--){
        rd(ql);
        rd(qr);
        printf("%d\n", query(1,0,n-1,ql-1,qr-1));
    }
    return 0;
}
