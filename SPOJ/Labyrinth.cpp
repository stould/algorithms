#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <set>
#include <math.h>
#include <string.h>
#define MAXN 208
#define INF 999999999
#define ll long long
 
using namespace std;
 
int T, n, m, vis[1001][1001], dist[1001][1001];
char lab[1001][1001];
int dx[] = {-1, 0, 1, 0};
int dy[] = {0, 1, 0, -1};
 
struct Node{
    int x, y, dist;
    Node(int x_, int y_){
        x = x_;
        y = y_;
        dist = -1;
    }
};
 
 Node bfs(int x, int y){
    queue<Node> q;
    Node st(x, y);
    q.push(st);
    Node ans(0, 0);
    vis[x][y] = 1;
    while(!q.empty()){
        Node top = q.front(); q.pop();
        for(int i = 0; i < 4; i++){
            int dxa = dx[i] + top.x;
            int dya = dy[i] + top.y;
            if(dxa >= 0 && dya >= 0 && dxa < n && dya < m && lab[dxa][dya] == '.' && !vis[dxa][dya]){
                Node next(dxa, dya);
                vis[dxa][dya] = 1;
                dist[dxa][dya] = dist[top.x][top.y] + 1;
                q.push(next);
                if(dist[dxa][dya] > ans.dist){
                    ans.dist = dist[dxa][dya];
                    ans.x = dxa;
                    ans.y = dya;
                }
            }
        }
    }
    return ans;
}
 
int main(void){
    scanf("%d", &T);
    while(T--){
        scanf("%d%d", &m, &n);
        int si = -1, sj = -1;
        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                scanf(" %c", &lab[i][j]);
                if(lab[i][j] == '.'){
                    si = i; sj = j;
                }
                dist[i][j] = 0;
                vis[i][j] = 0;
            }
        }
        Node sofar = bfs(si, sj);
        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                vis[i][j] = dist[i][j] = 0;
            }
        }
        if(sofar.dist <= 0){
            printf("Maximum rope length is 0.\n");
        }else{
            int ans = bfs(sofar.x, sofar.y).dist;
            printf("Maximum rope length is %d.\n", ans);
        }
    }
    return 0;
}
