#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 50007
#define ll long long
#define INF 0xffffff
#define PI acos(-1)

using namespace std;

ll n;

bool isPrime[MAXN];

vector<ll> primes;

void crivo(){
	for(int i = 0; i < MAXN; i++){
		isPrime[i] = 1;
	}
	isPrime[0] = isPrime[1] = 0;
	for(int i = 2; i < MAXN; i++){
		if(isPrime[i]){
			primes.push_back(i);
			for(int j = i*2; j < MAXN; j+=i){
				isPrime[j] = 0;
			}
		}
	}
}

int gcd(int a, int b) {
    if(b == 0) return a;
    return gcd(b, a % b);
}

int main(){
	crivo();
	while(cin >> n && n){
		bool neg = 0;
		if(n < 0) neg = 1;
		n = abs(n);
		int ans = -1;
		for(int i = 0; i < primes.size() && n > 1; i++){
			if(n % primes[i] == 0){
				int pot = 0;
				while(n % primes[i] == 0){
					n /= primes[i];
					pot++;
				}
				if(ans == -1){
					ans = pot;
				}else{
					ans = gcd(ans, pot);
				}
			}
		}
		if(ans == -1){
			ans = 1;
		}else{
			if(neg){
				if(ans % 2 == 0){
					bool find = 0;
					for(int i = ans-1; i > 1; i -= 2){
						if(ans % i == 0){
							ans = i;
							find = 1;
							break;
						}
					}
					if(!find){
						ans = 1;
					}
				}
			}
		}
		cout << ans << endl;
	}
	return 0;
}
