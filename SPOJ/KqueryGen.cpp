#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
#define MAXN 30003
#define LOGVAL 18
using namespace std;

 
int n, q, ql[MAXN], qr[MAXN], C[MAXN];
int v[MAXN], root[MAXN];
vector<int> package;

int idx;
map<int, int> cmp; // compress map

//Persistent SegTree
int INDEX;
int Lef[MAXN*4*LOGVAL];
int Rig[MAXN*4*LOGVAL];
int S[MAXN*4*LOGVAL];

void gen(){
	
	int maxn = 500;
	int maxv = 1000000000;
	int maxq = 500;
	n = 1 + rand() % maxn;
	for(int i = 0; i < n; i++){
		v[i] = 1 + rand() % maxv;
	}
	q = 1 + rand() % maxq;
	for(int i = 0; i < q; i++){
		ql[i] = 1 + rand() % n;
		qr[i] = 1 + rand() % n;
		C[i] = 1 + rand() % maxv;		
	}
}

void clear(){
	memset(ql, 0,sizeof(ql));
	memset(qr, 0,sizeof(qr));
	memset(C, 0,sizeof(C));
	memset(v, 0,sizeof(v));
	memset(root, 0,sizeof(root));
	memset(Lef, 0,sizeof(Lef));
	memset(Rig, 0,sizeof(Rig));
	memset(S, 0,sizeof(S));
	INDEX = idx = 1;
	package.clear();
	cmp.clear();
}

struct SegTreePersistent{
 
    SegTreePersistent(){
		INDEX = 1;
        build(0, 0, MAXN);
    }
 
    void build(int node, int l, int r){
        if(l == r){
            return;
        }else{
            int mid = (l+r) / 2;
            Lef[node] = INDEX++;
            Rig[node] = INDEX++;
            build(Lef[node], l, mid);
            build(Rig[node], mid+1, r);
        }
    }
 
    int query(int node, int l, int r, int K){
		if(r <= K){
			return 0;
		}else if(l > K){
            return S[node];
        }else{
            int mid = (l+r) / 2;
            int left = query(Lef[node], l, mid, K);
            int right = query(Rig[node], mid+1, r, K);
            return left + right;
        }
    }
 
    int update(int node, int l, int r, int pos){
        int next = INDEX++;
        Lef[next] = Lef[node];
        Rig[next] = Rig[node];
        S[next] = S[node];
        if(l == r){
            S[next]++;
        }else{
            int mid = (l+r) / 2;
            if(pos <= mid){
                Lef[next] = update(Lef[node], l, mid, pos);
            }else{
                Rig[next] = update(Rig[node], mid+1, r, pos);
            }
            S[next] = S[Lef[next]] + S[Rig[next]];
        }
        return next;
    }
}; 

inline void rd(int &x) {
    register int c = getchar_unlocked();    
    x = 0;
    int neg = 0;

    for (; ((c<48 || c>57) && c != '-'); c = getchar_unlocked());

    if (c=='-') {
        neg = 1;
        c = getchar_unlocked();
    }

    for ( ; c>47 && c<58 ; c = getchar_unlocked()) {
        x = (x<<1) + (x<<3) + c - 48;
    }

    if (neg) {
        x = -x;
    }
}

int bruteforce(int L, int R, int K){
	int ans = 0;
	for(int i = L; i <= R; i++){
		if(v[i] > K){
			ans++;
		}
	}
	return ans;
}

int main(){
	srand(time(NULL));
	int t = 0;
	while(1){
		cout << t++ << endl;
		clear();
		gen();
		//		rd(n);
		for(int i = 0; i < n; i++){
			//	rd(v[i]);
			package.push_back(v[i]);
		}    
		//		rd(q);
		for(int i = 0; i < q; i++){
			//	rd(ql[i]); rd(qr[i]); rd(C[i]);
			ql[i]--;
			qr[i]--;
			if(ql[i] > qr[i]) swap(ql[i], qr[i]);
			package.push_back(C[i]);
		}

		//compression
		sort(package.begin(), package.end());
		for(int i = 0; i < package.size(); i++){
			if(cmp.find(package[i]) == cmp.end()){
				cmp[package[i]] = idx++;
			}
		}
		SegTreePersistent tree;
		for(int i = 0; i < n; i++){
			root[i] = tree.update(root[i - 1 <= 0 ? 0 : i-1], 0, MAXN, cmp[v[i]]);
		}
		for(int i = 0; i < q; i++){
			int ans = tree.query(root[qr[i]], 0, MAXN, cmp[C[i]]);
			if(ql[i] > 0){
				ans -= tree.query(root[ql[i]-1], 0, MAXN, cmp[C[i]]);
			}
			if(ans != bruteforce(ql[i], qr[i], C[i])){
				cout << "ERROR" << endl;
				return 0;
			}
			//			printf("%d\n", ans);
		}
	}
    return 0;
}
