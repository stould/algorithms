#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 101
#define ll long long
#define INF 0x3f3f3f3f

using namespace std;

int n, t, f, v[41], soma[41], subtraindo[41];

struct state{
	int used;
	state(){used = -1;}
	state(int a){used = a;}
};

map<pair<int, int>, state> dp;

state func(int F, int idx){
	if(idx == n){
		return state(f == F);
	}else{
		pair<int, int> st = make_pair(F, idx);
		if(dp[st].used == -1){
			int sum = func(F+v[idx], idx+1).used;
			int sub = func(F-v[idx], idx+1).used;
			dp[st].used = sub | sum;
			if (sum) {
				soma[idx] = true;
			}
			if (sub) {
				subtraindo[idx] = true;
			}			
		}
		return dp[st];
	}
}

int main(void){
	while(scanf("%d%d", &n, &f) && n){
		for(int i = 0; i < n; i++){
			scanf("%d", &v[i]);
		}
		string ans = "";
		int tot = 0;
		dp.clear();
		memset(soma,0,sizeof(soma));
		memset(subtraindo,0,sizeof(subtraindo));
		func(0, 0);
		for(int i = 0; i < n; i++){
			int sum = soma[i];
			int sub = subtraindo[i];
			if( sum == 1 && sub == 1){
				ans += "?";
			}else if(sum == 1 && sub == 0){
				ans += "+";
			}else if(sum == 0 && sub == 1){
				ans += "-";
			}else{
				tot++;
			}
		}
		if(tot == n){
			printf("*\n");
		}else{
			printf("%s\n", ans.c_str());
		}
	}
    return 0;
}
