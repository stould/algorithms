#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 1000002
#define ll long long
#define INF 0x3f3f3f3f

using namespace std;
 
int n,k, ARRAY[MAXN], idx[MAXN], val[MAXN];
 
void msw(){
	vector<int> ans;
	if(k == 1){
		for(int i = 0; i < n; i++) ans.push_back(val[i]);
	}else{
		ARRAY[0] = val[0];
		int front = 0, back = 0;
		idx[back] = 0;
		for (int i = 1; i < n; ++i){
		    ARRAY[i] = val[i];
		    while(front <= back && idx[front] <= i-k){
		        front++;
		    }
		    while(front <= back && ARRAY[i] >= ARRAY[idx[back]]){
		        back--;
		    }
		    idx[++back] = i;
		    if(i >= k-1)
		        ans.push_back(ARRAY[idx[front]]);
		}
	}
	printf("%d", ans[0]);
	for(int i = 1; i < ans.size(); i++){
		printf(" %d", ans[i]);
	}
	puts("");
}

int main(void){
	while(scanf("%d", &n) == 1){
		for(int i = 0; i < n; i+=1)scanf("%d", &val[i]);
		scanf("%d", &k);
		msw();
	}
    return 0;
}
