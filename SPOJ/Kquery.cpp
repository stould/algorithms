#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
#define MAXN 100001
#define LOGVAL 18
#define ll long long
 
using namespace std;
 
int n, q, ql[MAXN], qr[MAXN], root[MAXN], C[MAXN], v[MAXN];
 
//Persistent SegTree
int INDEX;
int Lef[MAXN*4*LOGVAL];
int Rig[MAXN*4*LOGVAL];
int S[MAXN*4*LOGVAL];
 
struct SegTreePersistent{
 
    SegTreePersistent(){
		INDEX = 1;
        build(0, 0, MAXN);
    }
 
    void build(int node, int l, int r){
        if(l == r){
            return;
        }else{
            int mid = (l+r) / 2;
            Lef[node] = INDEX++;
            Rig[node] = INDEX++;
            build(Lef[node], l, mid);
            build(Rig[node], mid+1, r);
        }
    }
 
    int query(int nodeR, int nodeL, int l, int r, int K){
		if(l == r){
			return l;
        }else{
            int mid = (l+r) / 2;
			int diff = S[Lef[nodeR]] - S[Lef[nodeL]];
			if(diff >= K){
				return query(Lef[nodeR], Lef[nodeL], l, mid, K);				
			}
            return query(Rig[nodeR], Rig[nodeL], mid+1, r, K - diff);
        }
    }

    int update(int node, int l, int r, int pos){
        int next = INDEX++;
        Lef[next] = Lef[node];
        Rig[next] = Rig[node];
        S[next] = S[node];
        if(l == r){
            S[next]++;
        }else{
            int mid = (l+r) / 2;
            if(pos <= mid){
                Lef[next] = update(Lef[node], l, mid, pos);
            }else{
                Rig[next] = update(Rig[node], mid+1, r, pos);
            }
            S[next] = S[Lef[next]] + S[Rig[next]];
        }
        return next;
    }
}; 
 
int idx;
map<int, int> cmp; // compress map
map<int, int> ucmp; // compress map
 
inline void rd(int &x) {
    register int c = getchar_unlocked();    
    x = 0;
    int neg = 0;
 
    for (; ((c<48 || c>57) && c != '-'); c = getchar_unlocked());
 
    if (c=='-') {
        neg = 1;
        c = getchar_unlocked();
    }
 
    for ( ; c>47 && c<58 ; c = getchar_unlocked()) {
        x = (x<<1) + (x<<3) + c - 48;
    }
 
    if (neg) {
        x = -x;
    }
}
 
int main(){
    rd(n);rd(q);
    for(int i = 1; i <= n; i++){
		rd(v[i]);
		cmp[v[i]];
    }    
	//compression
	idx = 0;
	for(map<int, int>::iterator it = cmp.begin(); it != cmp.end(); it++){
		cmp[it->first] = idx;
		ucmp[idx] = it->first;
		idx++;
	}
	SegTreePersistent tree;
	for(int i = 1; i <= n; i++){
		root[i] = tree.update(root[i-1], 0, MAXN, cmp[v[i]]);
	}
	for(int i = 0; i < q; i++){
		rd(ql[i]); rd(qr[i]); rd(C[i]);
		int ans = ucmp[tree.query(root[qr[i]], root[ql[i]-1], 0, MAXN, C[i])];
		printf("%d\n", ans);
    }
    return 0;
}
