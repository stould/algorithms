#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100002
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int n, v[16];

struct hist{
	int count, len;
	hist(){
		len = count = 0;
	}
	hist(int a, int b){
		len = a;
		count = b;
	}
};

hist dp[1 << 16][17];

bool mask[1 << 16][17];

hist func(int mask, int last){
	if(mask == (1 << n) - 1){
		return hist(v[last], 1);
	}else{
		hist &ans = dp[mask][last];
		if(ans.len == -1){
			ans.len = 0;
			for(int i = 0; i < n; i++){
				if(!(mask & (1 << i))){
					hist tmp = func(mask | (1 << i), i);
					int val = tmp.len + abs(v[i]-v[last]);
					if(val > ans.len){
						ans.len = val;
						ans.count = tmp.count;
					}else if(val == ans.len){
						ans.count += tmp.count;

					}
				}
			}
		}
		return ans;
	}
}

int main(void){
	while(scanf("%d", &n) && n){
		for(int i = 0 ; i < n; i++){
			scanf("%d", &v[i]);
		}
		for(int i = 0; i < (1 << n); i++){
			for(int j = 0; j <= 16; j++){
				dp[i][j] = hist();
				dp[i][j].len = -1;
			}
		}
		hist ans = func(0, 16);
		printf("%d %d\n", ans.len + (n << 1), ans.count);
	}
	return 0;
}
