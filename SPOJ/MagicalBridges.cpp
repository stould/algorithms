#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <math.h>
#include <string.h>
#include <map>
#define MAXN 325
#define INF 0x3f3f3f3f
#define ll long long
 
using namespace std;
 
int t, n, f, m, q;
int dist[MAXN][MAXN];
int ut, uf, vt, vf, c, idx;
map<pair<int, int>, int> mapper;
map<int, pair<int, int> > rev;
 
int main(void){
    //freopen("in.in", "r", stdin);
    scanf("%d", &t);
    while(t--){
        mapper.clear();
        rev.clear();
        idx = 0;
        for(int i = 0; i < MAXN; i++){
            for(int j = 0; j < MAXN; j++){
                dist[i][j] = INF;
            }
            dist[i][i] = 0;
        }
        scanf("%d%d%d", &n, &f, &m);
        vector<vector<int> > tower(MAXN/3);
        for(int i = 0; i < m; i++){
            scanf("%d%d%d%d%d", &ut, &uf, &vt, &vf, &c);
            pair<int, int> tmpu = make_pair(ut, uf);
            pair<int, int> tmpv = make_pair(vt, vf);
            if(mapper.find(tmpu) == mapper.end()){
                mapper[tmpu] = idx++;
                rev[idx-1] = tmpu;
                tower[ut].push_back(idx-1);
            }
            if(mapper.find(tmpv) == mapper.end()){
                mapper[tmpv] = idx++;
                rev[idx-1] = tmpv;
                tower[vt].push_back(idx-1);
            }
            int U = mapper[tmpu];
            int V = mapper[tmpv];
            dist[U][V] = min(c, dist[U][V]);
            dist[V][U] = min(c, dist[V][U]);
 
 
        }
        for(int i = 1; i <= n; i++){
            pair<int, int> tmpu = make_pair(i, 1);
            pair<int, int> tmpv = make_pair(i+1, 1);
            if(i == n){
                tmpv = make_pair(1, 1);
            }
            if(mapper.find(tmpu) == mapper.end()){
                mapper[tmpu] = idx++;
                rev[idx-1] = tmpu;
                tower[i].push_back(idx-1);
            }
            if(mapper.find(tmpv) == mapper.end()){
                mapper[tmpv] = idx++;
                rev[idx-1] = tmpv;
                tower[tmpv.first].push_back(idx-1);
            }
            int U = mapper[tmpu];
            int V = mapper[tmpv];
            dist[U][V] = min(1, dist[U][V]);
            dist[V][U] = min(1, dist[V][U]);
        }
        for(int i = 1; i <= n; i++){
            for(int j = 0; j < tower[i].size(); j++){
                int floor = rev[tower[i][j]].second;
                int U = mapper[make_pair(i, 1)];
                int V = mapper[make_pair(i, floor)];
                dist[U][V] = min(abs(floor-1), dist[U][V]);
                dist[V][U] = min(abs(floor-1), dist[V][U]);
                for(int k = j+1; k < tower[i].size(); k++){
                    int floorl = rev[tower[i][k]].second;
                    int Vl = mapper[make_pair(i, floorl)];
                    dist[V][Vl] = min(abs(floor-floorl), dist[V][Vl]);
                    dist[Vl][V] = min(abs(floor-floorl), dist[Vl][V]);
                }
            }
        }
        for(int k = 0; k < idx; k++) {
            for(int i = 0; i < idx; i++) {
                for(int j = 0; j < idx; j++) {
                    dist[i][j] = min(dist[i][j], dist[i][k] + dist[k][j]);
                }
            }
        }
        scanf("%d", &q);
        while(q--){
            scanf("%d%d%d%d", &ut, &uf, &vt, &vf);
            int ans = INF;
            for(int i = 0; i < tower[ut].size(); i++){
                for(int j = 0; j < tower[vt].size(); j++){
                    pair<int, int> tmpu = rev[tower[ut][i]];
                    pair<int, int> tmpv = rev[tower[vt][j]];
                    int U = mapper[tmpu];
                    int V = mapper[tmpv];
                    ans = min(dist[U][V] + abs(tmpu.second-uf) + abs(tmpv.second-vf), ans);
                }
            }
            if(ut == vt){
                ans = min(ans, abs(vf-uf));
            }
            printf("%d\n", ans);
        }
    }
    return 0;
}
