#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 1001
#define ull long long
#define INF 0x3f3f3f3f
 
using namespace std;
 
ull n,k;
int T;
 
ull C(ull n, ull k) {
	ull res = 1;
	if(k > (n >> 1LL))
		k = n-k;
	for(ull i = 1; i <= k; i++, n--) {
		res =  (res*n)/(i);
	}
	return res;
}
 
int main(void){
	cin >> T;
	while(T--){
		cin >> n >> k;
		cout << C(n-1,k-1) << endl;
	}
	return 0;		
}/*
(n-k) * (n-k+1) * .... * (n-1) * n
__________________________________
              (n-k)!
 
n = 14
k = 3
1*2*3*4*5*6*7*8*9*10*11*12*13*14
___________________________________ =
(1*2*3) * (1*2*3*4*5*6*7*8*9*10*11)
4*5*6*7*8*9*10*11*12*13*14
___________________________________ =
(1*2*3*4*5*6*7*8*9*10*11)
*/
