#include <bits/stdc++.h>
#define MAXN 2000001
#define SQRTN 320
#define INF 0x3f3f3f3f
 
using namespace std;
 
 
string T,P;
int f[MAXN];
 
 
void buildAutomaton(){
    int i = 0,j = -1;
    f[0] = j;
    while(i < (int) P.size()){
        while(j >= 0 && P[i] != P[j]){
            j = f[j];
        }
        i++;j++;
        f[i] = j;
    }
}
 
void kmp(){
    int i = 0, j = -1;
    int ans = 0;
    while(i < (int) T.size()){
        while(j >= 0 && P[j] != T[i]){
            j = f[j];
        }
        i++; j++;
        if(j == (int) P.size()){
            cout << i - (P.size()) << endl;
        }
    }
}
 
int n;
 
int main(){
    while(cin >> n){
        cin >> P >> T;
        if((int)P.size() > (int)T.size()) continue;
        buildAutomaton();
        kmp();
        cout << endl;
    }
    return 0;
}
