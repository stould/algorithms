#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
#define MAXN 30000
#define MAXV 1000000000
#define MAXQ 200000

using namespace std;
 
int n, v[MAXN], q, ql, qr, k;

int main(){
	freopen("in.in", "w", stdout);
	srand(time(NULL));
	n = MAXN;
	cout << n << endl;
	for(int i = 0; i < n; i++){
		cout << 1 + rand() % MAXV << " ";
	}
	cout << endl;
	q = MAXQ;
	cout << q << endl;
	for(int i = 0; i < q; i++){
		ql = 1 + rand() % n;
		qr = 1 + rand() % n;
		k = 1 + rand() % MAXV;
		if(ql > qr) swap(ql, qr);
		cout << ql << " " << qr << " "<< k << endl;
	}
    return 0;
}
