#include <bits/stdc++.h>
#define ll long long
#define MAXN 100007
#define LOGMAXVAL 19
#define INF 10000000

using namespace std;

vector<vector<int> > g(MAXN);

//Persistent Segment Tree
int root[MAXN*LOGMAXVAL]; //The root of the new node
int INDEX;
int Lef[MAXN*4*LOGMAXVAL];
int Rig[MAXN*4*LOGMAXVAL];
int S[MAXN*4*LOGMAXVAL];


//For LCA:
int H[MAXN], L[MAXN << 1], E[MAXN << 1], idx, dp[MAXN*2][LOGMAXVAL], pre[MAXN*2], vis[MAXN], path[MAXN];

struct LCA{
    int n;
    LCA(int n_){
        n = n_;
        build();
    }
 
    void build(){
        int base = 1;
        int pot = 0;
        for(int i = 0; i < 2*n; i++){
            if(i >= base * 2){
                pot++;
                base *= 2;
            }
            pre[i] = pot;
            dp[i][0] = i;
        }
        base = 2;
        pot = 1;
        while(base <= 2*n){
            for(int i = 0; i + base / 2 < 2*n; i++){
                int before = base / 2;
                if(L[dp[i][pot-1]] < L[dp[i + before][pot-1]]){
                    dp[i][pot] = dp[i][pot-1];
                }else{
                    dp[i][pot] = dp[i + before][pot-1];
                }
            }
            base *= 2;
            pot++;
        }
    }
 
    int getLca(int u, int v){
        int l = H[u];
        int r = H[v];
        if(l > r){
            swap(l,r);
        }
        int len = r-l+1;
        if(len == 1){
            return E[dp[r][0]];
        }else{
            int base = (1 << pre[len]);
            int pot = pre[len];
            if(L[dp[l][pot]] < L[dp[r-base+1][pot]]){
                return E[dp[l][pot]];
            }else{
                return E[dp[r-base+1][pot]];
            }
        }
    }
 
};

struct PersistentSegTree{

    PersistentSegTree(){
        INDEX = 1;
        build(0, 0, MAXN);
    }
	
    //build the initial and empty tree
    void build(int node, int l, int r){
        if(l == r){
            return;
        }else{
            int mid = (l+r) / 2;
            Lef[node] = INDEX++;
            Rig[node] = INDEX++;
            build(Lef[node], l, mid);
            build(Rig[node], mid+1, r);
        }
    }
 
    /*query to count how many elements are > K
      here is the key of the problem.*/
    int query(int node, int l, int r, int C){
        if(l == r){
            return S[node];
        }else{
            int mid = (l+r) / 2;
            if(C <= mid){
                return query(Lef[node], l, mid, C);
            }else{
                return query(Rig[node], mid+1, r, C);
            }
        }
    }
 
    /*add a new node, we just need to copy log(n) nodes 
      from the previus tree add add the new one*/
    int update(int node, int l, int r, int pos){
        int next = INDEX++;
        Lef[next] = Lef[node];
        Rig[next] = Rig[node];
        S[next] = S[node];
        if(l == r){
            S[next] += 1;
        }else{
            int mid = (l+r) / 2;
            if(pos <= mid){
                Lef[next] = update(Lef[node], l, mid, pos);
            }else{
                Rig[next] = update(Rig[node], mid+1, r, pos);
            }
            S[next] = S[Lef[next]] + S[Rig[next]];
        }
        return next;
    }
};

int n, m, U, V, CQ, C[MAXN];

void dfs(int x, int depth, PersistentSegTree &tree){
    vis[x] = 1;//visited
    if(H[x] == -1) H[x] = idx;//mark first time the i'th node is visited
    L[idx] = depth;//when you visit a node you should mark the the depth you have found it.
    E[idx++] = x;//the i'th recursion, global variable
    for(int i = 0; i < (int) g[x].size(); i++){
        int next = g[x][i];
        int cost = C[next];
        if(!vis[next]){
            path[next] = x;
            root[next] = tree.update(root[x], 0, MAXN-1, cost);
            dfs(next, depth+1, tree);
            L[idx] = depth;
            E[idx++] = x;
        }
    }
}

void add(int a, int b){
    g[a].push_back(b);
    g[b].push_back(a);
}

int main(void){
    while(scanf("%d%d", &n, &m) == 2){
        idx = 0;
        for(int i = 0; i <= n; i++){
            g[i].clear();
            H[i] = -1;
            L[i] = E[i] = vis[i] = 0;
            path[i] = -1;
        }
        for(int i = 1; i <= n; i++){
            scanf("%d", &C[i]);
        }
        for(int i = 0; i < n - 1; i++){
            scanf("%d%d", &U, &V);
            add(U,V);
        }

        PersistentSegTree tree;
        root[1] = tree.update(root[0], 0, MAXN-1, C[1]);
        dfs(1,0, tree);
        LCA lca(n);
        for(int i = 0; i < m; i++){
            scanf("%d%d%d", &U, &V, &CQ);
            int toU = tree.query(root[U], 0, MAXN-1, CQ);
            int toV = tree.query(root[V], 0, MAXN-1, CQ);
            int nodeLCA = lca.getLca(U,V);
            int toLCA = tree.query(root[nodeLCA], 0, MAXN-1, CQ);
            int result = toU + toV - 2*toLCA;
            if(C[nodeLCA] == CQ){
                result++;
            }
            if(result > 0){
                puts("Find");
            }else{
                puts("NotFind");
            }
        }
        puts("");
    }
    return 0;
}
