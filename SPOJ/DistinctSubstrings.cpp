#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 1007
#define ull unsigned long long
#define INF 0xffffff
#define PI acos(-1)
 
using namespace std;
 
ull C[MAXN], pot[MAXN];
ull B1 = 33, B2 = 79;
ull hash[MAXN];
char str[1001];
int n,m;
 
//pega o hash da janela
ull getHash(int i, int j){
        return hash[j+1] - (hash[i] * pot[j-i+1]) + C[j-i+1];
}
 
void printSubstring(int st){
    for(int i = st; i < m; i++){
        cout << str[i];
    }
}
 
bool comp(int a, int b){
	int size = min(m-a, m-b), lo = 0, hi = size;
	while(lo < hi){
		int T = (lo+hi+1) / 2;
		if(getHash(a,a+T-1) == getHash(b,b+T-1)){
			lo = T;
		}else{
			hi = T-1;
		}
	}
	//	if(size == lo) return 1;
	if(str[a+lo] < str[b+lo]){
		return 1;
	}else{
		return 0;
	}
}
 
int lcp(int a, int b){
	//printSubstring(a);cout << " "; printSubstring(b);
        int lo = 0, hi = min(m-a, m-b);
        while(lo < hi){
                int T = (lo+hi+1) / 2;
                if(getHash(a,a+T-1) == getHash(b,b+T-1)){
                        lo = T;
                }else{
                        hi = T-1;
                }
        }
		return lo;
}
 
int t, pos[MAXN];
 
int main(){
	scanf("%d", &t);
	pot[0] = 1;
	C[0] = 1;
	for(int i = 0; i < MAXN; i++){
        pot[i+1] = pot[i] * B1;
        C[i+1] = C[i] * B2;
    }
	while(t--){
		scanf(" %s", str);
		m = strlen(str);
		for(int i = 0; i < m; i++){
			hash[i+1] = (int)str[i] + hash[i] * B1;
		}
		vector<int> sa;
		for(int i = 0; i < m; i++) sa.push_back(i);
		sort(sa.begin(), sa.end(), comp);
		int ans = 0;//ABABA
		
		for(int i = 1; i < m; i++){
			ans += lcp(sa[i-1], sa[i]);
		}
		printf("%d\n", m*(m+1)/2 - ans);
	}
	return 0;
}
