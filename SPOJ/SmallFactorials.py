t = int(input())
 
def fatorial (N):
	if N <= 0:
		return 1
	else:
		ans = 1
		for i in range(1, N + 1):
			ans = ans * i
		return ans
for i in range(t):
	value = int(input())
	print(fatorial(value)) 
