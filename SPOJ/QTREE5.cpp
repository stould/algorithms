#include <bits/stdc++.h>
#define ll long long
#define MAXN 100010
#define LOGN 20
#define INF 0x3f3f3f3f
 
using namespace std;
 
vector<set<int> > graph(MAXN);
 
int lca[MAXN][LOGN], level[MAXN], cnt[MAXN], pai[MAXN], status[MAXN], subTreeSize;
 
 
//preprocess LCA and set the height to each node
void dfs(int node, int parent){
    lca[node][0] = parent;
    for(int i = 1; i < LOGN; i++){
        lca[node][i] = lca[lca[node][i-1]][i-1];
    }
    for(auto next : graph[node]){
        if(next != parent){
            level[next] = level[node] + 1;
            dfs(next, node);
        }
    }
}
 
 
//query to getLCA of a and b.
int getLca(int a, int b){
    if(level[a] < level[b]){
        swap(a, b);
    }
    int diff = level[a] - level[b];
    for(int i = LOGN-1; i >= 0; i--){//put a in the same level as b (root as reference)
        if ((1 << i) & diff) {
            a = lca[a][i];
        }
    }
    if(a == b){//the case that A is straight ancestor of B
        return a;
    }else{
        for(int i = LOGN-1; i >= 0; i--){//going up with both a and b together
            if(lca[a][i] != lca[b][i]){
                a = lca[a][i];
                b = lca[b][i];
            }
        }
        return lca[a][0];//the asnwer will be in the parent of a (or b).
    }
}
 
int getDist(int a, int b){
    return level[a] + level[b] - 2 * level[getLca(a,b)];
}
 
//counting how many nodes are in the subtree of 'node'
void dfsCnt(int node, int parent){
    cnt[node] = 1;
    subTreeSize++;
    for(auto next : graph[node]){
        if(next != parent){
            dfsCnt(next, node);
            cnt[node] += cnt[next];
        }
    }
}
 
/*
  'let N = total amount of nodes in a splited subtree'
 
  A node will be a centroid of a subtree if for each child, 
  the subtree size is less than or equal to N / 2
*/
int getCentroid(int node, int parent){
    for(auto next : graph[node]){
        if(next != parent && cnt[next] * 2 >= subTreeSize){
            return getCentroid(next, node);
        }
    }
    return node;
}
 
 
//Generating the new tree, get a centroid, then, split the tree and go on..
void centroid(int root, int lastCen){
    subTreeSize = 0;
    dfsCnt(root, root);
    int centroidAt = getCentroid(root, root);
    if(root == lastCen){
        pai[centroidAt] = centroidAt;
    }else{
        pai[centroidAt] = lastCen;
    }
    for(auto next : graph[centroidAt]){
        graph[next].erase(graph[next].find(centroidAt));
        centroid(next, centroidAt);
    }
    graph[centroidAt].clear();
}
 
vector<multiset<int> > minDist(MAXN);
 
 
/*
  updating the centroid tree
  We just need to update all nodes going up from the 
  query node to the root of the centroid tree
*/
void update(int node){
    status[node] ^= 1;//black to white and vice-versa
    int u = node;
    while(1){
        int distance = getDist(node, u);
        if(status[node] == 1){
            minDist[u].insert(distance);
        }else{
            minDist[u].erase(minDist[u].find(distance));
        }
        if(u == pai[u]) break;
        u = pai[u];
    }
}
 
/*
  querying the centroid tree
  we just need to check all nodes going up from the 
  query node to the root of the centroid tree
*/
int query(int node){
    int u = node;
    int ans = INF;
    while(1){
        if(minDist[u].size() > 0){
            int near = *minDist[u].begin();
            int distance = getDist(node, u);
            //cout << "At node << " << u << " dist = " << near << " + " << distance << endl;
            ans = min(ans, near + distance);
        }
        if(u == pai[u]) break;
        u = pai[u];
    }
    return ans >= INF ? -1 : ans;
}
 
int N, U, V, Q, OP;
 
int main(void){
    scanf("%d", &N);
    for(int i = 0; i < N - 1; i++){
        scanf("%d%d", &U, &V);
        graph[U].insert(V);
        graph[V].insert(U);
    }
    dfs(1, 1);
    centroid(1, 1);
    scanf("%d",&Q);
    while(Q--){
        scanf("%d%d", &OP, &U);
        if(OP == 0){
            update(U);
        }else{
            printf("%d\n", query(U));
            //cout << endl;
        }
    }
    
    return 0;
}
