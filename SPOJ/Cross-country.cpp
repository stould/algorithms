#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <map>
#include <math.h>
#include <string.h>
#define INF 99999999
#define MAXN 2009
using namespace std;
 
int d, agnes[MAXN], other[MAXN];
 
int dp[MAXN][MAXN];
 
int lcs(int x, int y, int n, int m){
    if(x >= n || y >= m) return 0;
    if(dp[x][y] == 0){
        int ans;
        if (agnes[x] == other[y]) {
            ans = 1 + lcs(x+1, y+1, n, m);
        } else {
            ans = max(lcs(x+1,y,n,m), lcs(x, y+1,n,m));
        }
        dp[x][y] = ans;
    }
    return dp[x][y];
}
 
int tmp;
 
int main(void){
    scanf("%d", &d);
    for(; d-- ;){
        int na = 0, nb;
        //printf("Agnes:\n");
        while(scanf("%d", &tmp)){
            agnes[na++] = tmp;
            //printf("%d ", agnes[na-1]);
            if(tmp == 0) break;
        }
        //printf("\n");
        int max_ = 0;
        while(1){
            //printf("Outro:\n");
            nb = 0;
            while(scanf("%d", &tmp) && tmp > 0){
                other[nb++] = tmp;
                //printf("%d ", other[nb-1]);
            }
            if(nb){
                other[nb] = 0;
                //printf("%d", other[nb-1]);
                memset(dp, 0, sizeof(dp));
                max_ = max(max_, lcs(0,0,na,nb));
            }else{
                break;
            }
            //printf("\n");
        }
        printf("%d\n", max_);
    }
    return 0;
}
