#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 107
#define INF 0x3f3f3f3f3f3fLL
#define PI acos(-1)
#define ll long long
 
using namespace std;
 
ll w[MAXN], dp[MAXN][MAXN];
 
ll sum(int a, int b){
  int ans = 0;
  for(int i = a; i <= b; i++){
    ans = (w[i] + ans) % 100;
  }
  return ans;
}
 
ll func(int l, int r){
  if(l == r){
    return 0;
  }else{
    ll &ans = dp[l][r];
    if(ans == -1){
      ans = INF;
      for(int i = l; i <= r-1; i++){
	ans = min(ans, func(l,i) + sum(l,i)*sum(i+1,r) + func(i+1,r));
      }
      return ans;
    }
    return ans;
  }
}
 
int n;
 
int main(void){
  while(cin >> n){
    for(int i = 0; i < n; i++){
      cin >> w[i];
    }
    memset(dp,-1,sizeof(dp));
    cout << func(0,n-1) << endl; 
  }
  return 0;
}
