#include <bits/stdc++.h>
#define ll long long
#define MAXN 52
#define MOD 1000000007LL
using namespace std;

string num;
const int limiar = 17;
 
void subtract(string &nam){
    string ans = "";
    for(int i = (int)nam.size()-1; i >= 0; i--){
        if(nam[i] - '0' > 0){
            nam[i]--;
            break;
        }else{
            nam[i] = '9';
        }
    }
    while(nam.size() > 0 && nam[0] == '0'){
        nam = nam.substr(1);
    }
    if(nam.size() == 0) nam = "0";
}
 
ll pd[MAXN][limiar+1][limiar+1][limiar+1];
 
 
ll func(int id, int THREE, int SIX, int NINE, bool pref){
    if(id == 0){
        return (THREE > 0) && (THREE == SIX && SIX == NINE);
    }else if(THREE > limiar || SIX > limiar || NINE > limiar){
        return 0;
    }else{
        if(!pref && pd[id][THREE][SIX][NINE] != -1){
            return pd[id][THREE][SIX][NINE];
        }
        ll ans = 0;
        int limit = pref ? num[id] - '0' : 9;
        for(int i = 0; i <= limit; i++){
            ans = (ans + func(id-1, THREE + (i == 3), SIX + (i == 6), NINE + (i == 9), pref && (i == limit))) % MOD;
        }
        if(!pref) pd[id][THREE][SIX][NINE] = ans;
        return ans;
    }
}
 
int T;
string A,B;
 
int main(){
    ios::sync_with_stdio(0);
    cin.tie(0);
    cin >> T;
    memset(pd, -1, sizeof(pd));
    while(T--){
        cin >> A >> B;
        num = B;
        reverse(num.begin(), num.end());
        num = " " + num;
        ll cntB = func(num.size() - 1,0,0,0,1);
        subtract(A);
        num = A;
        reverse(num.begin(), num.end());
        num = " " + num;
        ll cntA = func(num.size() - 1,0,0,0,1);
        //        cout << cntB << " - " << cntA << endl;
        ll total = (cntB - cntA + MOD + MOD) % MOD;
        cout << total << endl;
    }
    return 0;
}
