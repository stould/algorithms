#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#define MAXN 35
#define INF 0x3f3f3f3f
#define ll long long

using namespace std;

int n,a,b, V[MAXN];

int lower(vector<int> &val, int key){
	int lo = 0, hi = val.size()-1, ans = INF;
	while(lo <= hi){
		int mid = (lo+hi) >> 1;
		if(val[mid] + key >= a){
			ans = min(ans, mid);
			hi = mid - 1;
		}else{
			lo = mid + 1;
		}
	}
	return ans;
}

int upper(int lo, vector<int> &val, int key){
	int hi = val.size()-1, ans = -1;
	while(lo <= hi){
		int mid = (lo+hi) >> 1;
		if(val[mid] + key > b){
			hi = mid - 1;
		}else{
			ans = max(ans, mid);
			lo = mid + 1;
		}
	}
	return ans;
}

int naive(){
	int ans = 0;
	for(int i = 0; i < (1 << n); i++){
		int v = 0;
		for(int j = 0; j < n; j++){
			if( (i & (1 << j)) ){
				v += V[j];
			}
		}
		if(v >= a && v <= b){
			ans++;
		}
	}
	return ans;
}

int main(void){
	scanf("%d%d%d", &n, &a,&b);
	int i,j,v;
	for(i = 0; i < n; i++){
		scanf("%d", &V[i]);
	}
	int left = n >> 1, right = n-left;
	ll ans = 0;
	vector<int> sl, sr;
	for(i = 1; i < (1 << left); i++){
		v = 0;
		for(j = 0; j < left; j++){
			if( (i & (1 << j)) ){
				v += V[j];
			}
		}
		sl.push_back(v);
	}
	for(i = 0; i < (1 << right); i++){
		v = 0;
		for(j = 0; j < right; j++){
			if( (i & (1 << j)) ){
				v += V[j+left];
			}
		}
		sr.push_back(v);
		if(v >= a && v <= b){
			ans++;
		}
	}
	sort(sr.begin(), sr.end());
	for(i = 0; i < sl.size(); i++){
		int lb = lower(sr, sl[i]);
		if(lb == INF){
			continue;
		}
		int ub = upper(lb, sr, sl[i]);
		ans += (ll)ub - (ll)lb + 1LL;
	}
	printf("%lld\n", ans);
    return 0;
}

