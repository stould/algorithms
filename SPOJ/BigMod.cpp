#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 50007
#define ll long long
#define INF 0xffffff
#define PI acos(-1)

using namespace std;

ll power(ll base, ll exp, ll mod){
	if(exp == 0){
		return 1;
	}else{
		if(exp % 2 == 1){
			return ((base % mod) * power(base,exp-1, mod)) % mod;
		}else{
			return power((base*base) % mod, exp/2, mod);
		}
	}
}

ll B,P,M;


int main(){
	while(cin >> B >> P >> M){
		cout << power(B,P,M) << endl;
	}
	return 0;
}
