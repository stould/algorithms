#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 50007
#define ull unsigned long long
#define INF 0xffffff
#define PI acos(-1)

using namespace std;

int t,k;
string str;

int dual(int p, int moves){
	int l = p-1;
	int r = p;
	if(l >= 0){
		int ans = 1;
			if(str[l] == str[r]){
				ans++;
			}else{
				if(moves == 0){
					return ans;
				}else{
					ans++;
					moves--;
				}
			}
		l--;
		r++;
		while(l >= 0 && r < str.size()){
			if(str[l] == str[r]){
				
				ans += 2;
			}else{
				if(moves == 0){
					return ans;
				}else{
					ans += 2;
					moves--;
				}
			}
			l--;
			r++;
		}
		return ans;
	}else{
		return 1;
	}
}

int single(int p, int moves){
	int l = p-1;
	int r = p+1;
	int ans = 1;
	while(l >= 0 && r < str.size()){
		if(str[l] == str[r]){
			ans += 2;
		}else{
			if(moves == 0){
				return ans;
			}else{
				ans += 2;
				moves--;
			}
		}
		l--;
		r++;
	}
	return ans;

}

int main(){
	cin >> t;
	while(t--){
		cin >> str >> k;
		int ans = 0;
		for(int i = 0; i < str.size(); i++){
			ans = max(ans, max(single(i,k), dual(i,k)));
		}
		cout << ans << endl;
	}
	return 0;
}
