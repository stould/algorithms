#include <bits/stdc++.h>
#define ll long long
#define MAXN 100007
#define INF 10000000

using namespace std;

vector<vector<int> > g(MAXN);
int cnt[MAXN], previous[MAXN], chainNode[MAXN], chainHead[MAXN], posInChain[MAXN], base[MAXN], level[MAXN], chainIdx, idxSegTree;

struct Node{
    int status, minor;
    Node(){
        status = 0;
        minor = INF;
    }
};

Node tree[MAXN*4];

struct SegTree{
 
    SegTree(){
        for(int i = 0; i < MAXN*4; i++){
            tree[i].status = 0;
            tree[i].minor = INF;
        }
    }
    
    int rmq(int node, int l, int r, int ra, int rb){
        if(l > rb || r < ra){
            return INF;
        }else if(l >= ra && r <= rb){
            return tree[node].minor;
        }else{
            int mid = (l + r) / 2;
            int q1 = rmq(node * 2, l, mid, ra, rb);
            int q2 = rmq(node * 2 + 1, mid + 1, r, ra, rb);
            return min(q1, q2);
        }
    }
       
    void update(int node, int l, int r, int pos, int value) {
        if (l > r) return;     
        if (l == r) {
            tree[node].status ^= value;
            if(tree[node].status == 1){
                tree[node].minor = l;
            }else{
                tree[node].minor = INF;
            }
        } else {
            int m = (l + r) >> 1;
            if (pos <= m) {
                update(2 * node, l, m, pos, value);
            } else {
                update(2 * node + 1, m + 1, r, pos, value);
            }
            int lef = tree[node * 2].status;
            int rig = tree[node * 2 + 1].status;
            if(lef){
                tree[node].status = 1;
                tree[node].minor = tree[node * 2].minor;
            }else if(rig){
                tree[node].status = 1;
                tree[node].minor = tree[node * 2 + 1].minor;
            }else{
                tree[node].status = 0;
                tree[node].minor = INF;
            }
        }
    }
};
 
//Decompose the tree into chains
void HLD(int node, int parent = -1){
    if(chainHead[chainIdx] == -1){
        chainHead[chainIdx] = node;
    }
    chainNode[node] = chainIdx;
    posInChain[node] = idxSegTree;
    base[idxSegTree++] = node;
    //cout << "pos of [" << node << "] = " << posInChain[node] << endl;
    int nodeHeavy = -1;
    //seeking the special child (the one with most childs on the subtrees)
    for(int i = 0; i < g[node].size(); i++){
        int next = g[node][i];
        if(next != parent && (nodeHeavy == -1 || cnt[next] > cnt[nodeHeavy])){
            nodeHeavy = next;
        }
    }
    if(nodeHeavy > -1){
        //expanding the current chain
        //cout << "Expanding current chain from " << node << " to " << nodeHeavy << endl;
        HLD(nodeHeavy, node);
    }
       
    for(int i = 0; i < g[node].size(); i++){
        int next = g[node][i];
        if(next != nodeHeavy && next != parent){
            chainIdx++;
            //     cout << "new Chain at " << next << endl;
            HLD(next, node);
        }
    }
 
}

void dfsCnt(int node, int parent, int depth = 0){
    level[node] = depth;
    cnt[node] = 1;
    for(int i = 0; i < (int) g[node].size(); i++){
        int next = g[node][i];
        if(next != parent){
            previous[next] = node;
            dfsCnt(next, node, depth + 1);
            cnt[node] += cnt[next];
        }
    }      
}
 
int walkChain(int U, int V, SegTree &q, int n){
    int ans = INF;
    while(chainNode[U] != chainNode[V]){
        int Left = posInChain[chainHead[chainNode[U]]];
        int Right = posInChain[U];
        //cout << Left << " " << Right << endl;
        int val = q.rmq(1, 0, n-1, Left, Right);
        ans = min(val, ans);
        U = previous[chainHead[chainNode[U]]];
    }

    int val = q.rmq(1, 0, n-1, 0, posInChain[U]);
    ans = min(val, ans);
    return ans;
}

void add(int a, int b){
    g[a].push_back(b);
    g[b].push_back(a);
}
 
int n, m, U, V;
 
int main(void){
    for(int i = 0; i < MAXN*4; i++){
        tree[i] = Node();
    }
    while(scanf("%d%d", &n, &m) == 2){
        chainIdx = idxSegTree = 0;
        for(int i = 0; i <= n; i++){
            cnt[i] = previous[i] = chainNode[i] = base[i] = level[i] = 0;
            chainHead[i] = posInChain[i] = -1;
            g[i].clear();
        }

        for(int i = 0; i < n - 1; i++){
            scanf("%d%d", &U, &V);
            U--, V--;
            add(U,V);
        }

        dfsCnt(0,-1);
        HLD(0);
        SegTree query;
        int type, V;
        for(int i = 0; i < m; i++){
            scanf("%d%d", &type, &V);
            V--;
            //cout << "Query: " << type << " " << V << endl;
            if(type == 0){
                query.update(1, 0, n-1, posInChain[V], 1);
            }else{
                int ans = walkChain(V, 0, query, n);
                //cout << " ans = " << ans << endl;
                if(ans == INF){
                    printf("-1\n");
                }else{
                    printf("%d\n", base[ans] + 1);
                }
            }
        }
    }
    return 0;
}
