#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100002
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

char str[1001];

int main(void){
	int v[26];
	memset(v,0,sizeof(v));
	cin >> str;
	int n = strlen(str);
	for(int i = 0; i < n; i++){
		v[str[i] - 'A']++;
	}
	printf("%d %d %d %d\n", v[0], v['C' - 'A'], v['G' - 'A'], v['T' - 'A']);
	return 0;
}
