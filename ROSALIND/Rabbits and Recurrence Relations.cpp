#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 100002
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

ll n, k, dp[41];

ll func(int v){
	if(v < 0) return 0;
	if(v == 0){
		return 1;
	}else if(v == 1){
		return 1;
	}else{
		if(dp[v] != -1) return dp[v];
		dp[v] = func(v-1) + k * func(v-2);
		return dp[v];
	}
}

int main(void){
	cin >> n >> k;
	memset(dp,-1,sizeof(dp));
	cout << func(n-1) << endl;
	return 0;

}
