#include <bits/stdc++.h>
#define LOGN 10
using namespace std;
 
int dp[LOGN][2];
 
string num;
 
int func(int id, bool prefix){
	if(id == num.size()){
		return 1;
	}else{
		int &ans = dp[id][prefix];
		if(ans == -1){
			ans = 0;
			for(int i = 0; i <= 9; i++){
				if(i > num[id] - '0' && prefix){
					continue;
				}else if(i == 3){
					continue;
				}
				ans += func(id+1, prefix && (num[id] - '0' == i));
			}
		}
		return ans;
	}
}
 
int T,N;
 
int main(){
	cin >> T;
	while(T--){
		cin >> num;
		memset(dp,-1,sizeof(dp));
		cout << func(0, 1) - 1 << endl;
	}
    return 0;
}
