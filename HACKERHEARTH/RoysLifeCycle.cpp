#include <bits/stdc++.h>
 
using namespace std;
 
int n;
 
char val;
 
int main(){
    scanf("%d", &n);
    int X = 0, Y = 0, streak = 0;
    char last = 0;
    for(int i = 0; i < n; i++){
    	int sum = 0;
    	for(int j = 0; j < 1440; j++){
    		scanf(" %c", &val);
    		if(val == 'C'){
    			sum++;
    			X = max(sum, X);
    			Y = max(Y, streak + sum);
    		}else{
    			sum = 0;
    			streak = 0;
    		}
    		last = val;
    	}
    	if(last == 'C'){
    		streak += sum;
    	}else{
    		streak = 0;
    	}
    }
    printf("%d %d\n", X, max(Y, X));
    return 0;
}
