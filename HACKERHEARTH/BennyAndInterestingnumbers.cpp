#include <bits/stdc++.h>
#define ll long long
#define INF 0x3f3f3f3f
#define SQMAXN 330001

using namespace std;

vector<ll> primes;
bool isPrime[SQMAXN];

void crivo(){
    for(int i = 0; i < SQMAXN; i++){
        isPrime[i] = 1;
    }
    isPrime[0] = isPrime[1] = 0;
    for(int i = 2; i < SQMAXN; i++){
        if(isPrime[i]){
            primes.push_back(i);
            for(int j = (ll) i * 2; j < SQMAXN; j += i){
                isPrime[j] = 0;
            }
        }
    }
}

bool decompose(ll N){
    int idx = 0;
    while(idx < (int) primes.size() && N > 1){
        if(N % primes[idx] == 0){
            N /= primes[idx];
        }
		if(N % primes[idx] == 0){
            return 0;
        }
        idx++;
    }
    return 1;
}

int T;
ll N;
 
int main(void){
    crivo();    
    scanf("%d", &T);
    while(T--){
        scanf("%lld", &N);
        cout << (decompose(N) ? "Yes" : "No") << endl;
    }
    return 0;
}
