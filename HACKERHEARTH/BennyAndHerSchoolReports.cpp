#include <bits/stdc++.h>
#define ll long long
#define INF 0x3f3f3f3f
#define MAXN 100007
using namespace std;


int T, N, M, X, v[MAXN];

int main(void){
    scanf("%d", &T);
    while(T--){
        scanf("%d%d%d", &N, &M, &X);
        ll sum = 0;
        for(int i = 0; i < N; i++){
            scanf("%d", &v[i]);
            sum += (ll) v[i];
        }
        int lo = 1, hi = M, ans = INF;
        while(lo <= hi){
            int mid = lo + (hi - lo) / 2;
            if(double(sum + mid) / double(N+1) >= (double) X){
                ans = min(ans, mid);
                hi = mid - 1;
            }else{
                lo = mid + 1;
            }
        }
        if(ans == INF){
            puts("Impossible");
        }else{
            printf("%d\n",  ans);
        }
    }
    return 0;
}
