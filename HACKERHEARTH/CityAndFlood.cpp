#include <bits/stdc++.h>
#define MAXN 100001

using namespace std;

int n,k, I, J;
int h[MAXN], id[MAXN];

void clear(){
	for(int i = 0; i < n; i++){
		h[i] = 1;
		id[i] = i;
	}
}

int root(int u){
	if(id[u] == u){
		return u;
	}
	return id[u] = root(id[u]);
}

bool find(int u, int v){
	return root(u) == root(v);
}

void merge(int u, int v){
	int ru = root(u);
	int rv = root(v);
	if(h[ru] > h[rv]){
		h[ru] += h[rv];
		id[rv] = id[ru];
	}else{
		h[rv] += h[ru];
		id[ru] = id[rv];
	}
}

int main(){
    cin >> n >> k;
    int ans = n;
    clear();
    for(int i = 0; i < k; i++){
    	cin >> I >> J;
    	if(!find(I,J)){
    		ans--;
    	}
    }
    cout << ans << endl;
    return 0;
}
