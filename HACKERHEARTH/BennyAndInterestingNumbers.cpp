#include <bits/stdc++.h>
#define ll long long
#define INF 0x3f3f3f3f
#define SQMAXN 330001

using namespace std;

vector<ll> primes;
bool isPrime[SQMAXN];

void crivo(){
    for(int i = 0; i < SQMAXN; i++){
        isPrime[i] = 1;
    }
    isPrime[0] = isPrime[1] = 0;
    for(int i = 2; i < SQMAXN; i++){
        if(isPrime[i]){
            primes.push_back(i);
            for(int j = (ll) i * 2; j < SQMAXN; j += i){
                isPrime[j] = 0;
            }
        }
    }
}

vector<pair<ll, int> > decomp;
set<ll> divisors;

void decompose(ll N){
    int idx = 0;
    while(idx < (int) primes.size() && N > 1){
        if(N % primes[idx] == 0){
            decomp.push_back(make_pair(primes[idx], 0));
        }
        while(N % primes[idx] == 0){
            decomp[decomp.size() - 1].second++;
            N /= primes[idx];
        }
        idx++;
    }
    if(N > 1){
        decomp.push_back(make_pair(N, 1));
    }
}


void gen(int id, ll sum, ll N){
    if(id > (int) decomp.size()){
        return;
    }
    if(sum < N){
        divisors.insert(sum);
    }
    for(int i = 0; i <= decomp[id].second; i++){
        gen(id+1, sum, N);
        sum *= decomp[id].first;
    }
}

set<ll> retBases(ll N){
    decomp.clear();
    decompose(N);
    divisors.clear();
    gen(0,1,N);
    set<ll> ans(divisors);
    return ans;
}

bool isP(ll n){
    if(n <= 1){
        return 0;
    }else if(n == 2){
        return 1;
    }else if(n % 2 == 0){
        return 0;
    }
    for(ll i = 3; i <= (int)sqrt(n) + 1; i += 2){
        if(n % i == 0) return 0;
    }
    return 1;
}

int X = 0;

map<ll, bool> dp;

bool backtrack(ll N){

    if(N == 1 || isP(N)){
        return 1;
    }else{

        if(dp.find(N) != dp.end()){
           return dp[N];
         }
        /*
           cout << N << ", divisors: " << endl;
           for(set<ll>::iterator it = divisors.begin(); it != divisors.end(); it++){
           cout << (*it) << endl;
           }
        */

        set<ll> divs = retBases(N);
        int sum = 0;
        for(set<ll>::iterator it = divs.begin(); it != divs.end(); it++){
            sum += backtrack(*it);
        }
        bool ans;
        if(sum % 2 == 0){
            ans = 0;
        }else{
            ans = 1;
        }
        dp[N] = ans;
        return ans;
    }
}

int T;
ll N;
 
int main(void){
    crivo();
    for(int i = 1; i <= 100; i++){
        
    }
    return 0;
    scanf("%d", &T);
    while(T--){
        scanf("%lld", &N);
        dp.clear();
        cout << ((backtrack(N) == 1) ? "Yes": "No") << endl;
    }
    return 0;
}
