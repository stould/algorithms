#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <math.h>
#include <string.h>
#include <stack>
#include <map>
#include <set>
#define ll long long
#define INF 0x3f3f3f3f
#define MAXN 1007
using namespace std;

int val[MAXN];
int n;
int main(void){
	scanf("%d", &n);
	vector<int> z,v;
	int zp = 0, vp = 0;
	for(int i = 0; i < n; i++){
		scanf("%d", &val[i]);
		if(val[i] % 2 == 0){
			z.push_back(val[i]);
			zp += val[i];
		}else{
			v.push_back(val[i]);
			vp += val[i];
		}
	}
	sort(z.begin(), z.end());
	sort(v.begin(), v.end());

	vector<int> ans;
	for(int i = 0; i < z.size(); i++){
		ans.push_back(z[i]);
	}
	ans.push_back(zp);
	for(int i = 0; i < v.size(); i++){
		ans.push_back(v[i]);
	}
	ans.push_back(vp);
	printf("%d", ans[0]);
	for(int i = 1; i < ans.size(); i++){
		printf(" %d", ans[i]);
	}
	printf("\n");
	return 0;
}
