#include <bits/stdc++.h>
#define MAXN 1001
#define ll long long
#define MOD 1000000007
using namespace std;

int n,k, I, J;
int h[MAXN], id[MAXN];
ll sum[MAXN];

void clear(){
	for(int i = 0; i < n; i++){
		h[i] = 1;
		id[i] = i;
	}
}

int root(int u){
	if(id[u] == u){
		return u;
	}
	return id[u] = root(id[u]);
}

bool find(int u, int v){
	return root(u) == root(v);
}

void merge(int u, int v){
	int ru = root(u);
	int rv = root(v);
	if(h[ru] > h[rv]){
		h[ru] += h[rv];
		id[rv] = id[ru];
	}else{
		h[rv] += h[ru];
		id[ru] = id[rv];
	}
}

bool vis[MAXN];
bool v[MAXN];

void groups(){
	
	for(int i = 0; i < n; i++){
		int ri = root(i);
		cout << "With " << i << ": ";
		for(int j = 0; j < n; j++){
			if(root(j) == ri & !v[j]){
				cout << j << " ";
				v[j] = 1;
			}
		}
		cout << endl;
	}
}

int main(){
    cin >> n;
    for(int i = 0; i < n; i++){
    	cin >> sum[i];
    }
    cin >> k;
    clear();
    for(int i = 0; i < k; i++){
    	cin >> I >> J;
    	I--;
    	J--;
    	if(!find(I,J)){
    		merge(I,J);
    	}
    }
    //groups();
    ll ans = 1LL;
    vector<vector<int> > s(n);
    for(int i = 0; i < n; i++){
    	s[root(i)].push_back(sum[i]);
    }
    for(int i = 0; i < s.size(); i++){
    	if(s[i].size() == 0) continue;
    	sort(s[i].begin(), s[i].end());
    	int j = 1;
    	while(j < s[i].size() && s[i][j] == s[i][j-1]){
    		j++;
    	}
    	ans = (ans * j) % MOD;
    }
    cout << ans << endl;
    return 0;
}
