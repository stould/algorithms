#include <bits/stdc++.h>
#define ll long long
#define INF 0x3f3f3f3f
#define MAXN 1000007
using namespace std;

string parse(string &tmp){
    int idx = 0;
    string value = "";
    bool can = 0;
    bool leading = 1;
    while(idx < (int) tmp.size()){
        if(can){
            if(tmp[idx] >= '0' && tmp[idx] <= '9'){
                if(tmp[idx] == '0'){
                    if(!leading){
                        value += tmp[idx];
                    }
                }else{
                    value += tmp[idx];
                    leading = 0;
                }
            }else if(tmp[idx] != ' '){
                can = 0;
            }
        }
        if(tmp[idx] == '$'){
            can = 1;
        }
        idx++;
    }
    return (int) value.size() == 0 ? "$0" : "$"+value;
}

int N;
string order;

int main(void){
    cin >> N;
    cin.ignore();
    while(N--){
        getline(cin, order);
        cout << parse(order) << endl;
    }
    return 0;
}
