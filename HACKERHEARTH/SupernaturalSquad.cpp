#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <math.h>
#include <string.h>
#include <stack>
#include <map>
#include <set>
#define ll long long
#define INF 0x3f3f3f3f
#define MAXN 207
using namespace std;

ll dp[MAXN][MAXN];//peopleLeft, last

int n,k,t;

ll f(int peopleLeft, int last){
	if(peopleLeft == 0){
		return 1;
	}else{
		ll &ans = dp[peopleLeft][last];
		if(ans == -1){
			ans = 0;
			for(int i = k; i <= peopleLeft; i++){
				if(i >= last){
					ans += f(peopleLeft-i, i);
				}
			}
		}
		return ans;
	}
}


int main(void){
	cin >> t;
	while(t--){
		cin >> n >> k;
		memset(dp,-1,sizeof(dp));
		ll ans = f(n,0);

		cout << ans << endl;
	}
	return 0;
}
