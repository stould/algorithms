#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <math.h>
#include <string.h>
#include <stack>
#include <map>
#include <set>
#define ll long long
#define INF 0x3f3f3f3f
#define MAXN 1000007
using namespace std;

int n,v,t, f[MAXN];

int main(void){
	scanf("%d", &t);
	while(t--){
		scanf("%d", &n);
		int odd = 0, even = 0;
		memset(f,0,sizeof(f));
		for(int i = 0; i < n; i++){
			scanf("%d", &v);
			if(v % 2 == 0)  even++;
			else odd++;
			f[v]++;
		}
		ll ans = (ll) odd*((ll)odd-1) / 2 + (ll)even * ((ll)even-1) / 2;
		for(int i = 0; i < MAXN; i++){
			if(f[i] > 1)
				ans -= f[i] * (f[i] -1) / 2;
		}
		printf("%lld\n", ans);
	}
	return 0;
}
