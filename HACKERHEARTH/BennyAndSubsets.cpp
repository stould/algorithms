#include <bits/stdc++.h>
#define MAX_XOR (1 << 11)
#define MAXN 1100
#define MOD 10000007ll
#define ll long long

using namespace std;

vector<int> small,big;
ll dp[MAXN][MAX_XOR];

int N, K, val;

int main(){
    cin >> N >> K;
    for(int i = 0; i < N; i++){
    	cin >> val;
    	if(val <= (1 << 10)){
    		small.push_back(val);
    	}else{
    		big.push_back(val);
    	}
    }
	dp[0][0] = 1;
	for(int i = 0; i < (int) small.size(); i++){
		for(int j = 0; j < MAX_XOR; j++){
			dp[i+1][j] = dp[i][j];
			dp[i+1][j] += dp[i][j ^ small[i]];
			dp[i+1][j] %= MOD;
		}
	}
	ll ans = 0;
	for(int mask = 0; mask < (1 << (int) big.size()); mask++){
		int sum = 0;
		for(int j = 0; j < (int) big.size(); j++){
			if((mask & (1 << j)) != 0){
				sum ^= big[j];
			}
		}
		int numIneed = sum ^ K;
		if(numIneed <= MAX_XOR){
			ans = (ans + dp[small.size()][numIneed]) % MOD;		
		}
	}
	cout << ans << endl;
    return 0;
}
