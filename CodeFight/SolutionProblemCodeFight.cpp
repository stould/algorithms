#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <math.h>
#include <string.h>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#define MAXN 10005
#define ll long long
#define INF 0x3f3f3f3f
#define PI acos(-1)

using namespace std;

int vis[30], idx, dp[30], degree[30];

std::map<int,std::string> B;
std::map<std::string,int> A;

std::vector<std::vector<int> > graph(30);
std::vector<int> sorted;

void topsort(std::vector<std::vector<int> > &g){
	std::priority_queue<int, std::vector<int>, std::greater<int> > q;
	for(int i = 0; i < idx; i++){
		if(degree[i] == 0){
			q.push(i);
		}
	}
    while(!q.empty()){
        int top = q.top(); q.pop();
         if(!vis[top] && degree[top] == 0){
             sorted.push_back(top);
             vis[top] = 1;
         }
        for(int i = 0; i < g[top].size(); i++){
            int next = g[top][i];
            degree[next]--;
            if(degree[next] == 0){
                q.push(next);
            }
        }
    }
}

void solve(std::vector<std::vector<int> > &g){
	for(int i = 0; i < sorted.size(); i++){
		for(int j = 0; j < g[sorted[i]].size(); j++){
			int next = g[sorted[i]][j];
			dp[next] |= (1 << sorted[i]) | dp[sorted[i]];
		}
	}
}

std::vector<int> minTaks(std::vector<std::string> U, std::vector<std::string> V, std::vector<std::string> Q){
	idx = 0;
	for(int i = 0; i < U.size(); i++){
		if(A.find(U[i]) == A.end()){
			B[idx] = U[i];
			A[U[i]] = idx++;
		}
		if(A.find(V[i]) == A.end()){
			B[idx] = V[i];
			A[V[i]] = idx++;

		}
		int X = A[U[i]], Y = A[V[i]];
		graph[Y].push_back(X);
		degree[X]++;
	}
	topsort(graph);
	solve(graph);
	std::vector<int> ans;
	for(int i = 0; i < Q.size(); i++){
		ans.push_back(__builtin_popcount(dp[A[Q[i]]]));
	}
	return ans;
}

int n,q;
string a,b;

int main(void){
	while(cin >> n){
		for(int i = 0; i < 30; i++){
			vis[i] = dp[i] = degree[i] = 0;
			graph[i].clear();
		}
		A.clear();
		B.clear();
		sorted.clear();

		vector<string> U,V,Q;	
		for(int i = 0; i < n; i++){
			cin >> a >> b;
			U.push_back(a);
			V.push_back(b);
		}
		cin >> q;
		for(int i = 0; i < q; i++){
			cin >> a;
			Q.push_back(a);
		}
		vector<int> ans = minTaks(U,V,Q);
		cout << "[";
		for(int i = 0; i < ans.size(); i++){
			if(i == ans.size()-1)
				cout << ans[i];
			else
				cout << ans[i] << ",";
		}
		cout << "],\n";
	}
	return 0;
}
